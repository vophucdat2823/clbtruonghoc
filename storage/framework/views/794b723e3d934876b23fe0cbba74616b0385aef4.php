<?php $__env->startSection('content'); ?>
<main>
    <div id="living">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>"><span class="color_blue">HOME > </span></a> <span class="color_gray"><?php echo e($menu['name']); ?></span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <?php if(!empty($posts)): ?>
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                <img src="<?php echo e($post->image); ?>" alt="" width="100%" height="310px" style="object-fit: cover">
                            </div>
                            <div class="txt_living">
                                <a href="<?php echo e(route('web.post.detail',[$menu['code'], str_slug($post->name)."-".$post->id])); ?>"><h3><?php echo e($post->name); ?></h3></a>
                                <hr>
                                <p><?php echo e($post->simulation); ?></p>
                            </div>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>