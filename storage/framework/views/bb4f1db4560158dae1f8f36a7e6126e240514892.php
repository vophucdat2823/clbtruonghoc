<?php $__env->startSection('content'); ?>
<main id="main_mobie">
     <div id="about_us" style="background: #fff;">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image != null): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($category->image); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>

                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
         <div class="activities_home">
            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                 <?php if($language_id == 'vi'): ?>
                    <p><?php echo e($category->name_vi); ?></p>
                <?php else: ?>
                    <p><?php echo e($category->name_en); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div id="social">
        <div class="main_soial">
            <div class="container">
                <div class="row">
                    <?php if(!empty($networks)): ?>
                        <?php $__currentLoopData = $networks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $network): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_main_h">
                            <a href="<?php echo e($network['link']); ?>" target="_blank">
                                <div class="img_content_main_h">
                                        <img src="<?php echo e(url('public/web/images/transparent/vnu_isocial_transparent.png')); ?>" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($network['image']); ?>')" alt="" class="transparent" width="100%">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="<?php echo e(url('public/upload/icon')); ?>/<?php echo e($network['icon']); ?>" alt="" width="50%">
                                        </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
    <div id="about_us" style="background: #fff;">
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<main id="main_pc">
    <div id="social">
        <div class="social_top_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        
        <div class="main_soial">
            <div class="container">
                <div class="row">
                    <?php if(!empty($networks)): ?>
                        <?php $__currentLoopData = $networks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $network): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_main_h">
                            <a href="<?php echo e($network['link']); ?>" target="_blank">
                                <div class="img_content_main_h">
                                        <img src="<?php echo e(url('public/web/images/transparent/vnu_isocial_transparent.png')); ?>" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($network['image']); ?>')" alt="" class="transparent" width="100%">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="<?php echo e(url('public/upload/icon')); ?>/<?php echo e($network['icon']); ?>" alt="" width="50%">
                                        </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>