<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                            <?php if(session('success')): ?>
                                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>
                            <?php if(session('error')): ?>
                                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-1">STT</th>
                                    <th class="col-md-2">Tên network</th>
                                    <th class="col-md-3">link</th>
                                    <th class="col-md-2">icon</th>
                                    <th class="col-md-2">Image</th>
                                    <th class="col-md-2">
                                        <a href="<?php echo e(route('admin.network.create')); ?>" type="button" class="btn btn-block btn-info">
                                            Thêm Mới
                                            <i class="fa fa-fw fa-plus-square"></i>
                                        </a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($netWorks)): ?>
                                    <?php $__currentLoopData = $netWorks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $netWork): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(1+$key++); ?></td>
                                    <td><?php echo e($netWork['name']); ?></td>
                                    <td><?php echo $netWork['link']; ?></td>
                                    <td><img src="<?php echo e($netWork['icon']); ?>" width="200px"></td>
                                    <td><img src="<?php echo e($netWork['image']); ?>" width="200px"></td>
                                    <td>
                                        <a href="<?php echo e(route('admin.network.edit', $netWork['id'])); ?>" type="button" class="btn btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                data-target="#modal-delete" data-id="<?php echo e($netWork['id']); ?>">
                                            <i class="fa fa-fw fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>