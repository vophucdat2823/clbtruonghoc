<?php $__env->startSection('content'); ?>
<main id="main_mobie">
    <div id="about_us">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image != null): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($category->image); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>

                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
        
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($getCart): ?>
                        <?php $__currentLoopData = $getCart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($language_id == 'vi'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding-top: 8.5vw;">
                                        <div class="activities_home" style="background: #E8B909;color: #102B4E">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #102B4E !important;text-transform: uppercase;" href="#<?php echo e(str_slug($gram->name_vi)); ?>">
                                                   <?php echo e($gram->name_vi); ?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="<?php echo e(str_slug($gram->name_vi)); ?>" class="collapse show" data-parent="#accordion" >
                                        <div class="card-body" style="background: #fff;padding: 3.25vw;">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="col-md-12" style="padding-left:0;padding-right: 0;">
                                                        <div class="item" style="margin-bottom: 4.25vw">
                                                            <img src="<?php echo e(url('public/web/images/transparent/transparent_program.png')); ?>" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($gram->image); ?>)" class="transparent" alt="" width="100%">
                                                            <div class="title_img_slide_home"><h6><?php echo e($gram->name_vi); ?></h6></div>
                                                        </div>
                                                        <?php 

                                                             $posts_child = DB::table('posts')
                                                                ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                                ->where('category_id', $gram->id)
                                                                ->where('language_id', $language_id)
                                                                ->where('status', 0)
                                                                ->orderBy('id','desc')
                                                                ->limit(6)
                                                                ->select('post_translations.*')
                                                                ->get();
                                                         ?>
                                                        <div class="txt_ct_cate">
                                                            
                                                            <?php if(count($posts_child)): ?>
                                                                <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="card-header" style="padding:1vw 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none;line-height: normal;">
                                                                        <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                            <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                        </a>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php elseif($language_id == 'en'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding-top: 8.5vw;">
                                        <div class="activities_home" style="background: #E8B909;color: #102B4E">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #102B4E !important;text-transform: uppercase;" href="#<?php echo e(str_slug($gram->name_en)); ?>">
                                                   <?php echo e($gram->name_en); ?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="<?php echo e(str_slug($gram->name_en)); ?>" class="collapse show" data-parent="#accordion" >
                                        <div class="card-body" style="background: #fff;padding: 3.25vw;">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="col-md-12" style="padding-left:0;padding-right: 0;">
                                                        <div class="item" style="margin-bottom: 4.25vw">
                                                            <img src="<?php echo e(url('public/web/images/transparent/transparent_program.png')); ?>" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($gram->image); ?>)" class="transparent" alt="" width="100%">
                                                            <div class="title_img_slide_home"><h6><?php echo e($gram->name_en); ?></h6></div>
                                                        </div>
                                                        <?php 

                                                             $posts_child = DB::table('posts')
                                                                ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                                ->where('category_id', $gram->id)
                                                                ->where('language_id', $language_id)
                                                                ->where('status', 0)
                                                                ->orderBy('id','desc')
                                                                ->limit(6)
                                                                ->select('post_translations.*')
                                                                ->get();
                                                         ?>
                                                        <div class="txt_ct_cate">
                                                            
                                                            <?php if(count($posts_child)): ?>
                                                                <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="card-header" style="padding:1vw 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none;line-height: normal;">
                                                                        <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                            <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                        </a>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<main id="main_pc">
        <div id="partnership">
            <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 dell-end-a">

                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">
                                    <?php if($language_id == 'en'): ?>
                                    HOME
                                    <?php elseif($language_id == 'vi'): ?>
                                    Trang chủ
                                    <?php endif; ?> > 
                                </span>
                            </a>
                             <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->last): ?>
                                    <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                                <?php else: ?>
                                    <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                                        <span class="color_blue">
                                            <?php echo e($key); ?> >
                                        </span>
                                    </a>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image): ?>
                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($category->image); ?>" alt="" width="100%">
                    <?php else: ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/alumni/banner4-02.png" alt="" width="100%">
                    <?php endif; ?>
                    <div class="text_img">
                        <p class="top_text"><?php echo e($language_id == 'en' ? $category->name_en : $category->name_vi); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="partnership">
            <div class="content_people">
                <div class="container">

                    <div class="row">
                        <style type="text/css" media="screen">
                            .alumi_st{
                                padding-left: 0;
                            }
                            .alumi_st li{
                                list-style-type: none;
                            }
                            .alumi_st li a{
                                text-decoration: none;
                                color: #000;
                                font-size: 18px
                            }
                            main #partnership .content_people .cate_pp_img .txt_cate_pp_img h5 {
                             display: inline;
                                text-transform: uppercase;
                                color: #102b4e;
                                font-size: 27px;
                                font-weight: 400;
                            }
                        </style>

                        
                        <?php if($getCart): ?>
                            <?php $__currentLoopData = $getCart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="cate_pp_img">
                                    <div class="img_cate">
                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($gC->image); ?>" alt="" width="100%" height="200px">
                                    </div>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square" style="line-height: 2rem">
                                            <img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="40%">
                                        </div>
                                        <?php if($language_id == 'vi'): ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="<?php echo e($gC->slug); ?>.html"><h5><?php echo e($gC->name_vi); ?></h5></a>
                                             <p style="padding-right: 35px;text-align: justify"><?php echo e($gC->title_vi); ?></p>
                                            <hr style="margin: 5px;">
                                        </div>
                                        <?php else: ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="<?php echo e($gC->slug); ?>.html"><h5><?php echo e($gC->name_en); ?></h5></a>
                                            <p style="padding-right: 35px;text-align: justify"><?php echo e($gC->title_en); ?></p>
                                            <hr style="margin: 5px;">
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row txt_ct_cate" style="padding-top: 0;">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul class="alumi_st">
                                                <?php 
                                                    $post_templade = DB::table('posts')
                                                        ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                        ->where('category_id', $gC->id)
                                                        ->where('language_id', $language_id)
                                                        ->where('status', 0)
                                                        ->orderBy('id','desc')
                                                        ->limit(6)
                                                        ->select('post_translations.*')
                                                        ->get();
                                                 ?>
                                                <?php $__currentLoopData = $post_templade; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gCpt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <img src="<?php echo e(url('public/web/images')); ?>/alumni/4x/Asset 13@4x.png" alt="" width="20%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="<?php echo e($gCpt->code); ?>.html">
                                                                <?php echo e(str_limit($gCpt->name,30)); ?>

                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>