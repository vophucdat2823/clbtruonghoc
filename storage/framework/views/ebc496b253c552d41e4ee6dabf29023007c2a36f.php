
<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    //dd($language);
    ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm danh mục</h5>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.program.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Tên danh mục:</label>

                                                <div class="input-group m-b"><span class="input-group-addon">vi</span> <input name="name_vi" type="text" placeholder="Tên danh mục tiếng việt" maxlength="255" value="<?php echo e(old('name_vi')); ?>" class="form-control"></div> 
                                                
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                 <div class="input-group m-b"><span class="input-group-addon">en</span> <input name="name_en" type="text" placeholder="Tên danh mục tiếng Anh" maxlength="255" id="name" value="<?php echo e(old('name_en')); ?>" class="form-control"></div>

                                                
                                                <?php if($errors->has('name_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <br><span>Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh</label> 

                                                <input type="text" id="slug" name="slug" placeholder="VD: dai-hoc-quoc-gia-ha-noi" value="<?php echo e(old('slug')); ?>" class="form-control">
                                                 <?php if($errors->has('slug')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('slug')); ?>

                                                    </span><br>
                                                <?php endif; ?>
                                                <span>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>

                                            <div class="form-group"><label>Chọn danh mục:</label>
                                                    <select name="parents_pgram" id="page_hien_thi" class="form-control">
                                                        <option value="0">-- Root -- </option>
                                                        <?php echo e(showProgram($pgramall)); ?>

                                                    </select>
                                                    <?php if($errors->has('parents_pgram')): ?>
                                                        <span class="text-center text-danger" role="alert">
                                                            <?php echo e($errors->first('parents_pgram')); ?>

                                                        </span><br>
                                                    <?php endif; ?>
                                                  
                                                   
                                                    
                                                <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn. Root là danh mục to nhất!</span>
                                            </div>
                                            
                                            
                                            <div class="form-group"><label>Mô tả thêm về danh mục của bạn:</label>
                                                <textarea type="text" class="form-control" name="title_vi"  style="margin-bottom: 5px" placeholder="Mô tả danh mục tiếng việt (không bắt buộc)" maxlength="255"><?php echo e(old('title_vi')); ?></textarea>
                                                <?php if($errors->has('title_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_vi')); ?>

                                                    </span>
                                                <?php endif; ?>

                                                <textarea type="text" class="form-control" name="title_en" placeholder="Mô tả danh mục tiếng anh (không bắt buộc)" maxlength="255"><?php echo e(old('title_en')); ?></textarea>
                                                <?php if($errors->has('title_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div id="image-diplay">
                                               <div class="form-group">
                                                <label>Ảnh danh mục (không bắt buộc):</label>

                                                <div class="input-group m-b"><span class="input-group-addon">URL</span> <input name="url_image" type="text" placeholder="Đường dẫn hình ảnh (mặc định #)" value="<?php echo e(old('name_vi')); ?>" class="form-control"></div> 

                                                   <div class="col-md-12">
                                                    <div class="row">
                                                       <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                           <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                           <img src="<?php echo e(url('public/web/images')); ?>/logo.png" alt=""> 
                                                           </div>
                                                           <div style="text-align: center">
                                                                <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                    <span class="fileinput-new" style="color: #fff"> Chọn ảnh </span>
                                                                    <span class="fileinput-exists" style="color: #fff"> Đổi ảnh </span>
                                                                    <input type="file" name="image" value="<?php echo e(old('image')); ?>">
                                                                </span>
                                                               <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                  data-dismiss="fileinput"> Xóa ảnh </a>
                                                               
                                                           </div>
                                                           <span class="text-center text-danger" role="alert">
                                                                <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                            </span>
                                                       </div>
                                                    </div>
                                                   </div>

                                               </div>
                                           </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Tất cả danh mục</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-4">Tên danh mục</th>
                                                        <th class="col-md-5">Mô tả</th>
                                                        
                                                        <th class="col-md-2">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($programs)): ?>
                                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <tr>
                                                        
                                                        <td><?php echo e($key+1); ?></td>
                                                        <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>.html" target="_blank"> <span class="tag-post"><?php echo e($cate->name_vi); ?></span></a></td>
                                                        <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>.html" target="_blank"> <span class="tag-post-tacgia"><?php echo $cate->title_vi != null ? str_limit($cate->title_vi,50) : '<i>-- Rỗng --</i>'; ?></span></a></td>
                                                        
                                                        

                                                        <td>
                                                            <a href="<?php echo e(route('admin.program.edit', $cate->id)); ?>" type="button" class="btn btn-info">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                    data-target="#modal-delete-<?php echo e($cate->id); ?>">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                            <div class="modal modal-danger fade" id="modal-delete-<?php echo e($cate->id); ?>">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span></button>
                                                                                <?php 
                                                                                // dd(count($cate->getProgram));
                                                                                    $delete = \App\Models\Admin\ProgramTranslation::where('program_id',$cate->id)->get();
                                                                                 ?>
                                                                                <?php if($delete->count() == 0 && count($cate->getProgram) == 0): ?>
                                                                                    <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                <?php elseif(count($cate->getProgram) >= 0): ?>
                                                                                    <h4 class="modal-title">
                                                                                        Danh mục hiện tại đang có "<?php echo e(count($cate->getProgram)); ?>" danh mục con!
                                                                                        (không thể xóa)
                                                                                    </h4>
                                                                                <?php elseif($delete->count() > 0): ?>
                                                                                    <h4 class="modal-title">
                                                                                   Danh mục hiện tại đang có "<?php echo e($delete->count()); ?>" bài viết!
                                                                                   (không thể xóa)
                                                                                    </h4>
                                                                                <?php endif; ?>
                                                                            
                                                                        </div>
                                                                        <div class="modal-body" style="background: white!important; text-align: center" ">
                                                                            <form method="get" action="<?php echo e(route('admin.program.destroy', $cate->id)); ?>">
                                                                                <?php if($delete->count() == 0 && count($cate->getProgram) == 0): ?>
                                                                                    <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                    <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                        <i class="fa fa-save"></i>
                                                                                    </button>
                                                                                <?php elseif(count($cate->getProgram) >= 0): ?>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                <?php elseif($delete->count() > 0): ?>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                <?php endif; ?>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                            <?php echo e($programs->links()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php 

      function showProgram($pgramall, $parent = 0, $char ='')
      {
        $cate_child = array();
        foreach ($pgramall as $key => $item) {
          if ($item->parents == $parent) 
          {
            $cate_child[] = $item;
            unset($pgramall[$key]);
          }
        }
        if ($cate_child) {
          foreach ($cate_child as $key => $item) {
            echo '<option value="'.$item->id.'"';
            if($parent==0){
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name_vi;
            echo '</option>';
            showProgram($pgramall ,$item->id , $char.'---| ');
          }
        }
      }



    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<script>

  $("#page_hien_thi").change(function() {
      
      var gia_tri = $(this).val();
      if(gia_tri == ''){
        alert('Vui lòng chọn trang hiển thị');
      }else {
          $.ajax({
           method:'get',
           url:"<?php echo e(url('')); ?>/admin/program/chon-danh-muc/"+gia_tri,
           dataType: 'json',
          }).done(function(data){
              console.log(data);
              $('#load_cate').load(location.href + " #load_cate>*");
          }).fail(function(error){
            console.log(error.responseText);
             $('#load_cate').html(error.responseText);
            // $('#load_cate').load(location.href + " #load_cate>*");
          });
          }
    
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>