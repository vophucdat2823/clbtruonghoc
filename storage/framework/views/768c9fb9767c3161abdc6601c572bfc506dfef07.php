  <!-- header -->
<style type="text/css">

	.student_submit{
	  width: 100% !important;
	  border-radius: 0 !important;
	  margin-bottom: 5px !important;
	  background-color: #E8B909 !important;
	  border:0 !important;
	  color: #102B4E !important;
	  font-weight: bold !important
	}

	.student_create_acc{
	  width: 40% !important;
	  border-radius: 0 !important;
	  background-color: #939598 !important;
	  border:0 !important;
	  color: #fff !important;
	  font-size: 14px !important
	}
	#resert_password{
	  color: #fff;
	  text-transform: uppercase!important;
	  padding: 3px 25px !important; 
	  border-radius: 0 !important
	}
	.box {
	  width: 200px;
	  height: 200px;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  overflow: hidden;
	}

	.box .b {
	  border-radius: 50%;
	  border-left: 4px solid;
	  border-right: 4px solid;
	  border-top: 4px solid transparent !important;
	  border-bottom: 4px solid transparent !important;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  animation: ro 2s infinite;
	}

	.box .b1 {
	  border-color: #4A69BD;
	  width: 120px;
	  height: 120px;
	}

	.box .b2 {
	  border-color: #F6B93B;
	  width: 100px;
	  height: 100px;
	  animation-delay: 0.2s;
	}

	.box .b3 {
	  border-color: #2ECC71;
	  width: 80px;
	  height: 80px;
	  animation-delay: 0.4s;
	}

	.box .b4 {
	  border-color: #34495E;
	  width: 60px;
	  height: 60px;
	  animation-delay: 0.6s;
	}

	@keyframes  ro {
	  0% {
	    transform: translate(-50%, -50%) rotate(0deg);
	  }
	  
	  50% {
	    transform: translate(-50%, -50%) rotate(-180deg);
	  }
	  
	  100% {
	    transform: translate(-50%, -50%) rotate(0deg);
	  }
	}
	.opacity_reset{
	  opacity: 0.6;
	}
	.active_menu{
	    color: #e8b909 !important
	  }
</style>
<?php 
  $questions = $language_id == 'en' ? 'Have any questions?' : 'Có câu hỏi nào không?';
  $register = $language_id == 'en' ? 'Register' : 'Đăng ký';
  $or = $language_id == 'en' ? 'or' : 'hoặc';
  $login = $language_id == 'en' ? 'Login' : 'Đăng nhập';
  $using = $language_id == 'en' ? ' Student ID' : 'ID Sinh Viên';
  $enter_value = $language_id == 'en' ? 'Enter your email or phone or student id' : 'Nhập email hoặc số điện thoại hoặc id sinh viên của bạn';
  $enter_password = $language_id =='en' ? 'Enter your password' : 'Nhập mật khẩu của bạn';

  $search = $language_id == 'en' ? 'Keyword search' : 'Tìm kiếm từ khóa';
  $search_kick = $language_id == 'en' ? 'Search' : 'Tìm kiếm';

  $questions = $language_id == 'en' ? 'Have any questions?' : 'Có câu hỏi nào không?';
  $register = $language_id == 'en' ? 'Register' : 'Đăng ký';
  $or = $language_id == 'en' ? 'or' : 'hoặc';
  $login = $language_id == 'en' ? 'Login' : 'Đăng nhập';
  $using = $language_id == 'en' ? ' Student ID' : 'ID Sinh Viên';
  $enter_value = $language_id == 'en' ? 'Enter your email or phone or student id' : 'Nhập email hoặc số điện thoại hoặc id sinh viên của bạn';
  $enter_password = $language_id =='en' ? 'Enter your password' : 'Nhập mật khẩu của bạn'
 ?>

<header class="header_mobie">
	<nav class="navbar navbar-toggleable-sm navbar-inverse fixed-top">
		<div class="container" style="">
			<div class="d-flex w-100 justify-content-between flex-last nav_mobie_button" style="">

				<div class="d-flex w-100 flex-first justify-content-between nav_mobie_button_0">

					<span style="border-right: 2px solid #BABCBE;">
						<button class="navbar-toggler align-self-end" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" style="padding: 0 !important;line-height: normal !important;width: 100% !important;height: 100%;">
							<img src="<?php echo e(url('public/web/images/icon_menu_mobie.png')); ?>" style="width: 80%" alt="">
						</button>
					</span>

					<a class="navbar-brand" href="<?php echo e(route('web.home')); ?>" style="width: 100%;padding: 1% 0;margin-right: 0 !important;text-align: center;line-height: normal">
						<img src="<?php echo e(url('public/web/images/logo_vang.png')); ?>" alt="" width="80%">
					</a>

					<span style="">
						<button class="navbar-brand navbar-toggler align-self-end" data-target="#collapsenav" data-toggle="collapse" type="button" style="padding: 0 !important;line-height: normal !important;width: 100% !important;height: 100%;display: block">
							<?php if(Auth::guard('student')->check()): ?>
							<img src="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg" class="img-circle" width="80%" alt="">
							<?php else: ?>
							<img src="<?php echo e(url('public/web/images/user_mobie_nav.png')); ?>" style="width: 80%" alt="">
							<?php endif; ?>
						</button>
					</span>

					<span style="">
						<?php if($language_id == 'vi'): ?>
						<form action="<?php echo e(route('web.language', 'english')); ?>" style="height: 100%">
							<button class="navbar-toggler align-self-end" style="padding: 0 !important;line-height: normal !important;width: 100% !important;height: 100%;">
								<img src="<?php echo e(url('public/web/images/english_icon.png')); ?>" style="width: 80%" alt="">
							</button>
						</form>
						<?php elseif($language_id == 'en'): ?>
						<form action="<?php echo e(route('web.language', 'vietnam')); ?>" style="height: 100%">
							<button class="navbar-toggler align-self-end" style="padding: 0 !important;line-height: normal !important;width: 100% !important;height: 100%;">
								<img src="<?php echo e(url('public/web/images/vietnam-icon.png')); ?>" style="width: 80%" alt="">
							</button>
						</form>
						<?php endif; ?>
					</span>
					<span style="border-left: 2px solid #BABCBE;">
						<button class="navbar-toggler align-self-end" data-target="#collapsenav_search" data-toggle="collapse" type="button" style="padding: 0 !important;line-height: normal !important;width: 100% !important;height: 100%;">
							<img src="<?php echo e(url('public/web/images/search_mobie_nav.png')); ?>" style="width: 70%" alt="">
						</button>
					</span>
				</div>
			</div>

			<div class="collapse navbar-collapse navbar-collapse-style" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">

					<?php if($language_id == 'vi'): ?>
					<?php $__currentLoopData = $menu_vi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php 
					$active_menu = $menu['link'] == url(''.$code.'.html') || $menu['link'] == url('home/'.$code.'.html') || $menu['link'] == url('admission/'.$code.'.html') ? 'active_menu' : '';
					 ?>
					<li class="nav-item nav-li-item active" style="border-bottom: 1.5px solid #ccc">
						<a class="nav-link <?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a>
					</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php elseif($language_id == 'en'): ?>
					<?php $__currentLoopData = $menu_en; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php 
					$active_menu = $menu['link'] == url(''.$code.'.html') || $menu['link'] == url('home/'.$code.'.html') || $menu['link'] == url('admission/'.$code.'.html') ? 'active_menu' : '';
					 ?>
					<li class="nav-item nav-li-item active" style="border-bottom: 1.5px solid #ccc">
						<a class="nav-link <?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a>
					</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</ul>
				
			</div>
			<div class="navbar-collapse collapse w-100" id="collapsenav" style="background: #102B4E">
				<div class="login_header w-100" style="background: #102B4E">

					<?php if(Auth::guard('student')->check()): ?>
					<div class="xtlab-ctmenu-item" style="width: 100%;color: #fff">Username: 
						<span style="color:yellow "><?php echo e(Auth::guard('student')->user()->name); ?></span>
					</div>
					<div class="modal-dialog speech-bubble" role="document" style="margin-bottom: 4%">
						<div class="modal-content">
							<div class="modal-body" style="padding:15px 0 0 0">
								<a type="#" style="width: 40%;border-radius: 0;background-color: #8CB6E5;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Thông tin</a>
								<a href="<?php echo e(route('web.logout.student')); ?>" style="width: 40%; float:right;border-radius: 0;background-color: red;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Đăng xuất?</a>
							</div>
						</div><!-- /.modal-content -->
					</div>
					<?php else: ?>
					<form action="#" method="post" role="form">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<div class="modal-body" style="padding:15px 0 3% 0">

							<div class="alert alert-danger error errorLogin" style="display: none;">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<p style="color:red; display:none;" class="error errorLogin"></p>
							</div>

							<div style="padding: 0 0 15px 0;padding-top: 0">
								<fieldset class="form-group">
									<label for="formGroupExampleInput" style="font-weight: bold;color: #fff">
										Username
									</label>

									<input type="text" class="form-control" name="value" id="formGroupExampleInput"  style="padding: 0;border: 0;border-bottom: 1px solid yellow;border-radius: 0;background: #102b4e !important;color: yellow" value="<?php echo e(old('value')); ?>" placeholder="<?php echo e($enter_value); ?>">

									<p style="color:red; display: none" class="error errorValue"></p>
								</fieldset>


								<fieldset class="form-group">
									<label for="formGroupExampleInput2" style="font-weight: bold;color: #fff">
										Password
									</label>

									<input type="password" name="password" class="form-control" id="formGroupExampleInput2" style="padding: 0;border: 0;border-bottom: 1px solid yellow;border-radius: 0;background: #102b4e !important;color: yellow" placeholder="<?php echo e($enter_password); ?>">

									<p style="color:red; display: none" class="error errorPassword"></p>
								</fieldset>
							</div>

							<button type="button" class="btn btn-primary student_submit dang-nhap">
								<?php echo e($language_id == 'en' ? 'Sing in' : 'Đăng nhập'); ?>

							</button>

							<button type="button"class="btn btn-primary student_create_acc">
								<?php echo e($language_id =='en' ? 'Create a new account' : 'Tạo tài khoản mới'); ?>

							</button>

							<a data-toggle="modal" href='#resert_password_modal' style="float:right;padding: .375rem .75rem" class="btn btn-primary student_create_acc" >
								<?php echo e($language_id == 'en' ? 'Forgot your password?' : 'Quên mật khẩu?'); ?>

							</a>
						</div>
					</form>

					<?php endif; ?>
				</div>
			</div>
			<div class="navbar-collapse collapse w-100" id="collapsenav_search">
				<form action="" class="form_search" style="margin-top: 3%;margin-left: 3%;margin-right: 3%;">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="<?php echo e($search); ?>">
						<div class="input-group-append">
							<button class="btn btn-warning" type="submit">
								<?php echo e($search_kick); ?>

							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</nav>
	<nav class="navbar navbar-toggleable-sm navbar-inverse fixed-bottom" style=" background: red;">
		<div class="container" style="">

			<div style="margin:0 auto;width: 100%;">
				<button class="navbar-brand navbar-toggler align-self-end" data-target="#register_mobie_nav" data-toggle="collapse" type="button" style="color: #fff;margin-right: 0;padding: 1.5vw;width: 100%;font-size: 6vw;text-transform: uppercase;font-weight: bold;">
					<?php echo e($register); ?>

				</button>
			</div>
			<div class="navbar-collapse collapse w-100" id="register_mobie_nav" style="background: #102B4E">
				<div class="login_header w-100" style="background: #102B4E">

					<?php if(Auth::guard('student')->check()): ?>
					<div class="xtlab-ctmenu-item" style="width: 100%;color: #fff">Username: 
						<span style="color:yellow "><?php echo e(Auth::guard('student')->user()->name); ?></span>
					</div>
					<div class="modal-dialog speech-bubble" role="document" style="margin-bottom: 4%">
						<div class="modal-content">
							<div class="modal-body" style="padding:15px 0 0 0">
								<a type="#" style="width: 40%;border-radius: 0;background-color: #8CB6E5;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Thông tin</a>
								<a href="<?php echo e(route('web.logout.student')); ?>" style="width: 40%; float:right;border-radius: 0;background-color: red;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Đăng xuất?</a>
							</div>
						</div><!-- /.modal-content -->
					</div>
					<?php else: ?>
					
					<form action="" method="get" role="form">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<div class="modal-body" style="padding:15px 0 3% 0">

							<div class="alert alert-danger error errorLogin" style="display: none;">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<p style="color:red; display:none;" class="error errorLogin"></p>
							</div>

							<div class="col-12 placeholder_mobie_input">
                                <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909" >Tên của bạn <span style="color: red">*</span></label>
                                    <input type="text" name="name" class="form-control" placeholder="Tên của bạn...">
                                  </div>
                                  <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909" >Điện thoại <span style="color: red">*</span></label>
                                    <input type="tel" name="phone" class="form-control" placeholder="Điện thoại...">
                                  </div>
                                  <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909">Email <span style="color: red">*</span></label>
                                    <input type="email" name="email" class="form-control" placeholder="Email...">
                                  </div>
                                  <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909">Trường THPT <span style="color: red">*</span></label>
                                    <input type="text" name="truong" class="form-control" placeholder="Trường THPT...">
                                  </div>
                                  <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909">Năm tốt nghiệp THPT<span style="color: red">*</span></label>
                                    <input type="text" name="nam_tn" class="form-control" placeholder="Năm tốt nghiệp THPT...">
                                  </div>
                                  <div class="form-group">
                                    <label for="" class="txt_mark_banner" style="color: #E8B909">Nội dung tư vấn<span style="color: red">*</span></label>
                                    <input type="text" name="title" class="form-control" placeholder="Nội dung tư vấn...">
                                  </div>
                            </div>

                            <button type="button" class="btn btn-primary student_submit">
                                <?php echo e($language_id == 'en' ? 'Register' : 'Đăng ký'); ?>

                            </button>
						</div>
					</form>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</nav>
</header>

  <header class="header_pc">
    <div class="top_header">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-12">
              <div class="container">
                  <div class="main_header_content">
                    <div class="left_content_header">
                      <p class="color_yellow"><?php echo e($questions); ?></p>
                      <p><i class="fas fa-phone-volume color_yellow"></i>
                        <a href="" class="color_white">
                          <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($cd->key_option == 'phone1'): ?>
                              <?php echo e($cd->value); ?>

                            <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </a>
                        <span class="color_yellow"> | </span>
                        <a href="" class="color_white">
                          <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($cd->key_option == 'phone2'): ?>
                              <?php echo e($cd->value); ?>

                            <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </a>
                      </p>
                      <p class="color_white"><i class="far fa-envelope color_yellow"></i>
                        <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($cd->key_option == 'email1'): ?>
                          <?php echo e($cd->value); ?>

                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </p>
                    </div>
                    <div class="login_header">
                        <?php if(Auth::guard('student')->check()): ?>
	                        <p class="color_yellow"><a href="" class="color_yellow">
	                          <img src="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg" width="34px" style="border-radius: 17px" alt=""></a>
	                        </p>
	                        <div class="menu1 xt-ct-menu">
	                            <div class="xtlab-ctmenu-item">Username: <span style="color:#102B4E "><?php echo e(Auth::guard('student')->user()->name); ?></span></div>
	                            <div class="xtlab-ctmenu-sub" style="display: none">
	                              <div class="modal-dialog speech-bubble" role="document">
	                                <div class="modal-content">
	                                  <div class="modal-body" style="padding:15px 0 0 0">
	                                    <a type="#" style="width: 40%;border-radius: 0;background-color: #8CB6E5;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Thông tin tài khoản</a>
	                                    <a href="<?php echo e(route('web.logout.student')); ?>" style="width: 40%; float:right;border-radius: 0;background-color: red;border:0;color: #fff;font-size: 14px;display: inline-block " class="btn btn-primary">Đăng xuất?</a>
	                                  </div>
	                                </div>
	                              </div>
	                            </div>
	                        </div>
                        <?php else: ?>
                          <div class="menu1 xt-ct-menu">
                              <div class="xtlab-ctmenu-item"><?php echo e($login); ?></div>
                              <div class="xtlab-ctmenu-sub" style="display: none">
                                <div class="modal-dialog speech-bubble" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <p class="modal-title" style=""><?php echo e($language_id == 'en' ?'For VNUIS student only' : 'Chỉ dành cho sinh viên VNUIS'); ?></p>

                                    </div>
                                    <form action="#" method="post" role="form">>
                                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                      <div class="modal-body" style="padding:15px 0 0 0">

                                        <div class="alert alert-danger error errorLogin" style="display: none;">
                                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                          <p style="color:red; display:none;" class="error errorLogin"></p>
                                        </div>

                                        <div style="padding: 15px;padding-top: 0">
                                          <fieldset class="form-group">
                                            <label for="formGroupExampleInput" style="font-weight: bold;color: #E8B909">
                                              Username
                                            </label>

                                            <input type="text" class="form-control" name="value" id="formGroupExampleInput3"  style="padding: 0;border: 0;border-bottom: 1px solid #ccc;border-radius: 0;" value="<?php echo e(old('value')); ?>" placeholder="<?php echo e($enter_value); ?>">

                                            <p style="color:red; display: none" class="error errorValue"></p>
                                          </fieldset>


                                          <fieldset class="form-group">
                                            <label for="formGroupExampleInput2" style="font-weight: bold;color: #E8B909">
                                              Password
                                            </label>

                                            <input type="password" name="password" class="form-control" id="formGroupExampleInput4" style="padding: 0;border: 0;border-bottom: 1px solid #ccc;border-radius: 0;" placeholder="<?php echo e($enter_password); ?>">

                                            <p style="color:red; display: none" class="error errorPassword"></p>
                                          </fieldset>
                                        </div>

                                        <button type="button" class="btn btn-primary student_submit dang-nhap">
                                          <?php echo e($language_id == 'en' ? 'Sing in' : 'Đăng nhập'); ?>

                                        </button>

                                        <button type="button"class="btn btn-primary student_create_acc">
                                          <?php echo e($language_id =='en' ? 'Create a new account' : 'Tạo tài khoản mới'); ?>

                                        </button>

                                        <a data-toggle="modal" href='#resert_password_modal' style="float:right;padding: .375rem .75rem" class="btn btn-primary student_create_acc" >
                                          <?php echo e($language_id == 'en' ? 'Forgot your password?' : 'Quên mật khẩu?'); ?>

                                        </a>
                                      </div>
                                    </form>
                                  </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                              </div>
                          </div>

                        <?php endif; ?>
                      	<p class="color_yellow"><a href="" class="color_yellow"><?php echo e($using); ?></a></p>
                    </div>
                  </div>
              </div>
              <div class="language_header">
                <?php if($language_id == 'vi'): ?>
                <a href="<?php echo e(route('web.language', 'vietnam')); ?>" class="color_yellow">Tiếng Việt</a>
                <a href="<?php echo e(route('web.language', 'english')); ?>" class="color_white">English</a>
                <?php elseif($language_id == 'en'): ?>
                <a href="<?php echo e(route('web.language', 'vietnam')); ?>" class="color_white">Tiếng Việt</a>
                <a href="<?php echo e(route('web.language', 'english')); ?>" class="color_yellow">English</a>
                <?php endif; ?>
              </div>
            </div>
          </div>
      </div>
    </div>


  <!-- menu chính -->
    <div class="menu_h">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-12 col-12" style="position: relative">
              <div class="logo_h" >
                <a href="<?php echo e(route('web.home')); ?>">
                  <img src="<?php echo e(url('public/upload/images/')); ?>/<?php echo e($logo->image); ?>" alt="" width="100%" style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);padding-top: 0 !important;width: 100%;height: 100%; object-fit: contain">
                </a>
              </div>
            </div>
            <div class="col-md-8 col-sm-12 col-12">
              <div class="main_menu">
                <style type="text/css">
                  
                </style>
                <ul id="menu">
                  <?php if($language_id == 'vi'): ?>
                    <?php $__currentLoopData = $menu_vi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php 
                        $active_menu = $menu['link'] == url(''.$code.'.html') || $menu['link'] == url('home/'.$code.'.html') || $menu['link'] == url('admission/'.$code.'.html') ? 'active_menu' : '';
                       ?>
                      <li><a class="<?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a><span class="color_yellow"> | </span></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php elseif($language_id == 'en'): ?>
                    <?php $__currentLoopData = $menu_en; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php 
                        $active_menu = $menu['link'] == url(''.$code.'.html') || $menu['link'] == url('home/'.$code.'.html') || $menu['link'] == url('admission/'.$code.'.html') ? 'active_menu' : '';
                       ?>
                      <li><a class="<?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a><span class="color_yellow"> | </span></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                </ul>
              </div>
              <form action="" class="form_search">
                  <div class="input-group mb-3">
                    <?php 
                      $search = $language_id == 'en' ? 'Keyword search' : 'Tìm kiếm từ khóa';
                      $search_kick = $language_id == 'en' ? 'Search' : 'Tìm kiếm';
                     ?>

                      <input type="text"  id="tags" class="form-control" placeholder="<?php echo e($search); ?>">
                      <div class="input-group-append">
                      <button class="btn btn-success" type="submit">
                        <?php echo e($search_kick); ?>

                      </button>
                      </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
    </div>
  <!-- end menu chính -->



  <!-- menu nhỏ bên phải -->
    <div class="menu_right">
      <div class="tool_icon_right tool_txt" style="z-index: 3;">
        <span class="trigger"></span>
        <div class="inner rotate">
          <p class="item item_border item_a " style="width: 110px;background-color: #C4161C">
            <a class="toolAction " data-toggle="modal" href='#register' style="color: #fff;text-transform: uppercase;text-decoration: none;padding: 3px 0px !important">
            <?php echo e($language_id == 'en' ?'Register' : 'Đăng kí'); ?>


            </a>
          </p>
        </div>
      </div>
    </div>
  <!-- end menu nhỏ bên phải -->
  <?php if($language_id == 'vi'): ?>
    <div class="modal fade"  id="register">
      <div class="modal-dialog" style="border: 20px solid red; width: 35% !important; max-width: 40%;">
        <div class="modal-content" style="width: 100% !important;border-radius: 0;border: 0">
          <div class="modal-body" style="background-color: red;border-radius: 0;padding: 0">
            <div class="col-md-12">
              <div class="row">
                <h5 class="modal-title" style="color: #e8b909;width: 100%">HÃY BẮT ĐẦU HÀNH TRÌNH CỦA BẠN NGAY TẠI ĐÂY!</h5>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <p style="color: #fff">Trở thành sinh viên tiếp theo của chương trình Cử nhân ... </p>
              </div>
            </div>
          </div>
          <form action="" method="POST" role="form">
          <div class="modal-body" style="padding:30px;padding-bottom: 20px">
              <div class="form-group">
                <label for="">Tên của bạn <span style="color: red">*</span></label>
                <input type="text" name="name" class="form-control" placeholder="Tên của bạn...">
              </div>
              <div class="form-group">
                <label for="">Điện thoại <span style="color: red">*</span></label>
                <input type="tel" name="phone" class="form-control" placeholder="Điện thoại...">
              </div>
              <div class="form-group">
                <label for="">Email <span style="color: red">*</span></label>
                <input type="email" name="email" class="form-control" placeholder="Email...">
              </div>
              <div class="form-group">
                <label for="">Trường THPT <span style="color: red">*</span></label>
                <input type="text" name="truong" class="form-control" placeholder="Trường THPT...">
              </div>
              <div class="form-group">
                <label for="">Năm tốt nghiệp THPT<span style="color: red">*</span></label>
                <input type="text" name="nam_tn" class="form-control" placeholder="Năm tốt nghiệp THPT...">
              </div>
              <div class="form-group">
                <label for="">Nội dung tư vấn<span style="color: red">*</span></label>
                <input type="text" name="title" class="form-control" placeholder="Nội dung tư vấn...">
              </div>

          </div>
          <div class="modal-footer" style="border:0;padding:0;padding:0 30px 25px 25px">
            <button type="button" class="btn btn-danger" style="color: #fff;text-transform: uppercase!important;padding: 3px 25px !important; border-radius: 0 !important">Đăng ký</button>
          </div>
          </form>
        </div>
      </div>
    </div>
  <?php else: ?>
    <div class="modal fade"  id="register">
      <div class="modal-dialog" style="border: 20px solid red; width: 35% !important; max-width: 40%;">
        <div class="modal-content" style="width: 100% !important;border-radius: 0;border: 0">
          <div class="modal-body" style="background-color: red;border-radius: 0;padding: 0">
            <div class="col-md-12">
              <div class="row">
                <h5 class="modal-title" style="color: #e8b909;width: 100%">START YOUR JOURNEY NOW HERE!</h5>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <p style="color: #fff">Become the next student of the Bachelor ... </p>
              </div>
            </div>
          </div>
          <form action="" method="POST" role="form">
          <div class="modal-body" style="padding:30px;padding-bottom: 20px">
              <div class="form-group">
                <label for="">Your name <span style="color: red">*</span></label>
                <input type="text" name="name" class="form-control" placeholder="Your name...">
              </div>
              <div class="form-group">
                <label for="">Telephone <span style="color: red">*</span></label>
                <input type="tel" name="phone" class="form-control" placeholder="Telephone...">
              </div>
              <div class="form-group">
                <label for="">Email <span style="color: red">*</span></label>
                <input type="email" name="email" class="form-control" placeholder="Email...">
              </div>
              <div class="form-group">
                <label for="">High school <span style="color: red">*</span></label>
                <input type="text" name="truong" class="form-control" placeholder="High school...">
              </div>
              <div class="form-group">
                <label for="">High school graduation year<span style="color: red">*</span></label>
                <input type="text" name="nam_tn" class="form-control" placeholder="High school graduation year...">
              </div>
              <div class="form-group">
                <label for="">Consulting content<span style="color: red">*</span></label>
                <input type="text" name="title" class="form-control" placeholder="Consulting content...">
              </div>

          </div>
          <div class="modal-footer" style="border:0;padding:0;padding:0 30px 25px 25px">
            <button type="button" class="btn btn-danger" style="color: #fff;text-transform: uppercase!important;padding: 3px 25px !important; border-radius: 0 !important"><?php echo e($language_id == 'en' ?'Register' : 'Đăng kí'); ?></button>
          </div>
          </form>
        </div>
      </div>
    </div>

  <?php endif; ?>

<?php if($language_id == 'vi'): ?>
    <div class="modal fade"  id="resert_password_modal" >
      <div class="modal-dialog resert_password_opacity " style="border: 20px solid #102B4E; width: 40% !important; max-width: 40%;">
        <div class="modal-content" style="width: 100% !important;border-radius: 0;border: 0">
          <div class="modal-body" style="background-color: #102B4E;border-radius: 0;padding: 0">
            <div class="col-md-12">
              <div class="row">
                <h5 class="modal-title" style="margin-bottom: 15px;color: #e8b909;width: 100%">Nhập email tài khoản của bạn!</h5>
              </div>
            </div>
          </div>
          <form action="#" method="POST" role="form">
          <div class="modal-body" style="padding:30px;padding-bottom: 20px">

              

              <div class="alert alert-danger error errorResert" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="color:red; display:none;" class="error error_resert"></p>
              </div>


              <div class="form-group">
                <label for="">Email tài khoản <span style="color: red">*</span></label>
                <input type="email" name="resert_password" class="form-control" placeholder="Nhập email tài khoản của bạn!">
                <p style="color:red; display: none" class="error errorResertPassword"></p>
              </div>
          </div>
          <div class="modal-footer" style="border:0;padding:0;padding:0 30px 25px 25px">
            <button type="button" class="btn btn-danger" id="resert_password">Gửi link phục hồi mật khẩu</button>
          </div>
          </form>
        </div>
      </div>
      <div class="box" style="display: none">
        <div class="b b1"></div>
        <div class="b b2"></div>
        <div class="b b3"></div>
        <div class="b b4"></div>
      </div>
    </div>
  <?php else: ?>
    <div class="modal fade"  id="resert_password_modal">
      <div class="modal-dialog resert_password_opacity" style="border: 20px solid #102B4E; width: 40% !important; max-width: 40%;">
        <div class="modal-content" style="width: 100% !important;border-radius: 0;border: 0">
          <div class="modal-body" style="background-color: #102B4E;border-radius: 0;padding: 0">
            <div class="col-md-12">
              <div class="row">
                <h5 class="modal-title" style="margin-bottom: 15px;color: #e8b909;width: 100%">Enter your account email!</h5>
              </div>

            </div>
            <div class="col-md-12">
              <div class="row">
                <p style="color: #fff">Become the next student of the Bachelor ... </p>
              </div>
            </div>
          </div>
          <form action="" method="POST" role="form">
          <div class="modal-body" style="padding:30px;padding-bottom: 20px">


              <div class="alert alert-danger error errorResert" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="color:red; display:none;" class="error error_resert"></p>
              </div>


              <div class="form-group">
                <label for="">Account email <span style="color: red">*</span></label>
                <input type="email" name="resert_password" class="form-control" placeholder="Enter your account email!">
                <p style="color:red; display: none" class="error errorResertPassword"></p>
              </div>
          </div>
          <div class="modal-footer" style="border:0;padding:0;padding:0 30px 25px 25px">
            <button type="button" class="btn btn-danger" id="resert_password" >Send password recovery link</button>
          </div>
          </form>
        </div>
      </div>
      <div class="box" style="display: none">
        <div class="b b1"></div>
        <div class="b b2"></div>
        <div class="b b3"></div>
        <div class="b b4"></div>
      </div>
    </div>

  <?php endif; ?>

  </header>
  <!-- end header -->
<?php $__env->startSection('javascript'); ?>


<?php $__env->stopSection(); ?>
