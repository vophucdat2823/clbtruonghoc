<?php $__env->startSection('content'); ?>
<main>
    <div id="departments">
        <div class="people_top_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <?php if(!empty($posts)): ?>
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($post->image); ?>" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="60%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                       
                                    <a href="<?php echo e(route('web.menu',['param'=>$post->code])); ?>"><h5><?php echo e($post->name); ?></h5></a>
                                        <hr>
                                        <p><?php echo e(str_limit($post->simulation, 110)); ?></p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>