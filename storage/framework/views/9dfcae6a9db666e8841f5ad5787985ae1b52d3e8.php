<?php $__env->startSection('content'); ?>

    <main>
        <div id="about_us">
            <div class="about_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray"><?php echo e($menu['name']); ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content_about_us">
                <div class="container mt-3">
                    <h2>Welcome to International School - VNU</h2>
                    <hr>
                    <div id="accordion">
                        <div class="card_bottom">
                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="card">
                                <div class="card-header">
                                    <a class="card-link" data-toggle="collapse" href="#collapseOne-<?php echo e($key); ?>">
                                        <img src="/public/web/images/squares.svg" alt="" width="1.2%"> <?php echo e($post->name); ?>

                                    </a>
                                </div>
                                <div id="collapseOne-<?php echo e($key); ?>" class="<?php echo e($key == 0 ? 'collapse show': 'collapse'); ?>" data-parent="#accordion">
                                    <div class="card-body content_cosllap_one">
                                        <?php echo $post->content; ?>

                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>