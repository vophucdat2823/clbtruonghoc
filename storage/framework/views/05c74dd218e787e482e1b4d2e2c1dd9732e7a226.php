<?php $__env->startSection('title','Clb Trường Học'); ?>
<?php $__env->startSection('content'); ?>
<main>
    <div class="">
        <div class="w-100 background_div">    
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                <div class="col-12 my-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-3" >
                                    <?php echo $__env->make('layouts.left_profile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <style>
                                    .style_profile a.active{
                                        color: #1B74BB !important;
                                    }
                                </style>
                                <div class="col-sm-9 bg-fff" style="background: #fff; border-radius: 5px">
                                    <div class="mb-1 style_profile">
                                        <ul class="nav nav-tabs" role="tablist ">
                                            <li class="nav-item">
                                              <a class="active" data-toggle="tab" href="#menu2" style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">Thông tin tài khoản</a>
                                            </li>
                                            <li class="nav-item">
                                              <a style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">|</a>
                                            </li>
                                            <li class="nav-item">
                                              <a data-toggle="tab" href="#menu3" style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">Đổi mật khẩu </a>
                                            </li>
                                        </ul>
                                    </div>                           
                                    <div class="tab-content  p-md-3">
                                        <div id="menu2" class="container tab-pane active row"><br>
                                            <form action="<?php echo e(route('changeAccound')); ?>" method="post" enctype="multipart/form-data">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="col-12 style_padding_box ">
                                                    <div class="col-sm-6 col-md-4 mb-4 " style="margin: 0 auto;">
                                                        <article class="boxfilter boxfilter--1 " style="border-radius: unset;">
                                                            <div class="boxfilter__info-hover" style="top: 32%;padding: 20px;text-align: center;">
                                                                <div style="height: 100%">
                                                                    <div class="Boxinfo ">
                                                                        <div>
                                                                            <div class="Boxinfo-action" style="z-index: 100">
                                                                                <label type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;padding: 10px; font-size: 17px" for="avatar_file">Thay đổi avatar</label>
                                                                                <input type="file" style="display: none;" name="avatar_file_image" id="avatar_file" value="<?php echo e(Auth::user()->avatar); ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="boxfilter__img"></div>
                                                            <label for="avatar_file" class="boxfilter_link">
                                                                <?php if(Auth::user()->avatar): ?>
                                                                <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e(Auth::user()->avatar); ?>'); border-radius: unset ; height: 100%">
                                                                    <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                                </div>
                                                                <?php else: ?>
                                                                    <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public')); ?>/avata_user_new.jpg'); border-radius: unset  ; height: 100%">
                                                                        <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                                    </div>
                                                                <?php endif; ?>
                                                            </label>
                                                        </article>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName3" class="col-sm-2 form-control-label">Tên</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="name" id="inputName3" value="<?php echo e(Auth::user()->name); ?>">
                                                        <?php if($errors->has('name')): ?>
                                                        <div class="help-block" style="color: red">
                                                          <?php echo $errors->first('name'); ?>

                                                        </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-2 form-control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="email" class="form-control" name="email" id="inputEmail3" value="<?php echo e(Auth::user()->email); ?>">
                                                        <?php if($errors->has('email')): ?>
                                                        <div class="help-block" style="color: red">
                                                          <?php echo $errors->first('email'); ?>

                                                        </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail3" class="col-sm-2 form-control-label">Số điện thoại</label>
                                                    <div class="col-sm-10">
                                                        <input type="tel" class="form-control" name="phone" id="inputEmail3" value="<?php echo e(Auth::user()->phone); ?>">
                                                        <?php if($errors->has('phone')): ?>
                                                        <div class="help-block" style="color: red">
                                                          <?php echo $errors->first('phone'); ?>

                                                        </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div style="margin: 0 auto">
                                                        <button type="submit" class="btn btn-secondary">Cập nhập thông tin</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="menu3" class="container tab-pane fade row"><br>
                                            <form action="<?php echo e(route('changePassWord')); ?>" method="post">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="form-group row">
                                                    <label for="check_pass_old" class="col-sm-2 form-control-label">Mật khẩu cũ:</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" name="password" class="form-control " id="check_pass_old">
                                                        <span class="check_pass_old_er" style="color: red;display: none"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="check_pass_new" class="col-sm-2 form-control-label">Mật khâu mới:</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" name="new_password" class="form-control" id="new_password">
                                                        <span class="check_new_password" style="color: red;display: none"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="confirm_new_password" class="col-sm-2 form-control-label">Nhập lại mật khẩu mới:</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" name="confirm_new_password" class="form-control" id="confirm_new_password">
                                                        <span class="confirm_new_password" style="color: red;display: none"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div style="margin: 0 auto">
                                                        <button type="submit" class="btn btn-secondary" id="change_pass">Thay đổi mật khẩu</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- Data picker -->
   <script src="<?php echo e(url('public/admin/js/plugins/datapicker/bootstrap-datepicker.js')); ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script type="text/javascript">
    $(document).on('click','#change_pass',function (event) {
        event.preventDefault();
        $('.check_pass_old_er').css('display', 'none');
        $('.check_new_password').css('display', 'none');
        $('.confirm_new_password').css('display', 'none');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post('<?php echo e(route('changePassWord')); ?>', {password: $("#check_pass_old").val(),new_password: $("#new_password").val(),confirm_new_password: $("#confirm_new_password").val()}, function(data) {
            if (data.error == true) {
                if (data.message.password) {
                    $('.check_pass_old_er').css('display', 'block');
                    $('.check_pass_old_er').html(data.message.password[0]);
                };
                if (data.message.new_password) {
                    $('.check_new_password').css('display', 'block');
                    $('.check_new_password').html(data.message.new_password[0]);
                };
                if (data.message.confirm_new_password) {
                    $('.confirm_new_password').css('display', 'block');
                    $('.confirm_new_password').html(data.message.confirm_new_password[0]);
                };
            }else {
                alert("Thay đổi mật khẩu thành công!");
                window.location="<?php echo e(route('infoAccount')); ?>";
                
            };
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>