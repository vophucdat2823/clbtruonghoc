<?php $__env->startSection('content'); ?>

    <main>
        <div id="programs">
            <div class="people_details_top_title">
                <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="content_people_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">TIẾNG ANH DỰ BỊ</h5>
                                        </div>
                                    </div>
                                    <img src="<?php echo e(url('public/web/images')); ?>/program.jpg" alt="" width="100%">
                                </div>
                                <div class="dif_info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul>
                                                
                                                <?php 
                                                    
                                                    $cate_tintuc = \App\Models\Admin\Category::where('parents',7)->get();
                                                    // dd($cate_tintuc);

                                                    
                                                 ?>
                                                <?php $__currentLoopData = $cate_tintuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php 
                                                $language_id = 'en';
                                                    if (Session::has('set_language')) {
                                                        $language_id = Session::get('set_language');
                                                        //dd($language_id);
                                                    }
                                                    $posts_tt = DB::table('posts')
                                                        ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                        ->where('category_id', $ca_tt->id)
                                                        ->where('language_id',$language_id)
                                                        ->select('post_translations.*')
                                                        ->get();
                                                 ?>
                                                    <?php $__currentLoopData = $posts_tt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li>
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="<?php echo e(route('web.menu',['param' => $po_tt->code])); ?>"><?php echo e(str_limit($po_tt->name,30)); ?></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="insta_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tag_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="">Cử nhân</a>
                                            <a href="">Kinh doanh</a>
                                            <a href="">Kế toán</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Máy tính</a>
                                            <a href="">Tin học</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Kinh doanh</a>
                                            <a href="">Kế toán</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Máy tính</a>
                                            <a href="">Tin học</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Luật</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        <?php $__currentLoopData = $cate_get_page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($element->parents == 5): ?>
                        
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <?php if($language_id == 'vi'): ?>
                                            <h5 class="color_yellow"><?php echo e($element->name_vi); ?></h5>
                                        <?php elseif($language_id == 'en'): ?>
                                            <h5 class="color_yellow"><?php echo e($element->name_en); ?></h5>
                                        <?php endif; ?>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="dif_info">
                                 <?php if($element->getChilds): ?> <?php $__currentLoopData = $element->getChilds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                
                                <?php 
                                    $language_id = 'en';
                                    if (Session::has('set_language')) {
                                        $language_id = Session::get('set_language');
                                        //dd($language_id);
                                    }
                                    $posts_child = DB::table('posts')
                                    ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                    ->where('category_id', $child->id)
                                    ->where('language_id',$language_id)
                                    ->select('post_translations.*')
                                    ->get();
                                    // dd($posts_child);
                                 ?>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-12">
                                                <?php 
                                                    $mau_sac = 'tt_dif_info';
                                                    if($key == 0){
                                                        $mau_sac = 'tt_dif_info';  
                                                    }elseif ($key == 1) {
                                                        $mau_sac = 'tt_dif_info_onsite';
                                                    }
                                                 ?>
                                                <div class="<?php echo e($mau_sac); ?>">
                                                    <?php 
                                                        $language_ids = 'en';
                                                        if (Session::has('set_language')) {
                                                            $language_ids = Session::get('set_language');
                                                        }
                                                     ?>
                                                    <?php if($language_ids == 'vi'): ?>
                                                    
                                                        <p><?php echo e($child->name_vi); ?></p>
                                                    <?php elseif($language_ids == 'en'): ?>
                                                        <p><?php echo e($child->name_en); ?></p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-12">
                                                <ul>
                                                    <?php 
                                                        
                                                        // dd($element->getChilds);
                                                     ?>
                                                    <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pot_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li>
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="<?php echo e(route('web.menu',['param'=>$pot_child->code])); ?>"><?php echo e($pot_child->name); ?></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                                    
                                </div>

                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>