<?php $__env->startSection('content'); ?>
<main id="main_mobie">
    <div id="about_us" style="background: #fff;">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image != null): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($category->image); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>

                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
         <div class="activities_home">
            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                 <?php if($language_id == 'vi'): ?>
                    <p><?php echo e($category->name_vi); ?></p>
                <?php else: ?>
                    <p><?php echo e($category->name_en); ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($posts): ?>
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="card_bottom">
                                <div style="background-color: transparent;padding: 3.5vw 0;">
                                    <div class="activities_home" style="color: #102B4E;background: #E8B909;">
                                        <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                            <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;" href="<?php echo e($gram->code); ?>.html" >
                                               <?php echo e($gram->name); ?>

                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<main id="main_pc">
    <div id="living">
        <div class="people_top_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <?php if(!empty($posts)): ?>
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($post->image); ?>" alt="" width="100%" height="310px" style="object-fit: cover">
                            </div>
                            <div class="txt_living">
                                <a href="<?php echo e(route('web.menu',['param'=>$post->code])); ?>"><h3><?php echo e($post->name); ?></h3></a>
                                
                                <hr>
                                <p><?php echo e($post->simulation); ?></p>
                            </div>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>