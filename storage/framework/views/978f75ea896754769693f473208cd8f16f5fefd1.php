<?php $__env->startSection('style.css'); ?>
    
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <form class="form-horizontal" action="<?php echo e(route('admin.network.store')); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tên mạng kết nối(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Tên mạng kết nối" value="<?php echo e(old('name')); ?>">
                        </div>
                        <?php if($errors->has('name')): ?>
                            <span class="text-center text-danger" role="alert">
                                <?php echo e($errors->first('name')); ?>

                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Mô phỏng bài viết(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" name="link" value="<?php echo e(old('link')); ?>" class="form-control">
                        </div>
                        <?php if($errors->has('link')): ?>
                            <span class="text-center text-danger" role="alert">
                                <?php echo e($errors->first('link')); ?>

                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Chọn ảnh hiển thị(*):</label>
                    <div class="col-md-8">
                        <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                 style="width: 100%; height: 250px;">

                            </div>
                            <div>
                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh </span>
                                        <input type="file" name="image">
                                    </span>
                                <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                   data-dismiss="fileinput"> Xóa ảnh </a>
                                
                            </div>
                            <span class="text-center text-danger" role="alert">
                                    <?php if($errors->has('image')): ?>
                                    <?php echo e($errors->first('image')); ?>

                                <?php endif; ?>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Chọn icon network(*):</label>
                    <div class="col-md-8">
                        <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                 style="width: 100%; height: 250px;">

                            </div>
                            <div>
                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                        <span class="fileinput-new"> Chọn icon </span>
                                        <span class="fileinput-exists"> Đổi icon </span>
                                        <input type="file" name="icon">
                                    </span>
                                <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                   data-dismiss="fileinput"> Xóa icon </a>
                                
                            </div>
                            <span class="text-center text-danger" role="alert">
                                    <?php if($errors->has('icon')): ?>
                                    <?php echo e($errors->first('icon')); ?>

                                <?php endif; ?>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>
                            Thêm Mới</button>
                        <a href="<?php echo e(route('admin.network.list')); ?>" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                        </a>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>