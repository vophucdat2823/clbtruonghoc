<?php $__env->startSection('title','Sản Phẩm | Chỉnh sửa'); ?>

<?php $__env->startSection('style'); ?>





    <link href="<?php echo e(url('admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Chỉnh sửa sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    <?php 
        $productStore=$productStore::find($_GET['id']);
     ?>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('updateProduct',['id'=>$productStore->id])); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Product info</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="<?php echo e(route('listProduct')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Name(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="<?php echo e($productStore->name); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug:</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" id='id' name="id" value="<?php echo e($productStore->id); ?>" />
                                            <div>
                                                <?php if($productStore->slug): ?>
                                                    <?php 
                                                        $slug_1 = explode('.', $productStore->slug);
                                                        $slug = explode('cua-hang-truc-tuyen-', $productStore->slug);
                                                       
                                                     ?>
                                                <?php endif; ?>

                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo e(url('chi-tiet')); ?>/cua-hang-truc-tuyen-</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" required value="<?php echo e(array_pop($slug)); ?>">
                                                    <span class="input-group-addon">.<?php echo e($slug_1[1]); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Price:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="price" class="form-control" placeholder="$160.00" required value="<?php echo e($productStore->price); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <?php if($productStore->image): ?>
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e($productStore->image); ?></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="<?php echo e($productStore->image); ?>" name=""><input type="file" name="image" value="<?php echo e($productStore->image); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            <?php elseif(old('image')): ?>
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e(old('image')); ?></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="<?php echo e(old('image')); ?>" name=""><input type="file" name="image" value="<?php echo e(old('image')); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            <?php else: ?>
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description"><?php echo $productStore->description; ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>


<?php 
    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {

        foreach ($pgramall as $key => $item) {
            if ($item->parents == $parent)
            {
                echo '<option value="'.$item->id.'"';
                if ($program_test) {
                    foreach ($program_test as $value) {
                        if($item->id == $value){
                            echo 'selected="selected"';
                        }
                    }
                }
                if($parent==0){
                    echo 'style="color:red"';
                }
                echo '>';
                if($item->parents == $program_test){
                    if ($item->parents == 0) {
                        echo $char . $item->name;
                    }
                    if ($item->parents != 0) {
                        echo $char . $item->name.' (danh mục con)';
                    }
                }
                if($item->parents != $program_test){
                    echo $char . $item->name;
                }
                echo '</option>';
                if ($item->parents != $program_test){
                    showProgram($pgramall,$program_test, $item->id, $char.'---| ');
                }

            }
            
        }
    }


?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>
    <script type="text/javascript">
        $('.city-country').select2({
            theme: 'bootstrap',
            placeholder: 'cityCountry',
            ajax: {
                url: '<?php echo e(url('')); ?>/admin/profiles/getSuggestCities',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term,
                    };
                },
                processResults: function (data, params) {
                    // console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.place_id,
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
        });
    </script>


    

    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("<?php echo e(url('')); ?>/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>

    <script type="text/javascript">
     $(document).ready(function () {
            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

             var x = 0;


         $(document).on("click",".addNewCkeditor",function(){
             x++;
            var thanhpho = 'thanhpho_' + x;
            var quanhuyen = 'quanhuyen_' + x;

            var str='<fieldset class="form-horizontal"><div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                str+='<div class="input-group">';
                    str+='<div class="input-group-addon" style="padding:0;border:0">';
                      str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                    str+='</div>';
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address1[]" id="'+thanhpho+'" data-placeholder="Choose a Country..." class="chosen-select form-control">';
                                str+='<option value="">Thành Phố</option>';
                                str+='<?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ci): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>';
                                str+='<option value="<?php echo e($ci->matp); ?>">';
                                str+='<?php echo e($ci->name); ?>';
                                str+='</option>';
                                str+='<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';                 
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Quận/Huyện:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address2[]" id="'+quanhuyen+'" data-placeholder="Choose a Country..." class="form-control">';
                                str+='<option value="">--Chưa chọn Quận/Huyện--</option>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';
                str+='</div></fieldset>';

                $(document).on("change","#"+thanhpho, function(event) {
                  var id_matp = $(this).val();
                  $.get("<?php echo e(url('')); ?>/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
                    $("#"+quanhuyen).html(data);
                  });
                });
            console.log(str);
              
            $(wrapper).append(str);
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });
 </script>
    <?php if($cityDistrict): ?>
        <?php $__currentLoopData = $cityDistrict; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       
            <script>
                $("body").on("change",".ThanhPhoedit_<?php echo e($key); ?>", function(event) {
                  var id_matp = $(this).val();
                    console.log(id_matp);
                  $.get("<?php echo e(url('')); ?>/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
                    $(".QuanHuyenedit_<?php echo e($key); ?>").html(data);
                  });
                });
            </script>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>