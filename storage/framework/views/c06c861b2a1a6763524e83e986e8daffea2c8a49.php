<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách danh mục</h2>
            <ol class="breadcrumb">
            <li>
                <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
            </li>
            <li class="active">
                <strong>Danh mục sự kiện</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm danh mục</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('cate-event.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Danh mục sự kiện(*):</label>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Tên tab danh mục tiếng việt" value="<?php echo e(old('name')); ?>" >
                                                <?php if($errors->has('name')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <br><span style="margin-top: 5px">Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh(*):</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">su-kien/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" value="">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                                 <?php if($errors->has('slug')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('slug')); ?>

                                                    </span><br>
                                                <?php endif; ?>
                                                <br><span style="margin-top: 5px">Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>

                                            <div class="form-group"><label>Chọn danh mục(*):</label>
                                                    <select name="parents" class="form-control" style="text-transform:uppercase">
                                                        
                                                        <option value="0">-- ROOT -- </option>
                                                        <?php echo e(showCatepost($cateEvent,0,'',old('choose_id'))); ?>

                                                    </select>
                                                    <?php if($errors->has('parents_cate')): ?>
                                                        <span class="text-center text-danger" role="alert">
                                                            <?php echo e($errors->first('parents_cate')); ?>

                                                        </span><br>
                                                    <?php endif; ?>
                                                <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn.</span>
                                            </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Danh mục cha</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th >STT</th>
                                                        <th >Danh mục sự kiện</th>
                                                        <th>Đường dẫn tĩnh</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($cateEvent0)): ?>
                                                        <?php $__currentLoopData = $cateEvent0; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td><?php echo e($key+1); ?></td>
                                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>" target="_blank"> <span class="tag-post"><?php echo e($cate->name); ?></span></a></td>

                                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>" target="_blank"> <span class="tag-post-tacgia"><?php echo e(str_limit($cate->slug,30)); ?></span></a></td>

                                                                <td>
                                                                    <a href="<?php echo e(route('cate-event.edit', $cate->id)); ?>" type="button" class="btn btn-info">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                            data-target="#modal-delete-<?php echo e($cate->id); ?>">
                                                                        <i class="fa fa-fw fa-trash-o"></i>
                                                                    </button>
                                                                    <div class="modal modal-danger fade" id="modal-delete-<?php echo e($cate->id); ?>">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span></button>
                                                                                        <?php 
                                                                                            $delete = \App\Event::where('cate_event_id',$cate->id)->get();
                                                                                            $delete_cart = \App\CateEvent::whereIn('parents',[$cate->id])->get();
                                                                                         ?>
                                                                                        <?php if($delete->count() == 0 && $delete_cart->count() == 0): ?>
                                                                                            <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                        <?php else: ?>
                                                                                            <h4 class="modal-title">
	                                                                                           Danh mục hiện tại đang chứa dữ liệu không thể xóa !
                                                                                            </h4>
                                                                                        <?php endif; ?>
                                                                                </div>
                                                                                <div class="modal-body" style="background: white!important; text-align: center">
                                                                                    <form method="POST" action="<?php echo e(route('cate-event.destroy', $cate->id)); ?>">
                                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                                        <?php echo e(csrf_field()); ?>


                                                                                        <?php if($delete->count() == 0 && $delete_cart->count() == 0): ?>
                                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                            <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                                <i class="fa fa-save"></i>
                                                                                            </button>
                                                                                        <?php else: ?>
                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                        <?php endif; ?>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Danh mục con</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th >STT</th>
                                                        <th >Danh mục sự kiện</th>
                                                        <th>Đường dẫn tĩnh</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($cateEvent1)): ?>
                                                        <?php $__currentLoopData = $cateEvent1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                                <td><?php echo e($key+1); ?></td>
                                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>" target="_blank"> <span class="tag-post"><?php echo e($cate->name); ?></span></a></td>

                                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>" target="_blank"> <span class="tag-post-tacgia"><?php echo e(str_limit($cate->slug,30)); ?></span></a></td>

                                                                <td>
                                                                    <a href="<?php echo e(route('cate-event.edit', $cate->id)); ?>" type="button" class="btn btn-info">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                            data-target="#modal-delete-<?php echo e($cate->id); ?>">
                                                                        <i class="fa fa-fw fa-trash-o"></i>
                                                                    </button>
                                                                    <div class="modal modal-danger fade" id="modal-delete-<?php echo e($cate->id); ?>">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                     <?php 
                                                                                        $delete = \App\Event::where('cate_event_id',$cate->id)->get();
                                                                                        $delete_cart = \App\CateEvent::whereIn('parents',[$cate->id])->get();
                                                                                     ?>
                                                                                    <?php if($delete->count() == 0  && $delete_cart->count() == 0): ?>
                                                                                        <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                    <?php else: ?>
                                                                                        <h4 class="modal-title">
                                                                                            Danh mục hiện tại đang chứa dữ liệu không thể xóa !
                                                                                        </h4>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                                <div class="modal-body" style="background: white!important; text-align: center">
                                                                                    <form method="POST" action="<?php echo e(route('cate-event.destroy', $cate->id)); ?>">
                                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                                        <?php echo e(csrf_field()); ?>

                                                                                        <?php if($delete->count() == 0  && $delete_cart->count() == 0): ?>
                                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                            <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                                <i class="fa fa-save"></i>
                                                                                            </button>
                                                                                        <?php else: ?>
                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                        <?php endif; ?>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 

function showCatepost($cateEvent, $parent = 0, $char ='',$select = 0)
{
    foreach ($cateEvent as $key => $item) {
        if ($item->parents == $parent) {
            echo '<option value="'.$item->id.'"';
            if($parent == 0){
                echo 'style="color:red"';
            }
            if($select != 0 && $item->id == $select){
                echo 'selected="selected"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCatepost($cateEvent ,$item->id , $char.'---| ',$select);
        }
    }
}
 ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>