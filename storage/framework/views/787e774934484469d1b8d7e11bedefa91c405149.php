<?php $__env->startSection('style.css'); ?>
<?php $__env->startSection('title','Quản lý hiển thị'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="wrapper wrapper-content animated fadeInRight"> 

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>PAGE</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-md-1">STT</th>
                                        <th class="col-md-3">Tên trang*</th>
                                        <th class="col-md-1">Giá trị</th>
                                        <th class="col-md-2"></th>
                                        <th class="col-md-2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($pg->type == 'page'): ?>
                                        <tr>
                                            <td><?php echo e($pg->id); ?></td>
                                            <td>
                                                <a href="add_post.html"> 
                                                    <span class="tag-post"><?php echo e($pg->name); ?>

                                                    </span>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="tag-post"><?php echo e($pg->value); ?>

                                                </span>
                                            </td>
                                            
                                            <td>
                                                <a class="btn btn-primary" data-toggle="modal" href='#edit-<?php echo e($pg->id); ?>'> 
                                                    <i class="fa fa-edit text-navy" style="color: #fff"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger" data-toggle="modal" href="#tempalte-<?php echo e($pg->id); ?>"> 
                                                    <i class="fa fa-check text-navy" style="color: #fff"></i> 
                                                    DEMO
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>PAGE ITEMS</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-md-1">STT</th>
                                        <th class="col-md-3">Tên trang*</th>
                                        <th class="col-md-3">Giá trị</th>
                                        <th class="col-md-2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($pg->type == 'page_item'): ?>
                                        <tr>
                                            <td><?php echo e($pg->id); ?></td>
                                            <td>
                                                <a href="add_post.html"> 
                                                    <span class="tag-post"><?php echo e($pg->name); ?>

                                                    </span>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="tag-post"><?php echo e($pg->value); ?>

                                                </span>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary" data-toggle="modal" href='#edit-<?php echo e($pg->id); ?>'> 
                                                    <i class="fa fa-edit text-navy" style="color: #fff"></i> 
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger" data-toggle="modal" href='#tempalte-<?php echo e($pg->id); ?>'>
                                                    <i class="fa fa-check text-navy" style="color: #fff"></i> 
                                                    DEMO
                                                </a>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="modal fade" id="edit-<?php echo e($pg->id); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sửa trang "<?php echo e($pg->name); ?>"</h4>
            </div>
            <form action="<?php echo e(url('admin/page/update')); ?>/<?php echo e($pg->id); ?>" method="POST" role="form">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="modal-body">
                     <div class="form-group">
                        <label for="">Đặt tên cho template</label>
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo e($pg->name); ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-primary">Edit changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 <?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="modal fade" id="tempalte-<?php echo e($pg->id); ?>">
    <div class="modal-dialog" style="width: 90%;height: 100%">
        <div class="modal-content" style="height: 90%">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Template</h3>
            </div>
            <div class="modal-body" style="height: 80%">
                <?php echo e($pg->template); ?>

                <iframe src="<?php echo e(url('public/web')); ?>/<?php echo e($pg->template); ?>" style="width: 100%;height: 100%"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>