<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    	<?php $__currentLoopData = $cateCourse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        <div class="col-lg-12">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                    <h5><?php echo e($cate->name); ?></h5>
	                    <div class="ibox-tools">
	                        <a class="collapse-link">
	                            <i class="fa fa-chevron-up"></i>
	                        </a>
	                        <a class="close-link">
	                            <i class="fa fa-times"></i>
	                        </a>
	                    </div>
	                </div>
	                <div class="ibox-content">
	                    
	                    <div class="table-responsive">
	                        <table class="table table-striped">
	                            <thead>
	                            <tr>
	                                <th>Tên khóa học</th>
                                    <th>Trạng Thái</th>
                                    <th>Action</th>
	                            </tr>
	                            </thead>
	                            <tbody>
                            	<?php $__currentLoopData = $cate->getCourse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                            <tr>
		                                <td><?php echo e($course->name); ?></td>
		                                <td>Chưa cung cấp</td>
		                                <td class="">
                                            <a href="<?php echo e(route('course-partner.edit',['course_id'=>$course->id ])); ?>" class="btn-white btn btn-xs" >Cung cấp khóa học này</a>
                                        </td>
		                            </tr>
                            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                            </tbody>
	                        </table>
	                    </div>

	                </div>
	            </div>
	        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_2.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>