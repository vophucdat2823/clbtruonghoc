	<!-- header -->
	
	<header>
		<div class="top_header">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-12">
							<div class="container">
									<div class="main_header_content">
										<?php 
											$questions = $language_id == 'en' ? 'Have any questions?' : 'Có câu hỏi nào không?';
											$register = $language_id == 'en' ? 'Register' : 'Đăng ký';
											$or = $language_id == 'en' ? 'or' : 'hoặc';
											$login = $language_id == 'en' ? 'Login' : 'Đăng nhập';
											$using = $language_id == 'en' ? ' Student ID' : 'ID Sinh Viên';
										 ?>
										<div class="left_content_header">
											<p class="color_yellow"><?php echo e($questions); ?></p>
											<p><i class="fas fa-phone-volume color_yellow"></i>
												<a href="" class="color_white">
													<?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													 	<?php if($cd->key_option == 'phone1'): ?>
													 		<?php echo e($cd->value); ?>

													 	<?php endif; ?>
													 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</a>
												<span class="color_yellow"> | </span>
												<a href="" class="color_white">
													<?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													 	<?php if($cd->key_option == 'phone2'): ?>
													 		<?php echo e($cd->value); ?>

													 	<?php endif; ?>
													 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</a>
											</p>
											<p class="color_white"><i class="far fa-envelope color_yellow"></i>sinhvien.khoaquocte@gmail.com</p>
										</div>
										<div class="login_header">
											
											 
									  		
									  			
												
											
											<style>
											    .xt-ct-menu {
											        position: relative;
											        display: inline-block;
											    }

											    .xtlab-ctmenu-item {
											        /* background-color: #4CAF50; */
											        color: #E8B909;
											        padding: 16px;
											        font-size: 16px;
											        border: none;
											        cursor: pointer;
											    }
											    .xtlab-ctmenu-item:hover, .xtlab-ctmenu-item:focus {
											        /* background-color: #9c3328; */
											        color: #E8B909;
											    }

											    .xtlab-ctmenu-sub {
											        /* display: none;
											        position: absolute;
											        background-color: #3e8e41;
											        min-width: 260px;
											        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
											        z-index: 100;
											        display: none;
 */
													position: absolute;

													background-color: #fff;

													min-width: 390%;

													box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);

													z-index: 100;

													margin-left: -26%;

													margin-top: 5px;

													/* max-width: -56%; */
											    }
											    .xtlab-ctmenu-sub a {
											        color: white;
											        padding: 12px 16px;
											        text-decoration: none;
											        display: block;
											    }
											    .xtlab-ctmenu-sub a:hover {
											        /* background-color: orange; */
											        border: none;
											        color: #E8B909;

											    }
											    .form-control:focus {

												    color: #495057;
												    background-color: #fff;
												    border-color: #80bdff;
												    outline: 0;
												    box-shadow: 0 0 0 .2rem rgba(0,0,0,0);

												}
												#formGroupExampleInput::placeholder,#formGroupExampleInput2::placeholder{
													color: #939598 !important;
													font-weight: normal;
													font-size: 15px;
													font-style: italic;
												}
											</style>

											<div class="menu1 xt-ct-menu">
											    <div class="xtlab-ctmenu-item"><?php echo e($login); ?></div>
											    <div class="xtlab-ctmenu-sub">
														<div class="modal-dialog" style="margin:0;padding:0 3px 3px 3px" role="document">
															<div class="modal-content" style="border:0;border-radius: 0">
																<div class="modal-header" style="padding-top: 5px;padding-bottom: 5px;background-color: #939598;border-radius: 0">
																	<p class="modal-title" style="margin-top: 10px;padding-left: 0;color:#fff">For VNUIS student only</p>
																	
																</div>
																<form >
																	<div class="modal-body" style="padding:15px 0 0 0">
																		<div style="padding: 15px;padding-top: 0">
																			<fieldset class="form-group">
																				<label for="formGroupExampleInput" style="font-weight: bold;color: #E8B909">Username</label>
																				<input type="text" class="form-control" id="formGroupExampleInput" style="padding: 0;border: 0;border-bottom: 1px solid #ccc;border-radius: 0;" placeholder="Enter your username...">
																			</fieldset>
																			<fieldset class="form-group">
																				<label for="formGroupExampleInput2" style="font-weight: bold;color: #E8B909">Password</label>
																				<input type="text" class="form-control" id="formGroupExampleInput2" style="padding: 0;border: 0;border-bottom: 1px solid #ccc;border-radius: 0;" placeholder="Enter your password">
																			</fieldset>
																		</div>
																		<button type="button" style="width: 100%;border-radius: 0;margin-bottom: 5px;background-color: #E8B909;border:0;color: #102B4E;font-weight: bold " class="btn btn-primary">Sign in</button>

																		<button type="button" style="width: 40%;border-radius: 0;background-color: #939598;border:0;color: #fff;font-size: 14px " class="btn btn-primary">Create a new account</button>
																		<button type="button" style="width: 40%; float:right;border-radius: 0;background-color: #939598;border:0;color: #fff;font-size: 14px " class="btn btn-primary">Forgot your password?</button>
																	</div>
																</form>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
											    </div>
											</div>
											<p class="color_yellow"><a href="" class="color_yellow"><?php echo e($using); ?></a></p>
										</div>
									</div>
							</div>
							
							<div class="language_header">
								<?php if($language_id == 'vi'): ?>
								<a href="<?php echo e(route('web.language', 'vietnam')); ?>" class="color_yellow">Tiếng Việt</a>
								<a href="<?php echo e(route('web.language', 'english')); ?>" class="color_white">English</a>
								<?php elseif($language_id == 'en'): ?>
								<a href="<?php echo e(route('web.language', 'vietnam')); ?>" class="color_white">Tiếng Việt</a>
								<a href="<?php echo e(route('web.language', 'english')); ?>" class="color_yellow">English</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
			</div>
		</div>


	<!-- menu chính -->
		<div class="menu_h">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 col-12">
							<div class="logo_h">
								<a href="<?php echo e(route('web.home')); ?>">
									<img src="<?php echo e(url('public')); ?>/web/images/logo.png" alt="" width="75%">
								</a>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-12">
							<div class="main_menu">
								<style type="text/css">
									.active_menu{
										color: #e8b909 !important
									}
								</style>

								<ul id="menu">
									<?php $__currentLoopData = $menu_top; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

									<?php 

									// dd(url());
										$active_menu = $menu['link'] == url(''.$code.'') || $menu['link'] == url('home/'.$code.'') || $menu['link'] == url('programs/'.$code.'') ? 'active_menu' : '';
									 ?>
									<?php if($language_id == 'vi'): ?>
										<li><a class="<?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a><span class="color_yellow"> | </span></li>
									<?php elseif($language_id == 'en'): ?>
										<li><a class="<?php echo e($active_menu); ?>" href="<?php echo e($menu['link']); ?>"><?php echo e($menu['label']); ?></a><span class="color_yellow"> | </span></li>
									<?php endif; ?>
										
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


									
								</ul>
							</div>
							<form action="" class="form_search"> 
									<div class="input-group mb-3">
										<?php 
											$search = $language_id == 'en' ? 'Keyword search' : 'Tìm kiếm từ khóa';
										 ?>

											
											<input type="text" class="form-control" placeholder="<?php echo e($search); ?>"> 
											<div class="input-group-append">
											<button class="btn btn-success" type="submit">
												<?php if($language_id == 'en'): ?>
												Search
												<?php elseif($language_id == 'vi'): ?>
												Tìm kiếm
												<?php endif; ?>
											</button>  
											</div>
									</div>
							</form>
						</div>
					</div>
				</div>
		</div>
	<!-- end menu chính -->

	<!-- menu nhỏ bên phải -->
		<div class="menu_right">
				<div class="tool_icon_right tool_show_txt" style="z-index: 3;">
				<span class="trigger"></span>
				<div class="inner">
					<p class="item item_border item_a">
						<a class="toolAction" href=""><i class="fab fa-instagram"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href=""><img src="<?php echo e(url('public')); ?>/web/images/home/subiz.png" alt="" width="70%"></a>
					</p>
					<p class="item item_border item_a">
						<a class="toolAction" href=""><i class="fab fa-facebook-f"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="" target="_blank"><img src="<?php echo e(url('public')); ?>/web/images/home/youtube.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="" target="_blank"><i class="fab fa-linkedin-in"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href=""><img src="<?php echo e(url('public')); ?>/web/images/home/zalo.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="" target="_blank"><i class="far fa-envelope"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="" target="_blank"><img src="<?php echo e(url('public')); ?>/web/images/home/printer.svg" alt="" width="50%"></a>
					</p>
					<p class="item get_info item_a">
						<a href="" target="_blank">nhận thông tin từ chúng tôi</a>
					</p>
				</div>
				</div>
		</div>
	<!-- end menu nhỏ bên phải -->

	</header>
	<!-- end header -->
	
