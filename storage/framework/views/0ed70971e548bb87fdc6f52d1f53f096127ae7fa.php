<?php $__env->startSection('title','Khóa học | Thêm mới'); ?>
<?php $__env->startSection('style'); ?>





    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thêm khóa học</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Thêm khóa học</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('course.store')); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin khóa học</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Địa chỉ</a></li>
                        
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="<?php echo e(route('course.index')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên khóa học(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên khóa học..." required value="<?php echo e(old('name')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo e(url('chi-tiet')); ?>/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn..." required value="<?php echo e(old('slug')); ?>">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Học phí/Buổi:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price" class="form-control" placeholder="$160.00" required value="<?php echo e(old('price')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Số buổi:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="number_lesson" class="form-control" placeholder="Số buổi" required value="<?php echo e(old('number_lesson')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Số học sinh:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="number_student" class="form-control" placeholder="Số học sinh" required value="<?php echo e(old('number_student')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Điều kiện học:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="study_condition" class="form-control" placeholder="Mặc định: Không" value="<?php echo e(old('study_condition')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục khóa học(*):</label>
                                        <div class="col-sm-10">

                                            <select name="cate_course_id[]" data-placeholder="Choose a Country..." class="chosen-select" required multiple tabindex="4">
                                                <?php echo e(showcateCourse($cateCourse,0,'',old('cate_course_id'))); ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Loại khóa học(*):</label>
                                        <div class="col-sm-10">
                                            <select name="type_course_id" class="chosen-select" required tabindex="4">
                                                <option value="">Phân loại</option>
                                                <?php $__currentLoopData = $typeCourse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($type->id); ?>"><?php echo e($type->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Banner(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="banner" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description"><?php echo old('description'); ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <label class="" for="email">
                                    <button type="button" class="btn btn-success addNewCkeditor">
                                          Add address +
                                      </button>
                                </label>
                                <div class="listTabCkeditor">
                                    <fieldset class="form-horizontal">
                                        <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                            <div class="input-group">
                                                <div class="input-group-addon" style="padding:0;border:0">
                                                  <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address1[]" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                            <option value="">Thành Phố</option>
                                                            <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ci): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($ci->matp); ?>"><?php echo e($ci->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Quận/Huyện:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address2[]" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control">
                                                            <option value="">--Chưa chọn Quận/Huyện--</option>
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Địa chỉ cụ thể:</label>
                                                    <div class="col-sm-10">
                                                         <input type="text" name="address3[]" class="form-control" placeholder="Mặc định: Không" required value="<?php echo e(old('address3')); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div><hr>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php

function showcateCourse($cateCourse, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateCourse as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateCourse[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showcateCourse($cateCourse, $item->id, $char . '---| ', $select);
        }
    }
}

?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>
    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("<?php echo e(url('')); ?>/partner/course/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>

    <script type="text/javascript">
     $(document).ready(function () {
            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

             var x = 0;


         $(document).on("click",".addNewCkeditor",function(){
             x++;
            var thanhpho = 'thanhpho_' + x;
            var quanhuyen = 'quanhuyen_' + x;

            var str='<fieldset class="form-horizontal"><div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                str+='<div class="input-group">';
                    str+='<div class="input-group-addon" style="padding:0;border:0">';
                      str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                    str+='</div>';
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address1[]" id="'+thanhpho+'" data-placeholder="Choose a Country..." class="chosen-select form-control">';
                                str+='<option value="">Thành Phố</option>';
                                str+='<?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ci): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>';
                                str+='<option value="<?php echo e($ci->matp); ?>">';
                                str+='<?php echo e($ci->name); ?>';
                                str+='</option>';
                                str+='<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';                 
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Quận/Huyện:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address2[]" id="'+quanhuyen+'" data-placeholder="Choose a Country..." class="form-control">';
                                str+='<option value="">--Chưa chọn Quận/Huyện--</option>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';

                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Địa chỉ cụ thể:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<input type="text" name="address3[]" class="form-control" placeholder="Mặc định: Không" required value="">';
                        str+='</div>';
                    str+='</div>';
                str+='</div></fieldset>';

                $(document).on("change","#"+thanhpho, function(event) {
                  var id_matp = $(this).val();
                  $.get("<?php echo e(url('')); ?>/partner/ajax/quanhuyen/"+id_matp,function(data) {
                    $("#"+quanhuyen).html(data);
                  });
                });
            console.log(str);
              
            $(wrapper).append(str);
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });




</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_2.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>