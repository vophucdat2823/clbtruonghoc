<?php $__env->startSection('title','Clb Trường Học'); ?>
<?php $__env->startSection('content'); ?>
<main>
    <div class="">
        <div class="w-100 background_div">    
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                <div class="col-12 my-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-3" >
                                    <?php echo $__env->make('layouts.left_profile', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <style>
                                    .style_profile a.active{
                                        color: #1B74BB !important;
                                    }
                                </style>
                                <div class="col-sm-9 bg-fff" style="background: #fff; border-radius: 5px">
                                    <div class="mb-1 style_profile">
                                        <ul class="nav nav-tabs" role="tablist ">
                                            <li class="nav-item">
                                              <a class="active" data-toggle="tab" href="#menu2" style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">Dịch vụ của tôi</a>
                                            </li>
                                            <li class="nav-item">
                                              <a style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">|</a>
                                            </li>
                                            <li class="nav-item">
                                              <a data-toggle="tab" href="#menu3" style="margin-bottom:0px;font-size: 20px;color: #000;font-weight: 300;letter-spacing: 1px;font-family: 'Roboto';line-height: 30px;display: block; padding:10px">Dịch vụ theo yêu cầu </a>
                                            </li>
                                        </ul>
                                    </div>                           
                                    <div class="tab-content  p-md-3">
                                        <div id="menu2" class="container tab-pane active row"><br>
                                            <?php if($send->total() > 0): ?>
                                                <?php echo e(csrf_field()); ?>

                                                <section class="posts endless-pagination row" data-next-page="<?php echo e($send->nextPageUrl()); ?>">
                                                    <?php $__currentLoopData = $send; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <div class="col-12 col-sm-6 col-md-4 mb-4 style_padding_box ">
                                                                <article class="boxfilter boxfilter--1 " style="border-radius: unset;">
                                                                    <div class="boxfilter__info-hover" >
                                                                        <div style="height: 100%">
                                                                            <div class="Boxinfo Boxinfo-100">
                                                                                <div class="Boxinfo-content">
                                                                                    <div class="Boxinfo-body">
                                                                                        <?php if($store->getService->getTypeServiceOne('unit')['name']): ?>
                                                                                            <p><i class="fal fa-university"></i> Đơn vị cung cấp: <?php echo e($store->getService->getTypeServiceOne('unit')['name']); ?></p>
                                                                                        <?php endif; ?>
                                                                                        
                                                                                        <?php if($store->getService->getTypeServiceOne('quality')['name']): ?>
                                                                                            <p><i class="fal fa-clock"></i> Chất lượng: <?php echo e($store->getService->getTypeServiceOne('quality')['name']); ?></p>
                                                                                        <?php endif; ?>
                                                                                        <?php if($store->getService->getTypeServiceOne('qty')['name']): ?>
                                                                                            <p><i class="fal fa-users"></i> Số lượng: <?php echo e($store->getService->getTypeServiceOne('qty')['name']); ?></p>
                                                                                        <?php endif; ?>
                                                                                        <p>
                                                                                            <i class="fal fa-paper-plane"></i> Yêu cầu: <?php echo e($store->getService->study_condition); ?>

                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                                        <form action="<?php echo e(url('/chi-tiet')); ?>/<?php echo e($store->getService->slug); ?>" method="get" role="form">
                                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">Gửi yêu cầu</button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="boxfilter__img"></div>
                                                                    <a href="#" class="boxfilter_link">
                                                                        <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($store->getService->image); ?>'); border-radius: unset ">
                                                                            <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                                        </div>
                                                                    </a>
                                                                    <div class="clearfix">
                                                                    </div>
                                                                    <div class="boxfilter__info boxfilter__info1" style=" border-radius: unset">
                                                                        <h3><?php echo e($store->getService->name); ?></h3>
                                                                        <p>
                                                                            <i class="fal fa-users"></i> Giá thành: <?php echo e($store->getService->getTypeServiceOne('price')['name']); ?>

                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-map-marker-alt"></i> Địa điểm:  <?php echo e($store->getService->getTypeServiceOne('address')['name']); ?>

                                                                        </p>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </section>
                                                <div class="mb-5">
                                                    <a class="btn btn-success" id="paginate123" style="color: #fff;padding: 0px 45px;border-radius: 15px;background: #5CC2A8;border: 0;">
                                                        Xem thêm
                                                    </a>
                                                </div>
                                            <?php else: ?>
                                                <div class="jumbotron">
                                                    <div class="container">
                                                        <h1>Xin chào, <?php echo e(Auth::user()->name); ?>!</h1>
                                                        <p>Bạn chưa đăng ký Dịch vụ nào cả !</p>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div id="menu3" class="container tab-pane fade"><br>
                                            <?php if($send->total() > 0): ?>
                                                <?php echo e(csrf_field()); ?>

                                                <section class="posts endless-pagination row" data-next-page="<?php echo e($send->nextPageUrl()); ?>">
                                                    <?php $__currentLoopData = $send; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <div class="col-12 col-sm-6 col-md-4 mb-4 style_padding_box ">
                                                                <article class="boxfilter boxfilter--1 " style="border-radius: unset;">
                                                                    <div class="boxfilter__info-hover" >
                                                                        <div style="height: 100%">
                                                                            <div class="Boxinfo Boxinfo-100">
                                                                                <div class="Boxinfo-content">
                                                                                    <div class="Boxinfo-body">
                                                                                        <p>
                                                                                            <i class="fal fa-university"></i> Đơn vị tổ chức: <?php echo e($store->getService->getTypeServiceOne('unit')['name']); ?>

                                                                                        </p>
                                                                                        <p>
                                                                                            <i class="fal fa-users"></i> Lớp tiêu chuẩn: <?php echo e($store->getService->getTypeServiceOne('qty')['name']); ?> học sinh
                                                                                        </p>
                                                                                        <p>
                                                                                           <i class="fal fa-usd-circle"></i> Học phí: <?php echo e($store->getService->getTypeServiceOne('tuition')['name']); ?>

                                                                                        </p>
                                                                                        <p>
                                                                                            <i class="fal fa-paper-plane"></i> Điều kiện học: <?php echo e($store->getService->study_condition); ?>

                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                                        <form action="<?php echo e(url('/chi-tiet')); ?>/<?php echo e($store->getService->slug); ?>" method="get" role="form">
                                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="boxfilter__img"></div>
                                                                    <a href="#" class="boxfilter_link">
                                                                        <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($store->getService->image); ?>'); border-radius: unset ">
                                                                            <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                                        </div>
                                                                    </a>
                                                                    <div class="clearfix">
                                                                    </div>
                                                                    <div class="boxfilter__info boxfilter__info1" style=" border-radius: unset">
                                                                        <h3><?php echo e($store->getService->name); ?></h3>
                                                                        <p>
                                                                            <i class="fal fa-clock"></i> Thời lượng : <?php echo e($store->getService->getTypeServiceOne('time')['name']); ?>

                                                                            
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-map-marker-alt"></i> Địa điểm:  <?php echo e($store->getService->getTypeServiceOne('address')['name']); ?>

                                                                            
                                                                        </p>
                                                                    </div>
                                                                </article>
                                                            </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </section>
                                                <div class="mb-5">
                                                    <a class="btn btn-success" id="paginate123" style="color: #fff;padding: 0px 45px;border-radius: 15px;background: #5CC2A8;border: 0;">
                                                        Xem thêm
                                                    </a>
                                                </div>
                                            <?php else: ?>
                                                <div class="jumbotron">
                                                    <div class="container">
                                                        <h1>Xin chào, <?php echo e(Auth::user()->name); ?>!</h1>
                                                        <p>Bạn chưa đăng ký Dịch vụ nào cả !</p>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- Data picker -->
   <script src="<?php echo e(url('public/admin/js/plugins/datapicker/bootstrap-datepicker.js')); ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script type="text/javascript">
    $(document).on('click', '#paginate123', function(event){
     event.preventDefault(); 
     var page = $('.endless-pagination').data('next-page');
     fetch_data(page);
    });
    function fetch_data(page) {
        if(page !== null) {
 
            $.get(page, function(data){
                $('.posts').append(data.sends);
                $('.endless-pagination').data('next-page', data.next_page);
                if (data.next_page == null){
                   $('#paginate123').remove();
                }
            });
 
        }
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>