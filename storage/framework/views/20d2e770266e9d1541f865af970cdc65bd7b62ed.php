<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách Menu</h3>
                            <?php if(session('success')): ?>
                                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>
                            <?php if(session('error')): ?>
                                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-1">STT</th>
                                    <th class="col-md-4">Tên menu</th>
                                    <th class="col-md-4">Mô phỏng</th>
                                    <th class="col-md-1">Thứ tự hiển thị</th>
                                    <th class="col-md-2">
                                        <a href="<?php echo e(route('admin.menu.create')); ?>" type="button" class="btn btn-block btn-info">
                                            Thêm Mới
                                            <i class="fa fa-fw fa-user-plus"></i>
                                        </a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e(1+$key++); ?></td>
                                        <td><?php echo e($menu['name']); ?></td>
                                        <td><?php echo e($menu['title']); ?></td>
                                        <td><?php echo e($menu['stt']); ?></td>
                                        <td>
                                            <a href="<?php echo e(route('admin.menu.edit', $menu['id'])); ?>" type="button" class="btn btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                    data-target="#modal-delete" data-id="<?php echo e($menu['id']); ?>">
                                                <i class="fa fa-fw fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>