<?php $__env->startSection('style.css'); ?>
    
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
            <?php if(session('success')): ?>
                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <?php if(session('error')): ?>
                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('error')); ?>

                </div>
            <?php endif; ?>
    <div class="wrapper wrapper-content">
        <main class="container">
            
            <?php 
                $data_vi = json_decode($chooses->mang_vi,true);
                $data_en = json_decode($chooses->mang_en,true);
             ?>
            <form id="app" method="post" action="<?php echo e(route('admin.chooses.post_create_item')); ?>">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <input type="hidden" name="id" value="<?php echo e($chooses->id); ?>">
                <h1>
                    <?php echo e($chooses->name_vi); ?>

                </h1>
                <hr>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button"  class="btn btn-block btn-success addNewApartment">
                            Add +
                        </button>
                    </div>
                </div>
                <div class="list-apartments">
                    <?php if(!empty($data_vi)): ?>
                        <?php for($i = 0; $i < count($data_vi); $i++): ?>
                           <div class="apartments">

                                <div class="row">
                                    <div class="col-xs-2">
                                        <label>&nbsp;</label>
                                        <button type="button"  class="btn btn-block btn-danger removeApartment">
                                            Rem -
                                        </button>
                                    </div>
                                     <div class="form-group col-xs-5">
                                        <label>Viet nam (*)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                            <input type="text"  class="form-control" name="mang_vi[]" placeholder="Tên bài viết" value="<?php echo e($data_vi[$i]); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-5">
                                        <label>English (*)</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                            <input type="text"  class="form-control" name="mang_en[]" placeholder="Tên bài viết" value="<?php echo e($data_en[$i]); ?>">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <?php endfor; ?>
                        <?php else: ?>
                        <div class="apartments">
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>&nbsp;</label>
                                    <button type="button"  class="btn btn-block btn-danger removeApartment">
                                        Rem -
                                    </button>
                                </div>
                                 <div class="form-group col-xs-5">
                                    <label>Viet nam (*)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                        <input type="text" v-model="apartment.mang_vi" class="form-control" name="mang_vi[]" placeholder="Tên bài viết">
                                    </div>
                                </div>
                                <div class="form-group col-xs-5">
                                    <label>English (*)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                        <input type="text" v-model="apartment.mang_en" class="form-control" name="mang_en[]" placeholder="Tên bài viết">
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php endif; ?>
                </div>
                <div class="row"  style="margin-top: 15px">
                    <div class="col-xs-2">
                        <button type="submit" class="btn btn-block btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;text-align: center">
                    <div class="col-xs-2">
                        <a href="<?php echo e(route('admin.chooses.list')); ?>" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>
                            Quay trở về
                        </a>
                    </div>
                </div>
                <hr>
                
            </form>
        </main>

    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('public/admin/js/vue.js')); ?>" type="text/javascript"></script>


<script type="text/javascript">

     $("body").on('click','.removeApartment',function(){
        $(this).parent().parent().parent().remove();
     });

     $(".addNewApartment").click(function(){
        // alert(1);
        // var data_select=$(this).parent().parent().siblings('.list-apartments').find('.apartments').last().clone();
        var str='<div class="apartments">';
            str+='<div class="row">';
            str+='<div class="col-xs-2"><label>&nbsp;</label><button type="button"  class="btn btn-block btn-danger removeApartment">Rem -</button></div>';
           str+='<div class="form-group col-xs-5"><label>Viet nam (*)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span><input type="text" v-model="apartment.mang_vi" class="form-control" name="mang_vi[]" placeholder="Tên bài viết"></div></div>';
           str+='<div class="form-group col-xs-5"><label>English (*)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span><input type="text" v-model="apartment.mang_en" class="form-control" name="mang_en[]" placeholder="Tên bài viết"></div></div>';
            str+='</div>';
            str+='</div>';
        $(".list-apartments").append(str);
     });


// window.app = new Vue({
//   el: '#app',
//   data: {
//     apartment: {
//       mang_vi: '',
//       mang_en: ''
//     },
//     apartments: [],
//   },
//   mounted: function () {
//     /*
//      * The "data-apartments" could come from serverside (already saved apartments)
//      */
//     this.apartments = JSON.parse(this.$el.dataset.apartments)
//   },
//   methods: {
//     addNewApartment: function () {
//       this.apartments.push(Vue.util.extend({}, this.apartment))
//     },
//     removeApartment: function (index) {
//       Vue.delete(this.apartments, index);
//     },
//     sumbitForm: function () {
      
//        * You can remove or replace the "submitForm" method.
//        * Remove: if you handle form sumission on server side.
//        * Replace: for example you need an AJAX submission.
       
//       console.info('<< Form Submitted >>')
//       console.info('Vue.js apartments object:', this.apartments)
//       window.testSumbit()
//     }
//   }
// })

// /*
//  * This is not Vue.js code, just a bit of jQuery to test what data would be submitted.
//  */
// window.testSumbit = function () {
//   if (!window.jQuery) {
//     console.warn('jQuery not present!')
//     return false
//   }
//   console.info('Submitted (serverside) array:', jQuery('form').serializeJSON())
// }



</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>