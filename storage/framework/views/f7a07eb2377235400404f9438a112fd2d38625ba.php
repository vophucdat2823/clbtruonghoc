<?php $__env->startSection('title','Sản Phẩm | Thêm mới'); ?>
<?php $__env->startSection('style'); ?>
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thêm mới sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Thêm mới sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('postProduct')); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin sản phẩm</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Giảm giá</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"> Hình ảnh</a></li>
                        <li class=""><button type="submit" id="addProduct" class="btn btn-success">Thêm sản phẩm </button></li>
                        <a href="<?php echo e(route('listProduct')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên sản phẩm(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="<?php echo e(old('name')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo e(url('chi-tiet')); ?>/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Slug..." required value="<?php echo e(old('slug')); ?>">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Thương hiệu(*):</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="trademark" id="trademark" class="form-control" placeholder="Thương hiệu..." required value="<?php echo e(old('trademake')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">SKU:</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="sku" id="sku" class="form-control" placeholder="SKU..."   value="<?php echo e(old('sku')); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Giá(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="price" class="form-control" placeholder="$160.00" required value="<?php echo e(old('price')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả ngắn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="short_description" id="short_description" class="form-control" placeholder="Mô tả ngắn..." required value="<?php echo e(old('short_description')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description"><?php echo old('description'); ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Giá sau khi giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="price_sale" class="form-control" placeholder="$160.00"  value="<?php echo e(old('price_sale')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Phần trăm được giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <input type="text" name="sale" class="form-control" placeholder="50"  value="<?php echo e(old('sale')); ?>">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- <label for="document">Documents</label>
                                    <div class="needsclick dropzone" id="document-dropzone">

                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG 1:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image1" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG 2:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image2" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG 3:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image3" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG 4:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image4" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>

    <script type="text/javascript">
        $("#form-create-product").validate({
            rules: {
                name: "required",
                slug: "required",
                price: {
                    "required": true,
                    "number": true
                },
                price_sale: {
                    "number": true
                },
                sale: {
                    "number": true
                },
                image: "required",
                category_id: "required",
                trademark: "required",
                short_description: "required",

            },
            messages: {
                "name": "Tên sản phẩm không được để trống",
                "slug": "Đường đẫn tĩnh không được để trống",
                "image": "Vui vòng thêm ảnh",
                "trademark": "Thương hiệu không được để trống",
                "short_description": "Mô tả ngắn không được để trống",
                "price": {
                    "required": "Vui lòng nhập giá sản phẩm",
                    "number": 'Giá sản phẩm là số'
                },
                "price_sale": {
                    "number": 'Vui lòng nhập số'
                },
                "sale": {
                    "number": 'Vui lòng nhập số'
                },
                "category_id": "Nghề nghiệp không được để trống",

            },
            errorPlacement: function (error, element) {
              error.appendTo(element.parent().parent().parent());
            },
            submitHandler: function(form) {
                $(form).submit();
            }
        });

        // $( document ).ready(function() {
        //     Dropzone.autoDiscover = false;
         
        //     var myDropzone = new Dropzone(".dropzone" ,{ 
        //         url: "<?php echo e(route('postProduct')); ?>",
        //         // uploadMultiple: true,
        //         // parallelUploads: 100,
        //         // maxFiles: 100,
        //         // autoProcessQueue: false  
        //         // addRemoveLinks: true,
        //         autoProcessQueue: false, // this is important as you dont want form to be submitted unless you have clicked the submit button
        //         // autoDiscover: false,
        //         // paramName: 'pic', // this is optional Like this one will get accessed in php by writing $_FILE['pic'] // if you dont specify it then bydefault it taked 'file' as paramName eg: $_FILE['file'] 
        //         // previewsContainer: '#dropzonePreview', // we specify on which div id we must show the files
        //         // clickable: false, // this tells that the dropzone will not be clickable . we have to do it because v dont want the whole form to be clickable 
        //         accept: function(file, done) {
        //             console.log("uploaded");
        //             done();
        //         },
        //         error: function(file, msg){
        //             alert(msg);
        //         },
        //         init: function() {
        //             var myDropzone = this;
        //             //now we will submit the form when the button is clicked
        //             $("#addProduct").on('click',function(e) {
        //                 e.preventDefault();
        //                 myDropzone.processQueue(); // this will submit your form to the specified action path
        //                 // after this, your whole form will get submitted with all the inputs + your files and the php code will remain as usual 
        //             //REMEMBER you DON'T have to call ajax or anything by yourself, dropzone will take care of that
        //                 });      
        //         } // init end
        //     });
                
        //             // $('#addProduct').click(function(){
        //             //     myDropzone.processQueue();
        //             // });
        // });
    </script> 

   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>