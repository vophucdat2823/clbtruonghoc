<?php $__env->startSection('title','Dịch Vụ | Chỉnh sửa'); ?>

<?php $__env->startSection('style'); ?>





    
<!-- Toastr style -->
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">



<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('question.store',['type'=>$type,'id'=>$id])); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Câu hỏi</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <?php if($type == 'service'): ?>
                            <a href="<?php echo e(route('service.index')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                        <?php elseif($type == 'course'): ?>
                            <a href="<?php echo e(route('course.index')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                        <?php endif; ?>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Thêm câu hỏi(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_unit">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_unit">
                                    <?php if(!empty(old('type'))): ?>
                                        <?php for($i = 0; $i < count(old('type')); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="hidden" v-model="apartment.mang_vi" class="form-control" name="type[]" value="<?php echo e($type); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_unit">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php elseif($questions): ?>
                                    <div class="loadTypeService_unit">
                                        <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $unit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="<?php echo e($unit->id); ?>" data-type="<?php echo e($type); ?>" class="form-control" name="name_update" value="<?php echo e($unit->question); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_unit" data-id="<?php echo e($unit->id); ?>">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <?php endif; ?>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Toastr -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/toastr/toastr.min.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script type="text/javascript">

        $("body").on('click','.removeApartment_unit',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_unit").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Đặt câu hỏi!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="<?php echo e($type); ?>">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_unit">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_unit").prepend(str);

        });



    </script>


    <script>
        $(document).on("click",".removeTypeService", function(event) {
          var id_matp = $(this).data('id');
          var _type = $(this).data('type');
          $.get("<?php echo e(url('')); ?>/admin/question-del/"+id_matp,function(data) {
            if (data) {
                toastr["success"]("Xóa thành công.");
                $(".loadTypeService"+_type).load(location.href + ' .loadTypeService'+_type+'>*');
            }else {
                toastr["error"]("Xóa thất bại.");
                
            };
          });
        });
    </script>

    <script>


        $(document).ready(function(){
           $(document).on("blur" ,"input[name='name_update']" ,function(){
            var id = $(this).data("id");
            var type = $(this).data("type");
            var name = $(this).val();
            if(id != '')
              {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               $.ajax({
                url:"<?php echo e(route('question.updateQuestion')); ?>",
                method:"POST",
                data:{name:name, type:type, id:id},
                success:function(data)

                {
                    if (data) {
                        toastr["success"]("Thay đổi thành công.");
                    }else {
                        toastr["error"]("Thay đổi thất bại.");
                    };
                }
               })
              }
            else
              {
               toastr["error"]("Dữ liệu lỗi.");
              }
          });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>