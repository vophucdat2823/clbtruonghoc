<?php $__env->startSection('title','Dịch Vụ | Danh sách'); ?>
<style>
    .cate_service_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách dịch vụ</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Danh sách dịch vụ</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">


                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên khách hàng</th>
                                    <th data-toggle="true">Loại dịch vụ</th>
                                    <th data-hide="phone">Email</th>
                                    <th data-hide="phone">Số điện thoại</th>
                                    <th data-hide="phone">Trạng Thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $send; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr >
                                            <td>
                                               <?php echo e($store['name']); ?>

                                            </td>
                                            <td>
                                               <?php if($store->type == 'service'): ?>
                                                    <?php echo e($store->getService->name); ?>

                                                <?php else: ?>
                                                    <?php echo e($store->getCourse->name); ?>

                                                <?php endif; ?>
                                            </td>
                                            <td>
                                               <?php echo e($store['email']); ?>

                                            </td>
                                            <td>
                                               <?php echo e($store['phone']); ?>

                                            </td>
                                            <td class="load_status_<?php echo e($store['id']); ?>">
                                                <?php echo e(csrf_field()); ?>

                                                <?php if($store['status'] == 0): ?>
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="<?php echo e($store['id']); ?>" data-stt="1">Đang chờ</button>
                                                <?php else: ?>
                                                <button type="button" class="btn btn-primary colum_status" data-id="<?php echo e($store['id']); ?>" data-stt="0">Đã xử lý</button>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-right">
                                                <form action="<?php echo e(route('send.destroy',['id'=>$store['id']])); ?>" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        <?php echo e(csrf_field()); ?>

                                                        <a href="<?php echo e(route('admin.send.index_detail',['id'=>$store['id']])); ?>" class="btn-white btn btn-xs" >Chi tiết</a>
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"<?php echo e(route('send.update_status')); ?>",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)

        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>