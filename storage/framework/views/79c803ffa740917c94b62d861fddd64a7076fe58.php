

<?php $__env->startSection('content'); ?>

<main id="main_mobie">
    <div id="about_us">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category['image'] != null): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($category['image']); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>

                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
        
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($cate_event): ?>
                        <?php $__currentLoopData = $cate_event; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($language_id == 'vi'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding-top: 8.5vw;">
                                        <div class="activities_home" style="background: #E8B909;color: #102B4E">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #102B4E !important;text-transform: uppercase;" href="#<?php echo e(str_slug($gram->name_vi)); ?>">
                                                   <?php echo e($gram->name_vi); ?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="<?php echo e(str_slug($gram->name_vi)); ?>" class="collapse show" data-parent="#accordion" >
                                        <div class="card-body" style="background: #fff;padding: 3.25vw;">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="col-md-12" style="padding-left:0;padding-right: 0;">
                                                        <div class="item" style="margin-bottom: 4.25vw">
                                                            <img src="<?php echo e(url('public/web/images/transparent/transparent_program.png')); ?>" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($gram->image); ?>)" class="transparent" alt="" width="100%">
                                                        </div>
                                                        <?php 

                                                             $posts_child = DB::table('posts')
                                                                            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                                            ->where('category_id', $gram->id)
                                                                            ->where('language_id', $language_id)
                                                                            ->orderBy('id','desc')
                                                                            ->where('status', 0)
                                                                            ->limit(10)
                                                                            ->select('post_translations.*')
                                                                            ->get();
                                                         ?>
                                                        <div class="txt_ct_cate">
                                                            
                                                            <?php if(count($posts_child)): ?>
                                                                <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate_ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="card-header" style="padding:1vw 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none;line-height: normal;">
                                                                        <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $cate_ev->code])); ?>">
                                                                            <strong style="color: #e8b909">
                                                                                <?php if($gram->id == 23): ?>
                                                                                        <?php if($language_id == 'vi'): ?>
                                                                                            Tin Tức
                                                                                        <?php else: ?> 
                                                                                            NEWS
                                                                                        <?php endif; ?>
                                                                                <?php else: ?>
                                                                                    <?php if($language_id == 'vi'): ?>
                                                                                        Sự kiện
                                                                                    <?php else: ?>
                                                                                        EVENT
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?> <?php echo e(1+$key++); ?>:
                                                                            </strong>
                                                                            <?php echo e($cate_ev->name); ?>

                                                                        </a>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php elseif($language_id == 'en'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding-top: 8.5vw;">
                                        <div class="activities_home" style="background: #E8B909;color: #102B4E">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #102B4E !important;text-transform: uppercase;" href="#<?php echo e(str_slug($gram->name_en)); ?>">
                                                   <?php echo e($gram->name_en); ?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="<?php echo e(str_slug($gram->name_en)); ?>" class="collapse show" data-parent="#accordion" >
                                        <div class="card-body" style="background: #fff;padding: 3.25vw;">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="col-md-12" style="padding-left:0;padding-right: 0;">
                                                        <div class="item" style="margin-bottom: 4.25vw">
                                                            <img src="<?php echo e(url('public/web/images/transparent/transparent_program.png')); ?>" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($gram->image); ?>)" class="transparent" alt="" width="100%">
                                                        </div>
                                                        <?php 

                                                             $posts_child = DB::table('posts')
                                                                            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                                            ->where('category_id', $gram->id)
                                                                            ->where('language_id', $language_id)
                                                                            ->orderBy('id','desc')
                                                                            ->where('status', 0)
                                                                            ->limit(10)
                                                                            ->select('post_translations.*')
                                                                            ->get();
                                                         ?>
                                                        <div class="txt_ct_cate">
                                                            
                                                            <?php if(count($posts_child)): ?>
                                                                <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate_ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="card-header" style="padding:1vw 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none;line-height: normal;">
                                                                        <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $cate_ev->code])); ?>">
                                                                            <strong style="color: #e8b909">
                                                                                <?php if($gram->id == 23): ?>
                                                                                        <?php if($language_id == 'vi'): ?>
                                                                                            Tin Tức
                                                                                        <?php else: ?> 
                                                                                            NEWS
                                                                                        <?php endif; ?>
                                                                                <?php else: ?>
                                                                                    <?php if($language_id == 'vi'): ?>
                                                                                        Sự kiện
                                                                                    <?php else: ?>
                                                                                        EVENT
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?> <?php echo e(1+$key++); ?>:
                                                                            </strong>
                                                                            <?php echo e($cate_ev->name); ?>

                                                                        </a>
                                                                    </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 8.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 8.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<main id="main_pc">
    <div id="news_even">
        <div class="news_even_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="content_news_even">
            <div class="container">
                <div class="row">
                    
                    <?php if(!empty($cate_event)): ?>
                    <?php $__currentLoopData = $cate_event; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate_ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <style>
                            .padding{
                                padding-right: 60px;
                            }
                        </style>
                        <?php 
                            $posts_category = DB::table('posts')
                                            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                            ->where('category_id', $cate_ev->id)
                                            ->where('language_id', $language_id)
                                            ->orderBy('id','desc')
                                            ->where('status', 0)
                                            ->limit(10)
                                            ->select('post_translations.*')
                                            ->get();
                         ?>
                        <?php if($loop->last): ?>
                            <div class="col-md-<?php echo e(count($cate_event) > 2 ? '4' : '6'); ?> col-sm-12 col-12 rp_mobile_news_even" style="padding-left: 60px">
                                <div class="col_content_h">
                                    <div class="vnu_is_content">
                                        <div class="top_vnu_is_content">
                                            <?php if($language_id == 'vi'): ?>
                                                <h5><?php echo e($cate_ev->name_vi); ?></h5>
                                                <hr>
                                                <p><?php echo e(str_limit($cate_ev->title_vi,100)); ?></p>
                                            <?php elseif($language_id == 'en'): ?>
                                                <h5><?php echo e($cate_ev->name_en); ?></h5>
                                                <hr>
                                                <p><?php echo e(str_limit($cate_ev->title_en,100)); ?></p>
                                            <?php endif; ?>
                                        </div>
                                        <div class="body_vnu_is_content">
                                            <div class="img_body_content">
                                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cate_ev->image); ?>" alt="" width="100%">
                                            </div>
                                            
                                            <ul>
                                                <?php $__currentLoopData = $posts_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li>
                                                        <strong>
                                                            <?php if($cate_ev->id == 23): ?>
                                                                    <?php if($language_id == 'vi'): ?>
                                                                        Tin Tức
                                                                    <?php else: ?> 
                                                                        NEWS
                                                                    <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php if($language_id == 'vi'): ?>
                                                                    Sự kiện
                                                                <?php else: ?>
                                                                    EVENT
                                                                <?php endif; ?>
                                                            <?php endif; ?> <?php echo e(1+$key++); ?>:
                                                        </strong>
                                                        <a href="<?php echo e($pt->code); ?>.html"><?php echo e($pt->name); ?></a> 
                                                        <hr>
                                                        <p><?php echo $pt->simulation; ?></p>
                                                    </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                        <div class="bottom_more_vnu_is_content">
                                            <a href="<?php echo e($pt->code); ?>"></a>
                                        </div>
                                        <div class="more_content_h">
                                                <a href="<?php echo e($pt->code); ?>">MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>

                        <div class="col-md-<?php echo e(count($cate_event) > 2 ? '4' : '6'); ?> col-sm-12 col-12 rp_mobile_news_even" style="padding-right: 60px">
                            <div class="col_content_h">
                                <div class="vnu_is_content">
                                    <div class="top_vnu_is_content">
                                        <?php if($language_id == 'vi'): ?>
                                            <h5><?php echo e($cate_ev->name_vi); ?></h5>
                                            <hr>
                                            <p><?php echo e(str_limit($cate_ev->title_vi,100)); ?></p>
                                        <?php elseif($language_id == 'en'): ?>
                                            <h5><?php echo e($cate_ev->name_en); ?></h5>
                                            <hr>
                                            <p><?php echo e(str_limit($cate_ev->title_en,100)); ?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="body_vnu_is_content">
                                        <div class="img_body_content">
                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cate_ev->image); ?>" alt="" width="100%">
                                        </div>
                                        <ul>

                                            
                                             <?php $__currentLoopData = $posts_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li>
                                                        <strong>
                                                            <?php if($cate_ev->id == 23): ?>
                                                                    <?php if($language_id == 'vi'): ?>
                                                                        Tin Tức
                                                                    <?php else: ?> 
                                                                        NEWS
                                                                    <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php if($language_id == 'vi'): ?>
                                                                    Sự kiện
                                                                <?php else: ?>
                                                                    EVENT
                                                                <?php endif; ?>
                                                            <?php endif; ?> <?php echo e(1+$key++); ?>:
                                                        </strong>
                                                        <a href="<?php echo e($pt->code); ?>.html"><?php echo e($pt->name); ?></a> 
                                                        <hr>
                                                        <p><?php echo $pt->simulation; ?></p>
                                                    </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                    <div class="bottom_more_vnu_is_content">
                                        <a href="<?php echo e($pt->code); ?>.html"></a>
                                    </div>
                                    <div class="more_content_h">
                                            <a href="<?php echo e($pt->code); ?>.html">MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>