

<?php $__env->startSection('content'); ?>
<style type="text/css">
    .sync1 .item {
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

    .sync2 .item {
        background: #C9C9C9;
        /* padding: 10px 0px; */
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        cursor: pointer;
    }

    .sync2 .item h1 {
        font-size: 18px;
    }

    .sync2 .current .item {
        background: #0c83e7;
    }

    .owl-theme .owl-nav [class*='owl-'] {
        transition: all .3s ease;
    }

    .owl-theme .owl-nav [class*='owl-'].disabled:hover {
        background-color: #D6D6D6;
    }

    .sync1.owl-theme {
        position: relative;
    }

    .sync1.owl-theme .owl-next,
    .sync1.owl-theme .owl-prev {
        width: 22px;
        height: 40px;
        margin-top: -20px;
        position: absolute;
        top: 30% !important;
    }

    .sync1.owl-theme .owl-prev {
        left: 33.5% !important;
    }

    .sync1.owl-theme .owl-next {
        right: 33.5% !important;
    }
    /* animate fadin duration 1.5s */
    .owl-carousel .animated{
        animation-duration: 1.5s !important;
    }
    /* 輪播的前後按鈕背景調大 */
    .sync1.owl-theme .owl-next, .sync1.owl-theme .owl-prev {
        width: 35px !important;
        height: 55px !important;
    }
    .sync1 svg {
        width: 22px !important;
    }
    .owl-item > div {
      cursor: pointer;
      margin: 6% 8%;
      transition: margin 0.4s ease;
    }
    .owl-item.center > div {
      cursor: auto;
      margin: 0;
    }
    .owl-item:not(.center) > div:hover {
      opacity: .75;
    }
    .owl-item:not(.center) > div {
      opacity: .75;
    }
    @media  screen and (max-width: 600px) {
       main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .center p{
            font-size: 14px !important;

        }
    }

    main #home .slide_home img{
        border-radius: unset !important;
        box-shadow: unset;
        width: 100%;

    }
    main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .owl-item p{
        display: none;

    }
    main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .center p{
        border-bottom: 1.5px solid #5393CF ;
        font-size: 20px;
        text-align: center;
        display: block;
        margin-top: 15px;

    }
    main #about_us .content_about_us .card-header{
        border-bottom: 0;
        padding-left: 0;
        background-color:transparent;
    }
    main #about_us .content_about_us .card-header a{
        font-size: 28px;
        font-weight: bold;
        color: #102B4E;
        text-transform: uppercase;
    }
    main #about_us .content_about_us .card-header a:hover{
        color: #e8b909 !important;
    }
    main #about_us .content_about_us .card-header a img {
        margin-right: 20px;
        margin-top: -6px;

    }
    .owl-prev{
        position: absolute;
        top: 25% !important;
        left: 33% !important;
    }
    .owl-prev span{
        top: 12px;
    }
    .owl-next{
        position: absolute;
        top: 25% !important;
        right: 33% !important;
    }
    .owl-next span{
        top: 12px;
    }
    .placeholder_mobie_input::placeholder{
        font-size: 4.5vw
    }
    .placeholder_mobie_input label{
        font-size: 5vw;
    }

    .double {
        font: 25px sans-serif;
        margin-top: 0px;
        position: relative;
        text-align: left;
        text-transform: uppercase;
        z-index: 1;
        font-weight: bold;
    }

    .double:before {
        border-top: 2px solid #dfdfdf;
        content:"";
        margin: 0 auto;
        position: absolute;
        top: 15px; left: 0; right: 0; bottom: 0;
        width: 100%;
        z-index: -1;
    }

    .double span {
        background: #fff;
        padding: 0 15px 0 0;
    }

    .double:before {
        border-top: none;
    }

    .double:after {
        border-bottom: 10px solid #E8B909;
        content: "";
        margin: 0 auto;
        position: absolute;
        top: 30%; left: 0; right: 0;
        width: 100%;
        z-index: -1;
    }
    .back_groud{
        background: #E6E7E8;
        padding: 20px;
        border-radius: 3px
    }

    .iframe_yt{
        padding:5px 0 5px 0;
        width: 100%;
        .video-background {
          background: #000;
          position: fixed;
          top: 0; right: 0; bottom: 0; left: 0;
          z-index: -99;
        }
        .video-foreground,
        .video-background iframe {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          pointer-events: none;
        }

        @media (min-aspect-ratio: 16/9) {
          .video-foreground { height: 300%; top: -100%; }
        }
        @media (max-aspect-ratio: 16/9) {
          .video-foreground { width: 300%; left: -100%; }
        }
        @media  all and (max-width: 600px) {
        .vid-info { width: 50%; padding: .5rem; }
        .vid-info h1 { margin-bottom: .2rem; }
        }
        @media  all and (max-width: 500px) {
        .vid-info .acronym { display: none; }
        }

    }
    .video-foreground iframe{
        width: 100%;
    }
</style>
<div class="load_main_page">

    <main id="main_mobie">
        <div id="about_us" style="margin-bottom: 10%">
            <div class="home">
                <div class="banner_h">
                    <div class="img_banner_h">
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    </div>
                </div>
            </div>

            <div class="activities_home">
                <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                    <p><?php echo e($postTrans->name); ?></p>
                </div>
            </div>

            <div class="content_about_us" >
                <div class="container mt-3">
                    <div class="col-12" style="background: #fff;">
                        <p style="font-weight: 500;font-size: 6vw;padding-top: 10px;padding-bottom:10px;margin-bottom: 0">
                            <?php if($language_id == 'vi'): ?>
                            GIỚI THIỆU CHUNG
                            <?php elseif($language_id == 'en'): ?>
                            GENERAL INTRODUCTION
                            <?php endif; ?>
                        </p>
                        <style type="text/css">
                            .mark_banner table{
                                width: 100%!important
                            }
                        </style>
                        <div class="mark_banner">
                            <?php echo $postTrans->content; ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="content_about_us" >
                <div class="container mt-3">
                    <div id="accordion">
                        <div class="card_bottom">
                            <div class="">
                                <div >
                                    <?php 
                                        $program_tab = \App\Models\Admin\ProgramTab::where('tab_gram_id',$postTrans->id)->first();
                                     ?>

                                    
                                    <?php if($program_tab): ?>
                                        <?php 
                                            $slider_tab     =   ($program_tab['slider_tab'] != null) ? json_decode($program_tab['slider_tab']) : 'null';
                                            $name_tab       =   ($program_tab['name_tab'] != null) ? json_decode($program_tab['name_tab']) : 'null';
                                            $content_tab    =   ($program_tab['content_tab'] != null) ? json_decode($program_tab['content_tab']) : 'null';

                                         ?>

                                        <div class="row">
                                            <?php for($j = 0; $j < count($slider_tab) ; $j++): ?>
                                            <?php 
                                                    $chooses_tab = \App\Models\Admin\Chooses::where('id',$slider_tab[$j])->first();
                                                    $image =  ($chooses_tab['image'] != null ) ? json_decode($chooses_tab['image']) : 'null';
                                                    $teachers =  ($chooses_tab['teachers'] != null ) ? json_decode($chooses_tab['teachers']) : 'null';
                                                    $title_vi =  ($chooses_tab['title_vi'] != null ) ? json_decode($chooses_tab['title_vi']) : 'null';
                                                    $title_en =  ($chooses_tab['title_en'] != null ) ? json_decode($chooses_tab['title_en']) : 'null';
                                             ?>
                                            <div class="col-12" style="margin-bottom: 15px">
                                                <div class="tab_program txt_mark_banner">
                                                    <a class="btn collapsed card-link btn-active" data-toggle="collapse" href="#<?php echo e(str_slug($name_tab[$j])); ?>">
                                                        <?php echo e($name_tab[$j]); ?>

                                                    </a>
                                                </div>
                                                <div id="<?php echo e(str_slug($name_tab[$j])); ?>" class="collapse " data-parent="#accordion" style="background: #fff ">
                                                    <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                                        <div id=home>
                                                            <?php if($chooses_tab['id'] != 0): ?>
                                                            <div class="slide_home" style="background: transparent;padding: 0">
                                                                <div class="">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="owl-carousel owl-theme sync1 <?php echo e(str_slug($name_tab[$j])); ?>1">
                                                                                <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                                                    <div>
                                                                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($image[$a]); ?>"  alt="landscape"/>
                                                                                        <p><?php echo e($teachers[$a]); ?></p>

                                                                                    </div>
                                                                                <?php endfor; ?>
                                                                            </div>
                                                                            <div  class="owl-carousel owl-theme sync2 <?php echo e(str_slug($name_tab[$j])); ?>2">
                                                                                 <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                                                    <?php if($language_id == 'vi'): ?>
                                                                                    <div style="margin:0;margin-top: 3%;text-align: justify;">
                                                                                        <?php echo $title_vi[$a]; ?>

                                                                                    </div>
                                                                                    <?php else: ?>
                                                                                   <div style="margin:0;margin-top: 3%;text-align: justify;">
                                                                                        <?php echo $title_en[$a]; ?>

                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                <?php endfor; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php endif; ?>
                                                            <div class="slide_home" style="background: transparent;padding: 0">
                                                                <div class="">
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <?php echo $content_tab[$j]; ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endfor; ?>
                                            <?php $__env->startSection('javascript'); ?>
                                               <?php for($x=0; $x < count($slider_tab) ;$x++): ?>
                                                   <script>
                                                        jQuery(document).ready(function() {
                                                            var sync1 = jQuery(".<?php echo e(str_slug($name_tab[$x])); ?>1");
                                                            var sync2 = jQuery(".<?php echo e(str_slug($name_tab[$x])); ?>2");
                                                            var slidesPerPage = 1; //globaly define number of elements per page
                                                            var syncedSecondary = true;

                                                            sync1.owlCarousel({

                                                              slideSpeed : 2500,
                                                              nav: true,
                                                              center: true,

                                                              animateOut: 'fadeOut',
                                                              // animateIn: 'fadeIn',
                                                              autoplayHoverPause: true,
                                                              autoplaySpeed: 2200,
                                                              dots: false,
                                                              loop: true,
                                                              responsiveClass:true,
                                                              responsive:{
                                                                0:{
                                                                    autoplay:false,
                                                                    items :1,
                                                                     nav: false,
                                                                },
                                                                768:{
                                                                    autoplay: true,
                                                                    items :1,
                                                                    nav: false,
                                                                },
                                                                1024:{
                                                                    autoplay: true,
                                                                    items:3.5,
                                                                    nav: true,
                                                                }
                                                              },
                                                              responsiveRefreshRate : 200,

                                                                navText: ["<img src='<?php echo e(url('public/web/images/program/Asset 1.png')); ?>'>","<img src='<?php echo e(url('public/web/images/program/Asset 2.png')); ?>'>"],
                                                                stagePadding:25,
                                                            }).on('changed.owl.carousel', syncPosition);

                                                            sync2
                                                              .on('initialized.owl.carousel', function () {
                                                                sync2.find(".owl-item").eq(0).addClass("current");
                                                              })
                                                              .owlCarousel({
                                                              items : slidesPerPage,
                                                              dots: true,
                                                              //   nav: true,
                                                              smartSpeed: 1000,
                                                              slideSpeed : 1000,
                                                              slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                                                              responsiveRefreshRate : 100
                                                            }).on('changed.owl.carousel', syncPosition2);

                                                            function syncPosition(el) {
                                                              //if you set loop to false, you have to restore this next line
                                                              //var current = el.item.index;

                                                              //if you disable loop you have to comment this block
                                                              var count = el.item.count-1;
                                                              var current = Math.round(el.item.index - (el.item.count/2) - .5);

                                                              if(current < 0) {
                                                                current = count;
                                                              }
                                                              if(current > count) {
                                                                current = 0;
                                                              }

                                                              //end block

                                                              sync2
                                                                .find(".owl-item")
                                                                .removeClass("current")
                                                                .eq(current)
                                                                .addClass("current");
                                                              var onscreen = sync2.find('.owl-item.active').length - 1;
                                                              var start = sync2.find('.owl-item.active').first().index();
                                                              var end = sync2.find('.owl-item.active').last().index();

                                                              if (current > end) {
                                                                sync2.data('owl.carousel').to(current, 100, true);
                                                              }
                                                              if (current < start) {
                                                                sync2.data('owl.carousel').to(current - onscreen, 100, true);
                                                              }
                                                            }

                                                            function syncPosition2(el) {
                                                              if(syncedSecondary) {
                                                                var number = el.item.index;
                                                                sync1.data('owl.carousel').to(number, 100, true);
                                                              }
                                                            }

                                                            sync2.on("click", ".owl-item", function(e){
                                                              e.preventDefault();
                                                              var number = jQuery(this).index();
                                                              sync1.data('owl.carousel').to(number, 300, true);
                                                            });

                                                          });
                                                    </script>   
                                                <?php endfor; ?>
                                            <?php $__env->stopSection(); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                


                                <hr>

                                <div>
                                    <?php if($language_id == 'vi'): ?>
                                        <div class="row">
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program txt_mark_banner" >
                                                    <a class="btn collapsed card-link" style="background-color: #102B4E;color:#e8b909;" href="#">Tải Brochure <i style="text-transform:none">tại đây</i>
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                    <?php elseif($language_id == 'en'): ?>
                                        <div class="row">
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program" >
                                                    <a class="btn collapsed card-link" style="background-color: #102B4E;color:#e8b909;" href="#">Download Brochure <i style="text-transform:none">here</i>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                            

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_pc">
        <div id="about_us" style="margin-bottom: 10%">
            <div class="about_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 dell-end-a">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">
                                    <?php if($language_id == 'en'): ?>
                                    HOME
                                    <?php elseif($language_id == 'vi'): ?>
                                    Trang chủ
                                    <?php endif; ?> >
                                </span>
                            </a>
                            <a href="<?php echo e(route('web.program')); ?>">
                                <span class="color_blue" style="text-decoration: none;text-transform: uppercase;">
                                    <?php if($language_id == 'en'): ?>
                                    ADMISSION
                                    <?php elseif($language_id == 'vi'): ?>
                                    Tuyển sinh
                                    <?php endif; ?> >
                                </span>
                            </a>
                            <?php $__currentLoopData = $array_link_title2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($loop->last): ?>
                                <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                            <?php else: ?>
                                <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?> | </span>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home">
                <div class="banner_h">
                    <div class="img_banner_h">
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($postTrans->image); ?>)" alt="" class="transparent" width="100%">
                    </div>
                </div>
            </div>
            <div class="content_about_us" >
                <div class="container mt-3">
                    <div id="accordion">
                        <div class="card_bottom">
                            <div class="">
                                <div class="card-header">
                                    <a class="collapsed card-link">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($postTrans->name); ?>

                                    </a>
                                </div>
                                <div>
                                    <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                        <h3 class="double"><span>
                                            <?php if($language_id == 'vi'): ?>
                                            GIỚI THIỆU
                                            <?php elseif($language_id == 'en'): ?>
                                            INTRODUCE
                                            <?php endif; ?> </span>
                                        </h3>

                                        <div class="mark_banner" style="margin-top: 20px">
                                            <div class="container">
                                                <div class="row <?php echo e($postTrans->color_contenrt == 1 ? 'back_groud' : ''); ?>">
                                                    <?php echo $postTrans->content; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                                <?php 
                                    $data_vi = json_decode($postTrans->mang_choose);
                                 ?>
                                <?php if($postTrans->mang_choose != null): ?>
                                    <div>
                                        <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                            <h3 class="double" style="margin-bottom: 20px"><span><?php echo e($postTrans->language_id == 'vi' ? 'Vì sao bạn nên chọn chương trình!' : 'WHY DO YOU CHOOSE THE PROGRAM?'); ?></span></h3>
                                            <div class="mark_banner">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <?php if(!empty($data_vi)): ?>
                                                                <?php for($i = 0; $i < count($data_vi); $i++): ?>
                                                                <?php if($data_vi[$i] != null): ?>
                                                                    <div style="padding:5px 0 5px 0">
                                                                        <a class="collapsed card-link" style="text-decoration: none; font-weight: bold;font-size:font-size: 2.1vw;">
                                                                            <i class="fas fa-angle-double-right" style="color:#E8B909; margin-right: 7px"></i><?php echo e($data_vi[$i]); ?>

                                                                        </a>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <?php endfor; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-6">

                                                            <div class="video-background" style="">
                                                                <div class="video-foreground" style="">
                                                                    <iframe width="560" height="315" src="<?php echo e($postTrans->link_youtube); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                <?php endif; ?>

                                <div >
                                    <?php 
                                        $program_tab = \App\Models\Admin\ProgramTab::where('tab_gram_id',$postTrans->id)->first();
                                     ?>

                                    
                                    <?php if($program_tab): ?>
                                        <?php 
                                            $slider_tab     =   ($program_tab['slider_tab'] != null) ? array_chunk(json_decode($program_tab['slider_tab']),4) : 'null';
                                            $name_tab       =   ($program_tab['name_tab'] != null) ? array_chunk(json_decode($program_tab['name_tab']),4) : 'null';
                                            $content_tab    =   ($program_tab['content_tab'] != null) ? array_chunk(json_decode($program_tab['content_tab']),4) : 'null';
                                            $count_tab      =   count($slider_tab);
                                            $dem            =   $count_tab % 4;

                                         ?>
                                        <?php for($i = 0; $i < $dem; $i++): ?>
                                            <div class="row">
                                                <?php for($j = 0; $j < count($slider_tab[$i]) ; $j++): ?>
                                                     <div class="col-md-3" style="margin-bottom: 15px">
                                                        <div class="tab_program">
                                                            <a class="btn collapsed card-link btn-active" data-toggle="collapse" href="#<?php echo e(str_slug($name_tab[$i][$j])); ?>tab_pc">
                                                                    <?php echo e($name_tab[$i][$j]); ?>

                                                            </a>
                                                        </div>
                                                     </div>

                                                <?php endfor; ?>
                                                <?php for($j = 0; $j < count($slider_tab[$i]) ; $j++): ?>
                                                    <?php 
                                                            $chooses_tab    = \App\Models\Admin\Chooses::where('id',$slider_tab[$i][$j])->first();
                                                            $image          =  ($chooses_tab['image'] != null ) ? json_decode($chooses_tab['image']) : 'null';
                                                            $teachers       =  ($chooses_tab['teachers'] != null ) ? json_decode($chooses_tab['teachers']) : 'null';
                                                            $title_vi       =  ($chooses_tab['title_vi'] != null ) ? json_decode($chooses_tab['title_vi']) : 'null';
                                                            $title_en       =  ($chooses_tab['title_en'] != null ) ? json_decode($chooses_tab['title_en']) : 'null';

                                                     ?>

                                                    <div class="col-sm-12">
                                                        <div id="<?php echo e(str_slug($name_tab[$i][$j])); ?>tab_pc" class="collapse"  data-parent="#accordion">
                                                            <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                                                <div id=home>
                                                                    <?php if($chooses_tab['id'] != 0): ?>
                                                                    <div class="slide_home" style="background: transparent;padding: 0">
                                                                        <div class="">
                                                                            <div class="col-md-12">
                                                                                <div class="row">
                                                                                        <div class="owl-carousel owl-theme sync1 <?php echo e(str_slug($name_tab[$i][$j])); ?>1">
                                                                                            <?php for($total_img = 0; $total_img < count($image) ; $total_img++): ?>
                                                                                                <div>
                                                                                                    <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($image[$total_img]); ?>" alt="landscape"/>
                                                                                                    <p><?php echo e($teachers[$total_img]); ?></p>

                                                                                                </div>
                                                                                            <?php endfor; ?>
                                                                                        </div>
                                                                                        <div class="owl-carousel owl-theme sync2 <?php echo e(str_slug($name_tab[$i][$j])); ?>2">
                                                                                             <?php for($total_img = 0; $total_img <count($image) ; $total_img++): ?>
                                                                                                <?php if($language_id == 'vi'): ?>
                                                                                                <div style="margin:0;margin-top: 3%;text-align: justify;">
                                                                                                    <?php echo $title_vi[$total_img]; ?>

                                                                                                </div>
                                                                                                <?php else: ?>
                                                                                               <div style="margin:0;margin-top: 3%;text-align: justify;">
                                                                                                    <?php echo $title_en[$total_img]; ?>

                                                                                                </div>
                                                                                                <?php endif; ?>

                                                                                            <?php endfor; ?>
                                                                                                <?php $__env->startSection('javascript'); ?>
                                                                                                    <script>
                                                                                                      jQuery(document).ready(function() {

                                                                                                            var sync1 = jQuery(".<?php echo e(str_slug($name_tab[$i][$j])); ?>1");
                                                                                                            var sync2 = jQuery(".<?php echo e(str_slug($name_tab[$i][$j])); ?>2");
                                                                                                            var slidesPerPage = 1; //globaly define number of elements per page
                                                                                                            var syncedSecondary = true;

                                                                                                            sync1.owlCarousel({

                                                                                                              slideSpeed : 2500,
                                                                                                              nav: true,
                                                                                                              center: true,

                                                                                                              animateOut: 'fadeOut',
                                                                                                              // animateIn: 'fadeIn',
                                                                                                              autoplayHoverPause: true,
                                                                                                              autoplaySpeed: 2200,
                                                                                                              dots: false,
                                                                                                              loop: true,
                                                                                                              responsiveClass:true,
                                                                                                              responsive:{
                                                                                                                0:{
                                                                                                                    autoplay:false,
                                                                                                                    items :1,
                                                                                                                     nav: false,
                                                                                                                },
                                                                                                                768:{
                                                                                                                    autoplay: true,
                                                                                                                    items :1,
                                                                                                                    nav: false,
                                                                                                                },
                                                                                                                1024:{
                                                                                                                    autoplay: true,
                                                                                                                    items:3.5,
                                                                                                                    nav: true,
                                                                                                                }
                                                                                                              },
                                                                                                              responsiveRefreshRate : 200,

                                                                                                                navText: ["<img src='<?php echo e(url('public/web/images/program/Asset 1.png')); ?>'>","<img src='<?php echo e(url('public/web/images/program/Asset 2.png')); ?>'>"],
                                                                                                                stagePadding:25,
                                                                                                            }).on('changed.owl.carousel', syncPosition);

                                                                                                            sync2
                                                                                                              .on('initialized.owl.carousel', function () {
                                                                                                                sync2.find(".owl-item").eq(0).addClass("current");
                                                                                                              })
                                                                                                              .owlCarousel({
                                                                                                              items : slidesPerPage,
                                                                                                              dots: true,
                                                                                                              //   nav: true,
                                                                                                              smartSpeed: 1000,
                                                                                                              slideSpeed : 1000,
                                                                                                              slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                                                                                                              responsiveRefreshRate : 100
                                                                                                            }).on('changed.owl.carousel', syncPosition2);

                                                                                                            function syncPosition(el) {
                                                                                                              //if you set loop to false, you have to restore this next line
                                                                                                              //var current = el.item.index;

                                                                                                              //if you disable loop you have to comment this block
                                                                                                              var count = el.item.count-1;
                                                                                                              var current = Math.round(el.item.index - (el.item.count/2) - .5);

                                                                                                              if(current < 0) {
                                                                                                                current = count;
                                                                                                              }
                                                                                                              if(current > count) {
                                                                                                                current = 0;
                                                                                                              }

                                                                                                              //end block

                                                                                                              sync2
                                                                                                                .find(".owl-item")
                                                                                                                .removeClass("current")
                                                                                                                .eq(current)
                                                                                                                .addClass("current");
                                                                                                              var onscreen = sync2.find('.owl-item.active').length - 1;
                                                                                                              var start = sync2.find('.owl-item.active').first().index();
                                                                                                              var end = sync2.find('.owl-item.active').last().index();

                                                                                                              if (current > end) {
                                                                                                                sync2.data('owl.carousel').to(current, 100, true);
                                                                                                              }
                                                                                                              if (current < start) {
                                                                                                                sync2.data('owl.carousel').to(current - onscreen, 100, true);
                                                                                                              }
                                                                                                            }

                                                                                                            function syncPosition2(el) {
                                                                                                              if(syncedSecondary) {
                                                                                                                var number = el.item.index;
                                                                                                                sync1.data('owl.carousel').to(number, 100, true);
                                                                                                              }
                                                                                                            }

                                                                                                            sync2.on("click", ".owl-item", function(e){
                                                                                                              e.preventDefault();
                                                                                                              var number = jQuery(this).index();
                                                                                                              sync1.data('owl.carousel').to(number, 300, true);
                                                                                                            });
                                                                                                          });
                                                                                                    </script>
                                                                                                <?php $__env->stopSection(); ?>
                                                                                        </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php endif; ?>
                                                                    <div class="slide_home" style="background: transparent;padding: 0">
                                                                        <div class="">
                                                                            <div class="col-md-12">
                                                                                <div class="row">

                                                                                    <?php echo $content_tab[$i][$j]; ?>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php endfor; ?>
                                                <?php $__env->startSection('javascript'); ?>
                                                   <?php for($x=0; $x < count($slider_tab[$i]) ;$x++): ?>
                                                        <script>
                                                            jQuery(document).ready(function() {
                                                                    console.log('<?php echo e($name_tab[$i][$x]); ?>');
                                                                    // var total = $name_tab[x];
                                                                    var sync1 = jQuery(".<?php echo e(str_slug($name_tab[$i][$x])); ?>1");
                                                                    var sync2 = jQuery(".<?php echo e(str_slug($name_tab[$i][$x])); ?>2");
                                                                    var slidesPerPage = 1; //globaly define number of elements per page
                                                                    var syncedSecondary = true;

                                                                    sync1.owlCarousel({

                                                                      slideSpeed : 2500,
                                                                      nav: true,
                                                                      center: true,

                                                                      animateOut: 'fadeOut',
                                                                      // animateIn: 'fadeIn',
                                                                      autoplayHoverPause: true,
                                                                      autoplaySpeed: 2200,
                                                                      dots: false,
                                                                      loop: true,
                                                                      responsiveClass:true,
                                                                      responsive:{
                                                                        0:{
                                                                            autoplay:false,
                                                                            items :1,
                                                                             nav: false,
                                                                        },
                                                                        768:{
                                                                            autoplay: true,
                                                                            items :1,
                                                                            nav: false,
                                                                        },
                                                                        1024:{
                                                                            autoplay: true,
                                                                            items:3.5,
                                                                            nav: true,
                                                                        }
                                                                      },
                                                                      responsiveRefreshRate : 200,

                                                                        navText: ["<img src='<?php echo e(url('public/web/images/program/Asset 1.png')); ?>'>","<img src='<?php echo e(url('public/web/images/program/Asset 2.png')); ?>'>"],
                                                                        stagePadding:25,
                                                                    }).on('changed.owl.carousel', syncPosition);

                                                                    sync2
                                                                      .on('initialized.owl.carousel', function () {
                                                                        sync2.find(".owl-item").eq(0).addClass("current");
                                                                      })
                                                                      .owlCarousel({
                                                                      items : slidesPerPage,
                                                                      dots: true,
                                                                      //   nav: true,
                                                                      smartSpeed: 1000,
                                                                      slideSpeed : 1000,
                                                                      slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                                                                      responsiveRefreshRate : 100
                                                                    }).on('changed.owl.carousel', syncPosition2);

                                                                    function syncPosition(el) {
                                                                      //if you set loop to false, you have to restore this next line
                                                                      //var current = el.item.index;

                                                                      //if you disable loop you have to comment this block
                                                                      var count = el.item.count-1;
                                                                      var current = Math.round(el.item.index - (el.item.count/2) - .5);

                                                                      if(current < 0) {
                                                                        current = count;
                                                                      }
                                                                      if(current > count) {
                                                                        current = 0;
                                                                      }

                                                                      //end block

                                                                      sync2
                                                                        .find(".owl-item")
                                                                        .removeClass("current")
                                                                        .eq(current)
                                                                        .addClass("current");
                                                                      var onscreen = sync2.find('.owl-item.active').length - 1;
                                                                      var start = sync2.find('.owl-item.active').first().index();
                                                                      var end = sync2.find('.owl-item.active').last().index();

                                                                      if (current > end) {
                                                                        sync2.data('owl.carousel').to(current, 100, true);
                                                                      }
                                                                      if (current < start) {
                                                                        sync2.data('owl.carousel').to(current - onscreen, 100, true);
                                                                      }
                                                                    }

                                                                    function syncPosition2(el) {
                                                                      if(syncedSecondary) {
                                                                        var number = el.item.index;
                                                                        sync1.data('owl.carousel').to(number, 100, true);
                                                                      }
                                                                    }

                                                                    sync2.on("click", ".owl-item", function(e){
                                                                      e.preventDefault();
                                                                      var number = jQuery(this).index();
                                                                      sync1.data('owl.carousel').to(number, 300, true);
                                                                    });

                                                                  });
                                                        </script>   
                                                    <?php endfor; ?>
                                                <?php $__env->stopSection(); ?>
                                                
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </div>


                                


                                <hr>

                                <div >
                                    <?php if($language_id == 'vi'): ?>
                                        <div class="row">
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program" >
                                                    <a class="btn collapsed card-link" style="background-color: #102B4E;color:#e8b909;" href="#">Tải Brochure <i style="text-transform:none">tại đây</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program">
                                                    <a class="btn card-link" style="background: red;color: #fff" data-toggle="modal" href='#register'>
                                                        Đăng ký
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    <?php elseif($language_id == 'en'): ?>
                                        <div class="row">
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program" >
                                                    <a class="btn collapsed card-link" style="background-color: #102B4E;color:#e8b909;" href="#">Download Brochure <i style="text-transform:none">here</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                            <div class="col-md-3" style="margin-bottom: 15px">
                                                <div class="tab_program">
                                                    <a class="btn card-link" style="background: red;color: #fff" data-toggle="modal" href='#register'>
                                                        Register
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </main>
</div>



<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(".btn-active").click(function(){
            if(!$(this).hasClass('active')){
                $("a.btn-active").removeClass('active');
                $(this).addClass('active');
            }
         });
    </script>
    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>