
<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm icon</h5>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                                    
                                        <form class="form-horizontal" action="<?php echo e(route('admin.network.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Name link:</label> 
                                                <input type="text" class="form-control" name="name" placeholder="Name" value="<?php echo e(old('name')); ?>" style="margin-bottom: 5px">
                                                <?php if($errors->has('name')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group">
                                                <label>URL</label> 
                                                
                                                <div class="input-group m-b"><span class="input-group-addon">URL</span> <input name="link" type="text" placeholder="Đường dẫn hình ảnh" value="<?php echo e(old('link')); ?>" class="form-control"></div>
                                                 <?php if($errors->has('link')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('link')); ?>

                                                    </span><br>
                                                <?php endif; ?>
                                                <span>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>

                                            
                                           <div id="image-diplay">
                                                <div class="form-group">
                                                    <label>Chọn icon hiển thị</label>
                                                       <div class="col-md-12" style="text-align: center">
                                                           <div class="fileinput fileinput-new" sty data-provides="fileinput" style="display:unset!important;">
                                                            <style type="text/css">
                                                                .preview_img{
                                                                    width: 150px;height: 150px;border-radius: 100%
                                                                }
                                                                .preview_img img{
                                                                    width: 80%;
                                                                    height: 80%;
                                                                    margin:0 auto;
                                                                    margin-top: 10%;
                                                                }
                                                            </style>
                                                               <div class="fileinput-preview thumbnail preview_img" data-trigger="fileinput">
                                                               </div>
                                                               <div style="text-align: center">
                                                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                        <span class="fileinput-new"> Chọn icon </span>
                                                                        <span class="fileinput-exists"> Đổi icon </span>
                                                                        <input type="file" name="icon" value="<?php echo e(old('icon')); ?>">
                                                                    </span>
                                                                   <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                      data-dismiss="fileinput"> Xóa icon </a>
                                                                   
                                                               </div>
                                                               <span class="text-center text-danger" role="alert">
                                                                    <?php if($errors->has('image')): ?>
                                                                       <?php echo e($errors->first('image')); ?>

                                                                   <?php endif; ?>
                                                                </span>
                                                           </div>
                                                       </div>
                                                </div>
                                            </div>




                                           <div id="image-diplay">
                                               <div class="form-group">
                                                <label>Chọn icon:</label>
                                                   <div class="col-md-12">
                                                       <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                           <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                           <img src="<?php echo e(url('public/web/images')); ?>/logo.png" alt=""> 
                                                           </div>
                                                           <div style="text-align: center">
                                                                <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                    <span class="fileinput-new"> Chọn ảnh </span>
                                                                    <span class="fileinput-exists"> Đổi ảnh </span>
                                                                    <input type="file" name="image" value="<?php echo e(old('image')); ?>">
                                                                </span>
                                                               <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                  data-dismiss="fileinput"> Xóa ảnh </a>
                                                               
                                                           </div>
                                                           <span class="text-center text-danger" role="alert">
                                                                <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                            </span>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>ADD +</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>List </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-2">Tên</th>
                                                        <th class="col-md-3">Url</th>
                                                        <th class="col-md-2">Icon</th>
                                                        <th class="col-md-2">Image</th>
                                                        <th class="col-md-2">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($networks)): ?>
                                                        <?php $__currentLoopData = $networks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $netw): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <tr>
                                                        <td><?php echo e($key+1); ?></td>
                                                        <td><a href="<?php echo e($netw->link); ?>.html" target="_blank"> <span class="tag-post"><?php echo e($netw->name); ?></span></a></td>
                                                        <td><a href="<?php echo e($netw->link); ?>.html" target="_blank"> <span class="tag-post-tacgia"><?php echo e(str_limit($netw->link,50)); ?></span></a></td>
                                                        
                                                        <td><a href="<?php echo e($netw->link); ?>"> <span class="tag-post-tacgia"><img src="<?php echo e(url('public/upload/icon')); ?>/<?php echo e($netw->icon); ?>" width="80%" alt=""></span></a></td>
                                                        <td><a href="<?php echo e($netw->link); ?>"> <span class="tag-post-tacgia"><img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($netw->image); ?>" width="100%" alt=""></span></a></td>
                                                        <td>
                                                            <a href="<?php echo e(route('admin.network.edit', ['id'=>$netw->id])); ?>" type="button" class="btn btn-info">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                    data-target="#modal-delete-<?php echo e($netw->id); ?>">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                            <div class="modal modal-danger fade" id="modal-delete-<?php echo e($netw->id); ?>">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span></button>
                                                                                    <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                            
                                                                        </div>
                                                                        <div class="modal-body" style="background: white!important; text-align: center">
                                                                               
                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                <i class="fa fa-fw fa-close"></i>
                                                                            </button>
                                                                            <a href="<?php echo e(route('admin.network.destroy', $netw->id)); ?>" style="margin-left: 15px" class="btn btn-success">Xác nhận
                                                                                <i class="fa fa-save"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>