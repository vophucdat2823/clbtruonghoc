<?php $__env->startSection('content'); ?>
<?php 
    $language_id = 'en';
    if (Session::has('set_language')) {
        $language_id = Session::get('set_language');
    }
 ?>
<main style="margin-bottom: 250px">
        <div id="partnership">
            <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 dell-end-a">

                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">
                                    <?php if($language_id == 'en'): ?>
                                    HOME
                                    <?php elseif($language_id == 'vi'): ?>
                                    Trang chủ
                                    <?php endif; ?> > 
                                </span>
                            </a>
                             <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->last): ?>
                                    <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                                <?php else: ?>
                                    <a href="<?php echo e($value); ?>" style="text-decoration: none;text-transform: uppercase;">
                                        <span class="color_blue">
                                            <?php echo e($key); ?> >
                                        </span>
                                    </a>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                    <img src="<?php echo e(url('public/web')); ?>/images/alumni/banner4-02.png" alt="" width="100%">
                    <div class="text_img">
                        <p class="top_text">ALUMNI</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="partnership">
            <div class="content_people">
                <div class="container">

                    <div class="row">
                        <style type="text/css" media="screen">
                            .alumi_st{
                                padding-left: 0;
                            }
                            .alumi_st li{
                                list-style-type: none;
                            }
                            .alumi_st li a{
                                text-decoration: none;
                                color: #000;
                                font-size: 18px
                            }
                            main #partnership .content_people .cate_pp_img .txt_cate_pp_img h5 {
                             display: inline;
                                text-transform: uppercase;
                                color: #102b4e;
                                font-size: 27px;
                                font-weight: 400;
                            }
                        </style>

                        
                        <?php if($getCart): ?>
                            <?php $__currentLoopData = $getCart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="cate_pp_img">
                                    <div class="img_cate">
                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($gC->image); ?>" alt="" width="100%" height="200px">
                                    </div>
                                    
                                    <?php if(count($gC->posts_trans)==0): ?>
                                    <div class="row txt_ct_cate" style="margin-bottom: 10px">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square" style="line-height: 2rem">
                                            <img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="40%">
                                        </div>
                                        <?php if($language_id == 'vi'): ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="<?php echo e($gC->slug); ?>"><h5><?php echo e($gC->name_vi); ?></h5></a>
                                                <hr>
                                                <p><?php echo e(str_limit($gC->title_vi,70)); ?></p>
                                        </div>
                                        <?php else: ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="<?php echo e($gC->slug); ?>"><h5><?php echo e($gC->name_en); ?></h5></a>
                                                <hr>
                                                <p><?php echo e(str_limit($gC->title_en,70)); ?></p>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row txt_ct_cate" style="margin-bottom: 10px">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul class="alumi_st">
                                                <?php $__currentLoopData = $gC->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gCpt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <img src="<?php echo e(url('public/web/images')); ?>/alumni/4x/Asset 13@4x.png" alt="" width="20%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="http://localhost:29/VNUIS_v_2/menu/student-services-student-servicelife-van-phong-nhan-vien-va-hanh-vi-hanh-chinh">
                                                                <?php echo e($gCpt->name); ?>

                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="60%">
                                        </div>
                                        <?php if($language_id == 'vi'): ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="<?php echo e($gC->slug); ?>"><h5><?php echo e($gC->name_vi); ?></h5></a>
                                            <hr>
                                        </div>
                                        <?php else: ?>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="<?php echo e($gC->slug); ?>"><h5><?php echo e($gC->name_en); ?></h5></a>
                                            <hr>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul class="alumi_st">
                                                <?php $__currentLoopData = $gC->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gCpt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <img src="<?php echo e(url('public/web/images')); ?>/alumni/4x/Asset 13@4x.png" alt="" width="20%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="<?php echo e($gCpt->code); ?>">
                                                                <?php echo e(str_limit($gCpt->name,30)); ?>

                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>