<?php $__env->startSection('style.css'); ?>
<?php $__env->stopSection(); ?>
<style>
    .cate_event_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách sự kiện</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách sự kiện</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên sự kiện</th>
                                    <th data-hide="all">Danh mục sự kiện</th>
                                    <th data-hide="phone">Loại sự kiện</th>
                                    <th data-hide="phone">Trạng thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $event; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr >
                                            <td>
                                               <?php echo e($ev->name); ?>

                                            </td>
                                            
                                            <?php 
                                               $cate_event_id = $ev->cate_event_id != null ? json_decode($ev->cate_event_id) : [];
                                             ?>

                                            <td>
                                                <?php if(count($cate_event_id)>0): ?>
                                                    <?php 
                                                        // dd();
                                                        $cateEvent = App\CateEvent::whereIn('id',$cate_event_id)->get();
                                                     ?>
                                                    <?php $__currentLoopData = $cateEvent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <b class="cate_event_id_product"><?php echo e($cate->name); ?></b>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php echo e($ev->even_type == 'Miễn Phí' ? $ev->even_type : $ev->even_type.'|'.$ev->price_range); ?>

                                            </td>
                                            
                                            <td class="load_status_<?php echo e($ev->id); ?>">
                                                <?php echo e(csrf_field()); ?>

                                                <?php if($ev->status == 0): ?>
                                                <button type="button" class="btn btn-primary colum_status" data-id="<?php echo e($ev->id); ?>" data-stt="1">Active</button>
                                                <?php else: ?>
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="<?php echo e($ev->id); ?>" data-stt="0">Pending</button>
                                                <?php endif; ?>
                                                
                                            </td>
                                            <td class="text-right">
                                                <form action="<?php echo e(route('event.destroy',['id'=>$ev->id])); ?>" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                        <a href="<?php echo e(route('event.edit',['id'=>$ev->id])); ?>" class="btn-white btn btn-xs" >Edit</a>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        var _token = $('input[name="_token"]').val();
        $(document).on('click', '.colum_status', function(){
            var status = $(this).data("stt");
            var id = $(this).data("id");
             
            if(id != '')
            {
                $.ajax({
                    url:"<?php echo e(route('event.update_status')); ?>",
                    method:"POST",
                    data:{status:status, id:id, _token:_token},
                    success:function(data)
                    {
                        $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
                    }
                })
            }
            else
            {
                $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
            }
        });
        
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>