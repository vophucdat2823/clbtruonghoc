<?php $__env->startSection('content'); ?>
<main>
    <div id="living">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">LIVING IN HANOI</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                <img src="https://www.vntrip.vn/cam-nang/wp-content/uploads/2017/07/ho-hoan-kiem-1.png" alt="" width="100%" height="310px">
                            </div>
                            <div class="txt_living">
                                <a href="#"><h3>Culture Hanoi<br> between past and future</h3></a>
                                <hr>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="img_cate">
                                    <img src="images/living/location.jpg" alt="" width="100%" height="310px">
                                </div>
                                <div class="txt_living">
                                    <a href="#"><h3>Culture Hanoi<br> between past and future</h3></a>
                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="img_cate">
                                    <img src="http://cdn01.diadiemanuong.com//ddau/640x/diadiemanuong-com-khach-tay-chia-se-bi-quyet-sang-duong-o-viet-nam1812e2eb635615918622266144.jpg" alt="" width="100%" height="310px">
                                </div>
                                <div class="txt_living">
                                    <a href="#"><h3>Culture Hanoi<br> between past and future</h3></a>
                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="img_cate">
                                    <img src="https://kenh14cdn.com/thumb_w/600/534992cb49/2015/08/14/DSC00017-55568.jpg" alt="" width="100%" height="310px">
                                </div>
                                <div class="txt_living">
                                    <a href="#"><h3>Culture Hanoi<br> between past and future</h3></a>
                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>