<div class="style_box_filter style_box_filter_profile" style="">
    <div class="second_content_pane clearfix mb-3 second_content_pane_profile" >
        <ul>
            <li>
                <a href="http://localhost:209/prima/chi-tiet/dich-vu-gia-su-tai-nha.html">
                    <div class="img_content_pane">
                        <div class="info_img_pane">
                            <?php if(Auth::user()->avatar): ?>
                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e(Auth::user()->avatar); ?>" alt="<?php echo e(Auth::user()->name); ?>" style="width: 50px;height: 100%;object-fit: cover;border-radius: 100%;">
                            <?php else: ?>
                                <img src="<?php echo e(url('public')); ?>/avata_user_new.jpg" alt="<?php echo e(Auth::user()->name); ?>" style="width: 50px;height: 100%;object-fit: cover;border-radius: 100%;">
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="text_content_pane">
                        <p class="font-Condensed" style="margin-bottom:0px;color: #000"><?php echo e(Auth::user()->name); ?></p>
                        <span class="text-small font-Light">Tham gia: <?php echo e(Auth::user()->updated_at); ?></span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <style type="text/css" media="screen">
        .active_profile_1{
            font-weight: 500;
            background: #EEF1F3;
        }
        .menu_profile_a{
            margin-bottom:0px;
            font-size: 15px;
            color: #1B74BB;
            font-weight: 300;
            letter-spacing: 1px;
            font-family: 'Roboto';
            line-height: 30px;
            display: block;
            padding:0 30px
        }
        .style_box_filter_profile{
            background: #fff;
            box-shadow: 4px 5px 10px #ccc;
            padding:0 0 30px 0;
        }
        .second_content_pane_profile{
            padding:30px 30px 0 30px
        }
    </style>
    <a class="menu_profile_a <?php echo e(Request::is('thong-tin-tai-khoan.html') ? 'active_profile_1' : ''); ?>" href="<?php echo e(route('infoAccount')); ?>"><i class="fal fa-user"></i> Quản lý tài khoản</a>
    <a class="menu_profile_a" href="#"><i class="fal fa-trophy-alt"></i> Thành tích</a>
    <a class="menu_profile_a <?php echo e(Request::is('thong-tin-dich-vu.html') ? 'active_profile_1' : ''); ?>" href="<?php echo e(route('profileService')); ?>"><i class="fal fa-university"></i> Dịch vụ</a>
    <a class="menu_profile_a <?php echo e(Request::is('thong-tin-khoa-hoc.html') ? 'active_profile_1' : ''); ?>" href="<?php echo e(route('profileCourse')); ?>"><i class="fal fa-graduation-cap"></i> Khóa học</a>
</div>