<?php 
    $caidat = \App\Models\Options::all();
    $language_id = 'en';
    if (Session::has('set_language')) {
        $language_id = Session::get('set_language');
        //dd($language_id);
    }
 ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-12 dell-end-a">
                <a href="<?php echo e(route('web.home')); ?>">
                    <span class="color_blue">
                        <?php if($language_id == 'en'): ?>
                        HOME
                        <?php elseif($language_id == 'vi'): ?>
                        Trang chủ
                        <?php endif; ?> > 
                    </span>
                </a>
                 <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($loop->last): ?>
                        <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                    <?php else: ?>
                        <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                            <span class="color_blue">
                                <?php echo e($key); ?> >
                            </span>
                        </a>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>

