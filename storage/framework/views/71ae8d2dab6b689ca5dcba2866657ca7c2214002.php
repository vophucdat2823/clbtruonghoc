<?php $__env->startSection('title','Admin - Danh sách tài khoản'); ?>
<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách tài khoản</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách tài khoản</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="mail-box-header">
                            <h2>
                                TÀI KHOẢN (<?php echo e($account->count()); ?>)
                            </h2>
                            <div class="mail-tools tooltip-demo m-t-md">
                                <form action="">
                                    <div class="input-group"><input type="text" name="name_search" value="" placeholder="Tìm kiếm theo tên tài khoản !" class="input-sm form-control"> <span class="input-group-btn">
                                            <button type="submit" class="btn btn-sm btn-primary"> SEARCH !</button> </span></div>
                                </form>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="col-md-2">Tên đối tác</th>
                                            <th class="col-md-2">Email</th>
                                            <th class="col-md-2">Số điện thoại</th>
                                            <th class="col-md-2">Địa chỉ</th>
                                            <th class="col-md-2">Quyền</th>
                                            <th class="col-md-1">Coppy</th>
                                            <th class="col-md-1">Edit</th>
                                            <th class="col-md-1">DEL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $account; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $acc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="load_status_149">
                                                <td>
                                                    <?php echo e($acc->name); ?>

                                                </td>
                                                <td>
                                                    <span class="tag-post-danhmuc">
                                                        <?php echo e($acc->email); ?>

                                                    </span>
                                                </td>
                                                <td>
                                                    <span class="tag-post-danhmuc">
                                                        <?php echo e($acc->phone); ?>

                                                    </span>
                                                </td>
                                                <td>
                                                    <span class="tag-post-danhmuc">
                                                        <?php echo e($acc->specific_address); ?>

                                                    </span>
                                                </td>
                                                <td>
                                                    <span class="tag-post-danhmuc">
                                                        <?php echo e($acc->role->name); ?>

                                                    </span>
                                                </td>
                                                <td>
                                                    <a class="btn btn-white btn-xs">Coppy</a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo e(route('event.edit',['id'=>$acc->id])); ?>" class="btn-white btn btn-xs" >Edit</a>
                                                </td>
                                                <td class="text-right">
                                                    <form action="<?php echo e(route('event.destroy',['id'=>$acc->id])); ?>" method="POST" style="margin-bottom: 0">
                                                        <div class="btn-group">
                                                            <?php echo e(csrf_field()); ?>

                                                            <input type="hidden" name="_method" value="DELETE">
                                                            <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                            
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        var _token = $('input[name="_token"]').val();
        $(document).on('click', '.colum_status', function(){
            var status = $(this).data("stt");
            var id = $(this).data("id");
             
            if(id != '')
            {
                $.ajax({
                    url:"<?php echo e(route('event.update_status')); ?>",
                    method:"POST",
                    data:{status:status, id:id, _token:_token},
                    success:function(data)
                    {
                        $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
                    }
                })
            }
            else
            {
                $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
            }
        });
        
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>