<?php if(isset($cate_page) && $cate_page!=null): ?>
<select name="category_id" id="danhmuc_menu" class="form-control">
    <?php echo e(showCategoryies($cate_page)); ?>

</select>
<?php else: ?>
<select name="category_id" id="danhmuc_menu" class="form-control">
  <option value="">-- Chọn danh mục -- </option>
</select>

<?php endif; ?>
<?php 

  function showCategoryies($cate_page, $parent = 0, $char ='')
  {
    $cate_child = array();
    foreach ($cate_page as $key => $item) {
      if ($item->parents == $parent) 
      {
        $cate_child[] = $item;
        unset($cate_page[$key]);
      }
    }
    if ($cate_child) {
      foreach ($cate_child as $key => $item) {

        echo '<option value="'.$item->id.'" >'.$char.$item->name_en.--$item->page_id.'</option>';
        showCategoryies($cate_page ,$item->id , $char.'|---');
        echo "</li>";
      }
    }
  }

?>