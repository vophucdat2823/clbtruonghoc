<!DOCTYPE html>
<html id="html_load_screen">
<head>
    <title>INTERNATIONAL SCHOOL</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Meta để lấy token khi gửi form -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Base Url khai báo khi lúc cần cho javascript --> 
    <script type="text/javascript">
            _base_url = "<?php echo e(url('/')); ?>";
    </script>
	<link rel="icon" href="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/owl.carousel.min.css')); ?>">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link href="<?php echo e(asset('/public/web/css/css.css')); ?>" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/style.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/style_mobie.css')); ?>">
	<?php echo $__env->yieldContent('stype.css'); ?>
	

</head>
<body>
	<?php 
	
		$menu_vi = Menu::getByName('menu-vi');
		$menu_en = Menu::getByName('menu-en');
		
	 	$categoryies = \App\Models\Admin\Category::orderBy('id', 'desc')->get();
	 	$caidat = \App\Models\Options::all();

	 	
		$language_id = 'vi';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
            //dd($language_id);
        }
	 ?>

	

	<?php echo $__env->make('web.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	
	<!-- main -->
	<?php echo $__env->yieldContent('content'); ?>
	<!-- end main -->

	<!-- footer -->
	<?php echo $__env->make('web.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- end footer -->

	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/jquery.min.js')); ?>"></script>
	
	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/bootstrap.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/owl.carousel.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/index.js')); ?>"></script>
	<script>
		jQuery.fn.extend({
		    setMenu:function () {
		        return this.each(function() {
		            var containermenu = $(this);

		            var itemmenu = containermenu.find('.xtlab-ctmenu-item');
		            itemmenu.click(function () {
		                var submenuitem = containermenu.find('.xtlab-ctmenu-sub');
		                submenuitem.slideToggle(500);

		            });

		            $(document).click(function (e) {
		                if (!containermenu.is(e.target) &&
		                    containermenu.has(e.target).length === 0) {
		                     var isopened =
		                        containermenu.find('.xtlab-ctmenu-sub').css("display");

		                     if (isopened == 'block') {
		                         containermenu.find('.xtlab-ctmenu-sub').slideToggle(500);
		                     }
		                }
		            });



		        });
		    },

		});


		$('.xt-ct-menu').setMenu();
	</script>
	<script>
	  $(function() {
	    $('.dang-nhap').click(function(e) {
	      
	      e.preventDefault();
	      $.ajaxSetup({
	              headers: {
	                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	              }
	      });
	      $.ajax({
	          'url' : '<?php echo e(route('web.login.student')); ?>',
	          'data': {
	            'value' : $('input[name="value"]').val(),
	            'password' : $('input[name="password"]').val()
	          },
	          'type' : 'POST',
	          success: function (data) {
	            console.log(data);
	            if (data.error == true) {

	              $('.error').hide();

	              if (data.message.value != undefined) {
	                $('.errorValue').show().text(data.message.value[0]);
	              }
	              if (data.message.password != undefined) {
	                $('.errorPassword').show().text(data.message.password[0]);
	              }
	              if (data.message.errorlogin != undefined) {
	                $('.errorLogin').show().text(data.message.errorlogin[0]);
	              }
	            } else {
	              window.location.reload();
	            }
	          }
	        });
	    })
	  });
	</script>
	<script>
	  $(function() {
	    $('#resert_password').click(function(e) {
	      $('.box').show();
	      $('.resert_password_opacity').addClass('opacity_reset');
	      e.preventDefault();
	      $.ajaxSetup({
	              headers: {
	                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	              }
	      });
	      $.ajax({
	          'url' : '<?php echo e(route('web.reset_password.student')); ?>',
	          'data': {
	            'value_email' : $('input[name="resert_password"]').val(),
	          },
	          'type' : 'POST',
	          success: function (data) {
	            console.log(data);
	            if (data.error == true) {
	              $('.box').hide();
	              $('.resert_password_opacity').removeClass('opacity_reset');
	              $('.error').hide();
	              if (data.message.value_email != undefined) {
	                $('.errorResertPassword').show().text(data.message.value_email[0]);
	              }
	              if (data.message.error_resert != undefined) {
	                $('.errorResert').show().text(data.message.error_resert[0]);
	              }
	            } else {
	              $('.box').hide();
	              $('.resert_password_opacity').removeClass('opacity_reset');
	              console.log(data);
	              $('#resert_password_modal').modal('hide');
	              alert('Vui lòng check mail, để thay đổi mật khẩu !');
	            }
	          }
	        });
	    })
	  });
	</script>
	<script>

		$(document).ready(function() {
			
			// $('#giang-vientab_pc').collapse({
			//   toggle: false
			// });

            var x = $(window).width();
			var onresize = function(e) {
	            width = e.target.outerWidth;
	            
		            if(x > 769 && width <= 769){
		            	window.location.reload(true);
		            	console.log(x);
		            	console.log(width);
		            }else if(x <= 769 && width > 769) {
		            	window.location.reload(true);
		            	console.log(x)+'x';
		            	console.log(width)+'width';
		            }else {
		            	console.log(x)+'x';
		            	console.log(width)+'width';
		            }

		    }
		        window.addEventListener("resize", onresize);

	        if (x >= 770) {
	            $('.header_mobie').remove();
	            $('#main_mobie').remove();
	            $('.footer_mobie').remove();
	        }else {
	            $('.header_pc').remove();
	            $('#main_pc').remove();
	            $('.footer_pc').remove();
	        };
		});
    </script>
    

	<?php echo $__env->yieldContent('javascript'); ?>
</body>
</html>