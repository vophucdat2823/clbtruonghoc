<?php $__env->startSection('style.css'); ?>

<?php $__env->stopSection(); ?>
<?php 
    $language_id = 'vi';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
        }
 ?>
<?php $__env->startSection('content'); ?>

    <main id="main_mobie">
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h w-100">
                   <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($cs_dt->class == 'banner'): ?>
                            <img src="<?php echo e(url('public/web/images/transparent')); ?>/transparent_banner.png" class="transparent" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->banner); ?>')" alt="" width="100%">
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="activities_home">
                <div>
                    <div class="activities_home" style="padding: 0 15px;padding-top: 3%">
                        <div class="col-md-12 col-sm-12 col-12 mobie_activities">
                            <h2><?php echo e($language_id == 'vi' ? 'Hoạt động' : 'Activities'); ?></h2>
                        </div>
                    </div>
                    <style type="text/css">
                        .padding_left_css{
                            padding-left: 8px !important;
                        }
                        .padding_right_css{
                            padding-right: 8px !important;
                        }
                    </style>
                    
                    <div class="h1_r" style="width: 100vw;">
                        <div class="mark_banner">
                            <div style="padding-right: 0;padding-left: 0; margin-right: auto;margin-left: auto;">
                                <div class="list_item_custom" style="margin-right: 0;margin-left: 0">
                                    <?php if($language_id == 'en'): ?>
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->sort <= 9 && $cs_dt->sort > 3): ?>
                                            <div class="style_itemdisplay_mobie">
                                            <div class="style_itemdisplay_mobie_0 <?php echo e($key % 2 == 0 ? 'padding_left_css' : 'padding_right_css'); ?>">
                                                <div class="img_mark_banner">
                                                    <div class="img_absolute">
                                                        <div class="img_hh">
                                                            <a href="<?php echo e($cs_dt->link); ?>">
                                                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="">
                                                            </a>
                                                        </div>
                                                         <div class="txt_mark_banner">
                                                           <a href="<?php echo e($cs_dt->link); ?>" style="color: #102B4E"><?php echo e($cs_dt->label_en); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>    
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->sort <= 9 && $cs_dt->sort > 3): ?>
                                            <div class="style_itemdisplay_mobie">
                                            <div class="style_itemdisplay_mobie_0 <?php echo e($key % 2 == 0 ? 'padding_left_css' : 'padding_right_css'); ?>">
                                                <div class="img_mark_banner">
                                                    <div class="img_absolute">
                                                        <div class="img_hh">
                                                            <a href="<?php echo e($cs_dt->link); ?>">
                                                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="">
                                                            </a>
                                                        </div>
                                                         <div class="txt_mark_banner">
                                                            <a href="<?php echo e($cs_dt->link); ?>" style="color: #102B4E"><?php echo e($cs_dt->label_vi); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="color_under_activities"></div>

            <div class="Facts_Figures_home" style="padding: 0 15px;padding-top: 3%">
                <div class="background_cus">
                    <div class="">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Facts & Figures</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="load_percent">
                                <div class="flex-wrapper">
                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart orange">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="72, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">72%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nước ngoài</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart green">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="40, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">40%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Tư Nhân</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="90, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">90%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Việc làm</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="62, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">62%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nhà nước</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="54, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">54%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Start up</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="padding-top: 5%;background: #dcddde;">
                <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 1): ?>
                    <div class="news_home" style="display: block;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12">
                                    <h2><?php echo e($language_id == 'vi' ? $cs_dt->label_vi : $cs_dt->label_en); ?></h2>
                                    <p><?php echo e($language_id == 'vi' ? $cs_dt->title_vi : $cs_dt->title_en); ?>. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="test_color"></div>

                    <div class="slide_home">
                        <div class="col-12">
                            <div class="owl-carousel owl-theme slide_img_home">
                                <?php 
                                    $slider = \App\Models\Admin\PostTranslation::where(['category_id'=>1,'language_id'=>$language_id])->get()->toArray();
                                    // dd($cs_dt->category_id);
                                 ?>
                                <?php if(!empty($slider)): ?>
                                    <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="item">
                                            <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                                <img src="<?php echo e($value['khoaquocte_id'] == null ? url('public/upload/images/').'/'.$value["image"] : $value['image']); ?>" alt="" width="80%" height="200px">
                                            </a>
                                                <div class="title_img_slide_home">
                                                    <h6 style="font-size: 3vw !important;margin-bottom: 0;">
                                                        <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                                            <?php echo e($value['name']); ?>

                                                        </a>
                                                    </h6>
                                                </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            

            <div class="activities_home" style="padding: 0 15px;padding-top: 3%">
                <div class="col-md-12 col-sm-12 col-12 mobie_activities">
                    <h2><?php echo e($language_id == 'vi' ? 'Liên hệ' : 'Contact'); ?></h2>
                </div>
            </div>
            <div class="activities_home">
                <div class="form_call_book">
                    <div style="width: 100%;">
                        <div class="call_to_action col-12">
                            <form action="">                                        
                                <h3>call to action</h3>
                                <div class="input_group_call_to_action">
                                    <input type="text" class="form-control" id="usr" name="username"
                                           placeholder="Name">
                                    <input type="mail" class="form-control" id="usr" name="mail"
                                           placeholder="Email">
                                    <input type="text" class="form-control" id="usr" name="phone"
                                           placeholder="Phone">


                                    <button type="submit" class="btn" style="text-transform: uppercase;font-size: 5vw;width: 100%;color: #fff">
                                        <span style="font-weight: bold;">brochure </span>download
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="color_under_Facts_Figures_home"></div>

            
        </div>
    </main>


    <main id="main_pc">
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                   <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($cs_dt->class == 'banner'): ?>
                            <img src="<?php echo e(url('public/web/images/transparent')); ?>/transparent_banner.png" class="transparent" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->banner); ?>')" alt="" width="100%">
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div class="text_img">
                        
                    </div>

                    <div class="mark_banner">
                    <div class="mark_opacity">
                    </div>
                        <div class="container">
                            <div class="row">
                                <?php if($language_id == 'en'): ?>
                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($cs_dt->sort <= 3 && $cs_dt->sort >= 1 && $cs_dt->class == 'icon'): ?>
                                        <div class="col-md-4 col-sm-12 col-12">
                                                <div class="img_mark_banner">
                                                    <div class="img_hh">
                                                        <a href="<?php echo e($cs_dt->link); ?>"  style="color: #fff">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="" width="60%">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="txt_mark_banner">
                                                    
                                                    <h3><a href="<?php echo e($cs_dt->link); ?>" style="color: #fff">
                                                        <?php echo e($cs_dt->label_en); ?>

                                                    </a></h3>
                                                    <p><a  style="color: #fff" href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->title_en); ?></a></p>
                                                </div>
                                            </div>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>    
                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($cs_dt->sort <= 3 && $cs_dt->sort >= 1 && $cs_dt->class == 'icon'): ?>
                                        <div class="col-md-4 col-sm-12 col-12">
                                                <div class="img_mark_banner">
                                                    <div class="img_hh">
                                                        <a href="<?php echo e($cs_dt->link); ?>" style="color: #fff">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="" width="60%">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="txt_mark_banner">
                                                    <h3><a style="color: #fff" href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->label_vi); ?></a></h3>
                                                    <p><a style="color: #fff" href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->title_vi); ?></a></p>
                                                </div>
                                            </div>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 1): ?>
                <div class="news_home">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <h2><?php echo e($language_id == 'vi' ? $cs_dt->label_vi : $cs_dt->label_en); ?></h2>
                                <p><?php echo e($language_id == 'vi' ? $cs_dt->title_vi : $cs_dt->title_en); ?>. </p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="test_color"></div>

                <div class="slide_home">
                    <div class="container">
                        <div class="owl-carousel owl-theme slide_img_home">
                            <?php 
                                $slider = \App\Models\Admin\PostTranslation::where(['category_id'=>$cs_dt->category_id,'language_id'=>$language_id])->get()->toArray();
                             ?>
                            <?php if(!empty($slider)): ?>
                                <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="item">
                                        <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                            <img src="<?php echo e($value['khoaquocte_id'] == null ? url('public/upload/images/').'/'.$value["image"] : $value['image']); ?>" alt="" width="80%" height="200px">
                                        </a>
                                        <div class="title_img_slide_home">
                                            <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                                <h6 ><?php echo e($value['name']); ?></h6>
                                            </a>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <div class="color_under_slide"></div>

            <div class="activities_home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            
                        </div>
                    </div>
                    <div class="row h1_r">
                        <div class="mark_banner">
                            <div class="container">
                                <div class="row">
                                    <style type="text/css">
                                        .txt_mark_banner a{
                                            text-decoration: none;
                                            color: #102B4E;
                                        }
                                    </style>
                                    <?php if($language_id == 'en'): ?>
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->sort <= 9 && $cs_dt->sort > 3): ?>
                                            <div class="col-md-4 col-sm-12 col-12">
                                                <div class="img_mark_banner">
                                                    <div class="img_hh">
                                                        <a href="<?php echo e($cs_dt->link); ?>">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="" width="60%">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="txt_mark_banner">
                                                    
                                                    <h3><a href="<?php echo e($cs_dt->link); ?>">
                                                        <?php echo e($cs_dt->label_en); ?>

                                                    </a></h3>
                                                    <p><a href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->title_en); ?></a></p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>    
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->sort <= 9 && $cs_dt->sort > 3): ?>
                                            <div class="col-md-4 col-sm-12 col-12">
                                                <div class="img_mark_banner">
                                                    <div class="img_hh">
                                                        <a href="<?php echo e($cs_dt->link); ?>">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt="" width="60%">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="txt_mark_banner">
                                                    <h3><a href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->label_vi); ?></a></h3>
                                                    <p><a href="<?php echo e($cs_dt->link); ?>"><?php echo e($cs_dt->title_vi); ?></a></p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="color_under_activities"></div>

            <div class="Facts_Figures_home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Facts & Figures</h2>
                            <p>Số liệu về kết quả và thành tựu của sinh viên - học viên Khoa Quốc tế sau khi tốt
                                nghiệp</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="load_percent">
                                <div class="flex-wrapper">
                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart orange">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="72, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">72%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nước ngoài</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart green">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="40, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">40%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Tư Nhân</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="90, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">90%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Việc làm</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="62, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">62%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nhà nước</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="54, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">54%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Start up</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="color_under_Facts_Figures_home"></div>


            <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 2): ?>

                <div class="slide_home_bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <h2><?php echo e($language_id == 'vi' ? $cs_dt->label_vi : $cs_dt->label_en); ?></h2>
                                <p><?php echo e($language_id == 'vi' ? $cs_dt->title_vi : $cs_dt->title_en); ?>. </p>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="owl-carousel owl-theme slide_img_home">
                            <?php 
                                $slider = \App\Models\Admin\PostTranslation::where(['category_id'=>$cs_dt->category_id,'language_id'=>$language_id])->get()->toArray();
                                
                             ?>
                            <?php if(!empty($slider)): ?>
                                <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="item">
                                            <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                                <img src="<?php echo e($value['khoaquocte_id'] == null ? url('public/upload/images/').'/'.$value["image"] : $value['image']); ?>" alt="" width="80%" height="200px">
                                            </a>
                                            <div class="title_img_slide_home">
                                                <a href="<?php echo e($value['code']); ?>.html" style="color:#fff">
                                                    <h6><?php echo e($value['name']); ?></h6>
                                                </a>
                                            </div>
                                        </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <div class="form_call_book">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="call_to_action">
                                <form action="">
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12 col-12">
                                            <h3>call to action</h3>
                                            <div class="input_group_call_to_action">
                                                <input type="text" class="form-control" id="usr" name="username"
                                                       placeholder="name">
                                                <input type="mail" class="form-control" id="usr" name="mail"
                                                       placeholder="email">
                                                <input type="text" class="form-control" id="usr" name="phone"
                                                       placeholder="phone">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="p_4_action">Thông tin của bạn chỉ được chúng tôi sử dụng cho việc
                                                gửi thông tin từ Khoa Quốc tế tới bạn và không được tiết lộ cho bên thứ
                                                ba.</p>
                                            <button type="submit" class="btn btn-warning" value="Submit"><strong>brochure</strong><span> download</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="book_library">
                                <h3>book library</h3>
                                <strong class="color_blue">Using student ID</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                                <a href="" class="color_blue">https://www.library.is.vnu.edu.vn</a>
                                <div class="button_book_library">
                                    <button type="button" class="btn btn-warning" style="width: 100%;padding: 3%" value="Submit"><strong>find
                                            more</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>