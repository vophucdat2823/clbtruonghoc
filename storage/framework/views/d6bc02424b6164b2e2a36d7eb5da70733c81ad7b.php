<?php $__env->startSection('content'); ?>

    <main style="margin-bottom: 5%;">
        <div id="about_us">
            <div class="about_top_title">
                <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="content_about_us">
                <div class="container mt-3">
                    <h2>Welcome to International School - VNU</h2>
                    <hr>
                    <div id="accordion">
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stt => $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            
                            $show = $stt == '0' ? 'show' : '';
                         ?>
                        <div class="card" style="margin-bottom: 10px">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapse<?php echo e($cart->id); ?>">
                                    <img src="<?php echo e(url('public/web/images')); ?>/squares.svg" alt="" width="1.2%"> <?php echo e($cart->name); ?>

                                </a>
                            </div>
                            <div id="collapse<?php echo e($cart->id); ?>" class="collapse <?php echo e($show); ?>" data-parent="#accordion">
                                <div class="card-body content_cosllap_one">
                                    <?php if($cart->image != null): ?>
                                        
                                    <?php endif; ?>
                                    <p><?php echo $cart->content; ?>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </main>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>