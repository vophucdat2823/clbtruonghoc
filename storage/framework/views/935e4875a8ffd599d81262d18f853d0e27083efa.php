<?php $__env->startSection('content'); ?>

<main>
    <div id="news_even">
        <div class="news_even_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray"><?php echo e($menu['name']); ?></span>
                        </div>
                    </div>
                </div>
        </div>

        <div class="content_news_even">
            <div class="container">
                <div class="row">
                    <?php if(!empty($tabEvents)): ?>
                        <?php $__currentLoopData = $tabEvents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabEvent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                        <div class="col_content_h">
                            <div class="vnu_is_content">
                                <div class="top_vnu_is_content">
                                    <?php if($language_id == 'vi'): ?>
                                    <h5><?php echo e($tabEvent['name_vi']); ?></h5>
                                    <?php else: ?>
                                    <h5><?php echo e($tabEvent['name_en']); ?></h5>
                                    <?php endif; ?>
                                    <hr>
                                    <?php if($language_id == 'vi'): ?>
                                    <p><?php echo e($tabEvent['title_vi']); ?></p>
                                    <?php else: ?>
                                    <p><?php echo e($tabEvent['title_en']); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="body_vnu_is_content">
                                    <div class="img_body_content">
                                        <img src="<?php echo e($tabEvent['image']); ?>" alt="" width="100%">
                                    </div>
                                    <ul>
                                        <?php if(count($tabEvent['event_translations']) > 0): ?>
                                            <?php $__currentLoopData = $tabEvent['event_translations']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($language_id == $event['language_id']): ?>
                                        <li>
                                            <strong>EVENT <?php echo e(1+$index++); ?>:</strong>
                                            <a href="<?php echo e(route('web.event.detail', str_slug($event['name']."-".$event['id']))); ?>"><?php echo e($event['name']); ?></a>
                                            <hr>
                                            <p><?php echo e($event['simulation']); ?></p>
                                        </li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="bottom_more_vnu_is_content">
                                    <a href="#"></a>
                                </div>
                                <div class="more_content_h">
                                        <a href="#">MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>