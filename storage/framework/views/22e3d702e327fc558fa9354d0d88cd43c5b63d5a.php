<?php if(isset($page) && $page!=null): ?>
<select name="page_id" id="danhmuc_menu" class="form-control">
  <?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca_pa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($ca_pa->id); ?>"><?php echo e($ca_pa->name); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
<?php else: ?>
<select name="page_id" id="danhmuc_menu" class="form-control">
  <option value="">-- Chọn danh mục -- </option>
</select>
<?php endif; ?>
