<?php $__env->startSection('style.css'); ?>
    
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php
    $menus = \App\Models\Admin\Menu::orderBy('stt', 'asc')->get()->toArray();
    $languages = \App\Models\Admin\Language::orderBy('language_id','desc')->get()->toArray();
    ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if(session('success')): ?>
                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 100%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <?php if(session('error')): ?>
                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 100%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('error')); ?>

                </div>
            <?php endif; ?>
            <form class="form-horizontal" action="<?php echo e(route('admin.post.update',$post['id'])); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tên bài viết(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Tên bài viết" value="<?php echo e($post['name']); ?>">
                        </div>
                        <?php if($errors->has('name')): ?>
                            <span class="text-center text-danger" role="alert">
                                <?php echo e($errors->first('name')); ?>

                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Mô phỏng bài viết(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <textarea type="text" class="form-control" name="simulation" placeholder="Mô phỏng bài viết" value=""><?php echo e($post['simulation']); ?></textarea>
                        </div>
                        <?php if($errors->has('simulation')): ?>
                            <span class="text-center text-danger" role="alert">
                                <?php echo e($errors->first('simulation')); ?>

                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div id="image-diplay">
                    <div class="form-group">
                        <label class="control-label col-md-3">Chọn ảnh hiển thị bài viết(*):</label>
                        <div class="col-md-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="width: 100%; height: 250px;">
                                    <img src="<?php echo e($post['image']); ?>">

                                </div>
                                <div>
                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh </span>
                                        <input type="file" name="image">
                                        <input type="hidden" name="update_image" value="<?php echo e($post['image']); ?>">
                                    </span>
                                    <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput"> Xóa ảnh </a>
                                    
                                </div>
                                <span class="text-center text-danger" role="alert">
                                    <?php if($errors->has('image')): ?>
                                        <?php echo e($errors->first('image')); ?>

                                    <?php endif; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr style="border: 1px solid black">
                <label class="control-label col-sm" for="email">Nội dung bài viết(*):</label><br><br>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <textarea type="text" class="form-control editor1" name="content"><?php echo $post['content']; ?></textarea>
                            <?php if($errors->has('content')): ?>
                                <span class="text-center text-danger" role="alert">
                                    <?php echo e($errors->first('content')); ?>

                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>
                            Lưu</button>
                        <a href="<?php echo e(route('admin.post.list')); ?>" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                        </a>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    
    <script>
        CKEDITOR.replaceClass = 'editor1';
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            /*$('body').on('click', '.add-image', function () {
                var html ='';
                html =
                    '<div class="form-group">'
                    +'<label class="control-label col-md-3"></label>'
                    +'<div class="col-md-8">'
                    +'<div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">'
                    +'<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: 250px;">'
                    +'</div>'
                    +'<div>'
                    +'<span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">'
                    +'<span class="fileinput-new"> Chọn ảnh </span>'
                    +'<span class="fileinput-exists"> Đổi ảnh </span>'
                    +'<input type="file" name="images[]" multiple>'
                    +'</span>'
                    +'<button type="button" class="btn btn-danger delete-image">Xóa bỏ</button>'
                    +'<button type="button" class="btn btn-success add-image">Thêm ảnh</button>'
                    +'</div>'
                    +'</div>'
                    +'</div>'
                    +'</div>';
                $("#image-diplay").append(html);
            });
            $('body').on('click', '.delete-image', function () {
                $(this).parent().parent().parent().remove();
            });*/
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>