<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    //dd($language);
    ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm phần hiển thị</h5>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.chooses.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Tên hiển thị(*):</label> 
                                                <input type="text" class="form-control" name="name_vi" placeholder="Tên hiển thị tiếng việt" value="<?php echo e(old('name_vi')); ?>" style="margin-bottom: 5px">
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <input type="text" id="name" class="form-control" name="name_en" placeholder="Tên hiển thị tiếng Anh" value="<?php echo e(old('name_en')); ?>">
                                                <?php if($errors->has('name_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group m-b" style="margin-top: 15px">

                                                    <span class="input-group-addon">Youtube</span> 

                                                    <input name="iframe" type="text" placeholder="Link video" value="<?php echo e(old('iframe')); ?>" class="form-control">
                                                </div> 
                                            </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Tất cả danh mục</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-3">Tên giới thiệu(vietnam)</th>
                                                        <th class="col-md-3">Tên giới thiệu(english)</th>
                                                        <th class="col-md-2">Link youtube</th>
                                                        <th class="col-md-3">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($chooses)): ?>
                                                        <?php $__currentLoopData = $chooses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $choos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <tr>
                                                        
                                                        <td><?php echo e($key+1); ?></td>
                                                        <td><a href="#"> <span class="tag-post"><?php echo e($choos->name_vi); ?></span></a></td>
                                                        <td><a href="#"> <span class="tag-post"><?php echo e($choos->name_en); ?></span></a></td>
                                                        <td><a href="#"> <span class="tag-post"><?php echo e($choos->iframe); ?></span></a></td>
                                                        <td>
                                                            
                                                            <a data-toggle="modal" href='#modal-edit<?php echo e($choos->id); ?>' class="btn btn-info">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                    data-target="#modal-delete-<?php echo e($choos->id); ?>">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                            <a href="<?php echo e(route('admin.chooses.create_item', ['id'=>$choos->id])); ?>" class="btn btn-primary">
                                                                <i class="fa fa-edit"></i> Thêm 
                                                            </a>
                                                            <div class="modal modal-danger fade" id="modal-delete-<?php echo e($choos->id); ?>">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span></button>
                                                                                <?php 
                                                                                    $delete = \App\Models\Admin\ProgramTranslation::where('choose_id',$choos->id)->get();
                                                                                 ?>
                                                                                <?php if($delete->count() == 0): ?>
                                                                                    <h4 class="modal-title">Bạn có chắc chắn muốn xóa phần này ?</h4>
                                                                                <?php else: ?>
                                                                                    <h4 class="modal-title">
                                                                                    Phần giới thiệu này đang có "<?php echo e($delete->count()); ?>" bài áp dụng!
                                                                                   (không thể xóa)
                                                                                    </h4>
                                                                                <?php endif; ?>
                                                                            
                                                                        </div>
                                                                        <div class="modal-body" style="background: white!important; text-align: center" ">
                                                                            <form method="get" action="<?php echo e(route('admin.chooses.destroy', $choos->id)); ?>">
                                                                                <?php if($delete->count() == 0): ?>
                                                                                    <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                    <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                        <i class="fa fa-save"></i>
                                                                                    </button>
                                                                                <?php else: ?>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                <?php endif; ?>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <?php if(!empty($chooses)): ?>
<?php $__currentLoopData = $chooses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $choos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="modal fade" id="modal-edit<?php echo e($choos->id); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit "<?php echo e($choos->name_vi); ?>" !</h4>
            </div>
                <form action="<?php echo e(route('admin.chooses.update')); ?>" class="form-update" method="POST" role="form">
            <div class="modal-body">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="id1" value="<?php echo e($choos->id); ?>">
                    <legend>Edit dữ liệu</legend>
                
                    <div class="form-group">
                        <label for="">Tên hiển thị tiếng việt</label>
                        <input type="text" name="ten_tiengviet" class="form-control" value="<?php echo e($choos->name_vi); ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Tên hiển thị tiếng anh</label>
                        <input type="text" name="ten_tienganh" class="form-control" value="<?php echo e($choos->name_en); ?>">
                    </div>
                    <div class="form-group">
                        <div class="input-group m-b" style="margin-top: 15px">
                            <span class="input-group-addon">Youtube</span> 
                            <input name="link_youtube" type="text" placeholder="Link video" value="<?php echo e($choos->iframe); ?>" class="form-control">
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button></form>
            </div>
        </div>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 

<script>
    $(document).ready(function () {
    $('.form-update').validate({ // initialize the plugin
        rules: {
            name_vi1: {
                required: true,
                maxlength: 255
            },
            name_en1: {
                required: true,
                maxlength: 255
            },
            iframe1: {
                required: true,
                maxlength: 255
            },
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>