<!DOCTYPE html>
<html>
<head>
    <title>INTERNATIONAL SCHOOL</title>
    
    <base href="<?php echo e(url('/web')); ?>/">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="https://lh3.googleusercontent.com/RLFb24VWKwBajRwiU3PAcAgWc0d33n5Xa7ovCGCG7ZDQpCt58R7253yd-iK5FFmQJL_lFA=s85">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald|Josefin+Sans" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/style.css')); ?>">
	<?php echo $__env->yieldContent('stype.css'); ?>
</head>
<body>

	<?php echo $__env->make('web.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<!-- main -->
	<?php echo $__env->yieldContent('content'); ?>
	<!-- end main -->

	<!-- footer -->
	<?php echo $__env->make('web.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- end footer -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/index.js')); ?>"></script>
</body>
</html>