<!DOCTYPE html>
<html>
<head>
    <title>INTERNATIONAL SCHOOL</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald|Josefin+Sans" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('/public/web/css/style.css')); ?>">
	<?php echo $__env->yieldContent('stype.css'); ?>

</head>
<body>
	<?php 
	
		$menu_top = Menu::getByName('menu-top');
		
	 	$categoryies = \App\Models\Admin\Category::orderBy('id', 'asc')->get();
	 	$caidat = \App\Models\Options::all();
		$language_id = 'vi';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
            //dd($language_id);
        }
	 ?>

	<?php echo $__env->make('web.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<!-- main -->
	<?php echo $__env->yieldContent('content'); ?>
	<!-- end main -->

	<!-- footer -->
	<?php echo $__env->make('web.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- end footer -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('/public/web/js/index.js')); ?>"></script>
	<script>
		jQuery.fn.extend({
		    setMenu:function () {
		        return this.each(function() {
		            var containermenu = $(this);

		            var itemmenu = containermenu.find('.xtlab-ctmenu-item');
		            itemmenu.click(function () {
		                var submenuitem = containermenu.find('.xtlab-ctmenu-sub');
		                submenuitem.slideToggle(500);

		            });

		            $(document).click(function (e) {
		                if (!containermenu.is(e.target) &&
		                    containermenu.has(e.target).length === 0) {
		                     var isopened =
		                        containermenu.find('.xtlab-ctmenu-sub').css("display");

		                     if (isopened == 'block') {
		                         containermenu.find('.xtlab-ctmenu-sub').slideToggle(500);
		                     }
		                }
		            });



		        });
		    },

		});


		$('.xt-ct-menu').setMenu();
	</script>
	<?php echo $__env->yieldContent('javascript'); ?>
</body>
</html>