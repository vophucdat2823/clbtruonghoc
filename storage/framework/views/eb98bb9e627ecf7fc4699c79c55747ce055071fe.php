<?php $__env->startSection('style.css'); ?>
    
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
        $languages = \App\Models\Admin\Language::orderBy('language_id','desc')->get()->toArray();
    ?>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                    <form class="form-horizontal" action="<?php echo e(route('admin.pgram_post.store.lang')); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="language_id" value="<?php echo e($language); ?>">
                        <input type="hidden" name="program_id" value="<?php echo e($program_id); ?>">
                        <input type="hidden" name="pgramPost_id" value="<?php echo e($pgramPost_id); ?>">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                    <h5>Thêm bài viết tiếng anh</h5>
                                    <div class="ibox-tools">
                                      <button type="submit" class="btn btn-primary">
                                          <i class="fa fa-save"></i>
                                          Lưu</button>
                                      <a href="<?php echo e(route('admin.post.list')); ?>" type="submit" class="btn btn-danger">
                                          <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                                      </a>
                                    </div>
                                </div>
                                    
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">

                                                    <label class="control-label col-sm-3" for="email">Phần giới thiệu thêm(*):</label>

                                                    <div class="col-sm-8">
                                                      <button type="button"  class="btn btn-block btn-success addNewApartment">
                                                        ADD +
                                                      </button>
                                                    </div>
                                                  </div>

                                                <div class="list-apartments">
                                                  <?php if(old('mang_choose')[0] != null): ?>
                                                    <?php for($i = 0; $i < count(old('mang_choose')); $i++): ?>

                                                      <div class="form-group">
                                                          <div class="col-xs-offset-1 col-xs-2">
                                                          </div>
                                                          <div class="col-sm-8">
                                                            <div class="row">
                                                             <div class="form-group col-xs-10"style="margin-left: 0">
                                                              <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="mang_choose[]" value="<?php echo e(old('mang_choose')[$i]); ?>">
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-2 pull-right" style="text-align: right">
                                                              <button type="button"  class="btn btn-block btn-danger removeApartment">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                              </button>
                                                            </div>
                                                          </div>
                                                          <div>
                                                          </div>
                                                        </div>
                                                      </div>

                                                    <?php endfor; ?>
                                                  <?php endif; ?>
                                                  
                                              </div>
                                              <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="email">Link youtube(*):</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">Youtube</span>
                                                            <input type="text" class="form-control" name="link_youtube" placeholder="link_youtube" value="<?php echo e(old('link_youtube')); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="email">Tên bài viết(*):</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                            <input type="text" class="form-control" name="name" placeholder="Tên bài viết" value="<?php echo e(old('name')); ?>">
                                                        </div>
                                                        <?php if($errors->has('name')): ?>
                                                            <span class="text-center text-danger" role="alert">
                                                                <?php echo e($errors->first('name')); ?>

                                                            </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Chọn ảnh hiển thị bài viết(*):</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content" style="padding: 0">
                                        <div id="image-diplay">
                                           <div class="form-group">
                                               <div class="col-md-12">
                                                <div class="input-group m-b" style="margin-top: 15px"><span class="input-group-addon">URL</span> <input name="url_image" type="text" placeholder="Đường dẫn hình ảnh (mặc định #)" value="<?php echo e(old('url_image')); ?>" class="form-control"></div>
                                                   <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                       <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                            <img src="<?php echo e(url('public/web/images')); ?>/logo.png" alt="">
                                                       </div>
                                                       <div style="text-align: center">
                                                            <span class="btn red btn-outline btn-file" style="background: #1f81ff !important; ">
                                                                <span class="fileinput-new" > Chọn ảnh </span>
                                                                <span class="fileinput-exists"> Đổi ảnh </span>
                                                                <input type="file" name="image">
                                                            </span>
                                                           <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                              data-dismiss="fileinput"> Xóa ảnh </a>
                                                           
                                                       </div>
                                                       <span class="text-center text-danger" role="alert">
                                                            <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                        </span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nội dung bài viết</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-lg-12">

                                              <label class="control-label col-sm" style="padding-top: 0;margin-right: 10px" for="email">Nền xám(*):</label>
                                              <label>
                                                <input type="radio" name="color_contenrt" id="input" value="0" checked="checked">
                                                Không
                                              </label>
                                              <label style="padding-left: 10px">
                                                <input type="radio" name="color_contenrt" id="input" value="1">
                                                Có
                                              </label><br><br>
                                            <label class="control-label col-sm" for="email">Nội dung chính bài viết(*):</label><br><br>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="input-group" style="width: 100%">
                                                        <textarea type="text" class="form-control" id="description123" name="content"><?php echo old('content'); ?></textarea>
                                                        <?php if($errors->has('content')): ?>
                                                            <span class="text-center text-danger" role="alert">
                                                                <?php echo e($errors->first('content')); ?>

                                                            </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-lg-12">
                                                <label class="control-label col-sm" for="email">Các tab bài viết(*):</label><br><br>
                                                <label class="" for="email">
                                                    <button type="button"  class="btn btn-success addNewCkeditor">
                                                          Add tab +
                                                      </button>
                                                </label>
                                                <div class="clearfix"></div>
                                                <div class="listTabCkeditor">
                                                  <?php if(old('name_tab')[0] != null || old('content_tab')[0] != null): ?>
                                                  
                                                  <?php for($i = 0; $i < count(old('slider_tab')) ; $i++): ?>
                                                  
                                                  <input type="hidden" class="tab_total" value="<?php echo e(count(old('name_tab'))); ?>">
                                                    <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                                      <div class="input-group">
                                                  

                                                        <div class="input-group-addon" style="padding:0;border:0">
                                                          <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Tab:</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group" style="width: 100%">
                                                                  <?php echo e($i+1); ?>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Slider giáo viên:</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group" style="width: 100%">
                                                                  <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                    <select name="slider_tab[]" id="input" class="form-control" required="required">
                                                                      <option value="0">-- Không hiển thị</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Tên bài viết(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                    <input type="text" class="form-control" name="name_tab[]" placeholder="Tên bài viết" value="<?php echo e(old('name_tab')[$i]); ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Nội dung bài viết(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                    <textarea type="text" name="content_tab[]"  class="form-control" id="ckeditor_<?php echo e($i); ?>"><?php echo e(old('content_tab')[$i]); ?></textarea>
                                                                     <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
                                                                      <script>

                                                                         CKEDITOR.replace( 'ckeditor_<?php echo e($i); ?>', {
                                                                          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
                                                                          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
                                                                          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
                                                                          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
                                                                          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
                                                                          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>',
                                                                        });
                                                                     </script>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div>
                                                   </div><hr>

                                                  <?php endfor; ?>
                                                  <?php else: ?>
                                                  <?php endif; ?>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </form>
            </div>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
      <script type="text/javascript">

     $("body").on('click','.removeApartment',function(){
        $(this).parent().parent().parent().parent().remove();
     });

     $(".addNewApartment").click(function(){
        // alert(1);
        // var data_select=$(this).parent().parent().siblings('.list-apartments').find('.apartments').last().clone();
        var str='<div class="form-group">';
            str+='<div class="col-xs-offset-1 col-xs-2">';
            str+='</div>';
              str+='<div class="col-sm-8">';
                str+='<div class="row">';
                 str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                  str+='<div class="input-group">';
                    str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                    str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="mang_choose[]" placeholder="Vì sao bạn nên chọn chương trình!">';
                  str+='</div>';
                str+='</div>';
                str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                  str+='<button type="button"  class="btn btn-block btn-danger removeApartment">';
                    str+='<i class="fa fa-fw fa-trash-o"></i>';
                  str+='</button>';
                str+='</div>';
              str+='</div>';
            str+='<div>';
        str+='</div>';
        str+='</div>';
        $(".list-apartments").append(str);
              
     });



</script>
 <script>
  CKEDITOR.replace( 'description123', {
      filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
      filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
      filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
      filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
      filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
      filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
    });
</script>
</script>

  <script type="text/javascript">
     $(document).ready(function () {



            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 0; //initlal text box count
         $(".addNewCkeditor").click(function(){
            // alert(1);/
            // var dataa = $(".total_123455").val();

            // var total = $(this).find('.total_123455').val();
            // alert(dataa);
            if(x < max_fields){ //max input box allowed
                    x++;
                    var editorId = 'ckeditor_' + x;


                    var str='<div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                      str+='<div class="input-group">';
                        str+='<div class="input-group-addon" style="padding:0;border:0">';
                          str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                        str+='</div>';
                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Tab:</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group" style="width: 100%">';
                                    str+=''+x+'';
                                  str+='</div>';
                              str+='</div>';
                          str+='</div>';
                        str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Slider giáo viên:</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="input-group" style="width: 100%">';
                                  str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                    str+='<select name="slider_tab[]" id="input" class="form-control" required="required">';
                                      str+='<option value="0">-- Không hiển thị</option>';
                                    str+='</select>';
                                str+='</div>';
                            str+='</div>';
                       str+=' </div>';
                        str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Tên bài viết(*):</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="input-group">';
                                    str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                    str+='<input type="text" class="form-control" name="name_tab[]" placeholder="Tên bài viết" value="">';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                        str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Nội dung bài viết(*):</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="input-group">';
                                    str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                    str+='<textarea type="text" name="content_tab[]" class="form-control" id="'+editorId+'"></textarea>';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                      str+='</div><hr>';
                   str+='</div>';

                    $(wrapper).prepend(str);

                    CKEDITOR.replace( editorId, {
                      filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
                      filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
                      filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
                      filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
                      filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
                      filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
                    });
                }
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });



</script>
<?php $__env->stopSection(); ?>

    
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>