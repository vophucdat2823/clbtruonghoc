<?php $__env->startSection('stype.css'); ?>
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
        .txt_cate_pp_img a{
            text-transform: uppercase;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <main>
        <div id="news_even_details">
            <div class="news_even_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray"><?php echo e($menu['name']); ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content_news_even_details">
                <div class="container">
                    <div class="row">
                        <!-- phần left content -->
                        <div class="col-md-4 col-sm-12 col-12">
                            <h2>CATEGORIES</h2>
                            <ul class="nav nav-pills" id="menu">
                                <?php if(count($tabEvents) > 0): ?>
                                    <?php $__currentLoopData = $tabEvents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tabEvent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="nav-item" style="text-transform: uppercase">
                                            <?php if($language_id == 'vi'): ?>
                                                <a class="nav_menu active" data-toggle="collapse"
                                                   href="#sub_menu_<?php echo e($tabEvent['id']); ?>"><img
                                                            src="/public/web/images/icon_square.png" alt=""
                                                            width="4%"><span><?php echo e($tabEvent['name_vi']); ?></span></a>
                                            <?php else: ?>
                                                <a class="nav_menu active" data-toggle="collapse"
                                                   href="#sub_menu_<?php echo e($tabEvent['id']); ?>">
                                                    <img src="/public/web/images/icon_square.png" alt=""
                                                                           width="4%"><span><?php echo e($tabEvent['name_en']); ?></span>
                                                </a>
                                            <?php endif; ?>
                                            <div id="sub_menu_<?php echo e($tabEvent['id']); ?>" class="collapse break">
                                                <ul class="nav">
                                                    <?php if(count($tabEvent['event_translations']) > 0): ?>
                                                        <?php $__currentLoopData = $tabEvent['event_translations']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $_event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($language_id == $_event['language_id']): ?>
                                                                <li class="nav-item">
                                                                    
                                                                    <a href="<?php echo e(route('web.event.detail', str_slug($_event['name']."-".$_event['id']))); ?>"><i class="fas fa-caret-right"></i><?php echo e($_event['name']); ?></a>
                                                                </li>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </ul>


                            <div class="insta_pp">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="/public/web/images/icon_square_xam.svg" alt="" width="40%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h5 class="color_yellow">Instagram</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>

                            <div class="tag_pp">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/icon_square_xam.svg" alt="" width="40%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h5 class="color_yellow">Tags</h5>
                                    </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- phần right content -->
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="tab-content">
                                <?php if(isset($event)): ?>

                                <div id="sp_content_1" class="container tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="main_content_news_even_details">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-12">
                                                        <div class="tt_top_main_even_details">
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <img src="/public/web/images/squares.svg" alt=""
                                                                         width="18%">
                                                                </div>

                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <h3 class="color_blue"><?php echo e($event['name']); ?></h3>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="body_main_even_details fix-size-image">
                                                            <?php echo $event['content']; ?>

                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <!-- more info -->
                                                

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>