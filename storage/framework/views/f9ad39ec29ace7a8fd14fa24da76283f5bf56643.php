<?php $__env->startSection('content'); ?>
            <style type="text/css">
                .text_img_style{
                    top: 0% !important;
                    left: 0 !important;
                    font-weight: bold !important;
                    height: 20%
                }
                .text_img_style p{
                    font-size: 25px !important;
                    font-weight: initial !important;
                    margin: 2% !important;
                    margin-left: 21% !important;
                    margin-bottom: 3px !important;
                }
                .text_img_style a{
                    text-decoration: none;
                    font-size: 18px !important;
                    font-weight: initial !important;
                    margin: 2% !important;
                    margin-left: 21% !important;
                    margin-top: 3px !important;
                    color: #fff;
                }
                .text_img_style a:hover{
                    color: #E8B909;
                }
                .transbox {
                    width: 100%;
                    background-image: linear-gradient(to bottom, rgb(0, 0, 0) 10%,rgba(0,0,0,0) 100%);
                    font-size: 1.25rem;
                }
                .transbox p {
                    margin: 5%;
                    font-weight: bold;
                    color: #000000;
                }
                .img_banner_h {
                    width: 100%;
                   background-image: linear-gradient(to bottom,  rgba(0,0,0,20) 50%,rgba(0,0,0,0) 100%);
                    font-size: 1.25rem;
                }
                .top_text{
                    font-size: 3.5vw !important;
                    font-weight: 100 !important;
                }
            </style>
<main id="main_mobie">
    <div id="about_us" style="background: #fff;">
        <div class="activities_home" style="background-color: #E8B909;color: #102B4E">
            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                <?php if($language_id == 'vi'): ?>
                    <p><?php echo e($category->name_vi); ?></p>
                <?php else: ?>
                    <p><?php echo e($category->name_en); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
        <div id=home>
            <?php $__currentLoopData = $category->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bai_viet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($language_id == 'vi'): ?>
                <?php if($bai_viet->language_id == 'vi'): ?>
                    <div class="banner_h">
                        <div class="img_banner_h">
                            <img src="<?php echo e(url('public/web/images')); ?>/transparent/alumni_transpanrent.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($bai_viet->image); ?>);background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;" alt="" width="100%">
                            <div class="text_img text_img_style transbox">
                                <p class="top_text" style="font-size: 3.5vw !important;font-weight: 600 !important;"><?php echo e($bai_viet->name); ?></p>
                                <a href="<?php echo e($bai_viet->code); ?>.html" style="font-size: 3vw !important" class="" >Chi tiết <span style="color: #E8B909;padding-left: 5px"><i class="fas fa-angle-double-right"></i></span></a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php elseif($language_id == 'en'): ?>
                <?php if($bai_viet->language_id == 'en'): ?>
                    <div class="banner_h">
                        <div class="img_banner_h">
                            <img src="<?php echo e(url('public/web/images')); ?>/transparent/alumni_transpanrent.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($bai_viet->image); ?>);background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;" alt="" width="100%">
                            <div class="text_img text_img_style">
                                <p class="top_text" style="font-size: 3.5vw !important;font-weight: 600 !important;"><?php echo e($bai_viet->name); ?></p>
                                <a href="<?php echo e($bai_viet->code); ?>.html" style="font-size: 3vw !important" class="" >Chi tiết <span style="color: #E8B909;padding-left: 5px"><i class="fas fa-angle-double-right"></i></span></a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </main>
<main id="main_pc" style="margin-bottom: 250px">
        <div id="partnership">
            <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">
                                    <?php if($language_id == 'en'): ?>
                                    HOME
                                    <?php elseif($language_id == 'vi'): ?>
                                    Trang chủ
                                    <?php endif; ?> > 
                                </span>
                            </a>
                             <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->last): ?>
                                    <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                                <?php else: ?>
                                    <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                                        <span class="color_blue">
                                            <?php echo e($key); ?> >
                                        </span>
                                    </a>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id=home>
            <style type="text/css">
                .text_img_style{
                    top: 0% !important;
                    left: 0 !important;
                    font-weight: bold !important;
                    height: 20%
                }
                .text_img_style p{
                    font-size: 25px !important;
                    font-weight: initial !important;
                    margin: 2% !important;
                    margin-left: 21% !important;
                    margin-bottom: 3px !important;
                }
                .text_img_style a{
                    text-decoration: none;
                    font-size: 18px !important;
                    font-weight: initial !important;
                    margin: 2% !important;
                    margin-left: 21% !important;
                    margin-top: 3px !important;
                    color: #fff;
                }
                .text_img_style a:hover{
                    color: #E8B909;
                }
                .transbox {
                    width: 100%;
                    background-image: linear-gradient(to bottom, rgb(0, 0, 0) 10%,rgba(0,0,0,0) 100%);
                    font-size: 1.25rem;
                }
                .transbox p {
                    margin: 5%;
                    font-weight: bold;
                    color: #000000;
                }
                .img_banner_h {
                    width: 100%;
                   background-image: linear-gradient(to bottom,  rgba(0,0,0,20) 50%,rgba(0,0,0,0) 100%);
                    font-size: 1.25rem;
                }
            </style>
            <?php $__currentLoopData = $category->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bai_viet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($language_id == 'vi'): ?>
                <?php if($bai_viet->language_id == 'vi'): ?>
                    <div class="banner_h">
                        <div class="img_banner_h">
                            <img src="<?php echo e(url('public/web/images')); ?>/transparent/alumni_transpanrent.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($bai_viet->image); ?>);background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;" alt="" width="100%">
                            <div class="text_img text_img_style transbox">
                                <p class="top_text"><?php echo e($bai_viet->name); ?></p>
                                <a href="#" class="">Chi tiết <span style="color: #E8B909;padding-left: 5px"><i class="fas fa-angle-double-right"></i></span></a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php elseif($language_id == 'en'): ?>
                <?php if($bai_viet->language_id == 'en'): ?>
                    <div class="banner_h">
                        <div class="img_banner_h">
                            <img src="<?php echo e(url('public/web/images')); ?>/transparent/alumni_transpanrent.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($bai_viet->image); ?>);background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;" alt="" width="100%">
                            <div class="text_img text_img_style">
                                <p class="top_text"><?php echo e($bai_viet->name); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>