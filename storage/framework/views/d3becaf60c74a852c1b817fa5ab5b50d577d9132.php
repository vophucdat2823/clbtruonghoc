<?php $__env->startSection('style'); ?>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="<?php echo e(url('public/vendor/harimayco-menu/style.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo Menu::render(); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
    var menus = {
        "oneThemeLocationNoMenus" : "",
        "moveUp" : "Move up",
        "moveDown" : "Mover down",
        "moveToTop" : "Move top",
        "moveUnder" : "Move under of %s",
        "moveOutFrom" : "Out from under  %s",
        "under" : "Under %s",
        "outFrom" : "Out from %s",
        "menuFocus" : "%1$s. Element menu %2$d of %3$d.",
        "subMenuFocus" : "%1$s. Menu of subelement %2$d of %3$s."
    };
    var arraydata = [];     
    var addcustommenur= '<?php echo e(route("haddcustommenu")); ?>';
    var updateitemr= '<?php echo e(route("hupdateitem")); ?>';
    var generatemenucontrolr= '<?php echo e(route("hgeneratemenucontrol")); ?>';
    var deleteitemmenur= '<?php echo e(route("hdeleteitemmenu")); ?>';
    var deletemenugr= '<?php echo e(route("hdeletemenug")); ?>';
    var createnewmenur= '<?php echo e(route("hcreatenewmenu")); ?>';
    var csrftoken="<?php echo e(csrf_token()); ?>";
    var menuwr = "<?php echo e(url()->current()); ?>";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrftoken
        }
    });
</script>
    
    <script type="text/javascript" src="<?php echo e(url('public/vendor/harimayco-menu/scripts.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('public/vendor/harimayco-menu/scripts2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('public/vendor/harimayco-menu/menu.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>