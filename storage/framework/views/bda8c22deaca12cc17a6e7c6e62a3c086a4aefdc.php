<?php $__env->startSection('title','Sản Phẩm | Chỉnh sửa'); ?>

<?php $__env->startSection('style'); ?>

    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Chỉnh sửa sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    <?php 
        $productStore=$productStore::find($_GET['id']);
        $productImgStore=$ProductImgStore->where('product_id','=',$productStore->id)->orderBy('id','desc')->get()->toarray();
     ?>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('updateProduct',['id'=>$productStore->id])); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin sản phẩm</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Giảm giá</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"> Hình ảnh</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="<?php echo e(route('listProduct')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Name(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="<?php echo e($productStore->name); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug:</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" id='id' name="id" value="<?php echo e($productStore->id); ?>" />
                                            <div>
                                                <?php if($productStore->slug): ?>
                                                    <?php 
                                                        $slug_1 = explode('.', $productStore->slug);
                                                        $slug = explode('cua-hang-truc-tuyen-', $productStore->slug);
                                                       
                                                     ?>
                                                <?php endif; ?>

                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo e(url('chi-tiet')); ?>/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" required value="<?php echo e(array_pop($slug)); ?>">
                                                    <span class="input-group-addon">.<?php echo e($slug_1[1]); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Thương hiệu(*):</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="trademark" id="trademark" class="form-control" placeholder="Thương hiệu..." required value="<?php echo e($productStore->trademark); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">SKU:</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="sku" id="sku" class="form-control" placeholder="SKU..."   value="<?php echo e($productStore->sku); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Price:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price" class="form-control" placeholder="$160.00" required value="<?php echo e($productStore->price); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục:</label>
                                        <div class="col-sm-10">
                                            <?php if($productStore->category_id != "null"): ?>
                                                <?php 
                                                    $category_id = json_decode($productStore->category_id);
                                                 ?>

                                                <select name="category_id[]" data-placeholder="Lựa chọn danh mục..."  class="chosen-select" multiple tabindex="4">
                                                    <?php echo e(showProgram($cateStore,$category_id,0, $char ='')); ?>

                                                </select>
                                            <?php else: ?>
                                                <select name="category_id[]" data-placeholder="Lựa chọn danh mục..."  class="chosen-select" multiple tabindex="4">
                                                    <?php echo e(showCateStore($cateStore,0,'',$productStore->category_id)); ?>

                                                </select>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <?php if($productStore->image): ?>
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e($productStore->image); ?></span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="<?php echo e($productStore->image); ?>" name=""><input type="file" name="image" value="<?php echo e($productStore->image); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            <!-- <?php elseif(old('image')): ?>
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e(old('image')); ?></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="<?php echo e(old('image')); ?>" name=""><input type="file" name="image" value="<?php echo e(old('image')); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div> -->
                                            <?php else: ?>
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            <?php endif; ?>

                                        </div>
                                        <!-- <div class="col-sm-10 col-xs-10">
                                            <?php if($productStore->image): ?>
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e($productStore->image); ?></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="<?php echo e($productStore->image); ?>" name="check_image">
                                                        <input type="file" name="image" id="image" value="<?php echo e($productStore->image); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            <?php else: ?>
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image" id="image">
                                                    </span>
                                                </div>
                                            <?php endif; ?>

                                        </div> -->
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả ngắn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="short_description" id="short_description" class="form-control" placeholder="Mô tả ngắn..." required value="<?php echo e($productStore->short_description); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description"><?php echo $productStore->description; ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Giá sau khi giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price_sale" class="form-control" placeholder="$160.00"  value="<?php echo e($productStore->price_sale); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Phần trăm được giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <input type="number" name="sale" class="form-control" placeholder="50"  value="<?php echo e($productStore->sale); ?>">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                            <!-- <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $productImgStore; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td>
                                                    <img width='100' height='100' src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($value['images']); ?>">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="<?php echo e(url('public/upload/images')); ?>/<?php echo e($value['images']); ?>">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div> -->


                                <?php for($i = 0; $i < 4; $i++): ?>
                                    <?php if(isset($productImgStore[$i])): ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">IMG</label>
                                            <div class="col-sm-10 col-xs-10">
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"><?php echo e($productImgStore[$i]['images']); ?></span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image<?php echo e($productImgStore[$i]['id']); ?>" value="<?php echo e($productImgStore[$i]['images']); ?>" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">IMG</label>
                                            <div class="col-sm-10 col-xs-10">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="imagei<?php echo e($i+1); ?>">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                  
                                <?php endfor; ?>
            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <?php 
    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {

        foreach ($pgramall as $key => $item) {
            if ($item->parents == $parent)
            {
                echo '<option value="'.$item->id.'"';
                if ($program_test) {
                    foreach ($program_test as $value) {
                        if($item->id == $value){
                            echo 'selected="selected"';
                        }
                    }
                }
                if($parent==0){
                    echo 'style="color:red"';
                }
                echo '>';
                if($item->parents == $program_test){
                    if ($item->parents == 0) {
                        echo $char . $item->name;
                    }
                    if ($item->parents != 0) {
                        echo $char . $item->name.' (danh mục con)';
                    }
                }
                if($item->parents != $program_test){
                    echo $char . $item->name;
                }
                echo '</option>';
                if ($item->parents != $program_test){
                    showProgram($pgramall,$program_test, $item->id, $char.'---| ');
                }

            }
            
        }
    }


?>


<?php

function showCateStore($cateStore, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateStore as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateStore[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateStore($cateStore, $item->id, $char . '---| ', $select);
        }
    }
}

?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>
    


    // <script type="text/javascript">
        
        
    //     $("#form-create-product").validate({
    //         rules: {
    //             name: "required",
    //             slug: "required",
    //             price: {
    //                 "required": true,
    //                 "number": true
    //             },
    //             price_sale: {
    //                 "number": true
    //             },
    //             sale: {
    //                 "number": true
    //             },
    //             // image: "required",
    //             category_id: "required",
    //             trademark: "required",
    //             short_description: "required",

    //         },
    //         messages: {
    //             "name": "Tên sản phẩm không được để trống",
    //             "slug": "Đường đẫn tĩnh không được để trống",
    //             //  "image": "Vui vòng thêm ảnh",
    //             "trademark": "Thương hiệu không được để trống",
    //             "short_description": "Mô tả ngắn không được để trống",
    //             "price": {
    //                 "required": "Vui lòng nhập giá sản phẩm",
    //                 "number": 'Giá sản phẩm là số'
    //             },
    //             "price_sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "category_id": "Nghề nghiệp không được để trống",

    //         },
    //         errorPlacement: function (error, element) {
    //           error.appendTo(element.parent().parent().parent());
    //         },
    //         submitHandler: function(form) {
    //             $(form).submit();
    //         }
    //     });
    // </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>