<?php $__env->startSection('title','Dịch Vụ | Danh sách'); ?>
<style>
    .cate_service_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách dịch vụ</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Danh sách dịch vụ</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="product_name">Tên khách hàng</label>
                            <input type="text" value="<?php echo e($send->name); ?>" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="price">Email</label>
                            <input type="text" value="<?php echo e($send->email); ?>" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="quantity">Số điện thoại</label>
                            <input type="text" value="<?php echo e($send->phone); ?>" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group load_status_<?php echo e($send->id); ?>">
                            <label class="control-label" for="status">Trạng thái</label>
                            <div class="clearfix"></div>
                            <?php echo e(csrf_field()); ?>

                            <?php if($send->status == 0): ?>
                             <button type="button" class="btn btn-danger colum_status"  data-id="<?php echo e($send->id); ?>" data-stt="1">Đang chờ</button>
                            <?php else: ?>
                            <button type="button" class="btn btn-primary colum_status" data-id="<?php echo e($send->id); ?>" data-stt="0">Đã xử lý</button>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Thời gian</label>
                            <input type="text" id="price" name="price" value="<?php echo e($send->time); ?>" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Ngày</label>
                            <input type="text" id="price" name="price" value="<?php echo e($send->date); ?>" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Số lượng</label>
                            <input type="text" id="price" name="price" value="<?php echo e($send->quantity); ?>" disabled class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                        <h3>
                            <?php echo e($send->getService->name); ?>

                        </h3>
                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">Câu hỏi</th>
                                        <th data-hide="phone">Câu trả lời</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $question; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ques): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                               <?php echo e($ques->getQuestion['name']); ?>

                                            </td>
                                            <td>
                                               <?php echo e($ques['rep_question']); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content m-b-sm border-bottom">
                <legend>Đối tác cung cấp "<?php echo e($send->getService->name); ?>"</legend>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Chất lượng</label>
                            <select name="status" id="status" class="form-control">
                                <?php $__currentLoopData = $send->getService->getTypeService('quality'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $quality): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($quality->id); ?>"><?php echo e($quality->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Số lượng</label>
                            <select name="status" id="status" class="form-control">
                                <?php $__currentLoopData = $send->getService->getTypeService('qty'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qty): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($qty->id); ?>"><?php echo e($qty->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Địa điểm</label>
                            <select name="status" id="status" class="form-control">
                                <?php $__currentLoopData = $send->getService->getTypeService('address'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($address->id); ?>"><?php echo e($address->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Giá thanh</label>
                            <select name="status" id="status" class="form-control">
                                <?php $__currentLoopData = $send->getService->getTypeService('price'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $price): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($price->id); ?>"><?php echo e($price->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                        <tr>
                                            <th data-toggle="true">Tên nha cung cấp</th>
                                            <th data-hide="phone">Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($send->getService->getServicePartner): ?>
                                            <?php $__currentLoopData = $send->getService->getServicePartner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td>
                                                       <?php echo e($partner->getUser->name); ?>

                                                    </td>
                                                    <td>
                                                       <?php echo e($partner->getUser->email); ?>

                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"<?php echo e(route('send.update_status')); ?>",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)

        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>