
<?php $__env->startSection('style.css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
$language = \App\Models\Admin\Language::getLanguage();
//dd($language);
?>
<div class="wrapper wrapper-content">
    <div class="row">
        
    </div>
    <?php if($name == 'home'): ?>
    <div class="row">
        <div class="col-lg-12">
           <?php if(session('success')): ?>
           <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo e(session('success')); ?>

            </div>
            <?php endif; ?>
            <?php if(session('error')): ?>
            <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo e(session('error')); ?>

            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit banner trang "<?php echo e($name); ?>"</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($cs_dt->class == 'banner'): ?>
                                    <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                        <div class="ibox-content" style="">
                                            <div id="image-diplay">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Banner hiện tại(*):</label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->banner); ?>" alt=""> 
                                                    </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                            <span class="fileinput-new"> Chọn ảnh </span>
                                                            <span class="fileinput-exists"> Đổi ảnh </span>
                                                            <input type="file" name="banner">
                                                        </span>
                                                        <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                        data-dismiss="fileinput"> Xóa ảnh </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <input type="hidden" name="class" value="banner">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                    </div>
                                    </div>
                                </form>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <?php if(!empty($custom_display_item)): ?>
                <?php 

                    function showCatepost($category_id, $parent = 0, $char ='',$select = 0)
                    {
                        foreach ($category_id as $key => $item) {
                            if ($item->parents == $parent) {
                                echo '<option value="'.$item->id.'"';
                                if($parent == 0){
                                    echo 'style="color:red"';
                                }
                                if($select != 0 && $item->id == $select){
                                    echo 'selected="selected"';
                                }
                                echo '>';
                                echo $char . $item->name_vi;
                                echo '</option>';
                                showCatepost($category_id ,$item->id , $char.'---| ',$select);
                            }
                        }
                    }
                 ?>
                <?php endif; ?>



                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit slide danh muc phía trên trang "<?php echo e($name); ?>"</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 1): ?>
                                    <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                        <div class="ibox-content"  style="">
                                                <legend>Slider top</legend>
                                                <div class="form-group col-sm-6">
                                                    <label for="">Tiêu đề (vietnam):</label>
                                                    <input type="text" name="icon_namevi" class="form-control" id="" value="<?php echo e($cs_dt->label_vi); ?>" placeholder="Input field">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="">Tiêu đề (english):</label>
                                                    <input type="text" name="icon_nameen" class="form-control" id="" value="<?php echo e($cs_dt->label_en); ?>" placeholder="Input field">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="">Mô tả ngắn (vietnam):</label>
                                                    <textarea name="icon_iconvi" id="input" class="form-control" rows="3" required="required">
                                                        <?php echo e($cs_dt->title_vi); ?>

                                                    </textarea>
                                                    
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="">Mô tả ngắn (english):</label>
                                                    <textarea name="icon_iconen" id="input" class="form-control" rows="3" required="required">
                                                        <?php echo e($cs_dt->title_en); ?>

                                                    </textarea>
                                                </div>
                                                <div class="form-group col-sm-offset-3 col-sm-6">
                                                    <label for="">Chọn danh mục:</label>
                                                    <select name="category_id" id="input" class="form-control" required="required">
                                                        <?php echo e(showCatepost($category_id,0,'',$cs_dt->category_id)); ?>

                                                    </select>
                                                    <span  style="margin-button: 20px"><i><b>Lưu ý:</b></i> Slide chuyển động các bài viết của danh mục, hãy chọn danh mục có bài viết ! </span>
                                                </div>
                                            <div class="clearfix">
                                            
                                            </div>
                                            <div style="text-align: center">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                                
                                            
                                        <input type="hidden" name="class" value="category_id">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    </div>
                                </form>
                                

                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                
                            </div>
                            </div>
                        </div>
                    </div>


                    <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit slide danh muc phía dưới trang "<?php echo e($name); ?>"</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 2): ?>
                                        <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                            <div class="ibox-content" style="; ">
                                                    <legend>Slider bottom</legend>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Tiêu đề (vietnam):</label>
                                                        <input type="text" name="icon_namevi" class="form-control" id="" value="<?php echo e($cs_dt->label_vi); ?>" placeholder="Input field">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Tiêu đề (english):</label>
                                                        <input type="text" name="icon_nameen" class="form-control" id="" value="<?php echo e($cs_dt->label_en); ?>" placeholder="Input field">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Mô tả ngắn (vietnam):</label>
                                                        <textarea name="icon_iconvi" id="input" class="form-control" rows="3" required="required">
                                                            <?php echo e($cs_dt->title_vi); ?>

                                                        </textarea>
                                                        
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Mô tả ngắn (english):</label>
                                                        <textarea name="icon_iconen" id="input" class="form-control" rows="3" required="required">
                                                            <?php echo e($cs_dt->title_en); ?>

                                                        </textarea>
                                                    </div>
                                                    <div class="form-group col-sm-offset-3 col-sm-6">
                                                        <label for="">Chọn danh mục:</label>
                                                        <select name="category_id" id="input" class="form-control" required="required">
                                                            <?php echo e(showCatepost($category_id,0,'',$cs_dt->category_id)); ?>

                                                        </select>
                                                        <span  style="margin-button: 20px"><i><b>Lưu ý:</b></i> Slide chuyển động các bài viết của danh mục, hãy chọn danh mục có bài viết ! </span>
                                                    </div>
                                                <div class="clearfix">
                                                
                                                </div>
                                                <div style="text-align: center">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                                    
                                                
                                            <input type="hidden" name="class" value="category_id">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        </div>
                                    </form>
                                    

                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                


                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit icon trang "<?php echo e($name); ?>"</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content" style="">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-2">Tên dường dẫn*</th>
                                                        <th class="col-md-3">Mô tả*</th>
                                                        <th class="col-md-3">Đường dẫn</th>
                                                        <th class="col-md-1">Icon</th>
                                                        <th class="col-md-1">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($cs_dt->class == 'icon'): ?>
                                                    <tr>
                                                        <td><span class="tag-post"><?php echo e($cs_dt->sort); ?></span></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post"><?php echo e($cs_dt->label_vi); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><?php echo e(str_limit($cs_dt->title_vi,40)); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><?php echo e(str_limit($cs_dt->link,40)); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" width="40%" alt=""></span></a></td>
                                                        <td>
                                                            <a class="btn btn-primary" data-toggle="modal" href='#edit-<?php echo e($cs_dt->id); ?>'> 
                                                                <i class="fa fa-edit text-navy" style="color: #fff"></i> 
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <div class="modal fade" id="edit-<?php echo e($cs_dt->id); ?>">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Sửa link "<?php echo e($cs_dt->label_vi); ?>"</h4>
                                                                </div>
                                                                <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label for="">Tên icon (vietnam):</label>
                                                                            <input type="text" name="icon_namevi" class="form-control" required="required" id="name" value="<?php echo e($cs_dt->label_vi); ?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Tên icon (english):</label>
                                                                            <input type="text" name="icon_nameen" class="form-control"  required="required" id="name" value="<?php echo e($cs_dt->label_en); ?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Mô tả icon (vietnam):</label>
                                                                            <textarea name="icon_iconvi" id="input" class="form-control" rows="3" required="required">
                                                                                <?php echo e($cs_dt->title_vi); ?>

                                                                            </textarea>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Mô tả icon (english):</label>
                                                                            <textarea name="icon_iconen" id="input" class="form-control" rows="3" required="required">
                                                                                <?php echo e($cs_dt->title_en); ?>

                                                                            </textarea>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Sửa link</label>
                                                                            <input type="text" name="link_modal" class="form-control" id="name" value="<?php echo e($cs_dt->link); ?>">
                                                                        </div>
                                                                        <div id="image-diplay">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Icon hiện tại(*):</label>
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt=""> 
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                                            <span class="fileinput-new"> Chọn ảnh </span>
                                                                                            <span class="fileinput-exists"> Đổi ảnh </span>
                                                                                            <input type="file" name="image">
                                                                                        </span>
                                                                                        <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                                        data-dismiss="fileinput"> Xóa ảnh </a>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="class" value="icon">
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php elseif($name == 'student-services'): ?>
    <div class="row">
        <div class="col-lg-12">
           <?php if(session('success')): ?>
           <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo e(session('success')); ?>

            </div>
            <?php endif; ?>
            <?php if(session('error')): ?>
            <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo e(session('error')); ?>

            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit banner trang "<?php echo e($name); ?>"</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($cs_dt->class == 'banner'): ?>
                                    <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                        <div class="ibox-content" style="">
                                            <div id="image-diplay">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Banner hiện tại(*):</label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->banner); ?>" alt=""> 
                                                    </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                            <span class="fileinput-new"> Chọn ảnh </span>
                                                            <span class="fileinput-exists"> Đổi ảnh </span>
                                                            <input type="file" name="banner">
                                                        </span>
                                                        <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                        data-dismiss="fileinput"> Xóa ảnh </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <input type="hidden" name="class" value="banner">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                    </div>
                                    </div>
                                </form>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <?php if(!empty($custom_display_item)): ?>
                <?php 
                    function showCatepost($category_id, $parent = 0, $char ='',$select = 0)
                    {
                        foreach ($category_id as $key => $item) {
                            if ($item->parents == $parent) {
                                echo '<option value="'.$item->id.'"';
                                if($parent == 0){
                                    echo 'style="color:red"';
                                }
                                if($select != 0 && $item->id == $select){
                                    echo 'selected="selected"';
                                }
                                echo '>';
                                echo $char . $item->name_vi;
                                echo '</option>';
                                showCatepost($category_id ,$item->id , $char.'---| ',$select);
                            }
                        }
                    }
                 ?>
                <?php endif; ?>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit icon trang "<?php echo e($name); ?>"</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content" style="">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-2">Tên dường dẫn*</th>
                                                        <th class="col-md-3">Mô tả*</th>
                                                        <th class="col-md-3">Đường dẫn</th>
                                                        <th class="col-md-1">Icon</th>
                                                        <th class="col-md-1">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($cs_dt->class == 'icon'): ?>
                                                    <tr>
                                                        <td><span class="tag-post"><?php echo e($cs_dt->sort); ?></span></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post"><?php echo e($cs_dt->label_vi); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><?php echo e(str_limit($cs_dt->title_vi,40)); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><?php echo e(str_limit($cs_dt->link,40)); ?></span></a></td>
                                                        <td><a href="<?php echo e($cs_dt->link); ?>"> <span class="tag-post-tacgia"><img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" width="40%" alt=""></span></a></td>
                                                        <td>
                                                            <a class="btn btn-primary" data-toggle="modal" href='#edit-<?php echo e($cs_dt->id); ?>'> 
                                                                <i class="fa fa-edit text-navy" style="color: #fff"></i> 
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php endif; ?>
                                                    <div class="modal fade" id="edit-<?php echo e($cs_dt->id); ?>">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Sửa link "<?php echo e($cs_dt->label_vi); ?>"</h4>
                                                                </div>
                                                                <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label for="">Tên icon (vietnam):</label>
                                                                            <input type="text" name="icon_namevi" class="form-control" required="required" id="name" value="<?php echo e($cs_dt->label_vi); ?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Tên icon (english):</label>
                                                                            <input type="text" name="icon_nameen" class="form-control" required="required" id="name" value="<?php echo e($cs_dt->label_en); ?>">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Mô tả icon (vietnam):</label>
                                                                            <textarea name="icon_iconvi" id="input" class="form-control" rows="3" required="required">
                                                                                <?php echo e($cs_dt->title_vi); ?>

                                                                            </textarea>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Mô tả icon (english):</label>
                                                                            <textarea name="icon_iconen" id="input" class="form-control" rows="3" required="required">
                                                                                <?php echo e($cs_dt->title_en); ?>

                                                                            </textarea>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="">Sửa link</label>
                                                                            <input type="text" name="link_modal" class="form-control" id="name" value="<?php echo e($cs_dt->link); ?>">
                                                                        </div>
                                                                        <div id="image-diplay">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Icon hiện tại(*):</label>
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cs_dt->image); ?>" alt=""> 
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                                            <span class="fileinput-new"> Chọn ảnh </span>
                                                                                            <span class="fileinput-exists"> Đổi ảnh </span>
                                                                                            <input type="file" name="image">
                                                                                        </span>
                                                                                        <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                                        data-dismiss="fileinput"> Xóa ảnh </a>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="class" value="icon">
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit slide danh muc phía dưới trang "<?php echo e($name); ?>"</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->class == 'category_id' && $cs_dt->sort == 2): ?>
                                        <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                            <div class="ibox-content" style="; ">
                                                    <legend>Slider bottom</legend>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Tiêu đề (vietnam):</label>
                                                        <input type="text" name="icon_namevi" class="form-control" id="" value="<?php echo e($cs_dt->label_vi); ?>" placeholder="Input field">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Tiêu đề (english):</label>
                                                        <input type="text" name="icon_nameen" class="form-control" id="" value="<?php echo e($cs_dt->label_en); ?>" placeholder="Input field">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Mô tả ngắn (vietnam):</label>
                                                        <textarea name="icon_iconvi" id="input" class="form-control" rows="3" required="required">
                                                            <?php echo $cs_dt->title_vi; ?>

                                                        </textarea>
                                                        
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="">Mô tả ngắn (english):</label>
                                                        <textarea name="icon_iconen" id="input" class="form-control" required="required">
                                                            <?php echo $cs_dt->title_en; ?>

                                                        </textarea>
                                                    </div>
                                                    <div class="form-group col-sm-offset-3 col-sm-6">
                                                        <label for="">Chọn danh mục:</label>
                                                        <select name="category_id" id="input" class="form-control" required="required">
                                                            <?php echo e(showCatepost($category_id,0,'',$cs_dt->category_id)); ?>

                                                        </select>
                                                        <span  style="margin-button: 20px"><i><b>Lưu ý:</b></i> Slide chuyển động các bài viết của danh mục, hãy chọn danh mục có bài viết ! </span>
                                                    </div>
                                                <div class="clearfix">
                                                
                                                </div>
                                                <div style="text-align: center">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                                    
                                                
                                            <input type="hidden" name="class" value="category_id">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        </div>
                                    </form>
                                    

                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php elseif($name == 'about-us'): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit tiêu đề  trang "<?php echo e($name); ?>"</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                   <?php $__currentLoopData = $custom_display_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cs_dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($cs_dt->class == 'title'): ?>
                                        <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($cs_dt->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="">Tiêu đề trang abouts us</label>
                                                <input type="text" name="icon_namevi" class="form-control" id="" value="<?php echo e($cs_dt->label_vi); ?>">

                                            </div><div class="form-group">
                                                <label for="">Tiêu đề trang abouts us</label>
                                                <input type="text" name="icon_nameen" class="form-control" id="" value="<?php echo e($cs_dt->label_en); ?>">
                                            </div>
                                            <input type="hidden" name="class" value="title">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit nội dung  trang "<?php echo e($name); ?>"</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                
                                <div class="ibox-content">
                                    <div>
                                        <div class="col-lg-12">
                                            <?php if(!empty($post_trans)): ?>
                                                <input type="hidden" class="total_123455" name="total" value="<?php echo e(count($post_trans)); ?>">
                                            <?php endif; ?>
                                            <div class="">
                                                <?php if(!empty($post_trans)): ?>
                                                    <?php $__currentLoopData = $post_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $ps_tran): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <form class="form-horizontal" action="<?php echo e(route('admin.post.update', $ps_tran->id)); ?>" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                    <input type="hidden" name="status_img" value="0">
                                                    
                                                        

                                                       <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="image-diplay">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-2" for="email">Tiêu đề(*):</label>
                                                                        <div class="col-sm-10">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                                <input type="text" class="form-control" name="name" placeholder="Tên bài viết" value="<?php echo e($ps_tran->name); ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-sm-2" for="email">Nội dung(*):</label>
                                                                        <div class="col-sm-10">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                                <textarea type="text" name="content" class="form-control" id="ckeditor_about_<?php echo e($key); ?>"><?php echo $ps_tran->content; ?></textarea>
                                                                                <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
                                                                                  <script>

                                                                                     CKEDITOR.replace( 'ckeditor_about_<?php echo e($key); ?>', {
                                                                                      filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
                                                                                      filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
                                                                                      filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
                                                                                      filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
                                                                                      filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
                                                                                      filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>',
                                                                                    });
                                                                                 </script>
                                                                            </div>
                                                                            <?php if($errors->has('content')): ?>
                                                                            <span class="text-center text-danger" role="alert">
                                                                                <?php echo e($errors->first('content')); ?>

                                                                            </span>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                        <div class="input-group-addon" style="padding:0;border:0;text-align: right;">
                                                            <?php 
                                                                $count_post = \App\Models\Admin\PostTranslation::where('post_id',$ps_tran->post_id)->where('language_id','en')->first();
                                                             ?>
                                                             <?php if(!empty($count_post)): ?> 
                                                            
                                                                <a href="<?php echo e(route('admin.post.edit', $count_post->id)); ?>" class="btn btn-default" title="Edit EN"><img src="<?php echo e(url('public')); ?>/upload/icon/english.png" width="30"><span></span></a>
                                                                &nbsp;
                                                            <?php else: ?>
                                                                <a title="Create EN" class="btn btn-default" href="<?php echo e(route('admin.post.create.lang', [$ps_tran->id,$ps_tran->category_id,'english'])); ?>">
                                                                    <img src="<?php echo e(url('public')); ?>/upload/icon/english.png" width="30">
                                                                </a>
                                                            <?php endif; ?>
                                                            <button type="submit" class="btn btn-primary" style="padding:7px 35px" ><i class="fa fa-edit"></i> Change edit</button>
                                                            <a href="<?php echo e(route('admin.post.delete',['id'=>$ps_tran->post_id])); ?>" class="btn btn-danger" style="padding:7px 35px"><i class="fa fa-fw fa-trash-o "></i> Remove</a>
                                                        </div>
                                                    
                                                    </form><hr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                    <h3>Thêm tab nội dung</h3>
                                        <button type="button" id="addNewCkeditor" class="btn btn-default ">Add tab +</button>
                                    <div class="row">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.post.store_about')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <div class="col-lg-12">
                                            <div class="listTabCkeditor">

                                               <!-- <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                                  <div class="input-group">
                                               
                                                    <div class="input-group-addon" style="padding:0;border:0">
                                                      <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2" for="email">Tên bài viết(*):</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                <input type="text" class="form-control" name="name_tab[]" placeholder="Tên bài viết" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2" for="email">Nội dung bài viết(*):</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                <textarea type="text" name="content_tab[]" class="form-control" id="ckeditor"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div><hr>
                                               </div> -->
                                            </div>
                                             <div style="text-align: center">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                    </div>

                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <form class="form-horizontal" action="<?php echo e(route('admin.post.store')); ?>" method="post" enctype="multipart/form-data">
                                <div class="ibox-content">
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
    $(document).ready(function () {
        $('.alert-dismissable').delay(5000).slideUp();
        $('.btn-delete').click(function () {
            var id = $(this).attr('data-id');
            $('#data-id').val(id);
        });
    });
</script>
    

    <script type="text/javascript">
     $(document).ready(function () {



            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 0; //initlal text box coun

            // var dataa = $(".tab_total").val();
            // if (dataa > 0) {
              // x = dataa;
              // alert(x);
            // };
         $("#addNewCkeditor").click(function(){
            // var dataa = $(".total_123455").val();

            // var total = $(this).find('.total_123455').val();
            // alert(dataa);
            if(x < max_fields){ //max input box allowed
                    x++;
                    var editorId = 'ckeditor_' + x;


                    var str='<div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                      str+='<div class="input-group">';
                        str+='<div class="input-group-addon" style="padding:0;border:0">';
                          str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                        str+='</div>';
                        str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Tên bài viết(*):</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="input-group">';
                                    str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                    str+='<input type="text" class="form-control" name="name_tab[]" placeholder="Tên bài viết" value="">';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                        str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Nội dung bài viết(*):</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="input-group">';
                                    str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                    str+='<textarea type="text" name="content_tab[]" class="form-control" id="'+editorId+'"></textarea>';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                      str+='</div><hr>';
                   str+='</div>';
                    $(wrapper).append(str);

                    CKEDITOR.replace( editorId, {
                      filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
                      filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
                      filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
                      filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
                      filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
                      filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
                    });
                }
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });

    </script>
<script>
      CKEDITOR.replace( 'ckeditor', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>