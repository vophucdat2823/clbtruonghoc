<?php $__env->startSection('stype.css'); ?>
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
        .txt_cate_pp_img a{
            text-transform: uppercase;
        }
        
    </style>
    <style type="text/css" media="screen">
        #main_mobie .mark_banner{
            padding-top: 1rem;padding-bottom: 1rem
        }
        #main_mobie .mark_banner img,.fix-size-image img{
            width: 100% !important;
        } 
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<main id="main_mobie">
    <div id="about_us" style="padding-bottom: 10%;padding-top:;">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($posts->status_img == 1): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style='background-image: url("<?php echo e($posts->khoaquocte_id == null ? url('public/upload/images/').'/'.$posts->image : $posts->image); ?>")' alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div>
            <div class="activities_home" style="background: #E8B909;color: #102B4E">
                <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                    <p style="text-transform: uppercase;"><?php echo e($posts->name); ?></p>
                </div>
            </div>

            <div class="content_about_us" >
                <div class="">
                    <div class="col-12" style="background: #fff;">
                        
                        <div class="mark_banner">
                            <?php echo $posts->content; ?>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>

<main id="main_pc">
    <?php 
        $language_id = 'vi';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
            //dd($language_id);
        }
     ?>
        <div id="student_service">
            <div class="student_service_top_title ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <?php if($language_id == 'en'): ?>

                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                                        <span class="color_blue">
                                            <?php echo e($key); ?> >
                                        </span>
                                    </a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                            <?php elseif($language_id == 'vi'): ?>
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">Trang chủ > </span>
                            </a>
                            
                            <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                                        <span class="color_blue">
                                            <?php echo e($key); ?> >
                                        </span>
                                    </a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endif; ?>
                            <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($posts->name); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_people_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <h3>MORE</h3>
                                    <ul>
                                     <?php if(!empty($category)): ?>
                                            <?php $__currentLoopData = $category->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($value->language_id == $language_id): ?>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                <img src="<?php echo e(asset('/public/web/images/squares.svg')); ?>" alt="" width="35%">
                                                            </div>
                                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="<?php echo e(route('web.menu',$value->code)); ?>"><?php echo e(str_limit($value->name,30)); ?></a>
                                                                    <span><?php echo e($value->created_at); ?></span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="dif_info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul>
                                                
                                                <?php 
                                                    
                                                $cate_tintuc = \App\Models\Admin\Category::where('parents',7)->get();
                                                 ?>
                                                <?php $__currentLoopData = $cate_tintuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php 
                                                    
                                                    $posts_tt = DB::table('posts')
                                                        ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                        ->where('category_id', $ca_tt->id)
                                                        ->where('language_id',$language_id)
                                                        ->select('post_translations.*')
                                                        ->get();
                                                 ?>
                                                    <?php $__currentLoopData = $posts_tt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li>
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="<?php echo e(route('web.menu',['param' => $po_tt->code])); ?>"><?php echo e(str_limit($po_tt->name,30)); ?></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="img_student_service">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="img_student_service">
                                                <?php if($category->images == null): ?>
                                                    <img src="<?php echo e(asset('/public/web')); ?>/images/student_service/student.jpg" alt="" width="100%">
                                                <?php else: ?>
                                                    <img src="<?php echo e(asset('/public/upload/image')); ?>/<?php echo e($category->image); ?>" alt="" width="100%">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="video_youtube_student_service">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="content_video_youtube_student_service">
                                                <iframe width="100%" height="auto" src="https://www.youtube.com/embed/snNFMlFmyzg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div> 
                                            <div class="txt_content_video_youtube">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-8 col-8">
                                                        <span>Rihanna - Don't Stop The Music</span>
                                                        <span>3:54</span>
                                                        <span>IShuffle</span>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                        <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                    </div>
                                                </div>
                                            </div>         
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="insta_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web')); ?>/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web')); ?>/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web')); ?>/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="<?php echo e(url('public/web')); ?>/images/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tag_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web')); ?>/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="">Cử nhân</a>
                                            <a href="">Kinh doanh</a>
                                            <a href="">Kế toán</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Máy tính</a>
                                            <a href="">Tin học</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Kinh doanh</a>
                                            <a href="">Kế toán</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Máy tính</a>
                                            <a href="">Tin học</a>
                                            <a href="">Luật</a>
                                            <a href="">Cử nhân</a>
                                            <a href="">Quản lý</a>
                                            <a href="">Luật</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="23%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h3 class="color_blue"><?php echo e($posts->name); ?></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <?php if($posts->status_img == 1): ?>
                                            <img src="<?php echo e($posts->khoaquocte_id == null ? url('public/upload/images/').'/'.$posts->image : $posts->image); ?>" alt="" width="100%">
                                        <?php endif; ?>
                                        <div class="fix-size-image">
                                            <p><?php echo $posts->content; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>