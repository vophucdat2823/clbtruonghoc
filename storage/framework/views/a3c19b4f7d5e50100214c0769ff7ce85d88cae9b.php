<?php $__env->startSection('content'); ?>

<main>
    <div id="social">
        <div class="social_top_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        
        <div class="main_soial">
            <div class="container">
                <div class="row">
                    <?php if(!empty($networks)): ?>
                        <?php $__currentLoopData = $networks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $network): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_main_h">
                            <a href="<?php echo e($network['link']); ?>" target="_blank">
                                <div class="img_content_main_h">
                                        <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($network['image']); ?>" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="<?php echo e(url('public/upload/icon')); ?>/<?php echo e($network['icon']); ?>" alt="" width="50%">
                                        </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>