<?php $__env->startSection('content'); ?>
<?php 
    $caidat = \App\Models\Options::all();
    $language_id = 'vi';
    if (Session::has('set_language')) {
        $language_id = Session::get('set_language');
        //dd($language_id);
    }
 ?>
<style type="text/css">
    main #about_us .content_about_us .card-header{
        border-bottom: 3px solid #102B4E;
        padding-left: 0
    }
    main #about_us .content_about_us .card-header a{
        font-size: 35px;
        font-weight: bold;
        color: #102B4E !important;
        text-transform: uppercase;
    }
    main #about_us .content_about_us .card-header a:hover{
        color: #e8b909 !important;
    }
    main #about_us .content_about_us .card-header a img {
        margin-right: 20px;
        margin-top: -6px;
        
    }
    
    .alumi_st{
        padding-left: 0;
    }
    .alumi_st li{
        list-style-type: none;
    }
    .alumi_st li a{
        text-decoration: none;
        color: #102B4E;
        font-size: 18px
    }
    main #partnership .content_people .cate_pp_img .txt_cate_pp_img h5 {
     display: inline;
        text-transform: uppercase;
        color: #102b4e;
        font-size: 27px;
        font-weight: 400;
    }
</style>
<main id="main_mobie">
    <div id="about_us">
        <div class="home">
            
        </div>
        
        
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($programs): ?>
                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                                $ppost = DB::table('program_posts')
                                    ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                                    ->where('program_id', $gram->id)
                                    ->where('language_id',$language_id)
                                    ->select('program_translations.*')
                                    ->first();
                             ?>
                            <?php if($language_id == 'vi'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding: 3.5vw 0;">
                                        <?php if($ppost == null): ?>
                                        <div class="activities_home">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #E8B909 !important;" href="#<?php echo e(str_slug($gram->name_vi)); ?>">
                                                   <?php echo e($gram->name_vi); ?>

                                                </a>
                                            </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="activities_home">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a style="font-size: 3.7vw;color: #E8B909 !important;" href="<?php echo e(route('web.program.post',[$ppost->code])); ?>" >
                                                   <?php echo e($gram->name_vi); ?><span style="color: #fff;font-weight: bold"> >></span>
                                                </a>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($ppost == null): ?>
                                    <div id="<?php echo e(str_slug($gram->name_vi)); ?>" class="collapse show" data-parent="#accordion" style="padding-bottom: 10%;">
                                        <div class="card-body" style="background: #fff">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <?php if($gram->getProgram): ?> <?php $__currentLoopData = $gram->getProgram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getGram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <div class="col-md-12" style="margin-bottom:45px;padding-left:0;padding-right: 0;">
                                                                    <div class="item" style="margin-bottom: 20px">
                                                                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($getGram->image); ?>)" class="transparent" alt="" width="100%">
                                                                        <div class="title_img_slide_home"><h6><?php echo e($getGram->name_vi); ?></h6></div>
                                                                    </div>
                                                                    <?php 
                                                                         $posts_child = DB::table('program_posts')
                                                                            ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                                                                            ->where('program_id', $getGram->id)
                                                                            ->where('language_id',$language_id)
                                                                            ->select('program_translations.*')
                                                                            ->get();
                                                                     ?>
                                                                    <div class="row txt_ct_cate">
                                                                        
                                                                        <?php if(count($posts_child)): ?>
                                                                            <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <div class="col-md-12 col-sm-12 col-12">
                                                                                <div class="card-header" style="padding: 5px 0 5px 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none">
                                                                                    <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                                        <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php elseif($language_id == 'en'): ?>
                                <div class="card_bottom">
                                    <div style="background-color: transparent;padding: 3.5vw 0;">
                                       <?php if($ppost == null): ?>
                                        <div class="activities_home">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a class="collapsed card-link" data-toggle="collapse" style="font-size: 4vw;color: #E8B909 !important;" href="#<?php echo e(str_slug($gram->name_en)); ?>">
                                                   <?php echo e($gram->name_en); ?>

                                                </a>
                                            </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="activities_home">
                                            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                                <a style="font-size: 3.7vw;color: #E8B909 !important;" href="<?php echo e(route('web.program.post',[$ppost->code])); ?>" >
                                                   <?php echo e($gram->name_en); ?><span style="color: #fff;font-weight: bold"> >></span>
                                                </a>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($ppost == null): ?>
                                    <div id="<?php echo e(str_slug($gram->name_en)); ?>" class="collapse show" data-parent="#accordion" style="padding-bottom: 10%;">
                                        <div class="card-body" style="background: #fff">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <?php if($gram->getProgram): ?> <?php $__currentLoopData = $gram->getProgram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getGram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <div class="col-md-12" style="margin-bottom:45px;padding-left:0;padding-right: 0;">
                                                                    <div class="item" style="margin-bottom: 20px">
                                                                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($getGram->image); ?>)" class="transparent" alt="" width="100%">
                                                                        <div class="title_img_slide_home"><h6><?php echo e($getGram->name_en); ?></h6></div>
                                                                    </div>
                                                                    <?php 
                                                                         $posts_child = DB::table('program_posts')
                                                                            ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                                                                            ->where('program_id', $getGram->id)
                                                                            ->where('language_id',$language_id)
                                                                            ->select('program_translations.*')
                                                                            ->get();
                                                                     ?>
                                                                    <div class="row txt_ct_cate">
                                                                        
                                                                        <?php if(count($posts_child)): ?>
                                                                            <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <div class="col-md-12 col-sm-12 col-12">
                                                                                <div class="card-header" style="padding: 5px 0 5px 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none">
                                                                                    <a style="font-size: 2.6vw;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                                        <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #E8B909">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #E8B909">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
<main id="main_pc">
    <div id="about_us" style="margin-bottom: 10%">
        <div class="about_top_title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12 dell-end-a">
                        <a href="<?php echo e(route('web.home')); ?>">
                            <span class="color_blue">
                                <?php if($language_id == 'en'): ?>
                                HOME
                                <?php elseif($language_id == 'vi'): ?>
                                Trang chủ
                                <?php endif; ?> > 
                            </span>
                        </a>
                        <span class="color_gray" style="text-decoration: none;text-transform: uppercase;">
                            <?php if($language_id == 'en'): ?>
                                ADMISSION
                                <?php elseif($language_id == 'vi'): ?>
                                Tuyển sinh
                                <?php endif; ?>
                            </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="content_about_us" >
            <div class="container mt-3">
                <div id="accordion">
                    <?php if($programs): ?>
                    <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                        $ppost = DB::table('program_posts')
                            ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                            ->where('program_id', $gram->id)
                            ->where('language_id',$language_id)
                            ->select('program_translations.*')
                            ->first();

                        
                     ?>
                    <?php if($language_id == 'vi'): ?>
                    
                    
                    <div class="card_bottom">
                        <div class="">
                            <div class="card-header" style="background-color: transparent">
                                <?php if($ppost == null): ?>
                                    <a class="collapsed card-link" data-toggle="collapse" href="#<?php echo e(str_slug($gram->name_vi)); ?>">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($gram->name_vi); ?>

                                    </a>
                                <?php else: ?>
                                    <a class="collapsed card-link" href="<?php echo e(route('web.program.post',[$ppost->code])); ?>">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($gram->name_vi); ?> <span style="color: #e8b909;font-weight: bold">>></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div id="<?php echo e(str_slug($gram->name_vi)); ?>" class="collapse show" data-parent="#accordion">
                                <div class="card-body" style="padding-left: 0;padding-right: 0">
                                    <div id=home>
                                        <div class="slide_home" style="background: transparent;padding: 0">
                                            <div class="">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <?php if($gram->getProgram): ?> <?php $__currentLoopData = $gram->getProgram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getGram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="col-md-6" style="margin-bottom:45px;padding-left:0;padding-right: 30px">
                                                            <div class="item" style="margin-bottom: 20px">
                                                                <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($getGram->image); ?>)" class="transparent" alt="" width="100%">
                                                                <div class="title_img_slide_home"><h6><?php echo e($getGram->name_vi); ?></h6></div>
                                                            </div>
                                                            <?php 
                                                                 $posts_child = DB::table('program_posts')
                                                                    ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                                                                    ->where('program_id', $getGram->id)
                                                                    ->where('language_id',$language_id)
                                                                    ->select('program_translations.*')
                                                                    ->get();
                                                             ?>
                                                            <div class="row txt_ct_cate">
                                                                
                                                                <?php if(count($posts_child)): ?>
                                                                    <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="col-md-12 col-sm-12 col-12">
                                                                        <div class="card-header" style="padding: 5px 0 5px 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none">
                                                                            <a style="font-size: 18px;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                                <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php elseif($language_id == 'en'): ?>
                    <div class="card_bottom">
                        <div class="">

                            <div class="card-header"  style="background-color: transparent">
                                <?php if($ppost == null): ?>
                                    <a class="collapsed card-link" data-toggle="collapse" href="#<?php echo e(str_slug($gram->name_en)); ?>">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($gram->name_en); ?>

                                    </a>
                                <?php else: ?>
                                    <a class="collapsed card-link" href="<?php echo e(route('web.program.post',[$ppost->code])); ?>">
                                        <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($gram->name_en); ?> <span style="color: #e8b909;font-weight: bold">>></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div id="<?php echo e(str_slug($gram->name_en)); ?>" class="collapse show" data-parent="#accordion">
                                <div class="card-body" style="padding-left: 0;padding-right: 0">
                                    <div id=home>
                                        <div class="slide_home" style="background: transparent;padding: 0">
                                            <div class="">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <?php if($gram->getProgram): ?> <?php $__currentLoopData = $gram->getProgram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getGram): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="col-md-6" style="margin-bottom:45px;padding-left:0;padding-right: 30px">
                                                            <div class="item" style="margin-bottom: 20px">
                                                                <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($getGram->image); ?>)" class="transparent" alt="" width="100%">
                                                                <div class="title_img_slide_home"><h6><?php echo e($getGram->name_en); ?></h6></div>
                                                            </div>
                                                            <?php 
                                                                 $posts_child = DB::table('program_posts')
                                                                    ->join('program_translations','program_posts.id', '=', 'program_translations.pgramPost_id')
                                                                    ->where('program_id', $getGram->id)
                                                                    ->where('language_id',$language_id)
                                                                    ->select('program_translations.*')
                                                                    ->get();
                                                             ?>
                                                            <div class="row txt_ct_cate">
                                                                
                                                                <?php if(count($posts_child)): ?>
                                                                    <?php $__currentLoopData = $posts_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="col-md-12 col-sm-12 col-12">
                                                                        <div class="card-header" style="padding: 5px 0 5px 0; margin-bottom: 0;background-color: transparent;border-bottom: 0;text-transform: none">
                                                                            <a style="font-size: 18px;color: #102B4E;text-decoration: none;" href="<?php echo e(route('web.program.post',['params' => $po_c->code])); ?>">
                                                                                <img src="<?php echo e(url('public/web')); ?>/images/alumni/4x/Asset 13@4x.png" style="border-radius: 0" alt="" width="2.8%"><?php echo e($po_c->name); ?>

                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>