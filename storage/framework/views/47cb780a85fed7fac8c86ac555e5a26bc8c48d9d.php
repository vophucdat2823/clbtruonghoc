<?php $__env->startSection('stype.css'); ?>
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
        .txt_cate_pp_img a{
            text-transform: uppercase;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <main>
        <div id="departments_details">
            <div class="people_details_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <a href="<?php echo e(route('web.menu', str_slug($menu['name']) ."-".$menu['id'])); ?>">
                                <span class="color_blue"><?php echo e($menu['name']); ?> > </span>
                            </a>
                            <span class="color_gray"><?php echo e($post['name']); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_people_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <h3>Categories</h3>
                                    <ul>
                                        <?php if(!empty($posts)): ?>
                                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <img src="<?php echo e(asset('/public/web/images/squares.svg')); ?>" alt="" width="60%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="<?php echo e(route('web.post.detail',[$menu['code'], str_slug($value->name)."-".$value->id])); ?>"><h5><?php echo e($value->name); ?></h5></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="dif_info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(asset('/public/web/images/icon_square_xam.svg')); ?>" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Thông báo - Tin tức</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Thời khóa biểu - Lịch thi</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Mẫu đơn - Xác nhận</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Học bổng</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="insta_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tag_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="/public/web/images/squares.svg" alt="" width="23%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h3 class="color_blue"><?php echo e($post['name']); ?></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <img src="<?php echo e($post['image']); ?>" alt="" width="100%">
                                        <div class="fix-size-image">
                                            <p><?php echo $post['content']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>