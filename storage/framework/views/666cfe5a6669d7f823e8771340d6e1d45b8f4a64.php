

<?php $__env->startSection('style.css'); ?>
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" action="<?php echo e(route('admin.chooses.post_create_item')); ?>" id="formDemo" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <input type="hidden" name="id" value="<?php echo e($id); ?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nội dung bài viết</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                              <div class="col-lg-12">
                                                <label class="" for="email">
                                                    <button type="button"  class="btn btn-success addNewCkeditor">
                                                          Add teachers +
                                                      </button>
                                                </label>
                                                <div class="clearfix"></div>
                                                <div class="listTabCkeditor">
                                                  <?php 
                                                    $teachers  = json_decode($chooses->teachers);
                                                    $office_vi = json_decode($chooses->office_vi);
                                                    $office_en = json_decode($chooses->office_en);
                                                    $title_vi  = json_decode($chooses->title_vi);
                                                    $title_en  = json_decode($chooses->title_en);
                                                    $image     = json_decode($chooses->image);
                                                   ?>
                                                  <?php if($teachers || $image): ?>

                                                    <?php for($i = 0; $i < count($teachers) ; $i++): ?>
                                                    
                                                    <div id="image-diplay" class="custom_item_ajax" style="margin-top: 20px;margin-bottom: 20px">
                                                      <div class="input-group">
                                                        <div class="input-group-addon" style="padding:0;border:0">
                                                          <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Tên giáo viên(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                    <input type="text" class="form-control name_teacher" name="teachers[]" placeholder="Tên giáo viên" value="<?php echo e($teachers[$i]); ?>">
                                                                </div>
                                                                    <span class="text-danger name_teacher_group" style="display: none" role="alert">
                                                                        Vui lòng điền tên giáo viên !
                                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Chức vụ(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">VI</span>
                                                                    <input type="text" class="form-control" name="office_vi[]" placeholder="Chức vụ" value="<?php echo e($office_vi[$i]); ?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Chức vụ(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">EN</span>
                                                                    <input type="text" class="form-control" name="office_en[]" placeholder="Chức vụ" value="<?php echo e($office_en[$i]); ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">VI</span>
                                                                    <textarea type="text" name="title_vi[]" class="form-control"><?php echo e($title_vi[$i]); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">EN</span>
                                                                    <textarea type="text" name="title_en[]" class="form-control" id="ckeditor"><?php echo e($title_en[$i]); ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                      <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($image[$i]); ?>" alt="">
                                                                  </div>
                                                                  <div style="text-align: center">
                                                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important; ">
                                                                    <span class="fileinput-new" > Chọn ảnh </span>
                                                                    <span class="fileinput-exists"> Đổi ảnh </span>
                                                                      
                                                                      <input type="file" name="upload[]" class="upload">
                                                                      <input type="hidden" value="<?php echo e($image[$i]); ?>" name="image[]">

                                                                      
                                                                    </span>
                                                                    <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                          data-dismiss="fileinput"> Xóa ảnh </a>
                                                                       
                                                                  </div>
                                                                  <span class="text-danger image_teachers_group" style="display: none" role="alert">
                                                                    Vui lòng thêm ảnh
                                                                  </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div><hr>
                                                   </div> 

                                                    <?php endfor; ?>
                                                  <?php else: ?>
                                                  <div id="image-diplay" class="custom_item_ajax" style="margin-top: 20px;margin-bottom: 20px">
                                                      <div class="input-group">
                                                        <div class="input-group-addon" style="padding:0;border:0">
                                                          <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Tên giáo viên(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                                    <input type="text" class="form-control name_teacher" name="teachers[]" placeholder="Tên giáo viên" value="">
                                                                </div>
                                                                    <span class="text-danger name_teacher_group" style="display: none" role="alert">
                                                                        Vui lòng điền tên giáo viên !
                                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Chức vụ(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">VI</span>
                                                                    <input type="text" class="form-control" name="office_vi[]" placeholder="Chức vụ" value="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Chức vụ(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">EN</span>
                                                                    <input type="text" class="form-control" name="office_en[]" placeholder="Chức vụ" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">VI</span>
                                                                    <textarea type="text" name="title_vi[]" class="form-control" id="ckeditor"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">EN</span>
                                                                    <textarea type="text" name="title_en[]" class="form-control" id="ckeditor"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>
                                                            <div class="col-sm-10">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                      
                                                                  </div>
                                                                  <div style="text-align: center">
                                                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important; ">
                                                                    <span class="fileinput-new" > Chọn ảnh </span>
                                                                    <span class="fileinput-exists"> Đổi ảnh </span>
                                                                      <input type="file" name="upload[]" class="upload">
                                                                      <input type="hidden" name="image[]" class="image_teachers">
                                                                    </span>
                                                                    <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                          data-dismiss="fileinput"> Xóa ảnh </a>
                                                                       
                                                                  </div>
                                                                  <span class="text-danger image_teachers_group" style="display: none" role="alert">
                                                                    Vui lòng thêm ảnh
                                                                  </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </div><hr>
                                                   </div> 
                                                  <?php endif; ?>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Lưu</button>
                                <a href="<?php echo e(route('admin.chooses.list')); ?>" class="btn btn-danger">
                                    <i class="fa fa-fw fa-close"></i>Quay lại
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
  <script>
    jQuery(document).ready(function($) {
      let array_teachers = [];
     $( "form" ).submit(function( event ) {
        $('.listTabCkeditor .custom_item_ajax').each(function() {
          let image = $(this).find('.image_teachers').val();
          let name_teacher = $(this).find('.name_teacher').val();
          if(image == ""){

            $(this).find('.image_teachers_group').css("display", "block");;
            event.preventDefault();
          }

          if(name_teacher == ""){
            $(this).find('.name_teacher_group').css("display", "block");;
            event.preventDefault();
          }
        })
      });
    });
  </script>       

  <script type="text/javascript">
     $(document).ready(function () {
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

         $(".addNewCkeditor").click(function(){
                    var str='<div id="image-diplay" class="custom_item_ajax" style="margin-top: 20px;margin-bottom: 20px">';
                        str+='<div class="input-group">';
                          str+='<div class="input-group-addon" style="padding:0;border:0">';
                            str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 200%;padding-top: 200%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                          str+='</div>';
                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Tên giáo viên(*):</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group name_teachers_group">';
                                      str+='<span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>';
                                      str+='<input type="text" class="form-control name_teacher" name="teachers[]" placeholder="Tên giáo viên" value="">';
                                  str+='</div>';
                                       str+='<span class="text-danger name_teacher_group" style="display: none" role="alert">';
                                         str+='Vui lòng điền tên giáo viên !';
                                      str+='</span>';
                              str+='</div>';
                          str+='</div>';
                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Chức vụ(*):</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group">';
                                      str+='<span class="input-group-addon">VI</span>';
                                      str+='<input type="text" class="form-control" name="office_vi[]" placeholder="Chức vụ" value="">';
                                  str+='</div>';
                              str+='</div>';
                          str+='</div>';

                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Chức vụ(*):</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group">';
                                      str+='<span class="input-group-addon">EN</span>';
                                      str+='<input type="text" class="form-control" name="office_en[]" placeholder="Chức vụ" value="">';
                                  str+='</div>';
                              str+='</div>';
                          str+='</div>';
                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group">';
                                      str+='<span class="input-group-addon">VI</span>';
                                      str+='<textarea type="text" name="title_vi[]" class="form-control" id="ckeditor"></textarea>';
                                  str+='</div>';
                              str+='</div>';
                          str+='</div>';
                          str+='<div class="form-group">';
                              str+='<label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>';
                              str+='<div class="col-sm-10">';
                                  str+='<div class="input-group">';
                                      str+='<span class="input-group-addon">EN</span>';
                                      str+='<textarea type="text" name="title_en[]" class="form-control" id="ckeditor"></textarea>';
                                  str+='</div>';
                              str+='</div>';
                          str+='</div>';
                          str+='<div class="form-group">';
                            str+='<label class="control-label col-sm-2" for="email">Mô tả thêm(*):</label>';
                            str+='<div class="col-sm-10">';
                                str+='<div class="fileinput fileinput-new image_teachers_group" data-provides="fileinput" style="display:unset!important;">';
                                  str+='<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">';
                                      // str+='<img src="<?php echo e(url('public/web/images')); ?>/logo.png" alt="">';
                                  str+='</div>';
                                  str+='<div style="text-align: center">';
                                    str+='<span class="btn red btn-outline btn-file" style="background: #1f81ff !important; ">';
                                    str+='<span class="fileinput-new" > Chọn ảnh </span>';
                                    str+='<span class="fileinput-exists"> Đổi ảnh </span>';
                                      str+='<input type="file" name="upload[]" class="upload">';
                                      str+='<input type="hidden" name="image[]" class="image_teachers">';
                                    str+='</span>';
                                    str+='<a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Xóa ảnh </a>';
                                  str+='</div>';
                                    str+='<span class="text-danger image_teachers_group" style="display: none" role="alert">';
                                      str+='Vui lòng thêm ảnh';
                                    str+='</span>';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                      str+='</div><hr>';
                      str+='</div>';
                    $(wrapper).append(str);
              $(".upload").change(function(e){
                 var fileName = e.target.files[0].name;
                    $(this).next().val(fileName);
              
          });
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();
         });

       $(".upload").change(function(e){
                 var fileName = e.target.files[0].name;
                    $(this).next().val(fileName);
              
          });

          
     });
        
  </script>           
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>