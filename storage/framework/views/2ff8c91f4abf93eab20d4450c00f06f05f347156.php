<?php $__env->startSection('title','Clb Trường Học'); ?>
<?php $__env->startSection('content'); ?>
    <main style="width: 75vw;margin:0 auto">
        <div class="my-5">
            <div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-3" >
                                <div class="style_box_filter">
                                    <form action="" method="get" accept-charset="utf-8">
                                        <div class="form-group">
                                            <label for="">Loại khoá học</label>
                                            <select name="type_course_id" id="input" class="form-control">
                                                <option value="">-- Loại khoá học --</option>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Khoảng học phí</label >
                                            <select  name="sale_price" id="sortButton" class="form-control">
                                                <option <?php if(request()->sale_price == ''): ?> selected <?php endif; ?> class="sort" value="">Default</option>
                                                <option <?php if(request()->sale_price == '0-100000'): ?> selected <?php endif; ?> class="sort" value="0-100000">0 -> 100.000đ</option>
                                                <option <?php if(request()->sale_price == '100000-200000'): ?> selected <?php endif; ?> class="sort" value="100000-200000">100.000đ -> 200.000đ</option>
                                                <option <?php if(request()->sale_price == '200000-400000'): ?> selected <?php endif; ?> class="sort" value="200000-400000">200.000đ -> 400.000đ</option>
                                                <option <?php if(request()->sale_price == '400000-800000'): ?> selected <?php endif; ?> class="sort" value="400000-800000">400.000đ -> 800.000đ</option>
                                                <option <?php if(request()->sale_price == '800000-1000000'): ?> selected <?php endif; ?> class="sort" value="800000-1000000">800.000 -> 1000.000</option>
                                                <option <?php if(request()->sale_price == '1000000-100000000000000000'): ?> selected <?php endif; ?> class="sort" value="1000000-100000000000000000">Từ 1 triệu trở lên</option>
                                            </select>
                                        </div>
                                        
                                        
                                        <input type="hidden" name="limit" value="<?php echo e(request()->limit); ?>">
                                        <button type="submit" class="btn w-100" style="background: #5CC2A8; color: #fff; font-weight: 400;text-transform: uppercase">Lọc kết quả</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                                           
                                <div class="row">

                                    <section class="boxfilters">
                                        <?php $__currentLoopData = $course; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-12 col-sm-6 col-md-4 mb-4 style_padding_box ">
                                                <article class="boxfilter boxfilter--1 ">
                                                    <div class="boxfilter__info-hover" >
                                                        <div class="" style="height: 100%">
                                                            <div class="Boxinfo Boxinfo-100">
                                                                <div class="Boxinfo-content">
                                                                    <div class="Boxinfo-body">
                                                                        <p>
                                                                            <i class="fal fa-university"></i> Đơn vị tổ chức: <?php echo e($prod->userCourses['name']); ?>

                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-users"></i> Lớp tiêu chuẩn: <?php echo e($prod['number_student']); ?> học sinh
                                                                        </p>
                                                                        <p>
                                                                           <i class="fal fa-usd-circle"></i> Học phí: <?php echo e(number_format($prod['price'])); ?>đ/khoá
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-paper-plane"></i> Điều kiện học: <?php echo e($prod['study_condition']); ?>

                                                                        </p>
                                                                    </div>
                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                        <form action="<?php echo e(url('/chi-tiet')); ?>/<?php echo e($prod->slug); ?>" method="get" role="form">
                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="boxfilter__img"></div>
                                                    <a href="#" class="boxfilter_link">
                                                        <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($prod->image); ?>') ">
                                                            <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                        </div>
                                                    </a>
                                                    <div class="clearfix">
                                                    </div>
                                                    <div class="boxfilter__info boxfilter__info1">
                                                        <p>
                                                            <i class="fal fa-clock"></i> Thời lượng : <?php echo e($prod->number_lesson); ?> buổi
                                                            
                                                        </p>
                                                        <p>
                                                            
                                                            
                                                        </p>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </section>
                                </div>
                                <?php if($course_all->count() >= 6 && $course_all->count() >= request()->limit): ?>
                                    <div class="mb-5">
                                        <a class="btn btn-success" href=""
                                        onclick="event.preventDefault();
                                                         document.getElementById('limit_id').submit();">
                                            Xem thêm
                                        </a>
                                        <form id="limit_id" action="" method="get" style="display: none;">
                                            <input type="hidden" name="sale_price" value="<?php echo e(request()->sale_price); ?>">
                                            <input type="hidden" name="address1" value="<?php echo e(request()->address1); ?>">
                                            <input type="hidden" name="address2" value="<?php echo e(request()->address2); ?>">
                                            <input type="hidden" name="limit" value="<?php echo e(request()->limit != null ? request()->limit + 6 : 12); ?>">
                                        </form>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script>
    $(document).on("change","#ThanhPho", function(event) {
      var id_matp = $(this).val();
      $.get("<?php echo e(url('')); ?>/boot/ajax/quanhuyen/"+id_matp,function(data) {
        console.log(data);
        $("#QuanHuyen").html(data);
      });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>