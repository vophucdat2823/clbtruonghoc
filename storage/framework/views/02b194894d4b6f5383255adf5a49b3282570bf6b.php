<?php $__env->startSection('content'); ?>

<main>
    <div id="news_even_details">
        <div class="news_even_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue"><a href="#">HOME > </a></span><span class="color_blue"><a href="#">NEWS & EVENTS > </a></span><span>Even3</span>
                        </div>
                    </div>
                </div>
        </div>

        <div class="content_news_even_details">
            <div class="container">
                    <div class="row">
                        <!-- phần left content -->
                        <div class="col-md-4 col-sm-12 col-12">
                            <h2>CATEGORIES</h2>
                            <ul class="nav nav-pills" id="menu">
                                <li class="nav-item">
                                    <a class="nav_menu active" data-toggle="collapse" href="#sub_menu_1"><img src="images/icon_square.png" alt="" width="4%"><span>NOW@VNU-IS</span></a>
                                    <div id="sub_menu_1" class="collapse break">
                                        <ul class="nav">
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sp_content_1"><i class="fas fa-caret-right"></i>EVENT 1</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sp_content_2"><i class="fas fa-caret-right"></i>EVENT 2</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sp_content_3"><i class="fas fa-caret-right"></i>EVENT 3</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sp_content_4"><i class="fas fa-caret-right"></i>EVENT 4</a></li>
                                        </ul>
                                    </div>  
                                </li>
                                <li class="nav-item">
                                    <a class="nav_menu" data-toggle="collapse" href="#sub_menu_2"><img src="images/icon_square.png" alt="" width="4%"><span>SAVE THE DATE</span></a>
                                    <div id="sub_menu_2" class="collapse break">
                                        <ul class="nav">
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dx_content_1"><i class="fas fa-caret-right"></i>EVENT 1</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dx_content_2"><i class="fas fa-caret-right"></i>EVENT 2</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dx_content_3"><i class="fas fa-caret-right"></i>EVENT 3</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dx_content_4"><i class="fas fa-caret-right"></i>EVENT 4</a></li>
                                        </ul>
                                    </div> 
                                </li>
                                <li class="nav-item">
                                    <a class="nav_menu" data-toggle="collapse" href="#sub_menu_3"><img src="images/icon_square.png" alt="" width="4%"><span>ARCHIVE</span></a>
                                    <div id="sub_menu_3" class="collapse break">
                                        <ul class="nav">
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#g_content_1"><i class="fas fa-caret-right"></i>EVENT 1</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#g_content_2"><i class="fas fa-caret-right"></i>EVENT 2</a></li>
                                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#g_content_3"><i class="fas fa-caret-right"></i>EVENT 3</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>


                            <div class="insta_pp">
                                    <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/icon_square_xam.svg" alt="" width="40%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <h5 class="color_yellow">Instagram</h5>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                                <img src="images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                                <img src="images/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                            </div>

                            <div class="tag_pp">
                                    <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/icon_square_xam.svg" alt="" width="40%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <h5 class="color_yellow">Tags</h5>
                                            </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                        </div>
                                    </div>
                                </div>

                        </div>


                        <!-- phần right content -->
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="tab-content">
                                <div id="sp_content_1" class="container tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="main_content_news_even_details">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-12">
                                                        <div class="tt_top_main_even_details">
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <img src="images/squares.svg" alt="" width="18%">
                                                                </div>

                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <h3 class="color_blue">EVENT 3</h3>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="body_main_even_details">
                                                            <p>To succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.</p>
                                                            <img src="images/news_event/nhac.jpg" alt="" width="100%">
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- more info -->
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-12">
                                                        <div class="more_info_youtube_h">
                                                            <h3>more infomation</h3>
                                                            <p>Metallica là ban nhạc thrash metal của Mỹ, thành lập ngày 28 tháng 10 năm 1981. Với hơn 100 triệu album bán ra toàn thế giới, riêng ở Mỹ là 57 triệu album, đây là ban nhạc heavy metal thành công nhất về mặt thương mại trong lịch sử. Wikipedia</p>
                                                            <span>Thành viên: James Hetfield, Lars Ulrich, Kirk Hammett, THÊM</span>
                                                            <span>Các thể loại: Nhạc Kim loại Nặng, Nhạc Thrash Metal</span>
                                                            <span>Giải thưởng: Giải Grammy cho Trình diễn Metal xuất sắc nhất</span>

                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                    <div class="video_youtube_student_service">
                                                                        <div class="content_video_youtube_student_service">
                                                                            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/snNFMlFmyzg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                                                        </div> 
                                                                        <div class="txt_content_video_youtube">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                                    <span>3:54</span>
                                                                                    <span>IShuffle</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                    <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                                                </div>
                                                                            </div>
                                                                        </div>   
                                                                    </div>      
                                                                </div>

                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                        <div class="video_youtube_student_service">
                                                                            <div class="content_video_youtube_student_service">
                                                                                <iframe width="100%" height="auto" src="https://www.youtube.com/embed/snNFMlFmyzg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                                                            </div> 
                                                                            <div class="txt_content_video_youtube">
                                                                                <div class="row">
                                                                                    <div class="col-md-8 col-sm-8 col-8">
                                                                                        <span>Rihanna - Don't Stop The Music</span>
                                                                                        <span>3:54</span>
                                                                                        <span>IShuffle</span>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                        <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                                                    </div>
                                                                                </div>
                                                                            </div>   
                                                                        </div>      
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sp_content_2" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            b
                                        </div>
                                    </div>
                                </div>
                                <div id="sp_content_3" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            hh
                                        </div>
                                    </div>
                                </div>
                                <div id="dx_content_1" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            c
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        
    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>