<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" src="<?php echo e(url('public/admin')); ?>/img/logo.png" />
                    </span>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear">
                    <span class="block m-t-xs"> <strong class="font-bold">CMS Webify V1.10</strong>
                    </span>
                      <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Về Webify</a></li>
                        <li><a href="#">Liên hệ</a></li>
                        <li><a href="#">Hướng dẫn sử dụng</a></li>
                        
                      </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="<?php echo e(Request::is('admin/dashboard') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fa fa-tachometer"></i> <span class="nav-label">Trang chủ</span>  </a>
            </li>
            <li class="<?php echo e(Request::is('admin/list') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.menu.list')); ?>"><i class="fa fa-cubes"></i> <span class="nav-label">Menu</span>  </a>
            </li>
            
            <li class="<?php echo e(Request::is('admin/cate-course') ||Request::is('admin/course') || Request::is('admin/yeu-cau-khach-hang/khoa-hoc') || Request::is('admin/course/create') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Quản lý khóa học</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="<?php echo e(Request::is('admin/yeu-cau-khach-hang/khoa-hoc') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.send.indexKh')); ?>">Yêu cầu khách hàng</a></li>
                    <li class="<?php echo e(Request::is('admin/cate-course') ? 'active' : ''); ?>"><a href="<?php echo e(route('cate-course.index')); ?>">Danh mục</a></li>

                    <li class="<?php echo e(Request::is('admin/course') || Request::is('admin/course/create')? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Khóa học</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse <?php echo e(Request::is('admin/course') ? 'in' : ''); ?>" style="">
                            <li class="<?php echo e(Request::is('admin/course') ? 'active' : ''); ?>"><a href="<?php echo e(route('course.index')); ?>"> Danh Sách KH</a></li>
                            <li class="<?php echo e(Request::is('admin/course/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('course.create')); ?>">Thêm mới KH</a></li>
                        </ul>
                    </li>
                </ul>
            </li>    
            <li class="<?php echo e(Request::is('admin/cate-service') ||Request::is('admin/service') || Request::is('admin/yeu-cau-khach-hang/dich-vu') || Request::is('admin/service/create') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Quản lý dịch vụ</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="<?php echo e(Request::is('admin/yeu-cau-khach-hang/dich-vu') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.send.indexDv')); ?>">Yêu cầu khách hàng</a></li>
                    <li class="<?php echo e(Request::is('admin/cate-service') ? 'active' : ''); ?>"><a href="<?php echo e(route('cate-service.index')); ?>">Danh mục dịch vụ</a></li>

                    <li class="<?php echo e(Request::is('admin/service') || Request::is('admin/service/create')? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Dịch vụ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse <?php echo e(Request::is('admin/service') ? 'in' : ''); ?>" style="">
                            <li class="<?php echo e(Request::is('admin/service') ? 'active' : ''); ?>"><a href="<?php echo e(route('service.index')); ?>"> Danh Sách Dịch Vụ</a></li>
                            <li class="<?php echo e(Request::is('admin/service/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('service.create')); ?>">Thêm mới Dịch Vụ</a></li>
                        </ul>
                    </li>
                </ul>
            </li>


            <li class="<?php echo e(Request::is('admin/cate-event') ||Request::is('admin/event') || Request::is('admin/event/create') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Sự Kiện</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="<?php echo e(Request::is('admin/cate-event') ? 'active' : ''); ?>"><a href="<?php echo e(route('cate-event.index')); ?>">Danh mục SK</a></li>

                    <li class="<?php echo e(Request::is('admin/event') || Request::is('admin/event/create')? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">QL Sự Kiện</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse <?php echo e(Request::is('admin/event') ? 'in' : ''); ?>" style="">
                            <li class="<?php echo e(Request::is('admin/event') ? 'active' : ''); ?>"><a href="<?php echo e(route('event.index')); ?>"> Danh sách SK</a></li>
                            <li class="<?php echo e(Request::is('admin/event/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('event.create')); ?>">Thêm mới SK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('admin/cate-bustle') ||Request::is('admin/bustle') || Request::is('admin/bustle/create') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-university"></i> <span class="nav-label">CLB_Trường Học</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="<?php echo e(Request::is('admin/cate-bustle') ? 'active' : ''); ?>"><a href="<?php echo e(route('cate-bustle.index')); ?>">Danh mục CLB</a></li>

                    <li class="<?php echo e(Request::is('admin/bustle') || Request::is('admin/bustle/create')? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">QL Câu Lạc Bộ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse <?php echo e(Request::is('admin/bustle') ? 'in' : ''); ?>" style="">
                            <li class="<?php echo e(Request::is('admin/bustle') ? 'active' : ''); ?>"><a href="<?php echo e(route('bustle.index')); ?>"> Danh sách CLB</a></li>
                            <li class="<?php echo e(Request::is('admin/bustle/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('bustle.create')); ?>">Thêm mới SK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            


            <li class="<?php echo e(Request::is('admin/box-link') || Request::is('admin/banner') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-sliders"></i>  <span class="nav-label">QL Giao diện</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    

                    <li class="<?php echo e(Request::is('admin/box-link') || Request::is('admin/banner') ? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Trang chủ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse <?php echo e(Request::is('admin/event') ? 'in' : ''); ?>" style="">
                            <li class="<?php echo e(Request::is('admin/box-link') ? 'active' : ''); ?>"><a href="<?php echo e(route('box-link.index')); ?>"> Box Link</a></li>
                            <li class="<?php echo e(Request::is('admin/banner') ? 'active' : ''); ?>"><a href="<?php echo e(route('banner.index')); ?>"> Banner</a></li>
                            
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="<?php echo e(Request::is('admin/account') || Request::is('admin/account/create') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">QL Tài Khoản</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/account') || Request::is('admin/account/create') ? 'active' : ''); ?>">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">TK DV_Khóa Học</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li ><a href="<?php echo e(route('account.index')); ?>">-- Danh sách TK</a></li>
                            <li class=""><a href="<?php echo e(route('account.create')); ?>">-- Thêm mới TK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('admin/email-newsletter') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('email_newsletter.index')); ?>"><i class="fa fa-envelope"></i> <span class="nav-label">Email nhận bản tin</span>  </a>
            </li>
            
            
            
        </ul>

    </div>
    </nav>