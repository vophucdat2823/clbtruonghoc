<?php $__env->startSection('title',$course->name); ?>
<?php $__env->startSection('style'); ?>





    <link href="<?php echo e(url('public/admin/css/plugins/dropzone/basic.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/dropzone/dropzone.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/jasny/jasny-bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/codemirror/codemirror.css')); ?>" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Cung cấp dịch vụ</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Cung cấp dịch vụ</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('course-partner.update',['id'=>$course->id])); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin dịch vụ</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên dịch vụ(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" id="name" class="form-control" disabled placeholder="Tên dịch vụ..." value="<?php echo e($course->name); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Đơn vị cung cấp(*):</label>
                                      <div class="col-sm-10">
                                            <select name="unit_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("unit",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Giá thành(*):</label>
                                      <div class="col-sm-10">
                                            <select name="price_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("price",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Chất lượng(*):</label>
                                      <div class="col-sm-10">
                                            <select name="quality_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("quality",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Thời lượng(*):</label>
                                      <div class="col-sm-10">
                                            <select name="time_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("time",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Số lượng(*):</label>
                                      <div class="col-sm-10">
                                            <select name="qty_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("qty",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Địa điểm(*):</label>
                                      <div class="col-sm-10">
                                            <select name="address_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("address",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Học phí(*):</label>
                                      <div class="col-sm-10">
                                            <select name="tuition_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                <?php $__currentLoopData = $course->getTypeCourse_id("tuition",$course->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>


    <?php $__env->startSection('scripts'); ?>
    <!-- Chosen -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

    <!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">
        $('.chosen-select').chosen({width: "100%"});
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_2.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>