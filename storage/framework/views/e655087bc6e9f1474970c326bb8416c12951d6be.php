<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    $menu = \App\Models\Admin\Menu::getAllMenu();
    //dd($language);
    ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                            <?php if(session('success')): ?>
                                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('success')); ?>

                                </div>
                            <?php endif; ?>
                            <?php if(session('error')): ?>
                                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo e(session('error')); ?>

                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-1">STT</th>
                                    <th class="col-md-3">Tên sản phẩm</th>
                                    <th class="col-md-4">Mô phỏng</th>
                                    <th class="col-md-2">Thuộc menu</th>
                                    <th class="col-md-2">
                                        <a href="<?php echo e(route('admin.tab_event.create')); ?>" type="button" class="btn btn-block btn-info">
                                            Thêm Mới
                                            <i class="fa fa-fw fa-plus-square"></i>
                                        </a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($eventTabs)): ?>
                                    <?php $__currentLoopData = $eventTabs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $eventTab): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(1+$key++); ?></td>
                                    <td><?php echo e($eventTab['name_vi']); ?></td>
                                    <td><?php echo e(str_limit($eventTab['title_vi'],100)); ?></td>
                                    <td><img src="<?php echo e($eventTab['image']); ?>" width="150"></td>
                                    <td>
                                        <a href="<?php echo e(route('admin.tab_event.edit', $eventTab['id'])); ?>" type="button" class="btn btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                data-target="#modal-delete-<?php echo e($eventTab['id']); ?>">
                                            <i class="fa fa-fw fa-trash-o"></i>
                                        </button>

                                        <div class="modal modal-danger fade" id="modal-delete-<?php echo e($eventTab['id']); ?>">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Bạn có chắc chắn muốn xóa bài viết này ?</h4>
                                                    </div>
                                                    <div class="modal-body" style="background: white!important;">
                                                        <form method="get" action="<?php echo e(route('admin.tab_event.destroy', $eventTab['id'])); ?>">
                                                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Hủy bỏ
                                                                <i class="fa fa-fw fa-close"></i>
                                                            </button>
                                                            <button type="submit" class="btn btn-success">Xác nhận
                                                                <i class="fa fa-save"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>