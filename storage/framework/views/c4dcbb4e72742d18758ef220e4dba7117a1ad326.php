<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    //dd($language);
    ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm danh mục</h5>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.category.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Tên danh mục(vietnam):</label> 
                                                <input type="text" class="form-control" name="name_vi" placeholder="Tên tab danh mục tiếng việt" value="<?php echo e(old('name_vi')); ?>" style="margin-bottom: 5px">
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <input type="text" id="name" class="form-control" name="name_en" placeholder="Tên tab danh mục tiếng Anh" value="<?php echo e(old('name_en')); ?>">
                                                <?php if($errors->has('name_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <br><span>Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh</label> 
                                                
                                                <input type="text" id="slug" name="slug" value="<?php echo e(old('slug')); ?>" class="form-control">
                                                 <?php if($errors->has('slug')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('slug')); ?>

                                                    </span><br>
                                                <?php endif; ?>
                                                <span>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>

                                            <div class="form-group"><label>Chọn danh mục</label>
                                                    <select name="parents_cate" id="page_hien_thi" class="form-control">
                                                        
                                                        <option value="0">-- ROOT -- </option>
                                                        <?php echo e(showCategoryies($catealls)); ?>

                                                    </select>
                                                    <?php if($errors->has('parents_cate')): ?>
                                                        <span class="text-center text-danger" role="alert">
                                                            <?php echo e($errors->first('parents_cate')); ?>

                                                        </span><br>
                                                    <?php endif; ?>
                                                  
                                                   
                                                    
                                                <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn.</span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Chọn giao diện</label>
                                                <div id="load_cate">
                                                    <select class="form-control m-b"  name="page_id">
                                                        <option value="" redonly>-- Chọn trang hiển thị</option>
                                                    </select>
                                                </div>
                                                <?php if($errors->has('page_id')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('page_id')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group"><label>Mô tả</label>
                                                <textarea type="text" class="form-control" name="title_vi"  style="margin-bottom: 5px" placeholder="Mô phỏng tab sự kiện tiếng Việt" maxlength="255"><?php echo e(old('title_vi')); ?></textarea>
                                                <?php if($errors->has('title_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_vi')); ?>

                                                    </span>
                                                <?php endif; ?>

                                                <textarea type="text" class="form-control" name="title_en" placeholder="Mô phỏng tab sự kiện tiếng Anh" maxlength="255"><?php echo e(old('title_en')); ?></textarea>
                                                <?php if($errors->has('title_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div id="image-diplay">
                                               <div class="form-group">
                                                <label>Chọn ảnh hiển thị:</label>
                                                   <div class="col-md-12">
                                                       <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                           <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                                style="width: 100%; height: 250px;">
                                                           </div>
                                                           <div style="text-align: center">
                                                                <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                    <span class="fileinput-new"> Chọn ảnh </span>
                                                                    <span class="fileinput-exists"> Đổi ảnh </span>
                                                                    <input type="file" name="image">
                                                                </span>
                                                               <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                  data-dismiss="fileinput"> Xóa ảnh </a>
                                                               
                                                           </div>
                                                           <span class="text-center text-danger" role="alert">
                                                                <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                            </span>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Tất cả danh mục</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th class="col-md-1">STT</th>
                                                        <th class="col-md-3">Tên danh mục</th>
                                                        <th class="col-md-4">Mô tả</th>
                                                        <th class="col-md-2">Đường dẫn tĩnh</th>
                                                        <th class="col-md-2">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($categorys)): ?>
                                                        <?php $__currentLoopData = $categorys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <tr>
                                                        
                                                        <td><?php echo e($key+1); ?></td>
                                                        <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>.html" target="_blank"> <span class="tag-post"><?php echo e($cate->name_vi); ?></span></a></td>
                                                        <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>.html" target="_blank"> <span class="tag-post-tacgia"><?php echo e(str_limit($cate->title_vi,50)); ?></span></a></td>
                                                        
                                                        <td><a href="<?php echo e(url('')); ?>/<?php echo e($cate->slug); ?>.html" target="_blank"> <span class="tag-post-tacgia"><?php echo e(str_limit($cate->slug,30)); ?></span></a></td>

                                                        <td>
                                                            <a href="<?php echo e(route('admin.category.edit', $cate->id)); ?>" type="button" class="btn btn-info">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                    data-target="#modal-delete-<?php echo e($cate->id); ?>">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                            <div class="modal modal-danger fade" id="modal-delete-<?php echo e($cate->id); ?>">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span></button>
                                                                                <?php 
                                                                                    $delete = \App\Models\Admin\PostTranslation::where('category_id',$cate->id)->get();
                                                                                 ?>
                                                                                <?php if($delete->count() == 0): ?>
                                                                                    <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                <?php else: ?>
                                                                                    <h4 class="modal-title">
                                                                                   Danh mục hiện tại đang có "<?php echo e($delete->count()); ?>" bài viết!
                                                                                   (không thể xóa)
                                                                                    </h4>
                                                                                <?php endif; ?>
                                                                            
                                                                        </div>
                                                                        <div class="modal-body" style="background: white!important; text-align: center" ">
                                                                            <form method="get" action="<?php echo e(route('admin.category.destroy', $cate->id)); ?>">
                                                                                <?php if($delete->count() == 0): ?>
                                                                                    <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                    <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                        <i class="fa fa-save"></i>
                                                                                    </button>
                                                                                <?php else: ?>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                        <i class="fa fa-fw fa-close"></i>
                                                                                    </button>
                                                                                <?php endif; ?>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                            <?php echo e($categorys->links()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php 

      function showCategoryies($catealls, $parent = 0, $char ='')
      {
        $cate_child = array();
        foreach ($catealls as $key => $item) {
          if ($item->parents == $parent) 
          {
            $cate_child[] = $item;
            unset($catealls[$key]);
          }
        }
        if ($cate_child) {
          foreach ($cate_child as $key => $item) {

            echo '<option value="'.$item->id.'" >'.$char.$item->name_en.'</option>';
            showCategoryies($catealls ,$item->id , $char.'|---');
            echo "</li>";
          }
        }
      }



    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<script>

  $("#page_hien_thi").change(function() {
      
      var gia_tri = $(this).val();
      if(gia_tri == ''){
        alert('Vui lòng chọn trang hiển thị');
      }else {
          $.ajax({
           method:'get',
           url:"<?php echo e(url('')); ?>/admin/category/chon-danh-muc/"+gia_tri,
           dataType: 'json',
          }).done(function(data){
              console.log(data);
              $('#load_cate').load(location.href + " #load_cate>*");
          }).fail(function(error){
            console.log(error.responseText);
             $('#load_cate').html(error.responseText);
            // $('#load_cate').load(location.href + " #load_cate>*");
          });
          }
    
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>