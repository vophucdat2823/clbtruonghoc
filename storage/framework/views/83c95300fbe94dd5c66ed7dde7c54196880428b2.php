<?php $__env->startSection('title','details-pro'); ?>
<?php $__env->startSection('content'); ?>

<main>
    <div class="w-100 background_div">
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-5 pb-5">
            <div class="col-lg-12 p-2 bg-white box-details-pro-header">
                <div class="row">
                    <div class="col-lg-4 pr-0 box-details-images">
                        <div class="images-product-big">
                            <img src="<?php echo e(url('public/upload/images/'.$details['image'])); ?>" alt="">
                        </div>
                        <div class="list-images-product">
                            
                        </div>
                    </div>
                    <div class="col-lg-8 pl-0 box-derestion-pro">
                        <div style="border-bottom: 1px solid #ececec;padding: 0 10px">
                            <h1><?php echo e($details->name); ?></h1>
                            <div class="info-thuonghieu">
                                <p>
                                    <span style="font-weight: 600;">Thương hiệu: </span><span style="color: #29ABE2;">Lock&Lock</span>
                                    <span> - </span>
                                    <span style="color: #999999;">SKU:  3312485690773</span>
                                </p>
                            </div>
                        </div>
                        <div style="padding: 0 10px">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="info-price">
                                        <p>Giá: <span style="font-size: 28px">279.000 ₫</span></p>
                                        <p>Tiết kiệm: <span style="font-size: 28px">50%</span> (280.000 ₫)</p>
                                        <p>Giá thị trường: 559.000 ₫</p>
                                    </div>
                                    <div class="tomtat">
                                        <p>
                                            Chất liệu thép không gỉ, bền chắc, chịu nhiệt tốt <br/>
                                            Giữ nóng 45 độ C trong vòng 24 giờ, giữ lạnh 13 độ C trong vòng 6 giờ<br/>
                                            Thiết kế nhỏ gọn, xinh xắn, tiện lợi
                                        </p>
                                    </div>
                                    <div class="add-to-cart">
                                        
                                        <div class="quantity">
                                            <button class="plus-btn" id="btnMinus" type="button" name="button">
                                                <img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-minus-512.png" alt="">
                                            </button>
                                            <input type="text" id="txtQuantity" name="name" value="1">
                                            <button class="minus-btn" type="button" name="button">
                                                <img src="http://simpleicon.com/wp-content/uploads/plus.svg" alt="">
                                            </button>
                                        </div>
                                        <button class="addcart">Thêm vào giỏ hàng</button>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="items-prima">
                                        asdasd
                                    </div>
                                    <div class="items-prima">
                                        asdasd
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-1 pb-1">
            <h5>MÔ TẢ SẢN PHẨM</h5>
            <div class="col-lg-12 p-2 bg-white box-mota">
                <div class="row">
                    <div class="col-lg-9 border-right">
                        <div class="motasanpham">
                            <?php echo e($details->description); ?>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-1 pb-1">
            <div class="col-lg-12 p-2 bg-white box-mota">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
$(document).ready(function(){
    var count = 1;
    $("#txtQuantity").val(count);
    $("#btnMinus").click(function(){
        $("#txtQuantity").val(count--);
    });
    $(".minus-btn").click(function(){
        $("#txtQuantity").val(count++);
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>