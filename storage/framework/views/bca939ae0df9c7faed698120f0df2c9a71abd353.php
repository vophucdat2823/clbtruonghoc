<footer class="footer_mobie">
    <div class="footer">
        <div style="width: 100%">
            <div class="col-12 col-md-12 col-sm-12 col-12">
                <div class="get_in_touch_footer">
                    <h3><?php echo e($language_id == 'en' ? 'get in touch' : 'Thông tin liên lạc'); ?> </h3>
                    <div class="home_get_in_touch_footer col-12">
                        <div class="row">
                            <div style="width: 10%">
                                <i class="fas fa-home color_yellow"></i>	
                            </div>
                            <div style="width: 90%">
                                <div class="content_right_home_get_in_touch_footer">
                                    <div>
                                        <div class="col-md-12">
                                            <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 1:</strong> 
                                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($cd->key_option == 'address1'): ?>
                                                    <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </span>
                                        </div>
                                        <div class="col-md-12">
                                            <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 2:</strong> 
                                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($cd->key_option == 'address2'): ?>
                                                    <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </span>
                                        </div>
                                        <div class="col-md-12">
                                            <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 3:</strong> 
                                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($cd->key_option == 'address3'): ?>
                                                    <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="phone_get_in_touch_footer">
                        <div class="container">
                            <div class="row">
                                <div style="width: 10%">
                                    <i class="fas fa-phone-volume color_yellow"></i>
                                </div>
                                <style type="text/css">
                                    .phone_style p{
                                        margin-bottom: 0;
                                        line-height: unset !important;
                                    }
                                </style>
                                <div style="width: 90%">
                                    <div class="container">
                                        <div>
                                            <div style="width: 100%">
                                                <div class="phone_left_phone_get_in_touch_footer phone_style row">
                                                    <div class="col-sm-6 phone_border_right" >
                                                        <p>
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($cd->key_option == 'phone3'): ?>
                                                                    <?php echo e($cd->value); ?>

                                                                <?php endif; ?>
                                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </p>
                                                        <p> <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($cd->key_option == 'phone4'): ?>
                                                                    <?php echo e($cd->value); ?>

                                                                <?php endif; ?>
                                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></p>
                                                        <p>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p>
                                                            <strong class="color_yellow">
                                                                <?php echo e($language_id == 'vi' ? 'Đại học' : 'University'); ?>: 
                                                            </strong>
                                                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($cd->key_option == 'phone5'): ?>
                                                                        <?php echo e($cd->value); ?>

                                                                    <?php endif; ?>
                                                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                                 | 
                                                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($cd->key_option == 'phone6'): ?>
                                                                        <?php echo e($cd->value); ?>

                                                                    <?php endif; ?>
                                                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </p>
                                                        <p>
                                                            <strong class="color_yellow">
                                                            <?php echo e($language_id == 'vi' ? 'Sau đại học' : 'After university'); ?>: 
                                                            </strong>
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($cd->key_option == 'phone7'): ?>
                                                                    <?php echo e($cd->value); ?>

                                                                <?php endif; ?>
                                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mail_get_in_touch_footer">
                        <div class="container">
                            <div class="row">
                                <div style="width: 10%">
                                    <i class="fas fa-envelope color_yellow"></i>
                                </div>
                                <div style="width: 90%">
                                    <div class="col-md-7 txt_mark_banner">
                                        <span >
                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($cd->key_option == 'email2'): ?>
                                                <?php echo e($cd->value); ?>

                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="socical_about_us_footer col-12" style="margin-top: 5vh;">
                    <ul class="list-inline" style="width: 100%;display: flex;">
                        <?php $__currentLoopData = $icon_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $icon_ft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="list-inline-item" style="width: 25%;text-align: center"><a href="<?php echo e($icon_ft->link); ?>"><img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($icon_ft->image); ?>" alt="" style="width: 75%"></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
        </div>
    </div>
</footer>



<footer class="footer_pc"> 
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="about_us_footer">
                        <h3><?php echo e($language_id == 'vi' ? $intro->label_vi : $intro->label_en); ?></h3>
                        <p style="text-align: justify"><?php echo e($language_id == 'vi' ? $intro->title_vi : $intro->title_en); ?></p>

                        <div class="socical_about_us_footer">
                            <ul>
                                <?php $__currentLoopData = $icon_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $icon_ft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e($icon_ft->link); ?>"><img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($icon_ft->image); ?>" alt="" width="90%"></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="quick_link_footer">
                        <h3><?php echo e($language_id == 'en' ? 'quick links' : 'Truy cập nhanh'); ?> </h3>
                        <ul>
                            <?php $__currentLoopData = $list_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list_ft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li style="text-align: justify"><a href="<?php echo e($list_ft->link); ?>"><i class="fas fa-angle-right color_yellow"></i><?php echo e($language_id == 'vi' ? $list_ft->label_vi : $list_ft->label_en); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-12">
                    <div class="get_in_touch_footer">
                        <h3><?php echo e($language_id == 'en' ? 'get in touch' : 'Thông tin liên lạc'); ?> </h3>
                        <div class="home_get_in_touch_footer">
                            <div class="row">
                                <div class="col-xs-1">
                                    <i class="fas fa-home color_yellow"></i>    
                                </div>
                                <div class="col-sm-11">
                                    <div class="content_right_home_get_in_touch_footer">
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 1:</strong> 
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address1'): ?>
                                                                <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 2:</strong> 
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address2'): ?>
                                                                <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                        </span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow"><?php echo e($language_id == 'vi' ? 'Cơ sở' : 'Basis'); ?> 3:</strong> 
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address3'): ?>
                                                                <?php echo e($language_id == 'vi' ? $cd->value : $cd->value_en); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                        </span>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="phone_get_in_touch_footer">
                            <div class="row">
                                <div class="col-xs-1">
                                    <i class="fas fa-phone-volume color_yellow"></i>
                                </div>
                                <style type="text/css">
                                    .phone_style p{
                                        margin-bottom: 0;
                                        line-height: unset !important;
                                    }
                                </style>
                                <div class="col-sm-11">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="phone_left_phone_get_in_touch_footer phone_style">
                                                <p>
                                                    <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone3'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                                <p> <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone4'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="phone_right_phone_get_in_touch_footer phone_style">
                                                <p>
                                                    <strong class="color_yellow">
                                                        <?php echo e($language_id == 'vi' ? 'Đại học' : 'University'); ?>: 
                                                    </strong>
                                                        <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'phone5'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                         | 
                                                        <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'phone6'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                                <p>
                                                    <strong class="color_yellow">
                                                    <?php echo e($language_id == 'vi' ? 'Sau đại học' : 'After university'); ?>: 
                                                    </strong>
                                                    <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone7'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="mail_get_in_touch_footer">
                            <div class="row">
                                <div class="col-xs-1">
                                    <i class="fas fa-envelope color_yellow"></i>
                                </div>
                                <div class="col-sm-11">
                                        <span>
                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($cd->key_option == 'email2'): ?>
                                                <?php echo e($cd->value); ?>

                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>