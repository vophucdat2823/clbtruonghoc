<?php $__env->startSection('content'); ?>

<main>
    <div id="news_even">
        <div class="news_even_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue"><a href="#">HOME > </a></span><span class="tt_news_even_h">NEWS & EVENTS</span>
                        </div>
                    </div>
                </div>
        </div>

        <div class="content_news_even">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                        <div class="col_content_h">
                            <div class="vnu_is_content">
                                <div class="top_vnu_is_content">
                                    <h5>Now@VNU-IS</h5>
                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                                <div class="body_vnu_is_content">
                                    <div class="img_body_content">
                                        <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                    </div>
                                    <ul>
                                        <li>
                                            <strong>EVENT 1:</strong>
                                            <a href="#">Department of Foudation</a> 
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </li>
                                        <li>
                                            <strong>EVENT 1:</strong>
                                            <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </li>
                                        <li>
                                            <strong>EVENT 1:</strong>
                                            <a href="#">department of Foudation</a> 
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </li>
                                        <li>
                                            <strong>EVENT 1:</strong>
                                            <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bottom_more_vnu_is_content">
                                    <a href="#"></a>
                                </div>
                                <div class="more_content_h">
                                        <a href="#">MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                            <div class="col_content_h">
                                <div class="vnu_is_content">
                                    <div class="top_vnu_is_content">
                                        <h5>Save the Date</h5>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                    <div class="body_vnu_is_content">
                                        <div class="img_body_content">
                                            <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                        </div>
                                        <ul>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Foudation</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">department of Foudation</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bottom_more_vnu_is_content">
                                        <a href="#"></a>
                                    </div>
                                    <div class="more_content_h">
                                            <a href="#">MORE</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                            <div class="col_content_h">
                                <div class="vnu_is_content">
                                    <div class="top_vnu_is_content">
                                        <h5>Archive</h5>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                    <div class="body_vnu_is_content">
                                        <div class="img_body_content">
                                                <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                        </div>
                                        <ul>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Foudation</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">department of Foudation</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                            <li>
                                                <strong>EVENT 1:</strong>
                                                <a href="#">Department of Social Sciences, Economics and Manangement</a> 
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bottom_more_vnu_is_content">
                                        <a href="#"></a>
                                    </div>
                                    <div class="more_content_h">
                                            <a href="#">MORE</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>