<?php $__env->startSection('content'); ?>
<?php 
    $language_id = 'vi';
    if(Session::has('set_language')){
        $language_id = Session::get('set_language');
    }
    
 ?>

    <main style="margin-bottom: 5%;">
        <div id="about_us">
            <div class="about_top_title">
                <div class="container">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 dell-end-a">
                            <a href="<?php echo e(route('web.home')); ?>">
                                <span class="color_blue">
                                    <?php if($language_id == 'vi'): ?>
                                    Trang chủ
                                    <?php elseif($language_id == 'en'): ?>
                                    HOME
                                    <?php endif; ?> > 
                                </span>
                            </a>
                            <span class="color_gray" style="text-decoration: none;text-transform: uppercase;">
                                <?php echo e($language_id == 'vi' ? 'Thay đổi mật khẩu' : 'Change the password'); ?>

                            </span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="content_about_us">
                <div class="container mt-3">
                    <h2><?php echo e($language_id == 'vi' ? 'Thay đổi mật khẩu' : 'Change the password'); ?></h2>
                    <hr>
                    <div id="accordion">
                        <form action="<?php echo e(route('post_new_pass')); ?>" method="post" role="form">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="token_reset" value="<?php echo e($token); ?>">
                            <fieldset class="form-group">
                                <label for="formGroupExampleInput"><?php echo e($language_id == 'vi' ? 'Mật khẩu mới' : 'A new password'); ?></label>
                                <input type="password" class="form-control color_blue" name="password_first" id="formGroupExampleInput" placeholder="<?php echo e($language_id == 'vi' ? 'Mật khẩu mới' : 'A new password'); ?>">
                                <?php if($errors->has('password_first')): ?>
                                    <span class="text-center text-danger" role="alert">
                                        <?php echo e($errors->first('password_first')); ?>

                                    </span>
                                <?php endif; ?>
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="formGroupExampleInput2"><?php echo e($language_id == 'vi' ? 'Nhập lại mật khẩu mới' : 'Enter a new password'); ?></label>
                                <input type="password" class="form-control color_blue" name="password_last" id="formGroupExampleInput2" placeholder="<?php echo e($language_id == 'vi' ? 'Nhập lại mật khẩu mới' : 'Enter a new password'); ?>">

                                <?php if($errors->has('password_last')): ?>
                                    <span class="text-center text-danger" role="alert">
                                        <?php echo e($errors->first('password_last')); ?>

                                    </span>
                                <?php endif; ?>
                            </fieldset>
                            <div style="text-align: center">
                                <button type="submit" class="btn" style="background-color: #102b4e;color: #fff; width: 100%"><?php echo e($language_id == 'vi' ? 'Xác nhận' : 'Confirm'); ?></button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>