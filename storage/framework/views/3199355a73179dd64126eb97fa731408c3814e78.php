<?php $__env->startSection('content'); ?>
<?php 
    $caidat = \App\Models\Options::all();
    $language_id = 'en';
    if (Session::has('set_language')) {
        $language_id = Session::get('set_language');
        //dd($language_id);
    }
 ?>
    <main>

    <div id="about_us" style="margin-bottom: 10%">
        <div class="about_top_title">
            <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-12 dell-end-a">
                <a href="<?php echo e(route('web.home')); ?>">
                    <span class="color_blue">
                        <?php if($language_id == 'en'): ?>
                        HOME
                        <?php elseif($language_id == 'vi'): ?>
                        Trang chủ
                        <?php endif; ?> > 
                    </span>
                </a>
                <a href="<?php echo e(route('web.program')); ?>">
                <span class="color_blue" style="text-decoration: none;text-transform: uppercase;">
                    <?php if($language_id == 'en'): ?>
                        ADMISSION
                        <?php elseif($language_id == 'vi'): ?>
                        Tuyển sinh
                        <?php endif; ?> >
                    </span>
                </a>
                 <?php $__currentLoopData = $array_link_title2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($loop->last): ?>
                        <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                    <?php else: ?>
                    <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?> | </span>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
        </div>
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($postTrans->image); ?>)" alt="" class="transparent" width="100%">
                </div>
            </div>
        </div>

        <style type="text/css">
        main #about_us .content_about_us .card-header{
            border-bottom: 0;
            padding-left: 0;
            background-color:transparent;
        }
        main #about_us .content_about_us .card-header a{
            font-size: 28px;
            font-weight: bold;
            color: #102B4E;
            text-transform: uppercase;
        }
        main #about_us .content_about_us .card-header a:hover{
            color: #e8b909 !important;
        }
        main #about_us .content_about_us .card-header a img {
            margin-right: 20px;
            margin-top: -6px;

        }

    </style>
    <div class="content_about_us" >
        <div class="container mt-3">
            <div id="accordion">
                <div class="card_bottom">
                    <div class="">
                        <div class="card-header">
                            <a class="collapsed card-link">
                                <img src="<?php echo e(url('public/web')); ?>/images/squares.svg" alt="" width="2.8%"><?php echo e($postTrans->name); ?>

                            </a>
                        </div>
                        <div>
                            <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                <style type="text/css">

                                .double {
                                    font: 25px sans-serif;
                                    margin-top: 0px;
                                    position: relative;
                                    text-align: left;
                                    text-transform: uppercase;
                                    z-index: 1;
                                    font-weight: bold;
                                }

                                .double:before {
                                    border-top: 2px solid #dfdfdf;
                                    content:"";
                                    margin: 0 auto;
                                    position: absolute;
                                    top: 15px; left: 0; right: 0; bottom: 0;
                                    width: 100%;
                                    z-index: -1;
                                }

                                .double span { 
                                    background: #fff; 
                                    padding: 0 15px 0 0; 
                                }

                                .double:before { 
                                    border-top: none; 
                                }

                                .double:after {
                                    border-bottom: 10px solid #E8B909;
                                    content: "";
                                    margin: 0 auto;
                                    position: absolute;
                                    top: 30%; left: 0; right: 0;
                                    width: 100%;
                                    z-index: -1;
                                }
                                .back_groud{
                                    background: #E6E7E8;
                                    padding: 20px;
                                    border-radius: 3px
                                }
                            </style>
                            <h3 class="double"><span> 
                                <?php if($language_id == 'vi'): ?>
                                GIỚI THIỆU
                                <?php elseif($language_id == 'en'): ?>
                                INTRODUCE
                                <?php endif; ?> </span>
                            </h3>
                            
                            <div class="mark_banner" style="margin-top: 20px">
                                <div class="container">
                                    <div class="row <?php echo e($postTrans->color_contenrt == 1 ? 'back_groud' : ''); ?>">
                                        <?php echo $postTrans->content; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <style type="text/css">
                            .iframe_yt{
                                padding:5px 0 5px 0;
                                width: 100%;
                                .video-background {
                                  background: #000;
                                  position: fixed;
                                  top: 0; right: 0; bottom: 0; left: 0;
                                  z-index: -99;
                                }
                                .video-foreground,
                                .video-background iframe {
                                  position: absolute;
                                  top: 0;
                                  left: 0;
                                  width: 100%;
                                  height: 100%;
                                  pointer-events: none;
                                }

                                @media (min-aspect-ratio: 16/9) {
                                  .video-foreground { height: 300%; top: -100%; }
                                }
                                @media (max-aspect-ratio: 16/9) {
                                  .video-foreground { width: 300%; left: -100%; }
                                }
                                @media  all and (max-width: 600px) {
                                .vid-info { width: 50%; padding: .5rem; }
                                .vid-info h1 { margin-bottom: .2rem; }
                                }
                                @media  all and (max-width: 500px) {
                                .vid-info .acronym { display: none; }
                                }
                                
                            }
                            .video-foreground iframe{
                                width: 100%;
                            }
                        </style>
                    </div>
                    <?php 
                        $data_vi = json_decode($postTrans->mang_choose);
                     ?>
                    <?php if($postTrans->mang_choose != null): ?>
                        
                            <div>
                                <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                    <h3 class="double" style="margin-bottom: 20px"><span><?php echo e($postTrans->language_id == 'vi' ? 'Vì sao bạn nên chọn chương trình!' : 'WHY DO YOU CHOOSE THE PROGRAM?'); ?></span></h3>
                                    <div class="mark_banner">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php if(!empty($data_vi)): ?>
                                                        <?php for($i = 0; $i < count($data_vi); $i++): ?>
                                                        <?php if($data_vi[$i] != null): ?>
                                                            <div style="padding:5px 0 5px 0">
                                                                <a class="collapsed card-link" style="text-decoration: none; font-weight: bold;font-size: 18px">
                                                                    <i class="fas fa-angle-double-right" style="color:#E8B909; margin-right: 7px"></i><?php echo e($data_vi[$i]); ?>

                                                                </a>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php endfor; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-6">
                                                    
                                                    <div class="video-background" style="">
                                                        <div class="video-foreground" style="">
                                                            <iframe width="560" height="315" src="<?php echo e($postTrans->link_youtube); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php else: ?>
                    <?php endif; ?>


                    <style type="text/css">
                        .tab_program{
                            width: 100%;
                        }
                        .tab_program a{
                            width: 100% !important;
                            background:#D1D3D4;
                            border-radius: 0;
                            color:#102B4E;
                            text-transform: uppercase;
                            font-weight: bold;
                        }
                        .tab_program a:hover{
                            width: 100%;
                            background:#E8B909;
                            text-transform: uppercase;
                        }
                        .tab_program a.active{
                            width: 100%;
                            background:#E8B909;
                        }
                    </style>
                    <div >
                        <?php 
                            $program_tab = \App\Models\Admin\ProgramTab::where('tab_gram_id',$postTrans->id)->first();
                         ?>

                        
                        <?php if($program_tab): ?>
                            <?php 
                            // dd($program_tab['content_tab'] != null);
                                $name_tab=($program_tab['name_tab'] != null) ? array_chunk(json_decode($program_tab['name_tab']),4) : 'null';
                                $content_tab= ($program_tab['content_tab'] != null) ? array_chunk(json_decode($program_tab['content_tab']),4) : 'null';
                                $count_tab=count($name_tab);
                                $dem=$count_tab % 4;
                               
                             ?>
                        <?php for($i = 0; $i < $dem; $i++): ?>
                        
                            <div class="row">
                                    <?php for($j = 0; $j < count($name_tab[$i]) ; $j++): ?>
                                         <div class="col-md-3" style="margin-bottom: 15px">
                                        <div class="tab_program">
                                            <a class="btn collapsed card-link btn-active" data-toggle="collapse" href="#<?php echo e(str_slug($name_tab[$i][$j])); ?>">
                                                <?php echo e($name_tab[$i][$j]); ?>

                                            </a>
                                        </div>
                                    </div>
                                       
                                    <?php endfor; ?>
                                <?php for($j = 0; $j < count($name_tab[$i]) ; $j++): ?>
                                <div class="col-sm-12">
                                    <div id="<?php echo e(str_slug($name_tab[$i][$j])); ?>" class="collapse " data-parent="#accordion">
                                    
                                        <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                            <div id=home>
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <?php echo $content_tab[$i][$j]; ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                   
                                  <?php endfor; ?>

                                
                            </div>
                          
                            <?php endfor; ?>

                        <?php endif; ?>


                    </div>
                        


                        <hr>

                        <div >
                        <?php if($language_id == 'vi'): ?>
                            <div class="row">
                                <div class="col-md-3" style="margin-bottom: 15px">
                                    <div class="tab_program">
                                        <a class="btn collapsed card-link" href="#">Tải Brochure <i st  yle="text-transform:none">tại đây</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3" style="margin-bottom: 15px">
                                    <div class="tab_program">
                                        <a class="btn card-link" style="background: red;color: #fff" data-toggle="modal" href='#register'>
                                            Đăng ký
                                        </a>
                                    </div>
                                </div>
                                    
                            </div>
                        <?php elseif($language_id == 'en'): ?>
                            <div class="row">
                                <div class="col-md-3" style="margin-bottom: 15px">
                                    <div class="tab_program">
                                        <a class="btn collapsed card-link" href="#">Download Brochure <i st  yle="text-transform:none">here</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3" style="margin-bottom: 15px">
                                    <div class="tab_program">
                                        <a class="btn card-link" style="background: red;color: #fff" data-toggle="modal" href='#register'>
                                            Register
                                        </a>
                                    </div>
                                </div>
                                    
                            </div>
                        <?php endif; ?>    
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade"  id="register">
    <div class="modal-dialog" style="border: 20px solid red; width: 40% !important; max-width: 40%;"> 
        <div class="modal-content" style="width: 100% !important;border-radius: 0;border: 0">
            <div class="modal-body" style="background-color: red;border-radius: 0;padding: 0">
                <div class="col-md-12">
                    <div class="row">
                        <h5 class="modal-title" style="color: #e8b909;width: 100%">HÃY BẮT ĐẦU HÀNH TRÌNH CỦA BẠN NGAY TẠI ĐÂY!</h5>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <p style="color: #fff">Trở thành sinh viên tiếp theo của chương trình Cử nhân ... </p>
                    </div>
                </div>
            </div>
            <form action="" method="POST" role="form">
            <div class="modal-body" style="padding:30px;padding-bottom: 20px">
                    <div class="form-group">
                        <label for="">Tên của bạn <span style="color: red">*</span></label>
                        <input type="text" name="name" class="form-control" id="" placeholder="Tên của bạn...">
                    </div>
                    <div class="form-group">
                        <label for="">Điện thoại <span style="color: red">*</span></label>
                        <input type="tel" name="phone" class="form-control" id="" placeholder="Điện thoại...">
                    </div>
                    <div class="form-group">
                        <label for="">Email <span style="color: red">*</span></label>
                        <input type="email" name="email" class="form-control" id="" placeholder="Email...">
                    </div>
                    <div class="form-group">
                        <label for="">Trường THPT <span style="color: red">*</span></label>
                        <input type="text" name="truong" class="form-control" id="" placeholder="Trường THPT...">
                    </div>
                    <div class="form-group">
                        <label for="">Năm tốt nghiệp THPT<span style="color: red">*</span></label>
                        <input type="text" name="nam_tn" class="form-control" id="" placeholder="Năm tốt nghiệp THPT...">
                    </div>
                    <div class="form-group">
                        <label for="">Nội dung tư vấn<span style="color: red">*</span></label>
                        <input type="text" name="title" class="form-control" id="" placeholder="Nội dung tư vấn...">
                    </div>
                
            </div>
            <div class="modal-footer" style="border:0;padding:0;padding:0 30px 25px 25px">
                <button type="button" class="btn btn-danger">Đăng ký</button>
            </div>
            </form>
        </div>
    </div>
</div>
</main>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(".btn-active").click(function(){
            if(!$(this).hasClass('active')){
                $("a.btn-active").removeClass('active');
                $(this).addClass('active');
            }
         });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>