<?php $__env->startSection('style.css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<!-- Toastr style -->
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Box list</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo e(route('admin.dashboard')); ?>">Home</a>
                    </li>
                    <li class="active">
                        <strong>Banner</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" >
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Banner slider</h5>
                            <div class="clearfix"></div>
                            <div>
                                <button type="button" class="btn btn-primary" data-toggle="modal" href='#add_banner_slider'>ADD +</button>
                            </div>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="modal fade modal-id" id="add_banner_slider">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Thêm ảnh slider</h4>
                                    </div>
                                    <form action="<?php echo e(route('banner.create')); ?>" method="POST" role="form" class="crete_form" enctype="multipart/form-data">
                                        <?php echo e(csrf_field()); ?>

                                        <input type="hidden" name="type" value="slider">

                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="">Name box(*):</label>
                                                <input type="text" name="name" class="form-control" required="required" id="name" value="<?php echo e(old('name') ==null ? 'Image ' : old('name')); ?>">
                                                <p style="color:red; display:none;" class="error errorName"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Link box</label>
                                                <input type="text" name="link" class="form-control" id="link" required="required" value="<?php echo e(old('link')); ?>">
                                                <p style="color:red; display:none;" class="error errorLink"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">IMG</label>
                                                <?php if(old('image')): ?>
                                                    <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                            <span class="fileinput-filename"><?php echo e(old('image')); ?></span>
                                                        </div>
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            <span class="fileinput-new">Select file</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="hidden" value="<?php echo e(old('image')); ?>" name=""><input type="file" name="image" value="<?php echo e($slider->images); ?>" aria-required="true" class="error" aria-invalid="true">
                                                        </span>
                                                    </div>
                                                <?php else: ?>
                                                <div >
                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput">
                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                            <span class="fileinput-filename"></span>
                                                        </div>
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            <span class="fileinput-new">Select file</span>
                                                            <span class="fileinput-exists">Change</span>        
                                                            <input type="file" name="image" required="required" >
                                                        </span>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <p style="color:red; display:none;" class="error errorFile"></p>
                                            </div>
                                        </div>
                                        <input type="hidden" name="class" value="icon">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                            <button type="submit" class="btn btn-primary">Edit changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny load_ajax_boxlink_slider" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Name</th>
                                    <th data-hide="all">Link banner</th>
                                    <th data-hide="phone">Image</th>
                                    
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $banner_slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                               <?php echo e($slider->name); ?>

                                            </td>
                                            <td>
                                                <?php echo e($slider->link); ?>

                                            </td>

                                            <td>
                                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($slider->images); ?>" alt="" width="70px">
                                            </td>
                                            
                                            
                                            
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a class="btn-white btn btn-xs" data-toggle="modal" href='#edit-<?php echo e($slider->id); ?>'>Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade modal-id" id="edit-<?php echo e($slider->id); ?>">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Sửa link "<?php echo e($slider->name); ?>"</h4>
                                                    </div>
                                                    <form action="<?php echo e(route('banner.store')); ?>" method="POST" role="form" class="upload_form_slider" enctype="multipart/form-data">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="id" value="<?php echo e($slider->id); ?>">
                                                        <input type="hidden" name="type" value="slider">

                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="">Name box(*):</label>
                                                                <input type="text" name="name" class="form-control" required="required" id="name" value="<?php echo e($slider->name); ?>">
                                                                <p style="color:red; display:none;" class="error errorName"></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Link box</label>
                                                                <input type="text" name="link" class="form-control" id="link" required="required" value="<?php echo e($slider->link); ?>">
                                                                <p style="color:red; display:none;" class="error errorLink"></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">IMG</label>
                                                                <?php if($slider->images): ?>
                                                                    <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                                        <div class="form-control" data-trigger="fileinput">
                                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                            <span class="fileinput-filename"><?php echo e($slider->images); ?></span>
                                                                        </div>
                                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                        <span class="input-group-addon btn btn-default btn-file">
                                                                            <span class="fileinput-new">Select file</span>
                                                                            <span class="fileinput-exists">Change</span>
                                                                            <input type="hidden" value="<?php echo e($slider->images); ?>" name=""><input type="file" name="image" value="<?php echo e($slider->images); ?>" aria-required="true" class="error" aria-invalid="true">
                                                                        </span>
                                                                    </div>
                                                                <?php else: ?>
                                                                <div >
                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                        <div class="form-control" data-trigger="fileinput">
                                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                            <span class="fileinput-filename"></span>
                                                                        </div>
                                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                        <span class="input-group-addon btn btn-default btn-file">
                                                                            <span class="fileinput-new">Select file</span>
                                                                            <span class="fileinput-exists">Change</span>        
                                                                            <input type="file" name="image" required="required" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <?php endif; ?>
                                                                <p style="color:red; display:none;" class="error errorFile"></p>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="class" value="icon">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                                            <button type="submit" class="btn btn-primary">Edit changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Banner image</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny load_ajax_boxlink_image"  data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Name</th>
                                    <th data-hide="all">Link banner</th>
                                    <th data-hide="phone">Image</th>
                                    
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $banner_image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                               <?php echo e($image->name); ?>

                                            </td>
                                            <td>
                                                <?php echo e($image->link); ?>

                                            </td>

                                            <td>
                                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($image->images); ?>" alt="" width="70px">
                                            </td>
                                            
                                            
                                            
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a class="btn-white btn btn-xs" data-toggle="modal" href='#edit-<?php echo e($image->id); ?>'>Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade modal-id" id="edit-<?php echo e($image->id); ?>">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Sửa link "<?php echo e($image->name); ?>"</h4>
                                                    </div>
                                                    <form action="<?php echo e(route('banner.store')); ?>" method="POST" role="form" class="upload_form_image" enctype="multipart/form-data">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="id" value="<?php echo e($image->id); ?>">
                                                        <input type="hidden" name="type" value="image">

                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="">Name box(*):</label>
                                                                <input type="text" name="name" class="form-control" required="required" id="name" value="<?php echo e($image->name); ?>">
                                                                <p style="color:red; display:none;" class="error errorName"></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Link box</label>
                                                                <input type="text" name="link" class="form-control" id="link" required="required" value="<?php echo e($image->link); ?>">
                                                                <p style="color:red; display:none;" class="error errorLink"></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">IMG</label>
                                                                <?php if($image->images): ?>
                                                                    <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                                        <div class="form-control" data-trigger="fileinput">
                                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                            <span class="fileinput-filename"><?php echo e($image->images); ?></span>
                                                                        </div>
                                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                        <span class="input-group-addon btn btn-default btn-file">
                                                                            <span class="fileinput-new">Select file</span>
                                                                            <span class="fileinput-exists">Change</span>
                                                                            <input type="hidden" value="<?php echo e($image->images); ?>" name=""><input type="file" name="image" value="<?php echo e($slider->images); ?>" aria-required="true" class="error" aria-invalid="true">
                                                                        </span>
                                                                    </div>
                                                                <?php else: ?>
                                                                <div >
                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                        <div class="form-control" data-trigger="fileinput">
                                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                            <span class="fileinput-filename"></span>
                                                                        </div>
                                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                        <span class="input-group-addon btn btn-default btn-file">
                                                                            <span class="fileinput-new">Select file</span>
                                                                            <span class="fileinput-exists">Change</span>        
                                                                            <input type="file" name="image" required="required" >
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <?php endif; ?>
                                                                <p style="color:red; display:none;" class="error errorFile"></p>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="class" value="icon">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                                            <button type="submit" class="btn btn-primary">Edit changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>




        </div>
        
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>

<!-- Toastr -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/toastr/toastr.min.js"></script>



<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
   $('.upload_form_slider').on('submit',function(event){
      event.preventDefault();
      // var id = $(this).data('id');
      $.ajax({
       url:"<?php echo e(route('banner.store')); ?>",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
                console.log(data);
            if (data.error == true) {
                toastr["error"]("Thay đổi dữ liệu box thất bại.");
                $('.error').hide();
            } else {
                toastr["success"]("Thay đổi dữ liệu box thành công!");
                    // $('.modal-id').modal('hide');
                setTimeout(function(){
                    $('.load_ajax_boxlink_slider').load(location.href + ' .load_ajax_boxlink_slider>*');
                    $('.modal-id').modal('hide');
                    }, 500);
               }
        }
        });
     });
</script>
<script type="text/javascript">
   $('.upload_form_image').on('submit',function(event){
      event.preventDefault();
      // var id = $(this).data('id');
      $.ajax({
       url:"<?php echo e(route('banner.store')); ?>",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
                console.log(data);
            if (data.error == true) {
                toastr["error"]("Thay đổi dữ liệu box thất bại.");
                $('.error').hide();
            } else {
                toastr["success"]("Thay đổi dữ liệu box thành công!");
                    // $('.modal-id').modal('hide');
                setTimeout(function(){
                    $('.load_ajax_boxlink_image').load(location.href + ' .load_ajax_boxlink_image>*');
                    $('.modal-id').modal('hide');
                    }, 500);
               }
        }
        });
     });
</script>

<script type="text/javascript">
   $('.crete_form').on('submit', function(event){
      event.preventDefault();
      // var id = $(this).data('id');
      $.ajax({
       url:"<?php echo e(route('banner.create')); ?>",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
                console.log(data);
            if (data.error == true) {
                toastr["error"]("Thêm mới thất bại.");
                $('.error').hide();
            } else {
                toastr["success"]("Thêm mới thành công.");
                setTimeout(function(){
                    $('.load_ajax_boxlink_slider').load(location.href + ' .load_ajax_boxlink_slider>*');
                    
                    $('.modal-id').modal('hide');
                    }, 500);
               }
        }
        });
     });
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>