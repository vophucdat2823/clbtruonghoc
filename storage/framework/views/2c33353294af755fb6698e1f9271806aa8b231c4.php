<?php $__env->startSection('title','Shop'); ?>
<?php $__env->startSection('content'); ?>
<style>
.items-products{
    border: 1px solid #cccccc50;
}
.items-products img{
    width: 100%;
}
.items-products h3{
    font-size: 18px;
    margin-top: 5px;
    padding: 0 5px;
}
.items-products .price p{
    font-weight: bold;
    padding: 0 5px;
    font-size: 16px;
}
</style>
<main>
    <div class="w-100 background_div">
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-1 pb-1">
            <h5>Sản phẩm nổi bật</h5>
            <div class="col-lg-12 p-4 bg-white box-mota">
                <div class="row">
                    <?php $__currentLoopData = $proHots; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proHot): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-2">
                            <div class="items-products">
                                <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                                <a href="#">
                                    <h3></h3>
                                </a>
                                <div class="price">
                                    <p>Giá: <span>4.000.000</span></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>