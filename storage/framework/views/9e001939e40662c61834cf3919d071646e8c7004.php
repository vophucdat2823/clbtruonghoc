<?php $__env->startSection('title','khóa học | Thêm mới'); ?>
<?php $__env->startSection('style'); ?>





    <link href="<?php echo e(url('public/admin/css/plugins/dropzone/basic.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/dropzone/dropzone.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/jasny/jasny-bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('public/admin/css/plugins/codemirror/codemirror.css')); ?>" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thêm khóa học</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Thêm khóa học</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('course.store')); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product"  >
            <?php echo e(csrf_field()); ?>

            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin khóa học</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Chi tiết</a></li>
                        
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="<?php echo e(route('course.index')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên khóa học(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name_dichvu" id="name" class="form-control" placeholder="Tên khóa học..." required value="<?php echo e(old('name_dichvu')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo e(url('chi-tiet')); ?>/khoa-hoc-</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn..." required value="<?php echo e(old('slug')); ?>">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Link combo(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">URL/</span>
                                                    <input type="text" class="form-control" name="url_link_combo" id="url_link_combo" placeholder="Đường dẫn:Mặc định #" value="<?php echo e(old('url_link_combo')); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Điều kiện:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="study_condition" class="form-control" placeholder="Mặc định: Không" value="<?php echo e(old('study_condition')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục khóa học(*):</label>
                                        <div class="col-sm-10">

                                            <select name="cate_course_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    <option value="">Chọn danh mục</option>
                                                    <?php $__currentLoopData = $cateService; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($cate->id); ?>"><?php echo e($cate->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Banner(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="banner" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ảnh mô tả thêm(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <input type="file" name="file_image[]" id="image_file" multiple required>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả khóa học:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Mô tả khóa học"><?php echo old('description'); ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Đơn vị cung cấp(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_unit">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_unit">
                                    <?php if(!empty(old('type') == "unit")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "unit"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="unit">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_unit">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Chất lượng(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_quality">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_quality">
                                    <?php if(!empty(old('type') == "quality")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "quality"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="quality">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_quality">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Thời lượng(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_time">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_time">
                                    <?php if(!empty(old('type') == "time")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "time"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="time">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_time">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Địa chỉ(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_address">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_address">
                                    <?php if(!empty(old('type') == "address")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "address"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="address">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_quality">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Giá thành(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_price">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_price">
                                    <?php if(!empty(old('type') == "price")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "price"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="<?php echo e(old('type')[$i]); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_price">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>

                                    
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Số lượng học viên(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_qty">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_qty">
                                    <?php if(!empty(old('type') == "qty")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "qty"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="qty">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_qty">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Học phí(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_tuition">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_tuition">
                                    <?php if(!empty(old('type') == "tuition")): ?>
                                        <?php for($i = 0; $i < count(old('type') == "tuition"); $i++): ?>
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="<?php echo e(old('name')[$i]); ?>">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="tuition">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_tuition">
                                                                Rem -
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </div>
                                    
                                </fieldset>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>


    <?php $__env->startSection('scripts'); ?>
    <!-- Chosen -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

    <!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>
    <script type="text/javascript">

        $("body").on('click','.removeApartment_unit',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_unit").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Đơn vị cung cấp!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="unit">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_unit">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_unit").prepend(str);

        });
        $("body").on('click','.removeApartment_price',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_price").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Giá thành!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="price">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_price">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_price").prepend(str);

        });
        $("body").on('click','.removeApartment_time',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_time").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Thời lượng!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="time">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_time">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_time").prepend(str);

        });

        $("body").on('click','.removeApartment_quality',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_quality").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Chất lượng!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="quality">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_quality">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_quality").prepend(str);

        });
        $("body").on('click','.removeApartment_qty',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_qty").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Số lượng!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="qty">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_qty">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_qty").prepend(str);

        });
        $("body").on('click','.removeApartment_address',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_address").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Địa chỉ!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="address">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_address">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_address").prepend(str);




        });
        $("body").on('click','.removeApartment_tuition',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_tuition").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Học phí!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="tuition">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_tuition">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_tuition").prepend(str);
         });

    </script>

    <style>
        .dropzone {
            border: 2px dashed #0087F7;
            border-radius: 5px;
            background: white;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>