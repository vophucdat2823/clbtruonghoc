<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $__env->yieldContent('title'); ?></title>
    <link rel="icon" href="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg">
    <link href="<?php echo e(url('public/admin')); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/select2/select2.min.css" rel="stylesheet">

    <?php echo $__env->yieldContent('style'); ?>
    <!-- Toastr style -->
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Toastr style -->
    
    

    <link href="<?php echo e(url('public/admin')); ?>/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

     <link href="<?php echo e(url('public/admin')); ?>/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/animate.css" rel="stylesheet">
    
    <link href="<?php echo e(url('public/admin')); ?>/css/style.css" rel="stylesheet">

    

</head>

<body>
    <div id="wrapper">
      
    <?php echo $__env->make('admin_2.layout.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
              <div class="form-group">
                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
              </div>
            </form>
          </div>
          <ul class="nav navbar-top-links navbar-right">
            <li>
              <span class="m-r-sm text-muted welcome-message">Chào đón "<?php echo e(Auth::user()->name); ?>" đến với CLB Trường Học</span>
            </li>
            


            <li>
              <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Đăng xuất
                </a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo e(csrf_field()); ?>

                </form>
            </li>
            
          </ul>
        </nav>
      </div>
      <?php echo $__env->yieldContent('content'); ?>
      <!-- /.content-wrapper -->
      <div class="footer">
        <div class="pull-right">
          10GB of <strong>250GB</strong> Free.
        </div>
        <div>
          <strong>Bản quyền </strong> Black Rose &copy; 2000-2019
        </div>
      </div>
    </div>
    
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo e(url('public/admin')); ?>/js/jquery-2.1.1.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <?php echo $__env->yieldContent('scripts'); ?>
    

    

    <!-- Custom and plugin javascript -->
    <script src="<?php echo e(url('public/admin')); ?>/js/inspinia.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    
    <!-- Ladda -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/ladda/ladda.jquery.min.js"></script>

    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript">
        <?php if(Session::has('error')): ?>
        $(window).load(function() {
            toastr["error"]("<?php echo Session::get('error'); ?>");
        });
        <?php endif; ?>
        <?php if(Session::has('success')): ?>
            $(window).load(function() {
                toastr["success"]("<?php echo Session::get('success'); ?>");
            });
        <?php endif; ?>
    </script>

    

  
    
    

    
    <script src="<?php echo e(url('public/admin')); ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/script.js"></script>

    
    <?php echo $__env->yieldContent('javascript'); ?>
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>

    
    

</body>
</html>
