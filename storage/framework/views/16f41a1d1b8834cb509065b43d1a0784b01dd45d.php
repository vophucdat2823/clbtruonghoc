<?php $__env->startSection('content'); ?>
<?php 
	$language_id = 'en';
    if (Session::has('set_language')) {
        $language_id = Session::get('set_language');
    }
 ?>



	<main>
		<div id="people_details">
			<div class="people_details_top_title">
				<div class="container">
			        <div class="row">
			            <div class="col-md-12 col-sm-12 col-12 dell-end-a">

			                <a href="<?php echo e(route('web.home')); ?>">
			                    <span class="color_blue">
			                        <?php if($language_id == 'en'): ?>
			                        HOME
			                        <?php elseif($language_id == 'vi'): ?>
			                        Trang chủ
			                        <?php endif; ?> > 
			                    </span>
			                </a>
			                 <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                 	<?php if($loop->last): ?>
		                        	<span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
		                        <?php else: ?>
		                         	<a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
		                         		<span class="color_blue">
		                         			<?php echo e($key); ?> >
		                         		</span>
		                         	</a>
		                        <?php endif; ?>
		                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="content_people_details">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 col-12">
							<div class="content_left_pp_details">
								<div class="cate_pp_details">
									<?php if($language_id == 'en'): ?>
									<h3>CATEGORYIES</h3>
									<?php elseif($language_id == 'vi'): ?>
									<h3>Danh Mục</h3>
									<?php endif; ?>
									<ul>
										<li>
											<div class="row">
												<div class="col-md-2 col-sm-2 col-2 logo_square">
													<img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="60%">
												</div>
												<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
													
													<?php if($language_id == 'vi'): ?>
														<a href="#" style="text-transform: uppercase;"><?php echo e($post_cate_first->name_vi); ?></a>
													<?php elseif($language_id == 'en'): ?>
														<a href="#" style="text-transform: uppercase;"><?php echo e($post_cate_first->name_en); ?></a>
													<?php endif; ?>
												</div>
											</div>
												<style>
													.dong-cuoi-cung:last-of-type{
														border-bottom:0 !important;
														text-transform: uppercase;
													}
												</style>
												<ul style="color: #ccc">
													
													
													<?php $__currentLoopData = $post_cate_first->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cpi_pt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<?php if($cpi_pt->language_id == $language_id): ?>
														<li class="dong-cuoi-cung">
															<div class="row">
																<div class="col-md-2 col-sm-2 col-2 logo_square" style="text-align: center">
																	<img src="<?php echo e(url('public/web/images')); ?>/alumni/4x/Asset 13@4x.png" alt="" width="25%">
																</div>
																<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img" class="dong-cuoi-cung">
																	<a href="<?php echo e(route('web.menu',['param'=>$cpi_pt->code])); ?>"><?php echo e(str_limit($cpi_pt->name,25)); ?></a>
																</div>
															</div>
														</li>
													<?php else: ?>
														<li class="dong-cuoi-cung">
															<div class="row">
																<div class="col-md-2 col-sm-2 col-2 logo_square" style="text-align: center">
																	<img src="<?php echo e(url('public/web/images')); ?>/alumni/4x/Asset 13@4x.png" alt="" width="25%">
																</div>
																<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
																	<a href="<?php echo e(route('web.menu',['param'=>$cpi_pt->code])); ?>"><?php echo e(str_limit($cpi_pt->name,25)); ?></a>
																</div>
															</div>
														</li>
													<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</ul>
										</li>
									</ul>
								</div>
								<div class="dif_info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul>
                                                
                                                <?php 
                                                    
                                                $cate_tintuc = \App\Models\Admin\Category::where('parents',7)->get();
                                                 ?>
                                                <?php $__currentLoopData = $cate_tintuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ca_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php 
                                                    
                                                    $posts_tt = DB::table('posts')
                                                        ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
                                                        ->where('category_id', $ca_tt->id)
                                                        ->where('language_id',$language_id)
                                                        ->select('post_translations.*')
                                                        ->get();
                                                 ?>
                                                    <?php $__currentLoopData = $posts_tt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $po_tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li>
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="<?php echo e(route('web.menu',['param' => $po_tt->code])); ?>"><?php echo e(str_limit($po_tt->name,30)); ?></a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
								<div class="insta_pp">
									<div class="row">
											<div class="col-md-2 col-sm-2 col-2 logo_square">
												<img src="<?php echo e(url('public/web/images')); ?>/icon_square_xam.svg" alt="" width="60%">
											</div>
											<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
												<h5 class="color_yellow">Instagram</h5>
											</div>
									</div>
									<div class="row">
										<div class="col-md-4 col-sm-4 col-4 img_insta">
											<img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
										</div>
										<div class="col-md-4 col-sm-4 col-4 img_insta">
												<img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
										</div>
										<div class="col-md-4 col-sm-4 col-4 img_insta">
												<img src="<?php echo e(url('public/web/images')); ?>/people/insta.png" alt="" width="100%">
										</div>
									</div>
								</div>
								<div class="tag_pp">
									<div class="row">
											<div class="col-md-2 col-sm-2 col-2 logo_square">
												<img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="60%">
											</div>
											<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
												<h5 class="color_yellow">Tags</h5>
											</div>
									</div>
									<div class="row a_tag_content">
										<div class="col-md-12 col-sm-12 col-12">
											<a href="#">Cử nhân</a>
											<a href="#">Kinh doanh</a>
											<a href="#">Kế toán</a>
											<a href="#">Quản lý</a>
											<a href="#">Máy tính</a>
											<a href="#">Tin học</a>
											<a href="#">Luật</a>
											<a href="#">Cử nhân</a>
											<a href="#">Quản lý</a>
											<a href="#">Luật</a>
											<a href="#">Cử nhân</a>
											<a href="#">Kinh doanh</a>
											<a href="#">Kế toán</a>
											<a href="#">Quản lý</a>
											<a href="#">Máy tính</a>
											<a href="#">Tin học</a>
											<a href="#">Luật</a>
											<a href="#">Cử nhân</a>
											<a href="#">Quản lý</a>
											<a href="#">Luật</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php 
							
						// dd($posts_trans_first);
						 ?>
						<?php if($posts_trans_first == null): ?>
						<div class="col-md-8 col-sm-12 col-12">
							<div class="content_right_pp_details">
								<div class="row tt_right_pp_details">
										<div class="col-md-2 col-sm-2 col-2 logo_square">
											<img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="23%">
										</div>
										<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
											<?php if($language_id == 'en'): ?>
												<h3 class="color_blue"><?php echo e($post_cate_first->name_en); ?></h3>
											<?php else: ?>
												<h3 class="color_blue"><?php echo e($post_cate_first->name_vi); ?></h3>
											<?php endif; ?>
										</div>
								</div>
								<div class="row">
									<?php if($language_id == 'vi'): ?>
										<div class="col-md-12 col-sm-12 col-12">
											<?php if($post_cate_first->image == null): ?>
												<img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
											<?php else: ?>
											<img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($post_cate_first->image); ?>" alt="" width="100%">
											<?php endif; ?>
											<p><?php echo $post_cate_first->title_vi; ?></p>
										</div>
									<?php else: ?>
									<div class="col-md-12 col-sm-12 col-12">
											<?php if($post_cate_first->image == null): ?>
												<img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
											<?php else: ?>
											<img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($post_cate_first->image); ?>" alt="" width="100%">
											<?php endif; ?>
											<p><?php echo $post_cate_first->title_en; ?></p>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
						
						<?php else: ?>
						<div class="col-md-8 col-sm-12 col-12">
							<div class="content_right_pp_details">
								<div class="row tt_right_pp_details">
										<div class="col-md-2 col-sm-2 col-2 logo_square">
											<img src="<?php echo e(url('public/web/images/squares.svg')); ?>" alt="" width="23%">
										</div>
										<div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
											<?php if($posts_trans_first->language_id == $language_id): ?>
												<h3 class="color_blue"><?php echo e($posts_trans_first->name); ?></h3>
											<?php else: ?>
												<h3 class="color_blue"><?php echo e($posts_trans_first->name); ?></h3>
											<?php endif; ?>
										</div>
								</div>
								<div class="row">
									<?php if($posts_trans_first->language_id == $language_id): ?>
										<div class="col-md-12 col-sm-12 col-12">
											<img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
											<p><?php echo $posts_trans_first->content; ?></p>
										</div>
									<?php else: ?>
									<div class="col-md-12 col-sm-12 col-12">
											<img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
											<p><?php echo $posts_trans_first->content; ?></p>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>