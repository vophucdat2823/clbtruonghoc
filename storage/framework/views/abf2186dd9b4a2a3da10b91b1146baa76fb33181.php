<?php $__env->startSection('content'); ?>

<main>
    <div id="news_even">
        <div class="news_even_title">
            <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="content_news_even">
            <div class="container">
                <div class="row">
                    
                    <?php if(!empty($cate_event)): ?>
                    <?php $__currentLoopData = $cate_event; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate_ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                            <div class="col_content_h">
                                <div class="vnu_is_content">
                                    <div class="top_vnu_is_content">
                                        <?php if($language_id == 'vi'): ?>
                                            <h5><?php echo e($cate_ev->name_vi); ?></h5>
                                            <hr>
                                            <p><?php echo e(str_limit($cate_ev->title_vi,50)); ?></p>
                                        <?php elseif($language_id == 'en'): ?>
                                            <h5><?php echo e($cate_ev->name_en); ?></h5>
                                            <hr>
                                            <p><?php echo e(str_limit($cate_ev->title_en,50)); ?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="body_vnu_is_content">
                                        <div class="img_body_content">
                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($cate_ev->image); ?>" alt="" width="100%">
                                        </div>
                                        <ul>
                                            <?php $__currentLoopData = $cate_ev->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           
                                            <?php if($language_id == 'vi'): ?>
                                                <?php if($pt->language_id == 'vi'): ?>
                                                <li>
                                                    <strong>EVENT <?php echo e(1+$key++); ?>:</strong>
                                                    <a href="<?php echo e($pt->code); ?>"><?php echo e($pt->name); ?></a> 
                                                    <hr>
                                                    <p><?php echo $pt->simulation; ?></p>
                                                </li>
                                                <?php endif; ?>
                                            <?php elseif($language_id == 'en'): ?>
                                                <?php if($pt->language_id == 'en'): ?>
                                                    <li>
                                                        <strong>EVENT <?php echo e(1+$key++); ?>:</strong>
                                                        <a href="<?php echo e($pt->code); ?>"><?php echo e($pt->name); ?></a> 
                                                        <hr>
                                                        <p><?php echo $pt->simulation; ?></p>
                                                    </li>
                                                <?php endif; ?>
                                             <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                    <div class="bottom_more_vnu_is_content">
                                        <a href="<?php echo e($pt->code); ?>"></a>
                                    </div>
                                    <div class="more_content_h">
                                            <a href="<?php echo e($pt->code); ?>">MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>