
<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    //dd($language);
    ?>
    <div class="wrapper wrapper-content">
        <div class="row">
            
            

        </div>
            <div class="row">
                <div class="col-lg-12">
                     <?php if(session('success')): ?>
                        <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('error')): ?>
                        <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo e(session('error')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Edit icon:</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <form action="<?php echo e(url('admin/network/update')); ?>/<?php echo e($networks->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                        <div class="form-group">
                                                            <label for="">Tên icon (vietnam):</label>
                                                            <input type="text" name="name_edit" class="form-control" id="name" value="<?php echo e($networks->name); ?>">
                                                             <?php if($errors->has('name_edit')): ?>
                                                                <span class="text-center text-danger" role="alert">
                                                                    <?php echo e($errors->first('name_edit')); ?>

                                                                </span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Sửa link</label>
                                                            <input type="text" name="link_edit" class="form-control" id="name" value="<?php echo e($networks->link); ?>">
                                                             <?php if($errors->has('link_edit')): ?>
                                                                <span class="text-center text-danger" role="alert">
                                                                    <?php echo e($errors->first('link_edit')); ?>

                                                                </span>
                                                            <?php endif; ?>
                                                        </div>

                                                        <div class="col-md-4" style="text-align: center;margin-top: 40px">
                                                            <div class="row">
                                                                <div id="image-diplay">
                                                                    <div class="form-group">
                                                                        <label>Chọn icon hiển thị</label>
                                                                        <div class="col-md-12" style="text-align: center">
                                                                           <div class="fileinput fileinput-new" sty data-provides="fileinput" style="display:unset!important;">
                                                                               <div class="fileinput-preview thumbnail preview_img" data-trigger="fileinput">
                                                                                    <img src="<?php echo e(url('public/upload/icon')); ?>/<?php echo e($networks->icon); ?>" alt=""> 
                                                                                </div>
                                                                                <div style="text-align: center">
                                                                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                                        <span class="fileinput-new"> Chọn icon </span>
                                                                                        <span class="fileinput-exists"> Đổi icon </span>
                                                                                        <input type="file" name="icon_edit">
                                                                                    </span>
                                                                                    <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                                    data-dismiss="fileinput"> Xóa icon </a>
                                                                                    
                                                                                </div>
                                                                                <span class="text-center text-danger" role="alert">
                                                                                    <?php if($errors->has('image')): ?>
                                                                                    <?php echo e($errors->first('image')); ?>

                                                                                    <?php endif; ?>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>   
                                                            </div>
                                                        </div>   
                                                        <div class="col-md-8" style="text-align: center;margin-top: 40px">
                                                            <div class="row">
                                                               <div id="image-diplay">
                                                                   <div class="form-group">
                                                                    <label>Chọn image:</label>
                                                                       <div class="col-md-12">
                                                                           <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                                               <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                                    <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($networks->image); ?>" alt=""> 
                                                                               </div>
                                                                               <div style="text-align: center">
                                                                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                                        <span class="fileinput-new"> Chọn ảnh </span>
                                                                                        <span class="fileinput-exists"> Đổi ảnh </span>
                                                                                        <input type="file" name="image_edit">
                                                                                    </span>
                                                                                   <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                                      data-dismiss="fileinput"> Xóa ảnh </a>
                                                                                   
                                                                               </div>
                                                                               <span class="text-center text-danger" role="alert">
                                                                                    <?php if($errors->has('image')): ?>
                                                                                       <?php echo e($errors->first('image')); ?>

                                                                                   <?php endif; ?>
                                                                                </span>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                        </div>
                                                    <div class="clearfix"></div>
                                                    <div style="text-align: center;margin-top: 40px">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>