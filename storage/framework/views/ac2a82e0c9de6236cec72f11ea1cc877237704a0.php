<?php $__env->startSection('content'); ?>
<?php 
    $chooses_tab = \App\Models\Admin\Chooses::where('id',$category->choose_id)->first();
    $image          =  ($chooses_tab['image'] != null ) ? json_decode($chooses_tab['image']) : 'null';
    $teachers       =  ($chooses_tab['teachers'] != null ) ? json_decode($chooses_tab['teachers']) : 'null';
    $title_vi       =  ($chooses_tab['title_vi'] != null ) ? json_decode($chooses_tab['title_vi']) : 'null';
    $title_en       =  ($chooses_tab['title_en'] != null ) ? json_decode($chooses_tab['title_en']) : 'null';
    $office_vi       =  ($chooses_tab['office_vi'] != null ) ? json_decode($chooses_tab['office_vi']) : 'null';
    $office_en       =  ($chooses_tab['office_en'] != null ) ? json_decode($chooses_tab['office_en']) : 'null';
 ?>
<style type="text/css">
    .sync1 .item {
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

    .sync2 .item {
        background: #C9C9C9;
        /* padding: 10px 0px; */
        margin: 5px;
        color: #000 !important;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
        cursor: pointer;
    }

    .sync2 .item h1 {
        font-size: 18px;
    }

    .sync2 .current .item {
        background: #0c83e7;
    }

    .owl-theme .owl-nav [class*='owl-'] {
        transition: all .3s ease;
    }

    .owl-theme .owl-nav [class*='owl-'].disabled:hover {
        background-color: #D6D6D6;
    }

    .sync1.owl-theme {
        position: relative;
    }

    .sync1.owl-theme .owl-next,
    .sync1.owl-theme .owl-prev {
        width: 22px;
        height: 40px;
        margin-top: -20px;
        position: absolute;
        top: 30% !important;
    }

    .sync1.owl-theme .owl-prev {
        left: 38.5% !important;
    }

    .sync1.owl-theme .owl-next {
        right: 38.5% !important;
    }
    /* animate fadin duration 1.5s */
    .owl-carousel .animated{
        animation-duration: 1.5s !important;
    }
    /* 輪播的前後按鈕背景調大 */
    .sync1.owl-theme .owl-next, .sync1.owl-theme .owl-prev {
        width: 22px !important;
        height: auto !important;
    }
    .sync1 svg {
        width: 22px !important;
    }
    .owl-item > div {
      cursor: pointer;
      margin: 6% 8%;
      transition: margin 0.4s ease;
    }
    .owl-item.center > div {
      cursor: auto;
      margin: 0;
    }
    .owl-item:not(.center) > div:hover {
      opacity: .75;
    }
    .owl-item:not(.center) > div {
      opacity: .75;
    }


    @media  screen and (max-width: 600px) {
       main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .center p{
            font-size: 14px !important;

        }
    }

    main #home .slide_home img{
        border-radius: unset !important;
        box-shadow: unset;
        width: 100%;

    }
    main #home .slide_home .sync1 .owl-stage-outer .owl-stage .owl-item p{
        display: none;

    }
    main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .center .name_top{
        text-align: center;
        display: block;
        margin-top: 15px;
        margin-bottom: 0;
        font-weight: bold;

    }
    main #home .slide_home .owl-carousel .owl-stage-outer .owl-stage .center .name_bottom{
        text-align: center;
        display: block;
        margin-bottom: 15px;
        font-weight: bold;

    }


    main #about_us .content_about_us .card-header{
        border-bottom: 0;
        padding-left: 0;
        background-color:transparent;
    }
    main #about_us .content_about_us .card-header a{
        font-size: 28px;
        font-weight: bold;
        color: #102B4E;
        text-transform: uppercase;
    }
    main #about_us .content_about_us .card-header a:hover{
        color: #e8b909 !important;
    }
    main #about_us .content_about_us .card-header a img {
        margin-right: 20px;
        margin-top: -6px;

    }
    .owl-prev{
        position: absolute;
        top: 25% !important;
        left: 33% !important;
    }
    .owl-prev span{
        top: 12px;
    }
    .owl-next{
        position: absolute;
        top: 25% !important;
        right: 33% !important;
    }
    .owl-next span{
        top: 12px;
    }


    .placeholder_mobie_input::placeholder{
        font-size: 4.5vw
    }
    .placeholder_mobie_input label{
        font-size: 5vw;
    }

    .double {
        font: 25px sans-serif;
        margin-top: 0px;
        position: relative;
        text-align: left;
        text-transform: uppercase;
        z-index: 1;
        font-weight: bold;
    }

    .double:before {
        border-top: 2px solid #dfdfdf;
        content:"";
        margin: 0 auto;
        position: absolute;
        top: 15px; left: 0; right: 0; bottom: 0;
        width: 100%;
        z-index: -1;
    }

    .double span {
        background: #fff;
        padding: 0 15px 0 0;
    }

    .double:before {
        border-top: none;
    }

    .double:after {
        border-bottom: 10px solid #E8B909;
        content: "";
        margin: 0 auto;
        position: absolute;
        top: 30%; left: 0; right: 0;
        width: 100%;
        z-index: -1;
    }
    .title_slider{
        text-align: center;
        text-transform: uppercase;
        font-size: 2.5vw;
        color: red;
    }
</style>
<main id="main_mobie">
    <div id="about_us" style="background: #fff;">
        <div class="home">
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image != null): ?>
                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url('<?php echo e(url('public//upload/images')); ?>/<?php echo e($category->image); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>

                        <img src="<?php echo e(url('public/web')); ?>/images/transparent/transparent_program_lite.png" style="background-image: url(http://student.isvnu.vn/public/upload/images/1553822818_MICROSITE-banners-BSM.jpg)" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="activities_home" style="background: transparent;color: #102B4E;">
            <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                <?php if($language_id == 'vi'): ?>
                    <p><?php echo e($category->name_vi); ?></p>
                <?php else: ?>
                    <p><?php echo e($category->name_en); ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="content_about_us col-12" style="">
            <div class="mark_banner">
                <div class="container">
                    <div class="row">
                        <?php echo $category->title_vi; ?>

                    </div>
                </div>
            </div>
        </div><br>
        <div class="clearfix"></div>
        <div class="col-12"></div>
        

        <div class="content_about_us col-12" style="margin-top: 5%">
            <div>
                <div id="accordion">
                    <?php if($image): ?>
                        <div id="home">
                            <div class="slide_home" style="background: transparent;padding: 0">
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="owl-carousel owl-theme sync1">
                                                <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                    <div>
                                                         <img src="<?php echo e(url('public/web/images/transparent_elip.png')); ?>" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($image[$a]); ?>');border-radius: 100% !important;" class="transparent"  alt="landscape"/>
                                                        <p class="name_top"><?php echo e($teachers[$a]); ?></p>
                                                        <p class="name_bottom"><?php echo e($language_id == 'vi' ? $office_vi[$a] : $office_en[$a]); ?></p>
                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                            <div  class="owl-carousel owl-theme sync2">
                                                <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                    <div style="margin:0;padding-top: 2%;text-align: center;width: 100%;margin: 0 auto;border-top: 1px solid #939598;">
                                                            <?php echo $language_id == 'vi' ? $title_vi[$a] : $title_en[$a]; ?>

                                                    </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="content_about_us col-12" >
            <div>
                <div id="accordion">
                    <?php if($language_id == 'vi'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Liên hệ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif($language_id == 'en'): ?>
                        <div class="card_bottom">
                            <div style="background-color: transparent;padding: 3.5vw 0;">
                                <div class="activities_home" style="background-color: #BABCBE">
                                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                                        <a style="font-size: 3.7vw;color: #102B4E !important;text-transform: uppercase;font-weight: bold;" href="#">
                                           Contact
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<main id="main_pc">
    <div id="about_us">
        <div class="about_top_title">
             <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                    <?php if($category->image): ?>
                        <img src="<?php echo e(url('public/web/images/transparent/transparent_program_lite.png')); ?>" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($category->image); ?>')" alt="" class="transparent" width="100%">
                    <?php else: ?>
                        <img src="<?php echo e(url('public/web/images/transparent/transparent_program_lite.png')); ?>" style="background-image: url('<?php echo e(url('public/web/images/banner.jpg')); ?>')" alt="" class="transparent" width="100%">
                    <?php endif; ?>
                    <div class="container">
                        <div class="text_img">
                            <p class="top_text"><?php echo e($language_id == 'vi' ? $category->name_vi : $category->name_en); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content_about_us" >
            <div class="container mt-3">
                <div id="accordion">
                    <div class="card_bottom">
                        <div class="">
                            <div>
                                <div class="card-body" style="padding-left: 15px;padding-right: 15px">
                                    <h3 class="double"><span><?php echo $language_id == 'vi' ? 'GIỚI THIỆU'  : 'INTRODUCE'; ?></span></h3>
                                    <div class="mark_banner" style="margin-top: 20px">
                                        <div class="container">
                                            <div class="row" style="margin-bottom: 5%">
                                                <?php echo $language_id == 'vi' ? $category->title_vi : $category->title_en; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="mark_banner" style="margin-top: 20px">
                                        <div class="title_slider">
                                            <p><?php echo e($language_id == 'vi' ? $chooses_tab->name_vi : $chooses_tab->name_en); ?></p>
                                        </div>
                                        <?php if($image): ?>
                                            <div id="home">
                                                <div class="slide_home" style="background: transparent;padding: 0">
                                                    <div class="">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="owl-carousel owl-theme sync1">
                                                                    <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                                        <div>
                                                                             <img src="<?php echo e(url('public/web/images/transparent_elip.png')); ?>" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($image[$a]); ?>');border-radius: 100% !important;" class="transparent"  alt="landscape"/>
                                                                            <p class="name_top"><?php echo e($teachers[$a]); ?></p>
                                                                            <p class="name_bottom"><?php echo e($language_id == 'vi' ? $office_vi[$a] : $office_en[$a]); ?></p>
                                                                        </div>
                                                                    <?php endfor; ?>
                                                                </div>
                                                                <div  class="owl-carousel owl-theme sync2">
                                                                    <?php for($a = 0; $a <count($image) ; $a++): ?>
                                                                        <div style="margin:0;padding-top: 2%;text-align: center;width: 60%;margin: 0 auto;border-top: 1px solid #939598;">
                                                                                <?php echo $language_id == 'vi' ? $title_vi[$a] : $title_en[$a]; ?>

                                                                        </div>
                                                                    <?php endfor; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
    jQuery(document).ready(function() {
        var sync1 = jQuery(".sync1");
        var sync2 = jQuery(".sync2");
        var slidesPerPage = 1; //globaly define number of elements per page
        var syncedSecondary = true;

        sync1.owlCarousel({

          slideSpeed : 2500,
          nav: true,
          center: true,

          animateOut: 'fadeOut',
          // animateIn: 'fadeIn',
          autoplayHoverPause: true,
          autoplaySpeed: 2200,
          dots: false,
          loop: true,
          responsiveClass:true,
          responsive:{
            0:{
                autoplay:false,
                items :3,
                 nav: false,
            },
            768:{
                autoplay: true,
                items :3,
                nav: false,
            },
            1024:{
                autoplay: true,
                items:5,
                nav: true,
            }
          },
          responsiveRefreshRate : 200,

            navText: ["<img src='<?php echo e(url('public/web/images/program/Asset 1.png')); ?>'>","<img src='<?php echo e(url('public/web/images/program/Asset 2.png')); ?>'>"],
            stagePadding:25,
        }).on('changed.owl.carousel', syncPosition);

        sync2
          .on('initialized.owl.carousel', function () {
            sync2.find(".owl-item").eq(0).addClass("current");
          })
          .owlCarousel({
          items : slidesPerPage,
          dots: true,
          //   nav: true,
          smartSpeed: 1000,
          slideSpeed : 1000,
          slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
          responsiveRefreshRate : 100
        }).on('changed.owl.carousel', syncPosition2);

        function syncPosition(el) {
          //if you set loop to false, you have to restore this next line
          //var current = el.item.index;

          //if you disable loop you have to comment this block
          var count = el.item.count-1;
          var current = Math.round(el.item.index - (el.item.count/2) - .5);

          if(current < 0) {
            current = count;
          }
          if(current > count) {
            current = 0;
          }

          //end block

          sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
          var onscreen = sync2.find('.owl-item.active').length - 1;
          var start = sync2.find('.owl-item.active').first().index();
          var end = sync2.find('.owl-item.active').last().index();

          if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
          }
          if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
          }
        }

        function syncPosition2(el) {
          if(syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
          }
        }

        sync2.on("click", ".owl-item", function(e){
          e.preventDefault();
          var number = jQuery(this).index();
          sync1.data('owl.carousel').to(number, 300, true);
        });

      });
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>