<?php $__env->startSection('content'); ?>

<main>
    <div id="people">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">PEOPLE</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                    <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="images/squares.svg" alt="" width="60%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                        <hr>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="cate_pp_img">
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="60%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                        </div>
                        <div class="cate_pp_img">
                                <div class="img_cate">
                                        <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                </div>
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="60%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                                
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="60%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="cate_pp_img">
                                    <div class="img_cate">
                                            <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                    </div>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="cate_pp_img">
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </div>
                                    </div>
                                    
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                            <div class="cate_pp_img">
                                <div class="img_cate">
                                        <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                </div>
                                <div class="row txt_ct_cate">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/squares.svg" alt="" width="60%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="cate_pp_img">
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="cate_pp_img">
                                    <div class="img_cate">
                                            <img src="http://vietteltphochiminh.com/wp-content/uploads/2017/10/sim-sv.jpg" alt="" width="100%">
                                    </div>
                                    <div class="row txt_ct_cate">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                <a href="#"><h5>Office of Personnel	and Administrative affairs</h5></a>
                                                <hr>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                                        </div>
                                    </div>
                                    
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>