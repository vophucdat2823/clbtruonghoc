<?php $__env->startSection('style'); ?>





    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo e(url('public/admin')); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách sự kiện</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('admin.dashboard')); ?>">Trang chủ</a>
                </li>
                <li>
                    <a href="<?php echo e(route('event.index')); ?>">Trang chủ</a>
                    <strong>Danh sách sự kiện</strong>
                </li>
                <li class="active">
                    <strong>Thêm sự kiện</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="<?php echo e(route('event.store')); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            <?php echo e(csrf_field()); ?>

            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin sự kiện</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Address</a></li>
                        
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="<?php echo e(route('event.index')); ?>" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên sự kiện(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên sự kiện..." required value="<?php echo e(old('name')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">su-kien/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn..." required value="<?php echo e(old('slug')); ?>">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Loại sự kiện(*):</label>
                                        <div class="col-sm-10">

                                            <select name="even_type" id="EventType" data-placeholder="Choose a Country..." class="chosen-select">
                                                <option value="1">Miễn Phí</option>
                                                <option value="2">Mất Phí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group price_range"><label class="col-sm-2 control-label">Khoảng giá(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="price_range" class="form-control" placeholder="VD: 50,000đ -> 150,000đ" value="<?php echo e(old('price_range')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ngày hoạt động(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="date" name="date_play" class="form-control" placeholder="VD: 50,000đ -> 150,000đ" required value="<?php echo e(old('date_play')); ?>">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục(*):</label>
                                        <div class="col-sm-10">
                                            <select name="cate_event_id[]" data-placeholder="Choose a Country..." class="chosen-select" required multiple tabindex="4">
                                                <?php echo e(showCateEvent($cateEvent,0,'',old('cate_event_id'))); ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description"><?php echo old('description'); ?></textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <label class="" for="email">
                                    <button type="button" class="btn btn-success addNewCkeditor">
                                          Add address +
                                      </button>
                                </label>
                                <div class="listTabCkeditor">
                                    <fieldset class="form-horizontal">
                                        <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                            <div class="input-group">
                                                <div class="input-group-addon" style="padding:0;border:0">
                                                  <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address1[]" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                            <option value="">Thành Phố</option>
                                                            <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ci): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($ci->matp); ?>"><?php echo e($ci->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Quận/Huyện:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address2[]" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control">
                                                            <option value="">--Chưa chọn Quận/Huyện--</option>
                                                       </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><hr>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php

function showCateEvent($cateEvent, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateEvent as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateEvent[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateEvent($cateEvent, $item->id, $char . '---| ', $select);
        }
    }
}

?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<!-- Chosen -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/codemirror.js"></script>
    <script src="<?php echo e(url('public/admin')); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
          filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
          filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
          filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
          filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
          filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
        });
    </script>
    <script type="text/javascript">
        $('.city-country').select2({
            theme: 'bootstrap',
            placeholder: 'cityCountry',
            ajax: {
                url: '<?php echo e(url('')); ?>/admin/profiles/getSuggestCities',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term,
                    };
                },
                processResults: function (data, params) {
                    // console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.place_id,
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
        });
    </script>


    

    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("<?php echo e(url('')); ?>/admin/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>
    <script>
        $('.price_range').hide();
        $(document).on("change","#EventType", function(event) {
            var value = $(this).val();
            if (value == 1) {
                $('.price_range').hide('150');
            }else {
                $('.price_range').show('150');
            };
            
        });
    </script>

    <script type="text/javascript">
     $(document).ready(function () {
            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

             var x = 0;


         $(document).on("click",".addNewCkeditor",function(){
             x++;
            var thanhpho = 'thanhpho_' + x;
            var quanhuyen = 'quanhuyen_' + x;

            var str='<fieldset class="form-horizontal"><div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                str+='<div class="input-group">';
                    str+='<div class="input-group-addon" style="padding:0;border:0">';
                      str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                    str+='</div>';
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address1[]" id="'+thanhpho+'" data-placeholder="Choose a Country..." class="chosen-select form-control">';
                                str+='<option value="">Thành Phố</option>';
                                str+='<?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ci): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>';
                                str+='<option value="<?php echo e($ci->matp); ?>">';
                                str+='<?php echo e($ci->name); ?>';
                                str+='</option>';
                                str+='<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';                 
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Quận/Huyện:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address2[]" id="'+quanhuyen+'" data-placeholder="Choose a Country..." class="form-control">';
                                str+='<option value="">--Chưa chọn Quận/Huyện--</option>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';
                str+='</div></fieldset>';

                $(document).on("change","#"+thanhpho, function(event) {
                  var id_matp = $(this).val();
                  $.get("<?php echo e(url('')); ?>/partner/ajax/quanhuyen/"+id_matp,function(data) {
                    $("#"+quanhuyen).html(data);
                  });
                });
            console.log(str);
              
            $(wrapper).append(str);
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });




</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>