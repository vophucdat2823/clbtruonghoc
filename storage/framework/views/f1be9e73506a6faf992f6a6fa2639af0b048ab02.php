

<?php $__env->startSection('title','Cài đặt'); ?>
<?php $__env->startSection('content'); ?>
<div class="wrapper wrapper-content">

  <div class="row">
    <div class="col-lg-12">
     <?php if(session('success')): ?>
     <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo e(session('success')); ?>

    </div>
    <?php endif; ?>
    <?php if(session('error')): ?>
    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo e(session('error')); ?>

    </div>
    <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Action</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                  <div class="ibox-content" style="">
                                        <a href="#logo_website" rel="follow, index" class="btn btn-primary">Logo website</a>
                                        <a href="#intro_footer"  rel="follow, index"class="btn btn-success">Giới thiệu cuối trang</a>
                                        <a href="#manager_infomation" rel="follow, index" class="btn btn-warning">QL thông tin chung website</a>
                                        <a href="#quich_links" rel="follow, index" class="btn btn-info">Quich links</a>
                                        <a href="#icon" rel="follow, index" class="btn btn-danger">Icon</a>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <div class="row" id="logo_website">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Logo website</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($logo->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                        <div class="ibox-content" style="">
                                            <div id="image-diplay">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Logo hiện tại(*):</label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                            <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($logo->image); ?>" alt=""> 
                                                    </div>
                                                    <div>
                                                        <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                            <span class="fileinput-new"> Chọn ảnh </span>
                                                            <span class="fileinput-exists"> Đổi ảnh </span>
                                                            <input type="file" name="image">
                                                        </span>
                                                        <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                        data-dismiss="fileinput"> Xóa ảnh </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      <input type="hidden" name="class" value="logo">
                                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Edit changes</button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row" id="intro_footer">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Giới thiệu cuối trang</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($intro->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                        <div class="ibox-content" style="">
                                            <div id="image-diplay">
                                                <?php echo e(csrf_field()); ?>

                                                <fieldset class="form-group">
                                                  <label for="exampleInputEmail1">Tiêu đề (vietnam)</label>
                                                  <input type="text" name="icon_namevi" class="form-control" id="exampleInputEmail1" placeholder="VD: About us" value="<?php echo e($intro->label_vi); ?>">
                                                  <small class="text-muted">Lưu ý: Giới hạn tiêu đề 50 kí tự.</small>
                                                </fieldset>


                                                <fieldset class="form-group">
                                                  <label for="exampleInputEmail1">Tiêu đề (english)</label>
                                                  <input type="text" name="icon_nameen" class="form-control" id="exampleInputEmail1" placeholder="VD: About us" value="<?php echo e($intro->label_en); ?>">
                                                  <small class="text-muted">Lưu ý: Giới hạn tiêu đề 50 kí tự.</small>
                                                </fieldset>

                                                <fieldset class="form-group">
                                                  <label for="exampleTextarea">Giới thiệu(vietnam)*:</label>
                                                  <textarea class="form-control" name="icon_iconvi" id="exampleTextarea" rows="3" placeholder="VD:Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."><?php echo e($intro->title_vi); ?></textarea>
                                                  <small class="text-muted">Lưu ý: Giới hạn 150 kí tự.</small>
                                                </fieldset>

                                                <fieldset class="form-group">
                                                  <label for="exampleTextarea">Giới thiệu(english)*:</label>
                                                  <textarea class="form-control" name="icon_iconen" id="exampleTextarea" rows="3" placeholder="VD:Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."><?php echo e($intro->title_en); ?></textarea>
                                                  <small class="text-muted">Lưu ý: Giới hạn 150 kí tự.</small>
                                                </fieldset>
                                            </div>
                                            <input type="hidden" name="class" value="intro">
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Edit changes</button>
                                            </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="row" id="manager_infomation">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Quản lý thông tin chung website</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content" style="">
                                        <table cid="example1" class="table table-bordered table-striped option-table">
                                          <thead>
                                            <tr>
                                              <th class="col-md-2">Tên thông tin</th>
                                              <th class="col-md-3">Vị trí</th>
                                              <th class="col-md-5">Giá trị</th>
                                              <th class="col-md-2">Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                              <td><?php echo e($cd->description); ?></td>
                                              <td><?php echo e($cd->position); ?></td>
                                              <td><?php echo e($cd->value); ?></td>
                                              <td style="text-align: center">
                                                <button type="button" title="sửa thứ tự menu" class="btn btn-info" data-toggle="modal" data-target="#<?php echo e($cd->id); ?>option">
                                                  <i class="fa fa-edit"></i>
                                                </button>
                                              </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="row" id="quich_links">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>QUICK LINKS</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content" style="">
                                        <table cid="example1" class="table table-bordered table-striped option-table">
                                          <thead>
                                            <tr>
                                              <th class="col-md-1">STT</th>
                                              <th class="col-md-3">Name(vietnam)</th>
                                              <th class="col-md-3">Name(english)</th>
                                              <th class="col-md-4">Url</th>
                                              <th class="col-md-2">Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php $__currentLoopData = $list_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($list->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                              <?php echo e(csrf_field()); ?>

                                              <input type="hidden" name="class" value="list_footer">
                                            <tr>
                                                <td><?php echo e($key+1); ?></td>
                                                <td><div class="input-group m-b"><span class="input-group-addon">VI</span> <input name="icon_namevi" type="text" placeholder="Đường dẫn icon" value="<?php echo e($list->label_vi); ?>" class="form-control"></div></td>

                                                <td><div class="input-group m-b"><span class="input-group-addon">EN</span> <input name="icon_nameen" type="text" placeholder="Đường dẫn icon" value="<?php echo e($list->label_en); ?>" class="form-control"></div></td>

                                                <td><div class="input-group m-b"><span class="input-group-addon">URL</span> <input name="link_modal" type="text" placeholder="Đường dẫn icon" value="<?php echo e($list->link); ?>" class="form-control"></div></td>

                                                <td style="text-align: center">
                                                  <button type="submit" title="Update icon" class="btn btn-info">
                                                    <i class="fa fa-edit"></i> Change edit
                                                  </button>
                                                </td>
                                              </tr>
                                              </form>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="row" id="icon">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>ICON</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content" style="">
                                        <table cid="example1" class="table table-bordered table-striped option-table">
                                          <thead>
                                            <tr>
                                              <th class="col-md-2">STT</th>
                                              <th class="col-md-3">Icon</th>
                                              <th class="col-md-5">Url</th>
                                              <th class="col-md-2">Action</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <style type="text/css">
                                              .thumbnail_style img{
                                                width: 50%; height: auto;
                                              }
                                            </style>
                                            <?php $__currentLoopData = $icon_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $icon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <form action="<?php echo e(url('admin/custom-display/update')); ?>/<?php echo e($icon->id); ?>" method="POST" role="form" enctype="multipart/form-data">
                                              <?php echo e(csrf_field()); ?>

                                              <input type="hidden" name="class" value="icon_footer">
                                              <tr>
                                                <td><?php echo e($key+1); ?></td>
                                                <td>
                                                  <div id="image-diplay" style="width: 100%">
                                                      <div class="form-group">
                                                          <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                              <div class="fileinput-preview thumbnail thumbnail_style" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                                  <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($icon->image); ?>" alt="">
                                                          </div>
                                                          <div>
                                                              <span class="btn red btn-outline btn-file" style="background: transparent !important;">
                                                                  <input type="file" name="image">
                                                              </span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                                </td>
                                                <td><div class="input-group m-b"><span class="input-group-addon">URL</span> <input name="link_modal" type="text" placeholder="Đường dẫn icon" value="<?php echo e($icon->link); ?>" class="form-control"></div></td>
                                                <td style="text-align: center">
                                                  <button type="submit" title="Update icon" class="btn btn-info">
                                                    <i class="fa fa-edit"></i> Change edit
                                                  </button>
                                                </td>
                                              </tr>
                                            </form>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                          </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="row">
              <div class="col-lg-10 col-lg-offset-1">
                <div class="ibox float-e-margins">
                  <div class="container">
                    <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="modal fade modal-edit-post" id="<?php echo e($cd->id); ?>option" style="display: none;">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="alert alert-danger hide">
                            Edit thất bại!
                          </div>
                          <div class="alert alert-success hide">
                            Edit thành công!
                          </div>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span></button>
                              <h4 class="modal-title">Thay đổi giá trị</h4>
                            </div>
                            <form method="get" id="form-edit<?php echo e($cd->id); ?>">
                              <?php echo e(csrf_field()); ?>

                              <input type="hidden" name="id" value="<?php echo e($cd->id); ?>">
                              <div class="modal-body">
                                <?php if($cd->key_option == 'address1' || $cd->key_option == 'address2' || $cd->key_option == 'address3'): ?>
                                <div class="form-group">
                                  <label><?php echo e($cd->description); ?>(vietnam): </label>
                                  <input type="text" class="form-control" id="usr" name="value" value="<?php echo e($cd->value); ?>">
                                  <p style="color:red; display:none;" class="error errorValue"></p>
                                </div>
                                <div class="form-group">
                                  <label><?php echo e($cd->description); ?>(english): </label>
                                  <input type="text" class="form-control" id="usr" name="value_en" value="<?php echo e($cd->value_en); ?>">
                                </div>
                                <?php else: ?>
                                  <label><?php echo e($cd->description); ?>: </label>
                                  <input type="text" class="form-control" id="usr" name="value" value="<?php echo e($cd->value); ?>">
                                  <input type="hidden" class="form-control" id="usr" name="value_en" value="<?php echo e($cd->value_en); ?>">
                                  <p style="color:red; display:none;" class="error errorValue"></p>
                                <?php endif; ?>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <button type="button" data-id="<?php echo e($cd->id); ?>" class="btn btn-primary edit_ajax_post">Save changes</button>
                              </div>
                            </form>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                  </div>
                </div>
              </div>




          </div>
        </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
      <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });

             $('.edit_ajax_post').click(function(){
                  var data_id = $(this).data('id');
                  var data = $('#form-edit'+data_id).serialize();
                  $.ajax({
                   url:"<?php echo e(route('postOption')); ?>",
                   method:"get",
                   data:data,
                   dataType:'JSON',
                   
                   success:function(data)
                   {
                    console.log(data);
                    
                    if (data.error == true) {
                        $('.alert-danger').addClass('show');
                        $('.error').hide();  
                        if (data.message.slug != undefined) {
                            $('.errorValue').show().text(data.message.value[0]);
                        }
                    } else {
                            $('.alert-success').addClass('show');
                        setTimeout(function(){

                            $('.table-striped').load(location.href + ' .table-striped>*');
                            $('.modal-edit-post').modal('hide');
                            $('.alert-success').removeClass('show');
                            $('.alert-danger').removeClass('show');
                            $('.alert-success').addClass('hide');
                            $('.alert-danger').addClass('hide');
                        }, 1000);
                    }
                   },

                  })
                });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>