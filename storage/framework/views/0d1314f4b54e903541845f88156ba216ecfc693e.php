<?php $__env->startSection('title','Sản Phẩm | Danh sách'); ?>
<?php $__env->startSection('style.css'); ?>
<?php $__env->stopSection(); ?>
<style>
    .category_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Sản Phẩm</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách sản Phẩm</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


    

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên sản phẩm</th>
                                    <!-- <th data-hide="all">Category</th> -->
                                    <th data-hide="phone">Giá</th>
                                    <th data-hide="phone">Trạng thái</th>
                                    <th class="text-right" data-sort-ignore="true">Thao tác</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $productStore; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr >
                                            <td>
                                               <?php echo e($store->name); ?>

                                            </td>
                                            <td>
                                                <?php echo e(number_format($store->price)); ?> đ
                                            </td>
                                            
                                            <td class="load_status_<?php echo e($store->id); ?>">
                                                <?php echo e(csrf_field()); ?>

                                                <?php if($store->status == 0): ?>
                                                <button type="button" class="btn btn-primary colum_status" data-id="<?php echo e($store->id); ?>" data-stt="1">Active</button>
                                                <?php else: ?>
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="<?php echo e($store->id); ?>" data-stt="0">Pending</button>
                                                <?php endif; ?>
                                                
                                            </td>
                                            <td class="text-right">
                                                
                                                    <div class="btn-group">
                                                        <a href="/admin/product/delete/<?php echo e($store->id); ?>" class="btn-white btn btn-xs" >Delete</a>
                                                        <a href="<?php echo e(route('editProduct',['id'=>$store->id])); ?>" class="btn-white btn btn-xs" >Edit</a>
                                                    </div>
                                                <!-- </form> -->
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"<?php echo e(route('update_status_product')); ?>",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)
        {
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>