<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    //dd($language);
    ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h4>Sửa danh mục</h4>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content" style="padding: 50px">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.program.update', $program->id)); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Tên danh mục(vietnam):</label> 
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">vi</span> 
                                                    <input name="name_vi" type="text" placeholder="Tên danh mục tiếng việt" maxlength="255" value="<?php echo e($program->name_vi); ?>" class="form-control">
                                                </div> 
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">en</span> 
                                                    <input name="name_en" type="text" id="name" placeholder="Tên danh mục tiếng Anh" maxlength="255" value="<?php echo e($program->name_en); ?>" class="form-control">
                                                </div> 
                                                <?php if($errors->has('name_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <br><span>Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh</label> 
                                                <?php 
                                                    $chuoi = explode('-', $program->slug);
                                                    array_splice($chuoi,0,1);
                                                    $chuoi2 = implode("-",$chuoi);
                                                 ?>

                                                <input type="text" id="slug" value="<?php echo e($chuoi2); ?>" name="slug" class="form-control">
                                                 <?php if($errors->has('slug')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('slug')); ?>

                                                    </span>
                                                <?php endif; ?>
                                                <span>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>
                                            
                
                                            <div class="form-group"><label>Danh mục</label>
                                                
                                                <select name="parents_pgram" id="page_hien_thi" class="form-control">
                                                    <option value="0" <?php echo e($program->parents == 0 ? 'selected' : ''); ?>>-- ROOT -- </option>
                                                    <?php 
                                                    // dd($program->parents);
                                                    $program_test = 0;
                                                    if ($program->parents > 0){
                                                        $program_test = \App\Models\Admin\Program::where('id',$program->parents)->value('id');
                                                    }
                                                     ?>
                                                    <?php echo e(showProgram($pgramall,$program_test,0, $char ='')); ?>

                                                </select>
                                                <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn.</span>
                                            </div>
                                            <div class="form-group"><label>Mô tả</label>
                                                <textarea type="text" class="form-control" name="title_vi"  style="margin-bottom: 5px" placeholder="Mô tả danh mục tiếng việt (không bắt buộc)" maxlength="255"><?php echo e($program->title_vi); ?></textarea>
                                                <?php if($errors->has('title_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_vi')); ?>

                                                    </span>
                                                <?php endif; ?>

                                                <textarea type="text" class="form-control" name="title_en" placeholder="Mô tả danh mục tiếng anh (không bắt buộc)" maxlength="255"><?php echo e($program->title_en); ?></textarea>
                                                <?php if($errors->has('title_en')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('title_en')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div id="image-diplay">
                                               <div class="form-group">
                                                <label>Ảnh danh mục (không bắt buộc):</label><br>
                                                <div class="row">    
                                                   <div class="col-md-12">
                                                       <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                           <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                                                style="width: 100%; height: 250px;">
                                                                    <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($program->image); ?> ">
                                                           </div>
                                                           <div style="text-align: center">
                                                                <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                                                    <span class="fileinput-new"> Chọn ảnh </span>
                                                                    <span class="fileinput-exists"> Đổi ảnh </span>
                                                                    <input type="file" name="image">
                                                                </span>
                                                               <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                                  data-dismiss="fileinput"> Xóa ảnh </a>
                                                               
                                                           </div>
                                                           <span class="text-center text-danger" role="alert">
                                                                <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                            </span>
                                                       </div>
                                                   </div>
                                                </div>
                                               </div>
                                           </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Sửa danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

    
    <?php 

    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {


      foreach ($pgramall as $key => $item) {


          if ($item->parents == $parent)
          {
            echo '<option value="'.$item->id.'"';
            if($item->id == $program_test){
                echo 'selected="selected"';
            }
            if($item->parents == $program_test){
                echo 'disabled="disabled"';
            }
            if($parent==0){
                echo 'style="color:red"';
            }
            echo '>';
            if($item->parents == $program_test){
                if ($item->parents == 0) {
                    echo $char . $item->name_vi;
                }
                if ($item->parents != 0) {
                    echo $char . $item->name_vi.' (danh mục con)';
                }
            }
            if($item->parents != $program_test){
                echo $char . $item->name_vi;
            }
            echo '</option>';
            if ($item->parents != $program_test){
                showProgram($pgramall,$program_test, $item->id, $char.'---| ');
            }

        }
        
    }
}


?>

   
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
    <script>

  $("#page_hien_thi").change(function() {
      
      var gia_tri = $(this).val();
      if(gia_tri == ''){
        alert('Vui lòng chọn trang hiển thị');
      }else {
          $.ajax({
           method:'get',
           url:"<?php echo e(url('')); ?>/admin/program/chon-danh-muc/"+gia_tri,
           dataType: 'json',
          }).done(function(data){
              console.log(data);
              $('#load_cate').load(location.href + " #load_cate>*");
          }).fail(function(error){
            console.log(error.responseText);
             $('#load_cate').html(error.responseText);
            // $('#load_cate').load(location.href + " #load_cate>*");
          });
          }
    
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>