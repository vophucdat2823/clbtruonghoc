<?php $__env->startSection('content'); ?>
<div class="load_main_page">
    <main id="main_mobie">
        <div id="about_us" style="padding-bottom: 10%;padding-top: 10%;">

             <div class="activities_home">
                    <div class="col-md-12 col-sm-12 col-12 mobie_activities txt_mark_banner">
                         <?php if($language_id == 'vi'): ?>
                            <p><?php echo e($category->name_vi); ?></p>
                        <?php else: ?>
                            <p><?php echo e($category->name_en); ?></p>
                        <?php endif; ?>
                    </div>
                </div>


            <div class="content_about_us">
                <div class="col-12 mt-3">
                    <h2 style="font-size: 5.5vw;padding: 2vw 0;}"><?php echo e($language_id == 'vi' ? $cus_dpl->label_vi : $cus_dpl->label_en); ?></h2>
                    <hr>
                    <div id="accordion">
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stt => $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            
                            $show = $stt == '0' ? 'show' : '';
                         ?>
                        <div class="card" style="margin-bottom: 5%">
                            <div class="tab_program txt_mark_banner" >
                                <a class="btn collapsed card-link btn-active" style="width: 100%;white-space: normal !important;font-size: 4vw;" data-toggle="collapse" href="#collapse<?php echo e($cart->id); ?>">
                                     <?php echo e($cart->name); ?>

                                </a>
                            </div>
                            
                            <div id="collapse<?php echo e($cart->id); ?>" class="collapse <?php echo e($show); ?>" data-parent="#accordion">
                                <div class="card-body content_cosllap_one">
                                    <?php if($cart->image != null): ?>
                                        
                                    <?php endif; ?>
                                    <p><?php echo $cart->content; ?>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_pc" style="margin-bottom: 5%;">
        <div id="about_us" style="padding-bottom: 10%;">
            <div class="about_top_title">
                <?php echo $__env->make('web.layout.title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="content_about_us">
                <div class="container mt-3">
                    <h2><?php echo e($language_id == 'vi' ? $cus_dpl->label_vi : $cus_dpl->label_en); ?></h2>
                    <hr>
                    <div id="accordion">
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stt => $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            
                            $show = $stt == '0' ? 'show' : '';
                         ?>
                        <div class="card" style="margin-bottom: 10px">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapse<?php echo e($cart->id); ?>">
                                    <img src="<?php echo e(url('public/web/images')); ?>/squares.svg" alt="" width="1.2%"> <?php echo e($cart->name); ?>

                                </a>
                            </div>
                            <div id="collapse<?php echo e($cart->id); ?>" class="collapse <?php echo e($show); ?>" data-parent="#accordion">
                                <div class="card-body content_cosllap_one">
                                    <?php if($cart->image != null): ?>
                                        
                                    <?php endif; ?>
                                    <p><?php echo $cart->content; ?>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                    <img src="<?php echo e(asset('/public/web/images/squares.svg')); ?>" alt="" width="1.2%">Organizationalstructure
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                    <img src="<?php echo e(asset('/public/web/images/squares.svg')); ?>" alt="" width="1.2%">Development
                                    Strategy
                                </a>
                            </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                    <img src="<?php echo e(asset('/public/web/images/squares.svg')); ?>" alt="" width="1.2%">Mission, Vision, Core values
                                    and Motto
                                </a>
                            </div>
                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>