<?php $__env->startSection('title','Cài đặt'); ?>
<?php $__env->startSection('content'); ?>
<div class="wrapper wrapper-content">

  <div class="row">
    <div class="col-lg-12">
     <?php if(session('success')): ?>
     <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo e(session('success')); ?>

    </div>
    <?php endif; ?>
    <?php if(session('error')): ?>
    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo e(session('error')); ?>

    </div>
    <?php endif; ?>
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="ibox float-e-margins">
          
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Table With Full Features</h3>
                      <?php if(session('success')): ?>
                      <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                      </div>
                      <?php endif; ?>
                      <?php if(session('error')): ?>
                      <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                      </div>
                      <?php endif; ?>
                    </div>

                    <!-- /.box-header -->
                    <style type="text/css" media="screen">
                          /* .option-table > thead > tr > th{
                            text-align: center
                          } 
                          .option-table > tbody > tr > td{
                            padding-left: 5%;
                            } */
                          </style>
                          <div class="box-body">
                            <h2>Quản lý thông tin chung website</h2>           
                            <table cid="example1" class="table table-bordered table-striped option-table">
                              <thead>
                                <tr>
                                  <th class="col-md-2">Tên thông tin</th>
                                  <th class="col-md-3">Vị trí</th>
                                  <th class="col-md-5">Giá trị</th>
                                  <th class="col-md-2">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                  <td><?php echo e($cd->description); ?></td>
                                  <td><?php echo e($cd->position); ?></td>
                                  <td><?php echo e($cd->value); ?></td>
                                  <td style="text-align: center">
                                    <button type="button" title="sửa thứ tự menu" class="btn btn-info" data-toggle="modal" data-target="#<?php echo e($cd->id); ?>option">
                                      <i class="fa fa-edit"></i>
                                    </button>
                                  </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </tbody>
                            </table>
                            
                          </div>
                          <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </section>
                  <!-- /.content -->
                </div>
                <div class="container">
                  <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="modal fade" id="<?php echo e($cd->id); ?>option" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Thay đổi giá trị</h4>
                          </div>
                          <form action="" method="post">
                            <div class="modal-body">
                              <label><?php echo e($cd->description); ?>: </label>
                              <input type="text" class="form-control" id="usr" name="value" value="<?php echo e($cd->value); ?>">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        



        <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>