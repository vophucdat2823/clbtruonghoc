
<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                 <?php if(session('success')): ?>
                    <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm nhóm</h5>
                                <div class="ibox-tools">                                          
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="<?php echo e(route('admin.chooses.store')); ?>" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="form-group">
                                                <label>Tên nhóm(vietnam):</label> 
                                                <input type="text" class="form-control" name="name_vi" placeholder="VD:Đội ngũ cán bộ" value="<?php echo e(old('name_vi')); ?>" style="margin-bottom: 5px">
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="form-group">
                                                <label>Tên nhóm(english):</label> 
                                                <input type="text" class="form-control" name="name_en" placeholder="VD: Officers team" value="<?php echo e(old('name_en')); ?>" style="margin-bottom: 5px">
                                                <?php if($errors->has('name_vi')): ?>
                                                    <span class="text-center text-danger" role="alert">
                                                        <?php echo e($errors->first('name_vi')); ?>

                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm nhóm</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">

                <?php if(!empty($chooses)): ?>
                    <?php $__currentLoopData = $chooses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $ch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?php echo e($ch->name_vi); ?></h5>

                            <div class="ibox-tools">
                                <div>
                                    <a href="<?php echo e(route('admin.chooses.create_item',['id'=>$ch->id])); ?>" class="btn btn-info">Chi tiết <span style="color: #f8ac59; font-weight: bold">>></span></a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <a href="<?php echo e(route('admin.chooses.destroy',['id'=>$ch->id])); ?>" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i> DELETE</a>
                                <a data-toggle="modal" href='#modal-id' class="btn btn-primary"><i class="fa fa-fw fa-trash-o"></i> EDIT NAME</a>
                            </div>
                            <div class="modal fade" id="modal-id">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">EDIT NAME</h4>
                                        </div>
                                            <form class="form-horizontal" action="<?php echo e(route('admin.chooses.update')); ?>" method="post" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                                            <input type="hidden" name="id_update" value="<?php echo e($ch->id); ?>">

                                                <div class="form-group">
                                                    <label>Tên nhóm(vietnam):</label> 
                                                    <input type="text" class="form-control" name="name_edit_vi" placeholder="VD:Đội ngũ cán bộ" value="<?php echo e($ch->name_vi); ?>" style="margin-bottom: 5px">
                                                    <?php if($errors->has('name_edit_vi')): ?>
                                                        <span class="text-center text-danger" role="alert">
                                                            <?php echo e($errors->first('name_edit_vi')); ?>

                                                        </span>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="form-group">
                                                    <label>Tên nhóm(english):</label> 
                                                    <input type="text" class="form-control" name="name_edit_en" placeholder="VD: Officers team" value="<?php echo e($ch->name_en); ?>" style="margin-bottom: 5px">
                                                    <?php if($errors->has('name_edit_en')): ?>
                                                        <span class="text-center text-danger" role="alert">
                                                            <?php echo e($errors->first('name_edit_en')); ?>

                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                        <div class="row" style="margin: 0 auto;text-align: center;">

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</div>
   
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>