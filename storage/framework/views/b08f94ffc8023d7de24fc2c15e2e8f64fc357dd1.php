<?php $__env->startSection('title','Clb Trường Học'); ?>
<?php $__env->startSection('content'); ?>
    <main style="width: 75vw;margin:0 auto">
        <div class="my-5">
            <div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-5">
                                    <legend><?php echo e($cateCourse->name); ?></legend>
                                </div>                           
                                <div class="row">
                                    <section class="boxfilters">
                                        <?php $__currentLoopData = $cateCourse->getCourse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-6 col-sm-6 col-md-4 mb-4 style_padding_box ">
                                                <article class="boxfilter boxfilter--1 ">
                                                    <div class="boxfilter__info-hover" >
                                                        <div class="" style="height: 100%">
                                                            <div class="Boxinfo Boxinfo-100">
                                                                <div class="Boxinfo-content">
                                                                    <div class="Boxinfo-body">
                                                                        <p>
                                                                            <i class="fal fa-university"></i> Đơn vị tổ chức: <?php echo e($course->getTypeCourseOne('unit')['name']); ?>

                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-users"></i> Lớp tiêu chuẩn: <?php echo e($course->getTypeCourseOne('qty')['name']); ?>

                                                                        </p>
                                                                        <p>
                                                                           <i class="fal fa-usd-circle"></i> Học phí: <?php echo e($course->getTypeCourseOne('tuition')['name']); ?>

                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-paper-plane"></i> Điều kiện học: <?php echo e($course['study_condition']); ?>

                                                                        </p>
                                                                    </div>
                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                        <form action="<?php echo e(url('/chi-tiet')); ?>/<?php echo e($course->slug); ?>" method="get" role="form">
                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="boxfilter__img"></div>
                                                    <a href="#" class="boxfilter_link">
                                                        <div class="boxfilter__img--hover" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($course->image); ?>') ">
                                                            <img src="<?php echo e(url('public/assets/transparent')); ?>/cuahang_home.png" alt="">
                                                        </div>
                                                    </a>
                                                    <div class="clearfix">
                                                    </div>
                                                    <div class="boxfilter__info boxfilter__info1">
                                                        <h3><?php echo e($course->name); ?></h3>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script>
    $(document).on("change","#ThanhPho", function(event) {
      var id_matp = $(this).val();
      $.get("<?php echo e(url('')); ?>/boot/ajax/quanhuyen/"+id_matp,function(data) {
        console.log(data);
        $("#QuanHuyen").html(data);
      });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>