<?php $__env->startSection('style.css'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    $category = \App\Models\Admin\Category::getAllCate();
    $postTrans = \App\Models\Admin\PostTranslation::getAllPost();
    //dd($language);
    ?>
    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Bài viết</h2>
                    
                </div>
                <div class="col-lg-7">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('error')): ?>
                        <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo e(session('error')); ?>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
           
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-content">
                            <div class="row">
                                
                                
                                
                            </div>
                            <form action="<?php echo e(route('admin.post.destroy')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <button class="btn btn-danger btn-delete" type="submit" style="margin-left: 0"><i class="fa fa-fw fa-trash-o"></i>Delete all</button>
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>

                                            <th class="col-md-1" style="text-align: center"><input type="checkbox" id="check-all" value=""></th>
                                            <th class="col-md-2">Tên bài viết</th>
                                            <th class="col-md-3">Tiêu đề bài viết</th>
                                            <th class="col-md-2">Đường dẫn tĩnh</th>
                                            <th class="col-md-1">Edit</th>
                                            <th class="col-md-2">Thuộc danh mục</th>
                                            <th class="col-md-1">Ngôn ngữ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($posts)): ?>
                                            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="text-align: center"><input type="checkbox" class="i-checks item-check" name="id[]" value="<?php echo e($post['id']); ?>"></td>
                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($postTrans['code'][$post['id']]); ?>.html" target="_blank">
                                                        <span class="tag-post">
                                                             <?php echo e(str_limit($postTrans['name'][$post['id']],25)); ?>

                                                        </span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo e(url('')); ?>/<?php echo e($postTrans['code'][$post['id']]); ?>.html" target="_blank"> 
                                                        <span class="tag-post-tacgia">
                                                            <?php echo e(str_limit($postTrans['simulation'][$post['id']],50)); ?>

                                                        </span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo e(url('')); ?>/<?php echo e($postTrans['code'][$post['id']]); ?>.html" target="_blank"> 
                                                        <span class="tag-post-tacgia">
                                                            <?php echo e(str_limit($postTrans['code'][$post['id']],50)); ?>

                                                        </span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" data-toggle="modal" href='#modal-edit<?php echo e($post['id']); ?>'><i class="fa fa-edit"></i></a>
                                                </td>
                                                <?php if($postTrans['category_id'][$post['id']] != null): ?>
                                                <td>
                                                    <a href="<?php echo e(url('')); ?>/<?php echo e($postTrans['code'][$post['id']]); ?>.html" target="_blank"> 
                                                        <span class="tag-post-danhmuc">
                                                            <?php echo e($category[$postTrans['category_id'][$post['id']]]); ?>

                                                        </span>
                                                    </a>
                                                </td>
                                                <?php else: ?>
                                                <td><a href="<?php echo e(url('')); ?>/<?php echo e($postTrans['code'][$post['id']]); ?>.html" target="_blank"> 
                                                        <span class="tag-post-danhmuc">
                                                            <i>chưa chọn danh mục</i>
                                                        </span>
                                                    </a>
                                                </td>
                                                <?php endif; ?>
                                                
                                                <td>
                                                    <?php if(count($post['post_translations']) == 2): ?> 
                                                    
                                                        <?php $__currentLoopData = $post['post_translations']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        
                                                            <?php if($language[$value['language_id']] == 'english'): ?>
                                                                &nbsp;
                                                                <a href="<?php echo e(route('admin.post.edit', $value['id'])); ?>" title="Edit EN"><img src="<?php echo e(url('public')); ?>/upload/icon/english.png" width="30"><span></span></a>
                                                                &nbsp;
                                                            <?php else: ?>
                                                                &nbsp;<a href="<?php echo e(route('admin.post.edit', $value['id'])); ?>" title="Edit VN"><img src="<?php echo e(url('public')); ?>/upload/icon/vietnam.png" width="30"><span></span></a>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                        <?php $__currentLoopData = $post['post_translations']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($language[$value['language_id']] == 'english'): ?>
                                                                &nbsp;<a href="<?php echo e(route('admin.post.edit', $value['id'])); ?>">
                                                                    <img src="<?php echo e(url('public')); ?>/upload/icon/english.png" width="30">
                                                                </a>&nbsp;
                                                                &nbsp;
                                                                <a href="<?php echo e(route('admin.post.create.lang', [$post['id'],$value['category_id'],'vietnam'])); ?>">
                                                                    <img src="<?php echo e(url('public')); ?>/upload/icon/vietnam.png" width="30">
                                                                </a>&nbsp;
                                                            <?php elseif($language[$value['language_id']] == 'vietnam'): ?>
                                                                &nbsp;
                                                                    <a href="<?php echo e(route('admin.post.edit', $value['id'])); ?>">
                                                                        <img src="<?php echo e(url('public')); ?>/upload/icon/vietnam.png" width="30">
                                                                    </a>&nbsp;
                                                                &nbsp;
                                                                    <a href="<?php echo e(route('admin.post.create.lang', [$post['id'],$value['category_id'],'english'])); ?>">
                                                                        <img src="<?php echo e(url('public')); ?>/upload/icon/english.png" width="30">
                                                                    </a>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </td>
                                                
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>

                                </div>   
                            </form>
                            
                            <div class="row" style="text-align: center">
                                <?php echo e($posts->links()); ?>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php if(!empty($posts)): ?>
        <?php 

            function showCatepost($cateall, $parent = 0, $char ='',$select = 0)
            {
                foreach ($cateall as $key => $item) {
                    if ($item->parents == $parent) {
                        echo '<option value="'.$item->id.'"';
                        if($parent == 0){
                            echo 'style="color:red"';
                        }
                        if($select != 0 && $item->id == $select){
                            echo 'selected="selected"';
                        }
                        echo '>';
                        echo $char . $item->name_vi;
                        echo '</option>';
                        showCatepost($cateall ,$item->id , $char.'---| ',$select);
                    }
                }
            }
         ?>
        <?php endif; ?>
        <?php if(!empty($posts)): ?>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php 
        // dd($postTrans['category_id'][$post['id']]);
            $cateall = \App\Models\Admin\Category::all();
         ?>
            <div class="modal fade modal-edit-post" id="modal-edit<?php echo e($post['id']); ?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit "<?php echo e($postTrans['name'][$post['id']]); ?>"</h4>
                        </div>
                        <div class="alert alert-danger hide">
                            Edit thất bại!
                        </div>
                        <div class="alert alert-success hide">
                            Edit thành công!
                        </div>
                        <form method="get" id="form-edit<?php echo e($post['id']); ?>" role="form">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="post_id" value="<?php echo e($post['id']); ?>">
                        <div class="modal-body">
                            <legend>Sửa danh mục và đường dẫn cho 2 ngôn ngữ</legend>
                        
                            <div class="form-group">
                                <label for="">Đường dẫn tĩnh (*):</label>
                                <input type="text" class="form-control" name="code" value="<?php echo e($postTrans['code'][$post['id']]); ?>" placeholder="Input field">
                                <p style="color:red; display:none;" class="error errorSlug"></p>
                            </div>
                            <div class="form-group">
                                <label for="">Danh mục (*):</label>
                                <select name="category_id" id="input" class="form-control" required="required">
                                    <?php echo e(showCatepost($cateall,0,'',$postTrans['category_id'][$post['id']])); ?>

                                </select>
                                 <p style="color:red; display:none;" class="error errorCategoryId"></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" data-id="<?php echo e($post['id']); ?>" class="btn btn-primary edit_ajax_post">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });

             $('.edit_ajax_post').click(function(){
                  var data_id = $(this).data('id');
                  var data = $('#form-edit'+data_id).serialize();
                  $.ajax({
                   url:"<?php echo e(url('admin/post/edit-post-dm')); ?>",
                   method:"get",
                   data:data,
                   dataType:'JSON',
                   
                   success:function(data)
                   {
                    console.log(data);
                    
                    if (data.error == true) {
                        $('.alert-danger').addClass('show');
                        $('.error').hide();  
                        if (data.message.slug != undefined) {
                            $('.errorSlug').show().text(data.message.slug[0]);
                        }
                        if (data.message.category_id != undefined) {
                            $('.errorCategoryId').show().text(data.message.category_id[0]);
                        }
                    } else {
                            $('.alert-success').addClass('show');
                        setTimeout(function(){

                            $('.table-striped').load(location.href + ' .table-striped>*');
                            $('.modal-edit-post').modal('hide');
                            $('.alert-success').removeClass('show');
                            $('.alert-success').addClass('hide');
                            $('.alert-danger').addClass('hide');
                        }, 1000);
                    }
                   },

                  })
                });
        });
        $(document).on('click', 'input#check-all', function(event) {
            var check = $(this).is(':checked');
            if (check) {
                // alert("hhg");
                $('input.item-check').prop({
                    checked: true
                });
            }else{
                $('input.item-check').prop({
                    checked: false
                });
            }
        });



    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>