<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" src="<?php echo e(url('public/admin')); ?>/img/logo.png" />
                        </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                        <span class="block m-t-xs"> <strong class="font-bold">CMS Webify V1.10</strong>
                        </span>
                          <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#">Về Webify</a></li>
                            <li><a href="#">Liên hệ</a></li>
                            <li><a href="#">Hướng dẫn sử dụng</a></li>
                            
                          </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="<?php echo e(Request::is('admin/home') ? 'active' : ''); ?>">
                    <a href="<?php echo e(URL::to('admin/home')); ?>" ><i class="fa fa-tachometer"></i> <span class="nav-label"> Trung tâm</span>  </a>
                </li>
                
                <li class="<?php echo e(Request::is('admin/post/list') || Request::is('admin/post/create') || Request::is('admin/category/list') ? 'active' : ''); ?>">
                    <a href="post.html"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Bài viết</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php echo e(Request::is('admin/post/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.post.list')); ?>">Tất cả bài viết</a></li>
                        <li class="<?php echo e(Request::is('admin/post/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.post.create')); ?>">Thêm bài viết</a></li>
                        <li class="<?php echo e(Request::is('admin/category/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.category.list')); ?>">Danh mục</a></li>
                        
                    </ul>
                </li>
               
                <li class="<?php echo e(Request::is('admin/page/list') ? 'active' : ''); ?>">
                    <a href="<?php echo e(route('admin.page.list')); ?>"><i class="fa fa-tachometer"></i> <span class="nav-label">Tất cả trang</span>  </a>
                </li>
                <li class="<?php echo e(Request::is('admin/program/list') || Request::is('admin/pgram_post/list') || Request::is('admin/pgram_post/create')? 'active' : ''); ?>">
                    <a href="#"><i class="fa fa-sliders"></i> <span class="nav-label">QL Programs</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php echo e(Request::is('admin/pgram_post/list')? 'active' : ''); ?>"><a href="<?php echo e(route('admin.pgram_post.list')); ?>">Bài viết program</a></li>


                        <li class="<?php echo e(Request::is('admin/pgram_post/create') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.pgram_post.create')); ?>">Thêm bài viết</a></li>


                        <li class="<?php echo e(Request::is('admin/program/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.program.list')); ?>">Danh mục program</a></li>
                        
                        
                               
                    </ul>
                </li>
                <li class="<?php echo e(Request::is('admin/chooses/list') ? 'active' : ''); ?>">
                    <a href="<?php echo e(route('admin.chooses.list')); ?>"><i class="fa fa-tachometer"></i> <span class="nav-label">Slider Teachers</span>  </a>
                </li>
                
                <li class="<?php echo e(Request::is('admin/menu/list') || Request::is('admin/custom-display/list/home') || Request::is('admin/network/list') || Request::is('admin/custom-display/list/student-services') || Request::is('admin/custom-display/list/about-us')? 'active' : ''); ?>">
                    <a href="#"><i class="fa fa-sliders"></i> <span class="nav-label">Giao diện</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php echo e(Request::is('admin/menu/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.menu.list')); ?>">Menu</a></li>
                        <li class="<?php echo e(Request::is('admin/custom-display/list/home') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.custom_display.list',['name'=>'home'])); ?>">Home</a></li>

                        <li class="<?php echo e(Request::is('admin/custom-display/list/student-services') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.custom_display.list',['name'=>'student-services'])); ?>">Student Service</a></li>

                        <li class="<?php echo e(Request::is('admin/custom-display/list/about-us') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.custom_display.list',['name'=>'about-us'])); ?>">About us</a></li>

                        <li class="<?php echo e(Request::is('admin/network/list') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.network.list')); ?>">VNU-ISOCIAL</a></li>
                        
                               
                    </ul>
                </li>
                
                <li class="<?php echo e(Request::is('admin/caidat') ? 'active' : ''); ?>">
                    <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Cấu hình</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="<?php echo e(Request::is('admin/caidat') ? 'active' : ''); ?>"><a href="<?php echo e(route('caidat')); ?>">Cài đặt hiển thị</a></li>
                        
                         
                    </ul>
                </li>
                <!-- <li>
                    <a href="#"><i class="fa fa-paper-plane"></i> <span class="nav-label">Marketing</span><span class="fa arrow"></span><span class="new label-warning pull-right">New</span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Tạo landingpage</a></li>
                   
                           
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">Báo cáo</span></a>
                    
                </li> -->
                <!-- <li>
                    <a href="#"><i class="fa fa-puzzle-piece"></i> <span class="nav-label">Ứng dụng</span><span class="fa arrow"></span><span class="new label-warning pull-right">New</span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Ứng dụng đã mua</a></li>
                        <li><a href="#">Kho ứng dụng</a></li>
                           
                    </ul>
                </li> -->
                
            </ul>

        </div>
    </nav>