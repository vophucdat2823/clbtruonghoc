<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="about_us_footer">
                        <h3>About us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                        <div class="socical_about_us_footer">
                            <ul>
                                <li><a href=""><img src="<?php echo e(url('public/web')); ?>/images/home/logo1.png" alt="" width="80%"></a></li>
                                <li><a href=""><img src="<?php echo e(url('public/web')); ?>/images/home/logo2.png" alt="" width="80%"></a></li>
                                <li><a href=""><img src="<?php echo e(url('public/web')); ?>/images/home/logo3.png" alt="" width="80%"></a></li>
                                <li><a href=""><img src="<?php echo e(url('public/web')); ?>/images/home/logo4.png" alt="" width="80%"></a></li>
                                <li><a href=""><img src="<?php echo e(url('public/web')); ?>/images/home/logo5.png" alt="" width="80%"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="quick_link_footer">
                        <h3>quick links</h3>
                        <ul>
                            <li><a href=""><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Đại học</a></li>
                            <li><a href=""><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Sau Đại học</a></li>
                            <li><a href=""><i class="fas fa-angle-right color_yellow"></i>Công tác HSSV</a></li>
                            <li><a href=""><i class="fas fa-angle-right color_yellow"></i>Alumni</a></li>
                            <li><a href=""><i class="fas fa-angle-right color_yellow"></i>Q&A</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-12">
                    <div class="get_in_touch_footer">
                        <h3>get in touch</h3>
                        <div class="home_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-home color_yellow"></i>	
                                </div>
                                <div class="col-md-11">
                                    <div class="content_right_home_get_in_touch_footer">
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 1:</strong> 
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address1'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 2:</strong>
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address2'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                        </span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 3:</strong> 
                                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'address3'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                        </span>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="phone_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-phone-volume color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="phone_left_phone_get_in_touch_footer">
                                                <p>
                                                    <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone3'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                                <p> <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone4'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="phone_right_phone_get_in_touch_footer">
                                                <p>
                                                    <strong class="color_yellow">
                                                        Đại học: 
                                                    </strong>
                                                        <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'phone5'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                         | 
                                                        <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($cd->key_option == 'phone6'): ?>
                                                                <?php echo e($cd->value); ?>

                                                            <?php endif; ?>
                                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                                <p>
                                                    <strong class="color_yellow">
                                                    Sau đại học: 
                                                    </strong>
                                                    <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($cd->key_option == 'phone7'): ?>
                                                            <?php echo e($cd->value); ?>

                                                        <?php endif; ?>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="mail_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-envelope color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                        <span>
                                            <?php $__currentLoopData = $caidat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($cd->key_option == 'email2'): ?>
                                                <?php echo e($cd->value); ?>

                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>