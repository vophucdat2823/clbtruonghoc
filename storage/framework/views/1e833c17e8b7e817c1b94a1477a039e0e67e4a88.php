<?php $__env->startSection('style.css'); ?>

<?php $__env->stopSection(); ?>
<?php 
    $language_id = 'en';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
        }
 ?>
<?php $__env->startSection('content'); ?>
<main>
    <div id="people_details">
        <div class="people_details_top_title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12 dell-end-a">

                        <a href="<?php echo e(route('web.home')); ?>">
                            <span class="color_blue">
                                <?php if($language_id == 'en'): ?>
                                HOME
                                <?php elseif($language_id == 'vi'): ?>
                                Trang chủ
                                <?php endif; ?> > 
                            </span>
                        </a>
                        <?php $__currentLoopData = $array_link_title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($loop->last): ?>
                        <span class="color_gray" style="text-decoration: none;text-transform: uppercase;"><?php echo e($key); ?></span>
                        <?php else: ?>
                        <a href="<?php echo e($value); ?>.html" style="text-decoration: none;text-transform: uppercase;">
                            <span class="color_blue">
                                <?php echo e($key); ?> >
                            </span>
                        </a>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                    <img src="<?php echo e(url('public/web/images')); ?>/transparent/alumni_transpanrent.png" style="background-image: url(<?php echo e(url('public/upload/images')); ?>/<?php echo e($post_cate_first->image); ?>);background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;" alt="" width="100%">
                    <div class="text_img" style="left: 23%;top: 90%">
                        <p class="down_text" style="color: #E8B909;font-size: 30px;font-weight: 400;"><?php echo e($post_cate_first->name_en); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-8" style="margin: 0 auto;margin-top: 20px">
                <div class="content_right_pp_details">
                    <div class="row tt_right_pp_details">

                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img" style="margin: 0 auto">
                            <h3 class="color_blue"><?php echo e($post_cate_first->title_vi); ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-md-offset-2  col-10" style="margin: 0 auto">
                            <div class="fix-size-image">
                                <p><?php echo $post_cate_first->title_vi; ?></p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div id="student_service">
                <div class="content_people_details">
                    <div class="content_left_pp_details">
                        <div class="content_right_pp_details">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8" style="margin: 0 auto;">
                                    <div class="col-md-10 col-sm-10 col-md-offset-2  col-10" style="margin: 0 auto">
                                    <div class="related_news_serivce">
                                        <h5>related news</h5>
                                        <?php 
                                            $related = \App\Models\Admin\Category::where('parents',$post_cate_first->parents)->get();
                                         ?>
                                        <?php $__currentLoopData = $related; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $re_la): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="<?php echo e(url('public/web/images')); ?>/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="<?php echo e($re_la->slug); ?>"><?php echo e($re_la->name_en); ?></a>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slide_home_bottom" style="margin-top: 25px">
                <div class="container">
                    <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">
                                <h2><?php echo e($post_slider->name_en); ?></h2>
                                <p><?php echo e($post_slider->title_en); ?> </p>
                            </div>
                    </div>
                </div>
                <div class="container">
                    <div class="owl-carousel owl-theme slide_img_home">
                        <?php $__currentLoopData = $post_slider->posts_trans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <img src="<?php echo e(url('public/upload/images')); ?>/<?php echo e($slider->image); ?>" alt="" width="80%" height="200px">
                                <div class="title_img_slide_home"><h6><?php echo e($slider->name); ?></h6></div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                    </div>
                </div>
            </div>
        </div>
    </main>

    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>