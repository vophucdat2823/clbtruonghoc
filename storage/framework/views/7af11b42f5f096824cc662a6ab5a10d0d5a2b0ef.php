

<?php $__env->startSection('title','Dịch Vụ | Danh sách'); ?>
<style>
    .cate_service_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

<?php $__env->startSection('content'); ?>

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Dịch vụ của tôi</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route('partner.dashboard')); ?>">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Dịch vụ của tôi</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">


                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên dịch vụ</th>
                                    <th data-hide="phone">Danh mục</th>
                                    <th data-hide="all">Đường dẫn</th>
                                    <th data-hide="all">Đơn vị cung cấp</th>
                                    <th data-hide="all" >Chất lượng</th>
                                    <th data-hide="all">Số lượng</th>
                                    <th data-hide="all">Địa điểm</th>
                                    <th data-hide="all">Giá thanh</th>
                                    <th data-hide="phone">Trạng Thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $servicePartner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    
                                        <tr >
                                            <td>
                                               <?php echo e($service->getService->name); ?>

                                            </td>
                                            <td>
                                               <?php echo e($service->getService->getCateService['name']); ?>

                                            </td>
                                            <td>
                                               <?php echo e($service->getService->slug); ?>

                                            </td>
                                            <td>
                                                    <?php echo e($service->getService_id($service->unit_id)->name); ?>

                                            </td>
                                            <td>
                                                    <?php echo e($service->getService_id($service->quality_id)->name); ?>

                                            </td>
                                            <td>
                                                    <?php echo e($service->getService_id($service->qty_id)->name); ?>

                                            </td>
                                            <td>
                                                    <?php echo e($service->getService_id($service->address_id)->name); ?>

                                            </td>
                                            <td>
                                                    <?php echo e($service->getService_id($service->price_id)->name); ?>

                                            </td>
                                            <td class="load_status_<?php echo e($service->id); ?>">
                                                <?php echo e(csrf_field()); ?>

                                                <?php if($service->status == 0): ?>
                                                <button type="button" class="btn btn-primary colum_status" data-id="<?php echo e($service->id); ?>" data-stt="1">Cung cấp</button>
                                                <?php else: ?>
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="<?php echo e($service->id); ?>" data-stt="0">Tạm ngưng cung cấp</button>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-right">
                                                <form action="<?php echo e(route('service-partner.destroy',['id'=>$service['id']])); ?>" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn-white btn btn-xs">Ngưng cung cấp</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<!-- FooTable -->
<script src="<?php echo e(url('public/admin')); ?>/js/plugins/footable/footable.all.min.js"></script>
<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });
</script>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"<?php echo e(route('service-partner.update_status')); ?>",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)
        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin_2.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>