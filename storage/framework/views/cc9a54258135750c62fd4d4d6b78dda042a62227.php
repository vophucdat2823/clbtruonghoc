<?php $__env->startSection('style.css'); ?>
    
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
        $languages = \App\Models\Admin\Language::orderBy('language_id','desc')->get()->toArray();
    ?>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                    <form class="form-horizontal" action="<?php echo e(route('admin.post.store.lang')); ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="language_id" value="<?php echo e($language); ?>">
                        <input type="hidden" name="category_id" value="<?php echo e($category_id); ?>">
                        <input type="hidden" name="post_id" value="<?php echo e($post_id); ?>">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <?php if($language == 'english'): ?>
                                            <h5>Thêm bài viết tiếng Anh</h5>
                                            <?php else: ?>
                                            <h5>Thêm bài viết tiếng việt</h5>
                                            <?php endif; ?>
                                        <div class="ibox-tools">                                          
                                        </div>
                                    </div>
                                    
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                
                                                 
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="email">Tên bài viết(*):</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                            <input type="text" class="form-control" name="name" placeholder="Tên bài viết" value="<?php echo e(old('name')); ?>">
                                                        </div>
                                                        <?php if($errors->has('name')): ?>
                                                            <span class="text-center text-danger" role="alert">
                                                                <?php echo e($errors->first('name')); ?>

                                                            </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                   <label class="control-label col-sm-3" for="email">Tiêu đề bài viết(*):</label>
                                                   <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                                                            <textarea type="text" class="form-control" name="simulation" placeholder="Mô phỏng bài viết" value=""><?php echo e(old('simulation')); ?></textarea>
                                                        </div>
                                                        <?php if($errors->has('simulation')): ?>
                                                            <span class="text-center text-danger" role="alert">
                                                                <?php echo e($errors->first('simulation')); ?>

                                                            </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <hr style="border: 1px solid black">
                                                <label class="control-label col-sm" for="email">Nội dung bài viết(*):</label><br><br>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <textarea type="text" class="form-control" id="description123" name="content"><?php echo old('content'); ?></textarea>
                                                            <?php if($errors->has('content')): ?>
                                                                <span class="text-center text-danger" role="alert">
                                                                    <?php echo e($errors->first('content')); ?>

                                                                </span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Chọn ảnh hiển thị bài viết(*):</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content" style="padding: 0">
                                        <div id="image-diplay">
                                           <div class="form-group">
                                               <div class="col-md-12">
                                                <label class="control-label col-sm-4" style="padding-top: 0;text-align: left" for="email">Hiển thị ảnh(*):</label>
                                                      <div class="col-sm-7" style="text-align: left">
                                                            <div class="input-group">
                                                              <label>
                                                                <input type="radio" name="status_img" id="input" value="0" checked="checked">
                                                                Không
                                                              </label>
                                                              <label style="padding-left: 10px">
                                                                <input type="radio" name="status_img" id="input" value="1">
                                                                Có
                                                              </label>
                                                            </div>
                                                      </div>
                                                   <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                                       <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: auto;">
                                                        <img src="<?php echo e(url('public/web/images')); ?>/logo.png" alt=""> 

                                                       </div>
                                                       <div style="text-align: center">
                                                            <span class="btn red btn-outline btn-file" style="background: #1f81ff !important; ">
                                                                <span class="fileinput-new" > Chọn ảnh </span>
                                                                <span class="fileinput-exists"> Đổi ảnh </span>
                                                                <input type="file" name="image">
                                                            </span>
                                                           <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                                              data-dismiss="fileinput"> Xóa ảnh </a>
                                                           
                                                       </div>
                                                       <span class="text-center text-danger" role="alert">
                                                            <?php if($errors->has('image')): ?>
                                                                   <?php echo e($errors->first('image')); ?>

                                                               <?php endif; ?>
                                                        </span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Lưu</button>
                                <a href="<?php echo e(route('admin.post.list')); ?>" type="submit" class="btn btn-danger">
                                    <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                                </a>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('public/pulgin/ckeditor/ckeditor.js')); ?>"></script>
 <script>
  CKEDITOR.replace( 'description123', {
      filebrowserBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html')); ?>',
      filebrowserImageBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Images')); ?>',
      filebrowserFlashBrowseUrl: '<?php echo e(asset('public/pulgin/ckfinder/ckfinder.html?type=Flash')); ?>',
      filebrowserUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')); ?>',
      filebrowserImageUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')); ?>',
      filebrowserFlashUploadUrl: '<?php echo e(asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')); ?>'
    });
</script>
<?php $__env->stopSection(); ?>

    
<?php echo $__env->make('admin.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>