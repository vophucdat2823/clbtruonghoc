<?php $__env->startSection('title','Clb Trường Học'); ?>
<?php $__env->startSection('stylesheet'); ?>
<link href="<?php echo e(url('public/admin/css/plugins/clockpicker/clockpicker.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('public/admin/css/plugins/datapicker/datepicker3.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('public/admin/css/plugins/daterangepicker/daterangepicker-bs3.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <main>
        <div class="box_title_categories box_title_categories_detail text-left" style="background: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($course_detail->banner); ?>') no-repeat;">
            <div class="bg_overlay"></div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <div class="content_categories content_categories_detail">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 pull-right box_detail_right">
                        <div class="info_box_detail_right">
                            <div class="box_top_right hidden-xs">
                                <img src="<?php echo e(url("public/assets/transparent/image_detail.png")); ?>" class="transparent" style="background-image: url('<?php echo e(url('public/upload/images')); ?>/<?php echo e($course_detail->image); ?>')" width="100%">
                            </div>
                            <div class="box_price_right">
                                <h5 class="text-center">
                                    <span class="text-uppercase font-Light">KHOÁ HỌC KỸ NĂNG SỐNG SỐ 1</span><br />
                                </h5>
                            </div><!-- end .box_price_right-->
                            <div class="bprod-btn_new">
                                <div class="bc-btn text-center">
                                    <a class="note border-top-5cc2a8 border-right-5cc2a8 bg-5cc2a8 bg-gradient-5cc2a8 color-000000"data-toggle="tab" href="#menu3">Đăng ký học</a>
                                </div>
                            </div>
                            <div class="ls_gr_text">
                                <ul>
                                    <li><i class="fal fa-university"></i> Đơn vị tổ chức: <?php echo e($course_detail->getTypeCourseOne('unit')['name']); ?></li>
                                    <li><i class="fal fa-clock"></i> Chất lượng: <?php echo e($course_detail->getTypeCourseOne('quality')['name']); ?></li>
                                    <li><i class="fal fa-clock"></i> Thời lượng: <?php echo e($course_detail->getTypeCourseOne('time')['name']); ?></li>
                                    <li><i class="fal fa-users"></i> Giá thành: <?php echo e($course_detail->getTypeCourseOne('price')['name']); ?></li>
                                    <li><i class="fal fa-users"></i> Lớp tiêu chuẩn: <?php echo e($course_detail->getTypeCourseOne('qty')['name']); ?></li>
                                    <li><i class="fal fa-usd-circle"></i> Học phí: <?php echo e($course_detail->getTypeCourseOne('tuition')['name']); ?></li>
                                    <li><i class="fal fa-paper-plane"></i> Điều kiện để học: <?php echo e($course_detail['study_condition']); ?></li>
                                </ul>
                            </div><!-- end .ls_gr_text-->
                        </div>
                    </div>
                    <div class="col-sm-9 col-xs-12 pull-left box_detail_left">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist box_detail_left_ul">
                            <li class="nav-item box_detail_left_li_1 " >
                              <a class="box_detail_left_a bg-5CC2A8" style="border-right: 0"  onclick="event.preventDefault();">&ensp;</a>
                            </li>
                            <li class="nav-item box_detail_left_li">
                              <a class="nav-link box_detail_left_a active" data-toggle="tab" href="#home">MÔ TẢ DỊCH VỤ</a>
                            </li>
                            <li class="nav-item box_detail_left_li">
                              <a class="nav-link box_detail_left_a" data-toggle="tab" href="#menu2">HÌNH ẢNH</a>
                            </li>
                            <li class="nav-item box_detail_left_li ">
                              <a class="nav-link box_detail_left_a " data-toggle="tab" href="#menu3">ĐĂNG KÝ</a>
                            </li>


                        </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div id="home" class="container tab-pane active "><br>
                                <div class="box_people_view m-b-40 bg-f6f6f6">
                                    <?php echo $course_detail->description; ?>

                                    <div class="box_viewmore">
                                        <ul class="list-inline">
                                            <li class="icon_click">Xem toàn bộ <i class="fa fa-chevron-circle-right"></i></li>
                                            
                                        </ul>
                                        <div class="bg_view"></div>
                                    </div><!-- end .box_viewmore-->
                                </div>
                            </div>
                            <div id="menu1" class="container tab-pane fade"><br>
                              <h3>Menu 1</h3>
                              <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <div id="menu2" class="container tab-pane fade"><br>
                              <h3>Menu 2</h3>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                            </div>
                            <div id="menu3" class="container tab-pane fade " style="padding: 0 !important"><br>
                              <div class="box_people_view m-b-40 bg-f6f6f6" style="max-height: 100%;">
                                    <form class="form-inline" action="<?php echo e(route('user.sendRequire')); ?>" method="POST">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="box_ls_people">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Bạn muốn đặt tiệc vào thời gian nào?</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people form-inline" >
                                                        <div class="form-group col-sm-6" style="padding-left: 0" id="data_1">
                                                            <div class="input-group date" style="position: relative;align-items: center;">
                                                                <input type="text" class="form-control" name="date" value="03/04/2014" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                                <span class="input-group-addon" style="position: absolute;right: 5px;">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group clockpicker col-sm-6" style="padding-right: 0" data-autoclose="true">
                                                            <div class="input-group" style="position: relative;align-items: center;">
                                                                <input type="text" class="form-control" value="09:30" name="time" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">

                                                                <span class="input-group-addon" style="position: absolute;right: 5px;">
                                                                    <i class="far fa-clock"></i>
                                                                </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Ngân sách tổ chức tiệc cho 1 HS là bao nhiêu?</p>
                                                    </div><!-- end .info_ls_people-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="price" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Số lượng suất ăn là bao nhiêu suất?</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="quantity" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Bạn có thể tham khảo combo tiệc sinh nhật có sẵn của CLBtruonghoc <a href="#">tại đây</a></p>
                                                    </div>
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Để CLB truonghoc có thể tư vấn chi tiết hơn, ban vui lòng để lại email và số điện thoại cá nhân nhé! </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Họ tên</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="name" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>

                                                
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Số điện thoại</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="phone" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Email</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="email" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <?php 
                                                    $question = App\Question::where('type','course')->where('primary_id',$course_detail->id)->get();
                                                 ?>

                                                <?php $__currentLoopData = $question; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ques): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-sm-6">
                                                        <div class="info_ls_people">
                                                            <label for="" style="text-align: left;float: inline-start;"><?php echo e($ques->question); ?></label>
                                                            <input type="text" class="form-control" name="question[]" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                            <input type="hidden" name="question_id[]" value="<?php echo e($ques->id); ?>">
                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div><!-- end .box_ls_people-->
                                        <div class="row box_ls_people" style="width: 100%;text-align: center;margin-top: 15px">
                                            <div class="col-12">
                                                <button type="submit" class="btn pull-right" style="background: #458CC7; border-radius: 15px;color: #fff;font-size: 13pt;padding: 3px 40px;">Gửi yêu cầu</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                          </div>


                        <!-- end .box_people-->
                        
                        

                        <!-- end .box_price-->
                    </div>
                </div>
            </div>
        </div><!-- end .content_categories_detail-->
    </main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <!-- Data picker -->
   <script src="<?php echo e(url('public/admin/js/plugins/datapicker/bootstrap-datepicker.js')); ?>"></script>
    <!-- Clock picker -->
    <script src="<?php echo e(url('public/admin/js/plugins/clockpicker/clockpicker.js')); ?>"></script>
    <!-- Date range picker -->
    <script src="<?php echo e(url('public/admin/js/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(".box_detail_left .box_people_view .box_viewmore li.icon_click").click(function () {
        $('.box_people_view').css("max-height", "100%");
        $('.box_viewmore').css('background', 'none');
        $('.bg_view').css('background', 'none');
        $('.box_viewmore ul li.icon_click').css("display", "none");
    });

    $('.expand-menu').click(function () {
        $('.toggle-navbar').addClass('open');
        $('.submenu-bg').show();
        $('html').css('overflow', 'hidden');
    });
    $('.toggle-navbar .close').click(function () {
        $('.toggle-navbar').removeClass('open');
        $('.submenu-bg').hide();
        $('html').css('overflow', 'inherit');
    });

    $('.left-menu .menu-list ul li .sub-btn').click(function () {
        $('.left-menu .menu-list .menu-content').addClass('open');
        $(this).parent().addClass('active');
        $('.left-menu').addClass('amz-leftmn');
    });

    $('.left-menu .menu-list ul li .sub-content ul li.back').click(function () {
        $('.left-menu .menu-list .menu-content').removeClass('open');
        $('.left-menu .menu-list ul li').removeClass('active');
        $('.left-menu').removeClass('amz-leftmn').removeClass('ebay-leftmn');
    });

    $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    $('.clockpicker').clockpicker();
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>