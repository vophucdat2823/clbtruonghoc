<?php return array (
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'harimayco/laravel-menu' => 
  array (
    'providers' => 
    array (
      0 => 'Harimayco\\Menu\\MenuServiceProvider',
    ),
    'aliases' => 
    array (
      'Menu' => 'Harimayco\\Menu\\Facades\\Menu',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'zizaco/entrust' => 
  array (
    'providers' => 
    array (
      0 => 'Zizaco\\Entrust\\EntrustServiceProvider',
    ),
    'aliases' => 
    array (
      'Entrust' => 'Zizaco\\Entrust\\EntrustFacade',
    ),
  ),
);