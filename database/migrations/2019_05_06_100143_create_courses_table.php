<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('image');
            $table->string('study_condition')->nullable()->default('Không');
            $table->text('description')->nullable();
            $table->double('price',10,0);
            $table->smallInteger('number_lesson')->unsigned();
            $table->smallInteger('number_student')->unsigned();
            $table->smallInteger('status')->unsigned()->default(1);
            $table->unsignedInteger('cate_course_id')->nullable();
            // $table->foreign('cate_course_id')->references('id')->on('cate_courses')->onDelete('cascade');
            $table->unsignedInteger('user_id')->nullable();
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
