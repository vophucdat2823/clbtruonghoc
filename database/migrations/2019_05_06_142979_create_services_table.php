<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('url_link_combo')->unique();
            $table->string('image');
            $table->string('study_condition')->nullable()->default('Không');
            $table->text('description')->nullable();
            $table->string('price');
            $table->smallInteger('number_people')->unsigned();
            $table->smallInteger('number_date')->unsigned();
            $table->unsignedInteger('user_id')->nullable();
            $table->smallInteger('status')->unsigned()->default(0);
            $table->unsignedInteger('cate_service_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
