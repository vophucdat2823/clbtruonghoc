<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_parents', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('passport');
            $table->string('job')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->text('name_student')->nullable();
            $table->smallInteger('city_id');
            $table->string('level_school');
            $table->string('name_school');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_parents');
    }
}
