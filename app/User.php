<?php

namespace App;

use App\Permission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','avatar', 'email','phone', 'password','address','specific_address','role_id'
    ];
    protected function user_admin()
    {
        return Auth::guard('admin')->user();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function hasPermision(Permission $permission)
    {
        return !! $this->role != null ? $this->role->permissions ? $this->role->permissions->contains($permission) != null ? true : false : false : false;
        // optional(optional($this->role)->permissions)->contains($permission) != null ? true : false;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
