<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendRequire extends Model
{
    protected $table = 'send_requires';
    protected $fillable = [
        'user_id',
        'primary_id',
        'type',
        'date',
        'time',
        'quantity',
        'name',
        'email',
        'phone',
        'status',
    ];
    public function getService(){
        return $this->hasOne(Service::class,'id', 'primary_id');
    }
    public function getCourse(){
        return $this->hasOne(Course::class,'id', 'primary_id');
    }
}
