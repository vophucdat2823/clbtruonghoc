<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;


/**
 * 
 */
class Orders extends Model
{
	protected $table = 'order_stores';
	protected $fillable = [
			'user_id',
			'status'
	];
	
    public function order_detai()
    {
        return $this->hasMany(OrderDetails::class, 'order_id','id');
    }

    public function order_user()
    {
        return $this->hasOne(Users::class, 'id','user_id');
    }
    // ===============  Cách 1 =================
    // public function total_amount(){
    //     $order_detais = OrderDetails::where('order_id',$this->id)->get();
    //     $t = 0;
    //     foreach ($order_detais as $od_dt) {
    //         $t = $t + ($od_dt->price*$od_dt->quantity);
    //     }
    //     return $t;
    // }
    // ===============  Cách 2 =================
    // public function total_amount(){
    //     // $amount = Transaction::select(DB::raw('sum(price * quantity) as total'))->get();
    //     $sum = OrderDetails::where('order_id',$this->id)->select(DB::raw('sum(price * quantity) as total'))->first();
    //     // first đưa về 1 giá trị; get đua fvề cả mảngt
    //     // $qr = DB::table('order_detail')->select()->get();

    //     return $sum['total'];
    // }

    // ===============  Cách 3 =================
    public function total_amount(){

        $sum = DB::table('order_detail')->select(DB::raw('sum(price * quantity) as total'))->where('order_id',$this->id)->first();


        return $sum->total;
    }

}


 ?>