<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStore extends Model
{
    protected $table = 'product_stores';
    protected $fillable = [
        'status',
        'name',
        'slug',
        'image',
        'description',
        'price',
        'sale',
        'price_sale',
        'trademark',
        'sku',
        'category_id'
    ];

    public static function prodAddress($id)
    {
        return self::where('id', '=', $id)->first();
    }
    
    
}
