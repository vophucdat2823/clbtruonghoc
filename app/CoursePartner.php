<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoursePartner extends Model
{
    protected $table = 'course_partners';
    protected $fillable = [
		'course_id',
		'user_id',
		'unit_id',
		'price_id',
        'tuition_id',
        'time_id',
		'qty_id',
        'status',
		'quality_id',
		'address_id',
    ];
    public function getCourse_id($id){
        return $this->hasMany(TypeCourse::class,'course_id', 'course_id')->where('id',$id)->first();
    }
    public function getCourse(){
        return $this->hasOne(Course::class, 'id','course_id');
    }
    public function getUser(){
        return $this->hasOne(User::class, 'id','user_id');
    }
    
}
