<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuCustom extends Model
{
    protected $table = 'admin_menus';

    public function getNameMenu(){
        return $this->hasMany(MenuItemCustom::class,'menu', 'id');
    }

    public static function getById($id)
    {
        return self::where('id', '=', $id)->first();
    }

}
