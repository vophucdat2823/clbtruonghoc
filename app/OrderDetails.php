<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;


/**
 * 
 */
class OrderDetails extends Model
{
	protected $table = 'order_detail_stores';
	protected $fillable = [
			'order_id',
			'product_id',
			'price',
			'quantity'

	];
	public $timestamps = false;
	
	public function orders()
    {
        return $this->hasOne(Orders::class, 'id','order_id');
    }
    
    public function product_order()
    {
        return $this->hasOne(ProductStore::class, 'id','product_id');
    }

}


 ?>