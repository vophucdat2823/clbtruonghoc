<?php  
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ProductStore;
use App\Course;
use App\Helper\Cart;
use App\Helper\MailerShipped;
use Auth;
use Mail;
use App\Orders;
use App\OrderDetails;




/**
 * 
 */
class CartController extends Controller
{
	
	public function add_cart(Cart $cart,Request $req)
	{
		$id = $req->get('id');
		$quantity = $req->get('quantity');
		$product = ProductStore::find($id)->toArray();
		$cart->add($product,$quantity);
		return response()->json([
	       'message'   => 'Thêm vào giỏ hàng thành công',
	    ]);
	}
	public function remode_cart($id,Cart $cart )
	{
		// $prod = Product::find($id)->toArray();
		
		if ($cart->remode_cart($id)) {
            return response()->json([
                'message' => 'Xóa sản phẩm thành công',
            ]);
        } else {
            return response()->json([
                'message' => 'Xóa sản phẩm không thành công',
                'error'   => true,
            ]);
        }		
	}
	public function update_cart(Request $req,Cart $cart )
	{
		$cart->update($req->all());
		return redirect()->back();	
	}
	public function check_cart()
	{
		return view('check_cart');
	}




	public function buy_cart()
	{
		$carts = session('cart') ? session('cart') : [];
		if ($carts != null) {
			if(Auth::check()){
				return view('buy_cart');
			}else{
				return redirect()->route('user');
			}
		}else{
			return redirect()->route('home');
		}


	}

	public function post_buy_cart( Request $rq,Cart $carts)
	{
		$this->validate($rq,[
			'payment_method' => 'required',
			'shipping_method' => 'required',
		],[
			'payment_method.required' =>'Phương thức thanh toán chưa được chọn',
			'shipping_method.required' =>'Phương thức giao hàng chưa được chọn',

		]);
		
			$user = Auth::user()->id;
			$rq->merge(['user_id'=>Auth::id()]);
			// dd($carts->items);
			$orders = Orders::create($rq->all());
			if($orders){

					$carts = session('cart') ? session('cart') : [];
					// dd($carts);
					$order_id = $orders->id;
					foreach ($carts as $key => $cart) {
						$qty = $cart['quantity'];
						$price = $cart['price'];
						$product_id = $cart['id'];
						$color = $cart['color'];
						$size = $cart['size'];
						OrderDetails::create([
							'order_id' => $order_id,
							'product_id' => $product_id,
							'price' => $price,
							'quantity' => $qty,
							'color' => $color,
							'size' => $size,
						]);
					}
				Mail::to(Auth::user()->email)->send(new MailerShipped($orders));
				// echo "<pre>";
				// var_dump($orders);die();
				// $rq->session()->flush();
				$rq->session()->forget('cart');
				return redirect()->route('home')->with('success','Đặt hàng thành công,, cảm ơn bạn đã mưa hàng nha!');
			}
	}

	public function history_cart()
	{
		if (Auth::check()) {
			$user = Auth::user()->id;

			return view('history_cart',[
				'order' => Orders::Where('user_id',Auth::user()->id)->get(),
			]);
		}
	}
	public function view_cart($id)
	{
		if (Auth::check()) {
			return view('view_cart',[
				'view_cart' => OrderDetails::Where('order_id',$id)->get(),
				'order' => Orders::find($id),
			]);
		}
	}

	

// làm giỏ hàng 2
	// public function add_cart($pro, Cart2 $cart)
	// {
	// 	$pro = Product::find($id);
	// 	if($pro){
	// 		$cart->add($pro);
	// 		return redirect()->route('view_cart')
	// 	}
	// }
	// public function update_cart($id, Cart2 $cart)
	// {
		
	// }
	// public function remove_cart($id, Cart2 $cart)
	// {
	// 	$cart->remove($pro);
	// 	return redirect()->back()
	// }
	// public function clear_cart()
	// {
		
	// }
	// public function view_cart()
	// {
	// 	return view('view_cart');
	// }
}



?>