<?php

namespace App\Http\Controllers\Admin;

use App\CateEvent;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CateEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateEvent = CateEvent::all();
        $cateEvent0 = CateEvent::where('parents', 0)->get();
        $cateEvent1 = CateEvent::where('parents', '<>', 0)->get();
        return view('admin.cateEvent.index', compact('cateEvent', 'cateEvent0', 'cateEvent1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
            'parents' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (CateEvent::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'su-kien/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'su-kien/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $inserts          = new CateEvent();
        $inserts->name    = $request->name;
        $inserts->parents = 0;
        $inserts->slug    = $slug;
        try {
            $inserts->save();
            return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CateEvent  $cateEvent
     * @return \Illuminate\Http\Response
     */
    public function show(CateEvent $cateEvent)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CateEvent  $cateEvent
     * @return \Illuminate\CateEvent\Response
     */
    public function edit(CateEvent $cateEvent)
    {
        $cateEventAll = CateEvent::all();;
        return view('admin.cateEvent.edit', compact('cateEvent', 'cateEventAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CateEvent  $cateEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CateEvent $cateEvent)
    {

        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
            'parents' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $slug = '';
        if (CateEvent::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
            $slug = 'su-kien/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'su-kien/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }
        $cateEvent->name    = $request->name;
        $cateEvent->parents = 0;
        $cateEvent->slug    = $slug;
        try {
            $cateEvent->save();
            return redirect('admin/cate-event')->with(['success' => 'Sửa danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CateEvent  $cateEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(CateEvent $cateEvent)
    {
        $cateEvent->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
