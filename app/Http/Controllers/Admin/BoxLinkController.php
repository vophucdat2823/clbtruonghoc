<?php

namespace App\Http\Controllers\Admin;
use App\BoxLink;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BoxLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxLink = BoxLink::all();
        return view('admin.boxLink.index', compact('boxLink'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $boxLink    = BoxLink::findOrFail($request->id);
        // dd($boxLink);
        $validation = Validator::make($request->all(),
            [
                'name' => 'required',
                'link' => 'required',
                // 'stt'  => 'required',
            ], [

                'name.required' => 'Tên không được rỗng!',
                'link.required' => 'Đường dẫn tĩnh không được rỗng!',
                // 'stt.required'  => 'Số thứ tự không được rỗng!',

            ]);
        if ($validation->passes()) {

            $file_image = $boxLink->image ? $boxLink->image : '';

            if ($request->hasFile('image')) {
                $check_file = checkFileImage($request->image);
                if ($check_file == CHECK_FILE) {
                    if (\File::exists(public_path('upload/images/' . $boxLink->image))) {
                        \File::delete(public_path('upload/images/' . $boxLink->image));
                    }
                    $file_image = uploadFile($request->file('image'));
                } else {
                    return redirect()->route('product-store.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
                }
            }
            $boxLink->update([
                'image' =>  $file_image,
                'name' =>  $request->name,
                'link' =>  $request->link,
            ]);


            return response()->json([
                'message'    => 'Thay đổi dữ liệu box thành công!',
                'class_name' => 'alert-success',
            ]);
        } else {
            return response()->json([
                'message' => $validation->errors(),
                'error'   => true,
                'data'    => $request->all(),
            ]);
        }
    }
}
