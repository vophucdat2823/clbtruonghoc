<?php

namespace App\Http\Controllers\Admin;

use App\TypeCourse;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypeCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $typeCourse = TypeCourse::all();
        // return view('admin.typeCourse.index', compact('typeCourse'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'name'    => 'required|max:225',
        //     'slug'    => 'required|max:225',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withErrors($validator)
        //         ->withInput($request->all());
        // }
        // $slug = '';
        // if (TypeCourse::where('slug', $request->slug)->value('slug') == null) {
        //     $slug = 'loai-' . $request->slug . '.' . 'html';
        // } else {
        //     $slug = 'loai-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        // }

        // $inserts          = new TypeCourse();
        // $inserts->name    = $request->name;
        // $inserts->slug    = $slug;
        // try {
        //     $inserts->save();
        //     return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        // } catch (Exception $message) {
        //     return redirect()->back()->with(['error' => $message->getMessage()]);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeCourse  $TypeCourse
     * @return \Illuminate\Http\Response
     */
    public function show(TypeCourse $typeCourse)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypeCourse  $typeCourse
     * @return \Illuminate\TypeCourse\Response
     */
    public function edit(TypeCourse $typeCourse)
    {
        // $typeCourseAll = TypeCourse::all();;
        // return view('admin.typeCourse.edit', compact('typeCourse', 'typeCourseAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypeCourse  $typeCourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeCourse $typeCourse)
    {

        // $validator = Validator::make($request->all(), [
        //     'name'    => 'required|max:225',
        //     'slug'    => 'required|max:225',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withErrors($validator)
        //         ->withInput($request->all());
        // }

        // $slug = '';
        // if (TypeCourse::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
        //     $slug = 'loai-' . $request->slug . '.' . 'html';
        // } else {
        //     $slug = 'loai-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        // }
        // $typeCourse->name    = $request->name;
        // $typeCourse->slug    = $slug;
        // try {
        //     $typeCourse->save();
        //     return redirect('admin/type-course')->with(['success' => 'Sửa danh mục thành công !']);
        // } catch (Exception $message) {
        //     return redirect()->back()->with(['error' => $message->getMessage()]);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypeCourse  $typeCourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeCourse $typeCourse)
    {
        // $typeCourse->delete();
        // return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
