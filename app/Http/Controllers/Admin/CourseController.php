<?php

namespace App\Http\Controllers\Admin;

use App\CateCourse;
use App\Course;
use App\CourseImage;
use App\Http\Controllers\Controller;
use App\TypeCourse;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::all();
        return view('admin.course.index', compact('course'));
    }
    public function typeCourse($id)
    {
        if (TypeCourse::destroy($id)) {
            echo true;
        } else {
            echo false;
        }

    }
    public function deleteImage($id)
    {
        if ($img = CourseImage::find($id)) {
            if (File::exists(public_path('upload/images/' . $img->image))) {
                File::delete(public_path('upload/images/' . $img->image));
            }
            CourseImage::destroy($id);
            echo true;
        } else {
            echo false;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cateService = CateCourse::where('parents', 0)->get();
        return view('admin.course.create', compact('cateService'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name_dichvu' => 'required|max:225',
            'slug'        => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        $slug = 'khoa-hoc-' . $request->slug . '.' . 'html';
        if (Course::where('slug', $slug)->value('slug') == null) {
            $slug = $slug;
        } else {
            $slug = 'khoa-hoc-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('course.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $file_banner = '';
        if ($request->banner) {
            $check_file = checkFileImage($request->banner);
            if ($check_file == CHECK_FILE) {
                $file_banner = uploadFile($request->file('banner'));
            } else {
                return redirect()->route('course.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $inserts                  = new Course();
        $inserts->name            = $request->name_dichvu;
        $inserts->slug            = $slug;
        $inserts->url_link_combo  = $request->url_link_combo != null ? $request->url_link_combo : '#';
        $inserts->study_condition = $request->study_condition != null ? $request->study_condition : 'Không';
        $inserts->description     = $request->description;
        $inserts->banner          = $file_banner;
        $inserts->image           = $file_image;
        $inserts->cate_course_id  = $request->cate_course_id;
        try {
            $inserts->save();
            if ($request->type[0]) {
                foreach ($request->type as $key => $value) {
                    $typeService            = new TypeCourse();
                    $typeService->course_id = $inserts->id;
                    $typeService->name      = $request->name[$key];
                    $typeService->type      = $value;
                    $typeService->save();
                }
            }
            if ($request->file_image != null) {
                $imageFiles      = $request->file('file_image');
                $folderDir       = 'upload/images/';
                $destinationPath = public_path($folderDir);
                foreach ($request->file_image as $fileObject) {
                    $destinationFileName = time() . $fileObject->getClientOriginalName();
                    $fileObject->move($destinationPath, $destinationFileName);
                    CourseImage::create([
                        'course_id' => $inserts->id,
                        'image'     => $destinationFileName,
                    ]);
                }
            }
            return redirect()->route('course.index')->with(['success' => 'Thêm khóa học thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Course $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $cateCourse = CateCourse::all();
        return view('admin.course.edit', compact('course', 'cateCourse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $validator = Validator::make($request->all(), [
            'name_dichvu' => 'required|max:225',
            'slug'        => 'required|max:225',
            'price'       => 'numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (Course::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'khoa-hoc-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'khoa-hoc-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = $course->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $course->image))) {
                    File::delete(public_path('upload/images/' . $course->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('course.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $file_banner = $course->banner;
        if ($request->banner) {
            $check_file = checkFileImage($request->banner);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $course->banner))) {
                    File::delete(public_path('upload/images/' . $course->banner));
                }
                $file_banner = uploadFile($request->file('banner'));
            } else {
                return redirect()->route('course.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $course->name            = $request->name_dichvu;
        $course->slug            = $slug;
        $course->url_link_combo  = $request->url_link_combo != null ? $request->url_link_combo : '#';
        $course->study_condition = $request->study_condition != null ? $request->study_condition : 'Không';
        $course->description     = $request->description;
        $course->banner          = $file_banner;
        $course->image           = $file_image;
        $course->cate_course_id  = $request->cate_course_id;
        try {
            $course->save();

            if ($request->type[0]) {
                foreach ($request->type as $key => $value) {
                    $typeCourse            = new TypeCourse();
                    $typeCourse->course_id = $course->id;
                    $typeCourse->name      = $request->name[$key];
                    $typeCourse->type      = $value;
                    $typeCourse->save();
                }
            }
            if ($request->old_img != null) {
                foreach ($request->old_img as $id_img => $one_img) {
                    $public_path  = public_path('upload/images/');
                    $one_img_link = time() . $one_img->getClientOriginalName();
                    $one_img->move($public_path, $one_img_link);
                    CourseImage::where('id', $id_img)->update([
                        'image' => $one_img_link,
                    ]);
                }
            }
            if ($request->file_image != null) {
                $imageFiles      = $request->file('file_image');
                $folderDir       = 'upload/images/';
                $destinationPath = public_path($folderDir);
                foreach ($request->file_image as $fileObject) {
                    $destinationFileName = time() . $fileObject->getClientOriginalName();
                    $fileObject->move($destinationPath, $destinationFileName);
                    CourseImage::create([
                        'course_id' => $course->id,
                        'image'     => $destinationFileName,
                    ]);
                }
            }
            return redirect()->route('course.index')->with(['success' => 'Sửa khóa học thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        foreach ($course->getImages as $img) {
            if (File::exists(public_path('upload/images/' . $img->image))) {
                File::delete(public_path('upload/images/' . $img->image));
            }
        }
        CourseImage::whereIn('course_id', [$course->id])->delete();
        if (File::exists(public_path('upload/images/' . $course->image))) {
            File::delete(public_path('upload/images/' . $course->image));
        }
        TypeCourse::whereIn('course_id', [$course->id])->delete();
        $course->delete();
        return redirect()->back()->with(['success' => 'Xóa dịch vụ thành công !']);
    }

    public function getSuggestCities(Request $request)
    {
        if ($request->keyword) {
            $keyword  = $request->keyword;
            $input    = Input::all();
            $apiLink  = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
            $type     = '(cities)';
            $language = 'vi';
            $key      = 'AIzaSyAM3feqazSoRdenb6DEtZDJQZL_WGH67n4';
            $data     = "input=$keyword&types=($type)&language=$language&key=$key";

            $getSuggestCities = @file_get_contents($apiLink . $data);

            $suggestCities = [];

            if ($getSuggestCities) {
                $getSuggestCities = json_decode($getSuggestCities);

                if ($getSuggestCities->status == 'OK') {
                    $cities = $getSuggestCities->predictions;

                    foreach ($cities as $city) {
                        $suggestCities[] = [
                            'place_id'    => $city->place_id,
                            'description' => $city->description,
                        ];
                    }
                }
            }

            return Response::json($suggestCities);
        }
    }

    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            Course::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }
    public function updateTypeCourse(Request $request)
    {
        if ($request->ajax()) {
            TypeCourse::findOrFail($request->id)->update(['name' => $request->name, 'type' => $request->type]);
            echo true;
        } else {
            echo false;
        }
    }
}
