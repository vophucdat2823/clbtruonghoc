<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function create($type, $id)
    {

        $questions = Question::where('type', $type)->where('primary_id', $id)->get();
        return view('admin.question.edit', compact('questions', 'type', 'id'));
    }
    public function store(Request $request,$type, $id)
    {
        if ($request->name[0]) {
            foreach ($request->type as $key => $value) {
                $question             = new Question();
                $question->primary_id = $id;
                $question->question   = $request->name[$key];
                $question->type       = $type;
                $question->save();
            }
        }
        if ($type == "service") {
	        return redirect()->route('service.index')->with(['success' => 'Thêm câu hỏi thành công !']);
        }elseif ($type == "course"){
        	return redirect()->route('course.index')->with(['success' => 'Thêm câu hỏi thành công !']);
        }
    }
    public function updateQuestion(Request $request)
    {
        if ($request->ajax()) {
            Question::findOrFail($request->id)->update(['question' => $request->name, 'type' => $request->type]);
            echo true;
        } else {
            echo false;
        }
    }
    public function question($id)
    {
        if (Question::destroy($id)) {
            echo true;
        } else {
            echo false;
        }

    }
}
