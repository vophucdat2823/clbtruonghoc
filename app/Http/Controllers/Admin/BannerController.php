<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner_image  = Banner::where('type', 'image')->get();
        $banner_slider = Banner::where('type', 'slider')->get();
        return view('admin.banner.index', compact('banner_image', 'banner_slider'));
    }

    public function store(Request $request)
    {
        $banner     = Banner::findOrFail($request->id);
        $validation = Validator::make($request->all(),
            [
                'name' => 'required',
                'link' => 'required',
                // 'stt'  => 'required',
            ], [

                'name.required' => 'Tên không được rỗng!',
                'link.required' => 'Đường dẫn tĩnh không được rỗng!',
                // 'stt.required'  => 'Số thứ tự không được rỗng!',

            ]);
        if ($validation->passes()) {

            $file_image = $banner->images ? $banner->images : '';

            if ($request->hasFile('image')) {
                $check_file = checkFileImage($request->image);
                if ($check_file == CHECK_FILE) {
                    if (\File::exists(public_path('upload/images/' . $banner->images))) {
                        \File::delete(public_path('upload/images/' . $banner->images));
                    }
                    $file_image = uploadFile($request->file('image'));
                } else {
                    return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
                }
            }
            $banner->update([
                'images' => $file_image,
                'name'  => $request->name,
                'link'  => $request->link,
                'type'  => $request->type,
            ]);

            return response()->json([
                'message'    => 'Thay đổi dữ liệu box thành công!',
                'class_name' => 'alert-success',
            ]);
        } else {
            return response()->json([
                'message' => $validation->errors(),
                'error'   => true,
                'data'    => $request->all(),
            ]);
        }
    }

    public function create(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'name' => 'required',
                'link' => 'required',
                // 'stt'  => 'required',
            ], [

                'name.required' => 'Tên không được rỗng!',
                'link.required' => 'Đường dẫn tĩnh không được rỗng!',
                // 'stt.required'  => 'Số thứ tự không được rỗng!',

            ]);
        if ($validation->passes()) {

            $file_image = '';

            if ($request->hasFile('image')) {
                $check_file = checkFileImage($request->image);
                if ($check_file == CHECK_FILE) {
                    $file_image = uploadFile($request->file('image'));
                } else {
                    return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
                }
            }
            Banner::create([
                'images' => $file_image,
                'name'  => $request->name,
                'link'  => $request->link,
                'type'  => $request->type,
            ]);

            return response()->json([
                'message'    => 'Thay đổi dữ liệu box thành công!',
                'class_name' => 'alert-success',
            ]);
        } else {
            return response()->json([
                'message' => $validation->errors(),
                'error'   => true,
                'data'    => $request->all(),
            ]);
        }
    }
}
