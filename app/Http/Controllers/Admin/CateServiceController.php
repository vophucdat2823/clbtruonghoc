<?php

namespace App\Http\Controllers\Admin;

use App\CateService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use File;

class CateServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateService0 = CateService::where('parents', 0)->get();
        return view('admin.cateService.index', compact('cateService0'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:191',
            'title'    => 'required|max:191',
            'slug'    => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $slug = '';
        if (CateService::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'dich-vu-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'dich-vu-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $inserts          = new CateService();
        $inserts->name    = $request->name;
        $inserts->title    = $request->title;
        $inserts->image   = $file_image;
        $inserts->parents = 0;
        $inserts->slug    = $slug;
        try {
            $inserts->save();
            return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CateService  $cateService
     * @return \Illuminate\Http\Response
     */
    public function show(CateService $cateService)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CateService  $cateService
     * @return \Illuminate\Http\Response
     */
    public function edit(CateService $cateService)
    {
        $cateServiceAll = CateService::all();
        return view('admin.cateService.edit', compact('cateService', 'cateServiceAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CateService  $cateService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CateService $cateService)
    {

        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:191',
            'title'    => 'required|max:191',
            'slug'    => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = $cateService->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $cateService->image))) {
                    File::delete(public_path('upload/images/' . $cateService->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $slug = '';
        if (CateService::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
            $slug = 'dich-vu-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'dich-vu-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }
        $cateService->name    = $request->name;
        $cateService->title    = $request->title;
        $cateService->parents = 0;
        $cateService->image   = $file_image;
        $cateService->slug    = $slug;
        try {
            $cateService->save();
            return redirect('admin/cate-service')->with(['success' => 'Sửa danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CateService  $cateService
     * @return \Illuminate\Http\Response
     */
    public function destroy(CateService $cateService)
    {
        $cateService->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
