<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Images;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        return view('admin.index');
    }


    function postImages(Request $request,$type)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file')) {
                $imageFiles = $request->file('file');
                // set destination path
                $folderDir = 'upload/icon';
                $destinationPath = public_path($folderDir);
                // this form uploads multiple files
                foreach ($request->file('file') as $fileKey => $fileObject ) {
                    // make sure each file is valid
                    if ($fileObject->isValid()) {
                        // make destination file name
                        $destinationFileName = time() . $fileObject->getClientOriginalName();
                        // move the file from tmp to the destination path
                        $fileObject->move($destinationPath, $destinationFileName);
                        // save the the destination filename
                        Images::create([
                            'image' => $destinationFileName,
                            'type' =>$type

                        ]);
                    }
                }
            }
        }
    }
     function deleteImg(Request $request)
    {
        dd($request->all());
        if (File::exists(public_path('upload/icon/' . $request->id))) {
            File::delete(public_path('upload/icon/' . $request->id));
        }
    }
 
}
