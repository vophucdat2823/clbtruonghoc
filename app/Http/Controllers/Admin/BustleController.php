<?php

namespace App\Http\Controllers\Admin;

use App\Bustle;
use App\CateBustle;
use App\City;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class BustleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bustle = Bustle::all();
        return view('admin.bustle.index', compact('bustle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cateBustle = CateBustle::all();
        $city       = City::all();
        return view('admin.bustle.create', compact('cateBustle', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'           => 'required|max:225',
            'slug'           => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        $slug = 'clb-truong-hoc/' . $request->slug . '.' . 'html';
        if (Bustle::where('slug', $slug)->value('slug') == null) {
            $slug = $slug;
        } else {
            $slug = 'clb-truong-hoc/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('bustle.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $inserts                 = new Bustle();
        $inserts->name           = $request->name;
        $inserts->slug           = $slug;
        $inserts->image          = $file_image;
        $inserts->cate_bustle_id = json_encode($request->cate_bustle_id);
        $inserts->description    = $request->description;
        $inserts->address        = $request->address;
        try {
            $inserts->save();
            return redirect()->route('bustle.index')->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bustle  $bustle
     * @return \Illuminate\Http\Response
     */
    public function show(Bustle $bustle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bustle  $bustle
     * @return \Illuminate\Http\Response
     */
    public function edit(Bustle $bustle)
    {
        $cateBustle = CateBustle::all();
        $city       = City::all();
        // $district      = District::all();
        return view('admin.bustle.edit', compact('bustle', 'cateBustle', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bustle  $bustle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bustle $bustle)
    {
        $validator = Validator::make($request->all(), [
            'name'           => 'required|max:225',
            'slug'           => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (Bustle::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'clb-truong-hoc/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'clb-truong-hoc/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = $bustle->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $bustle->image))) {
                    File::delete(public_path('upload/images/' . $bustle->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('bustle.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $bustle->name = $request->name;
        $bustle->slug = $slug;

        $bustle->image          = $file_image;
        $bustle->cate_bustle_id = json_encode($request->cate_bustle_id);
        $bustle->description    = $request->description;
        $bustle->address        = $request->address;
        try {
            $bustle->save();
            return redirect()->route('bustle.index')->with(['success' => 'Thêm bài viết thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bustle  $bustle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bustle $bustle)
    {
        if (File::exists(public_path('upload/images/' . $bustle->image))) {
            File::delete(public_path('upload/images/' . $bustle->image));
        }
        $bustle->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }

    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            Bustle::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }
}
