<?php

namespace App\Http\Controllers\Admin;

use App\CateEvent;
use App\City;
use App\CityEvent;
use App\District;
use App\Event;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();
        return view('admin.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cateEvent = CateEvent::all();
        $city      = City::all();
        return view('admin.event.create', compact('cateEvent', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:225',
            'slug'  => 'required|max:225',
            'price' => 'numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        $slug = 'su-kien/' . $request->slug . '.' . 'html';
        if (Event::where('slug', $slug)->value('slug') == null) {
            $slug = $slug;
        } else {
            $slug = 'su-kien/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('event.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $inserts                = new Event();
        $inserts->name          = $request->name;
        $inserts->slug          = $slug;
        $inserts->even_type     = $request->even_type == 1 ? 'Miễn Phí' : 'Mất Phí';
        $inserts->price_range   = $request->even_type == 2 ? $request->price_range : '';
        $inserts->date_play     = $request->date_play;
        $inserts->image         = $file_image;
        $inserts->cate_event_id = json_encode($request->cate_event_id);
        $inserts->description   = $request->description;
        try {
            $inserts->save();
            if ($request->address1[0]) {
                foreach ($request->address1 as $key => $value) {
                    $CityEvent              = new CityEvent();
                    $CityEvent->city_id     = $value;
                    $CityEvent->district_id = $request->address2[$key];
                    $CityEvent->event_id    = $inserts->id;
                    $CityEvent->save();
                }
            }
            return redirect()->route('event.index')->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $cityEvent = CityEvent::whereIn('event_id', [$event->id])->get();
        $cateEvent = CateEvent::all();
        $city      = City::all();
        // $district      = District::all();
        return view('admin.event.edit', compact('event', 'cateEvent', 'cityEvent', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:225',
            'slug'  => 'required|max:225',
            'price' => 'numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (Event::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'su-kien/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'su-kien/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = $event->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $event->image))) {
                    File::delete(public_path('upload/images/' . $event->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('event.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $event->name          = $request->name;
        $event->slug          = $slug;
        $event->even_type     = $request->even_type == 1 ? 'Miễn Phí' : 'Mất Phí';
        $event->price_range   = $request->even_type == 2 ? $request->price_range : '';
        $event->date_play     = $request->date_play;
        $event->image         = $file_image;
        $event->cate_event_id = json_encode($request->cate_event_id);
        $event->description   = $request->description;
        try {
            $event->save();
            CityEvent::whereIn('event_id', [$event->id])->delete();
            if ($request->address1[0]) {
                foreach ($request->address1 as $key => $value) {
                    $CityEvent              = new CityEvent();
                    $CityEvent->city_id     = $value;
                    $CityEvent->district_id = $request->address2[$key];
                    $CityEvent->event_id    = $event->id;
                    $CityEvent->save();
                }
            }
            return redirect()->route('event.index')->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        CityEvent::whereIn('event_id', [$event->id])->delete();
        if (File::exists(public_path('upload/images/' . $event->image))) {
            File::delete(public_path('upload/images/' . $event->image));
        }
        $event->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
    public function getSuggestCities(Request $request)
    {
        if ($request->keyword) {
            $keyword  = $request->keyword;
            $input    = Input::all();
            $apiLink  = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
            $type     = '(cities)';
            $language = 'vi';
            $key      = 'AIzaSyAM3feqazSoRdenb6DEtZDJQZL_WGH67n4';
            $data     = "input=$keyword&types=($type)&language=$language&key=$key";

            $getSuggestCities = @file_get_contents($apiLink . $data);

            $suggestCities = [];

            if ($getSuggestCities) {
                $getSuggestCities = json_decode($getSuggestCities);

                if ($getSuggestCities->status == 'OK') {
                    $cities = $getSuggestCities->predictions;

                    foreach ($cities as $city) {
                        $suggestCities[] = [
                            'place_id'    => $city->place_id,
                            'description' => $city->description,
                        ];
                    }
                }
            }

            return Response::json($suggestCities);
        }
    }

    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            Event::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }

    public function get_quan_huyen($id_matp)
    {
        $quanhuyen = District::where('matp', $id_matp)->get();
        foreach ($quanhuyen as $key => $qh_ies) {
            echo "<option value='" . $qh_ies->maqh . "'>" . $qh_ies->name . "</option>";
        }
    }
}
