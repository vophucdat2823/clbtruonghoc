<?php

namespace App\Http\Controllers\Admin;

use App\EmailNewsletter;
use App\Http\Controllers\Controller;
use Excel;
use Illuminate\Http\Request;
// use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Validator;


class EmailNewsletterController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $emailNewSletter = EmailNewsletter::all();
        return view('admin.emailNewSletter.index',compact('emailNewSletter'));
    }
    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            EmailNewsletter::findOrFail($request->id)->update(['status' => $request->status]);
            return response()->json([
                'message' => 'Cập nhập trạng thái thành công !',
            ]);
        }
    }

    public function delete_email($id)
    {
        if (EmailNewsletter::destroy($id)) {
            return response()->json([
                'message' => 'Xóa mail thành công !',
            ]);
        }else{
            return response()->json([
                'message' => 'Xóa mail thất bại !',
                'error'   => true,
            ]);
        }
    }

    public function excel_email(Request $request)
    {
        if ($request->status_export) {
            $export = EmailNewsletter::select('id','email','status','created_at',
            \DB::raw('(CASE 

                        WHEN email_newsletters.status = "0" THEN "Đã xử lý" 

                        WHEN email_newsletters.status = "1" THEN "Chưa xử lý" 

                        END) AS status')
            );
            if ($request->status_export == 2) {
                $export = $export->where('status',0);
            }
            if ($request->status_export == 3) {
                $export = $export->where('status',1);
            }
            
            $export = $export->get();

            $fileName = $request->name_excel != null ? $request->name_excel : "file-excel";

            Excel::create("file-excel", function($excel) use($export){
                
            dd($export);
                $excel->sheet('Tất cả', function($sheet) use($export){
                    $sheet->fromArray($export);
                });
            })->export('xlsx');
            return redirect()->back()->with('success','Đang xuất file');
        }else{
            return redirect()->back()->with('error','Vui lòng chọn kiểu xuất');
        }
    }


}
