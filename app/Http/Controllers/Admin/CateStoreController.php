<?php

namespace App\Http\Controllers\Admin;

use App\CateStore;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CateStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateStore0 = CateStore::where('parents', 0)->get();
        return view('admin.cateStore.index', compact('cateStore0'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (CateStore::where('slug', $request->slug)->value('slug') == null) {
            $slug = $request->slug . '.' . 'html';
        } else {
            $slug = $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $inserts          = new CateStore();
        $inserts->name    = $request->name;
        $inserts->parents = 0;
        $inserts->slug    = $slug;
        try {
            $inserts->save();
            return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CateStore  $cateStore
     * @return \Illuminate\Http\Response
     */
    public function show(CateStore $cateStore)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CateStore  $cateStore
     * @return \Illuminate\Http\Response
     */
    public function edit(CateStore $cateStore)
    {
        $cateStoreAll = CateStore::all();
        return view('admin.cateStore.edit', compact('cateStore', 'cateStoreAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CateStore  $cateStore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CateStore $cateStore)
    {

        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $slug = '';
        if (CateStore::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
            $slug =$request->slug . '.' . 'html';
        } else {
            $slug =$request->slug . '-' . str_random(1) . '.' . 'html';
        }
        $cateStore->name    = $request->name;
        $cateStore->parents = 0;
        $cateStore->slug    = $slug;
        try {
            $cateStore->save();
            return redirect('admin/cate-store')->with(['success' => 'Sửa danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CateStore  $cateStore
     * @return \Illuminate\Http\Response
     */
    public function destroy(CateStore $cateStore)
    {
        $cateStore->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
