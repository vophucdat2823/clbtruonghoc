<?php

namespace App\Http\Controllers\Admin;

use App\CateStore;
use App\City;
use App\CityDistrict;
use App\District;
use App\Http\Controllers\Controller;
use App\ProductStore;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\ProductImgStore;
class ProductStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $productStore = ProductStore::where('user_id', Auth::id())->orderBy('id','desc')->get();
        return view('admin.productStore.index', compact('productStore'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cateStore = CateStore::all();
        $city      = City::all();
        return view('admin.productStore.create', compact('cateStore', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->name){
            return redirect()->back()->withInput()->with(['error' => "Tên sản phẩm không được để trống"]);
        }
        
        if(!$request->slug){
            return redirect()->back()->withInput()->with(['error' => "Slug không được để trống"]);
        }
        if(!$request->price){
            return redirect()->back()->withInput()->with(['error' => "Giá sản phẩm được để trống"]);
        }
        if(!$request->trademark){
            return redirect()->back()->withInput()->with(['error' => "Thương hiệu không được để trống"]);
        }
        if(!$request->category_id){
            return redirect()->back()->withInput()->with(['error' => "Danh mục không được để trống"]);
        }
        if(!$request->short_description){
            return redirect()->back()->withInput()->with(['error' => "Mô tả ngắn không được để trống"]);
        }
        $validator = Validator::make($request->all(), [
            'price' => 'numeric',
            
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with(['error' => "Giá sản phẩm phải là số"]);
        }
        $slug = '';
        $slug = $request->slug . '.' . 'html';
        if (ProductStore::where('slug', $slug)->value('slug') == null) {
            $slug = $slug;
        } else {
            $slug = $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = '';
        $file_image1 = '';
        $file_image2 = '';
        $file_image3 = '';
        $file_image4 = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->withInput()->with(['error' => "Image không được để trống"]);
            }
        }

        if ($request->image1) {
            $check_file = checkFileImage($request->image1);
            if ($check_file == CHECK_FILE) {
                $file_image1 = uploadFile($request->file('image1'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->image2) {
            $check_file = checkFileImage($request->image2);
            if ($check_file == CHECK_FILE) {
                $file_image2 = uploadFile($request->file('image2'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->image3) {
            $check_file = checkFileImage($request->image3);
            if ($check_file == CHECK_FILE) {
                $file_image3 = uploadFile($request->file('image3'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->image4) {
            $check_file = checkFileImage($request->image4);
            if ($check_file == CHECK_FILE) {
                $file_image4 = uploadFile($request->file('image4'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $inserts              = new ProductStore();
        $inserts->name        = $request->name;
        $inserts->slug        = $slug;
        $inserts->price       = $request->price;
        $inserts->image       = $file_image;
        $inserts->category_id = json_encode($request->category_id);
        $inserts->description = $request->description;
        $inserts->user_id     = Auth::id();
        $inserts->trademark       = $request->trademark;
        $inserts->short_description       = $request->short_description;
        $inserts->sku       = isset($request->sku)?$request->sku:null;
        $inserts->price_sale       = isset($request->price_sale)?$request->price_sale:null;
        $inserts->sale       = isset($request->sale)?$request->sale:null;
        try {
            $inserts->save();
            $insertedId = $inserts->id;
          
            if($file_image1!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image1;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image2!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image2;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image3!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image3;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image4!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image4;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            return redirect()->route('listProduct')->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductStore  $productStore
     * @return \Illuminate\Http\Response
     */
    public function show(ProductStore $productStore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductStore  $productStore
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductStore $productStore,ProductImgStore $ProductImgStore)
    {
        $cityDistrict = CityDistrict::whereIn('product_store_id', [$productStore->id])->get();
        $cateStore = CateStore::all();
        $city      = City::all();
        // $district      = District::all();
        // dd($productStore);

        return view('admin.productStore.edit', compact('productStore','cateStore', 'ProductImgStore', 'cityDistrict', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductStore  $productStore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductStore $productStore)
    {
        $inputFill=[];
        if(!$request->name){
            return redirect()->back()->withInput()->with(['error' => "Tên sản phẩm không được để trống"]);
        }
        
        if(!$request->slug){
            return redirect()->back()->withInput()->with(['error' => "Slug không được để trống"]);
        }
        if(!$request->price){
            return redirect()->back()->withInput()->with(['error' => "Giá sản phẩm được để trống"]);
        }
        if(!$request->trademark){
            return redirect()->back()->withInput()->with(['error' => "Thương hiệu không được để trống"]);
        }
        if(!$request->category_id){
            return redirect()->back()->withInput()->with(['error' => "Danh mục không được để trống"]);
        }
        if(!$request->short_description){
            return redirect()->back()->withInput()->with(['error' => "Mô tả ngắn không được để trống"]);
        }
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:225',
            'slug'  => 'required|max:225',
            'price' => 'numeric',
        ]);
      
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with(['error' => "Một vài trường không được để trống"]);
               
        }

        $slug = '';
        $arrSlug=explode('.',$request->slug);
        isset($arrSlug[1])?$slug=$request->slug:$slug=$request->slug . '.' . 'html';
      
        $file_image = $productStore->image;
    
        
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $productStore->image))) {
                    File::delete(public_path('upload/images/' . $productStore->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
            $inputFill['image']=$file_image;
        }
        
        $file_image1 = '';
        $file_image2 = '';
        $file_image3 = '';
        $file_image4 = '';

        if ($request->imagei1) {
            $check_file = checkFileImage($request->imagei1);
            if ($check_file == CHECK_FILE) {
                $file_image1 = uploadFile($request->file('imagei1'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->imagei2) {
            $check_file = checkFileImage($request->imagei2);
            if ($check_file == CHECK_FILE) {
                $file_image2 = uploadFile($request->file('imagei2'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->imagei3) {
            $check_file = checkFileImage($request->imagei3);
            if ($check_file == CHECK_FILE) {
                $file_image3 = uploadFile($request->file('imagei3'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($request->imagei4) {
            $check_file = checkFileImage($request->imagei4);
            if ($check_file == CHECK_FILE) {
                $file_image4 = uploadFile($request->file('imagei4'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $inputFill['slug']=$slug;
        $inputFill['name']=$request->name;
        $inputFill['price']=$request->price;
        $inputFill['description']=$request->description;
        $inputFill['category_id']=json_encode($request->category_id);
        $inputFill['trademark']       = $request->trademark;
        $inputFill['short_description']       = $request->short_description;
        $inputFill['price_sale']       = isset($request->price_sale)?$request->price_sale:null;
        $inputFill['sale']       = isset($request->sale)?$request->sale:null;
        $inputFill['sku']       = isset($request->sku)?$request->sku:null;
        $id=$request->id;
        $tmp=ProductStore::find($id);
       
        if($tmp)
        {
            $tmp->fill($inputFill);
            $tmp->save();
            $insertedId = $tmp->id;

            $productImgStore = ProductImgStore::where('product_id', $insertedId)->get()->toarray();
            foreach ($productImgStore as $key => $value) {
                $tmp='image'.$value['id'];
              
                if (isset($request->$tmp)) {
                    $check_file = checkFileImage($request->$tmp);
                    if ($check_file == CHECK_FILE) {
                        $delete = ProductImgStore::where('id',$value['id'])->delete();
                        if (File::exists(public_path('upload/images/' . $value['images']))) {
                            File::delete(public_path('upload/images/' . $value['images']));
                        }
                        $file_image = uploadFile($request->file($tmp));
                        $ImgStore              = new ProductImgStore();
                        $ImgStore->images=$file_image;
                        $ImgStore->product_id=$insertedId;
                        $ImgStore->save();
                    } else {
                        return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
                    }
                   
                }
            }
            if($file_image1!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image1;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image2!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image2;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image3!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image3;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            if($file_image4!=='')
            {
                $ProductImgStore              = new ProductImgStore();
                $ProductImgStore->images=$file_image4;
                $ProductImgStore->product_id=$insertedId;
                $ProductImgStore->save();
            }
            return redirect()->route('listProduct')->with(['success' => 'Sửa sản phẩm thành công !']);
        }
        else{
            return redirect()->back()->with(['error' => "Server Error"]);
        }

       
    }

    /**
     * Remove the specified resource from stor  age.
     *
     * @param  \App\ProductStore  $productStore
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // CityDistrict::whereIn('product_store_id',[$productStore->id])->delete();
       
        $tmp=ProductStore::find($id);
        if($tmp){
            if (File::exists(public_path('upload/images/' . $tmp->image))) {
                File::delete(public_path('upload/images/' . $tmp->image));
            }
            $productImgStore = ProductImgStore::where('product_id', $tmp->id)->get()->toarray();
            foreach ($productImgStore as $key => $value) {
                if (File::exists(public_path('upload/images/' . $value['images']))) {
                    File::delete(public_path('upload/images/' . $value['images']));
                }
            }
            $delete = ProductImgStore::where('product_id',$tmp->id)->delete();
            $tmp->delete();
            return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
        }
        else{
            return redirect()->back()->with(['error' => 'Không tìm thấy sản phẩm này !']);
        }
       
    }
    public function getSuggestCities(Request $request)
    {
        if ($request->keyword) {
            $keyword  = $request->keyword;
            $input    = Input::all();
            $apiLink  = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
            $type     = '(cities)';
            $language = 'vi';
            $key      = 'AIzaSyAM3feqazSoRdenb6DEtZDJQZL_WGH67n4';
            $data     = "input=$keyword&types=($type)&language=$language&key=$key";

            $getSuggestCities = @file_get_contents($apiLink . $data);

            $suggestCities = [];

            if ($getSuggestCities) {
                $getSuggestCities = json_decode($getSuggestCities);

                if ($getSuggestCities->status == 'OK') {
                    $cities = $getSuggestCities->predictions;

                    foreach ($cities as $city) {
                        $suggestCities[] = [
                            'place_id'    => $city->place_id,
                            'description' => $city->description,
                        ];
                    }
                }
            }

            return Response::json($suggestCities);
        }
    }

    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            ProductStore::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }

    public function get_quan_huyen($id_matp)
    {
        $quanhuyen = District::where('matp', $id_matp)->get();
        foreach ($quanhuyen as $key => $qh_ies) {
            echo "<option value='" . $qh_ies->maqh . "'>" . $qh_ies->name . "</option>";
        }
    }
}
