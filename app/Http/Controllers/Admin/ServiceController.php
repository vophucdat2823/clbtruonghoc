<?php

namespace App\Http\Controllers\Admin;

use App\CateService;
use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceImage;
use App\TypeService;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::all();
        return view('admin.service.index', compact('service'));
    }
    public function typeService($id)
    {
        if (TypeService::destroy($id)) {
            echo true;
        } else {
            echo false;
        }

    }
    public function deleteImage($id)
    {
        if ($img = ServiceImage::find($id)) {
            if (File::exists(public_path('upload/images/' . $img->image))) {
                File::delete(public_path('upload/images/' . $img->image));
            }
            ServiceImage::destroy($id);
            echo true;
        } else {
            echo false;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cateService = CateService::where('parents', 0)->get();
        return view('admin.service.create', compact('cateService'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name_dichvu' => 'required|max:225',
            'slug'        => 'required|max:225',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        $slug = 'dich-vu-' . $request->slug . '.' . 'html';
        if (Service::where('slug', $slug)->value('slug') == null) {
            $slug = $slug;
        } else {
            $slug = 'dich-vu-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('service.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $file_banner = '';
        if ($request->banner) {
            $check_file = checkFileImage($request->banner);
            if ($check_file == CHECK_FILE) {
                $file_banner = uploadFile($request->file('banner'));
            } else {
                return redirect()->route('service.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $inserts                  = new Service();
        $inserts->name            = $request->name_dichvu;
        $inserts->slug            = $slug;
        $inserts->url_link_combo  = $request->url_link_combo != null ? $request->url_link_combo : '#';
        $inserts->study_condition = $request->study_condition != null ? $request->study_condition : 'Không';
        $inserts->description     = $request->description;
        $inserts->image           = $file_image;
        $inserts->banner          = $file_banner;
        $inserts->cate_service_id = $request->cate_service_id;
        try {
            $inserts->save();
            if ($request->type[0]) {
                foreach ($request->type as $key => $value) {
                    $typeService             = new TypeService();
                    $typeService->service_id = $inserts->id;
                    $typeService->name       = $request->name[$key];
                    $typeService->type       = $value;
                    $typeService->save();
                }
            }
            if ($request->file_image != null) {
                $imageFiles      = $request->file('file_image');
                $folderDir       = 'upload/images/';
                $destinationPath = public_path($folderDir);
                foreach ($request->file_image as $fileObject) {
                    $destinationFileName = time() . $fileObject->getClientOriginalName();
                    $fileObject->move($destinationPath, $destinationFileName);
                    ServiceImage::create([
                        'service_id' => $inserts->id,
                        'image'      => $destinationFileName,
                    ]);
                }
            }
            return redirect()->route('service.index')->with(['success' => 'Thêm dịch vụ thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $cateService = CateService::all();
        return view('admin.service.edit', compact('service', 'cateService'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, service $service)
    {
        $validator = Validator::make($request->all(), [
            'name_dichvu' => 'required|max:225',
            'slug'        => 'required|max:225',
            'price'       => 'numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (Service::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'dich-vu-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'dich-vu-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $file_image = $service->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $service->image))) {
                    File::delete(public_path('upload/images/' . $service->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->route('service.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $file_banner = $service->banner;
        if ($request->banner) {
            $check_file = checkFileImage($request->banner);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $service->banner))) {
                    File::delete(public_path('upload/images/' . $service->banner));
                }
                $file_banner = uploadFile($request->file('banner'));
            } else {
                return redirect()->route('service.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        $service->name            = $request->name_dichvu;
        $service->slug            = $slug;
        $service->url_link_combo  = $request->url_link_combo != null ? $request->url_link_combo : '#';
        $service->study_condition = $request->study_condition != null ? $request->study_condition : 'Không';
        $service->description     = $request->description;
        $service->image           = $file_image;
        $service->banner           = $file_banner;
        $service->cate_service_id = $request->cate_service_id;
        try {
            $service->save();

            if ($request->type[0]) {
                foreach ($request->type as $key => $value) {
                    $typeService             = new TypeService();
                    $typeService->service_id = $service->id;
                    $typeService->name       = $request->name[$key];
                    $typeService->type       = $value;
                    $typeService->save();
                }
            }
            if ($request->old_img != null) {
                foreach ($request->old_img as $id_img => $one_img) {
                    $public_path  = public_path('upload/images/');
                    $one_img_link = time() . $one_img->getClientOriginalName();
                    $one_img->move($public_path, $one_img_link);
                    ServiceImage::where('id', $id_img)->update([
                        'image' => $one_img_link,
                    ]);
                }
            }
            if ($request->file_image != null) {
                $imageFiles      = $request->file('file_image');
                $folderDir       = 'upload/images/';
                $destinationPath = public_path($folderDir);
                foreach ($request->file_image as $fileObject) {
                    $destinationFileName = time() . $fileObject->getClientOriginalName();
                    $fileObject->move($destinationPath, $destinationFileName);
                    ServiceImage::create([
                        'service_id' => $service->id,
                        'image'      => $destinationFileName,
                    ]);
                }
            }
            return redirect()->route('service.index')->with(['success' => 'Sửa dịch vụ thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        foreach ($service->getImages as $img) {
            if (File::exists(public_path('upload/images/' . $img->image))) {
                File::delete(public_path('upload/images/' . $img->image));
            }
        }
        ServiceImage::whereIn('service_id', [$service->id])->delete();
        if (File::exists(public_path('upload/images/' . $service->image))) {
            File::delete(public_path('upload/images/' . $service->image));
        }
        TypeService::whereIn('service_id', [$service->id])->delete();
        $service->delete();
        return redirect()->back()->with(['success' => 'Xóa dịch vụ thành công !']);
    }

    public function getSuggestCities(Request $request)
    {
        if ($request->keyword) {
            $keyword  = $request->keyword;
            $input    = Input::all();
            $apiLink  = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?';
            $type     = '(cities)';
            $language = 'vi';
            $key      = 'AIzaSyAM3feqazSoRdenb6DEtZDJQZL_WGH67n4';
            $data     = "input=$keyword&types=($type)&language=$language&key=$key";

            $getSuggestCities = @file_get_contents($apiLink . $data);

            $suggestCities = [];

            if ($getSuggestCities) {
                $getSuggestCities = json_decode($getSuggestCities);

                if ($getSuggestCities->status == 'OK') {
                    $cities = $getSuggestCities->predictions;

                    foreach ($cities as $city) {
                        $suggestCities[] = [
                            'place_id'    => $city->place_id,
                            'description' => $city->description,
                        ];
                    }
                }
            }

            return Response::json($suggestCities);
        }
    }

    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            Service::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }
    public function updateTypeService(Request $request)
    {
        if ($request->ajax()) {
            TypeService::findOrFail($request->id)->update(['name' => $request->name, 'type' => $request->type]);
            echo true;
        } else {
            echo false;
        }
    }
}
