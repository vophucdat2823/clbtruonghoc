<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SendRequire;
use App\SendQuestion;

class SendRequireController extends Controller
{
    public function indexKh()
    {
        $send = SendRequire::where('type','course')->get();
    	return view('admin.send.indexKh',compact('send'));
    }

    public function indexDv()
    {
    	$send = SendRequire::where('type','service')->get();
        return view('admin.send.indexDv',compact('send'));
    }
    public function index_detailKh($id)
    {
    	$send = SendRequire::find($id);
    	$question = SendQuestion::whereIn('send_id',[$id])->get();
    	return view('admin.send.index_detailKh',compact('send','question'));
    }
    public function index_detailDv($id)
    {
        $send = SendRequire::find($id);
        $question = SendQuestion::whereIn('send_id',[$id])->get();
        return view('admin.send.index_detailDv',compact('send','question'));
    }
    public function update_status(Request $request)
    {
        if ($request->ajax()) {
            SendRequire::findOrFail($request->id)->update(['status' => $request->status]);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }
    public function destroy($id)
    {
    	SendQuestion::whereIn('send_id',[$id])->delete();
    	SendRequire::destroy($id);
    	return redirect()->back()->with(['success' => 'Xóa thành công !']);
    }
}
