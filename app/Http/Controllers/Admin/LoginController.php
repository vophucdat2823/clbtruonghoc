<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;



class LoginController extends Controller
{
    public function getLogin(){
        if (Auth::guard('admin')->check()) {

            if(Auth::guard('admin')->user()->role_id != 2){
                return redirect()->route('admin.dashboard')->with('success', 'Đăng nhập thành công');
            }else{
                Auth::guard('admin')->logout();
                return view('admin.login')->with('error', 'Tài khoản không có quyền vào trang quản trị !') ;
            }
        } else {
            return view('admin.login');
        }

        
       
        
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|check_password',

        ],[
            'email.required' => 'Vui lòng điền email của bạn!',
            'email.email' => 'Email bạn nhập không đúng định dạng !',
            'password.required' => 'Vui lòng nhập mật khẩu !',
            'password.check_password' => 'Mật khẩu của bạn không đúng !',
        ]);
        

        if (Auth::guard('admin')->attempt($request->only('email','password'))) {
            if(Auth::guard('admin')->user()->role_id != 2){	
                return redirect()->route('admin.dashboard')->with('success', 'Đăng nhập thành công');
            }else{
                return redirect('/') ;
            }
            
        }else{
            return redirect()->back()->with(['error'=>'Đăng nhập không thành công, vui lòng kiểm tra lại!']);
        }
        // else 

        // return redirect()->back()->with(['error'=>' email khoặc mật khẩu không đúng ']);

        // return view('admin.login');

    }

    public function getLogout(Request $request)
    {
       if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
            return redirect('/');
        }
    }
}
