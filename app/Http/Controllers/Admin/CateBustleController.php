<?php

namespace App\Http\Controllers\Admin;

use App\CateBustle;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CateBustleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateBustle = CateBustle::all();
        $cateBustle0 = CateBustle::where('parents', 0)->get();
        $cateBustle1 = CateBustle::where('parents', '<>', 0)->get();
        return view('admin.cateBustle.index', compact('cateBustle', 'cateBustle0', 'cateBustle1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
            'parents' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $slug = '';
        if (CateBustle::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'clb-truong-hoc/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'clb-truong-hoc/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $inserts          = new CateBustle();
        $inserts->name    = $request->name;
        $inserts->parents = 0;
        $inserts->slug    = $slug;
        try {
            $inserts->save();
            return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cateBustle  $cateBustle
     * @return \Illuminate\Http\Response
     */
    public function show(CateBustle $cateBustle)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cateBustle  $cateBustle
     * @return \Illuminate\cateBustle\Response
     */
    public function edit(CateBustle $cateBustle)
    {
        $cateBustleAll = CateBustle::all();;
        return view('admin.cateBustle.edit', compact('cateBustle', 'cateBustleAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cateBustle  $cateBustle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CateBustle $cateBustle)
    {

        $validator = Validator::make($request->all(), [
            'name'    => 'required|max:225',
            'slug'    => 'required|max:225',
            'parents' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $slug = '';
        if (CateBustle::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
            $slug = 'clb-truong-hoc/' . $request->slug . '.' . 'html';
        } else {
            $slug = 'clb-truong-hoc/' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }
        $cateBustle->name    = $request->name;
        $cateBustle->parents = 0;
        $cateBustle->slug    = $slug;
        try {
            $cateBustle->save();
            return redirect('admin/cate-bustle')->with(['success' => 'Sửa danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cateBustle  $cateBustle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CateBustle $cateBustle)
    {
        $cateBustle->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
