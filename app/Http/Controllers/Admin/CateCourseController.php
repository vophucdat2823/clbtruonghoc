<?php

namespace App\Http\Controllers\Admin;

use App\CateCourse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use File;

class CateCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateCourse0 = CateCourse::where('parents', 0)->get();
        return view('admin.cateCourse.index', compact('cateCourse0'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:191',
            'title' => 'required|max:191',
            'slug'  => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        $slug = '';
        if (CateCourse::where('slug', $request->slug)->value('slug') == null) {
            $slug = 'khoa-hoc-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'khoa-hoc-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }

        $inserts          = new CateCourse();
        $inserts->name    = $request->name;
        $inserts->title   = $request->title;
        $inserts->image   = $file_image;
        $inserts->parents = 0;
        $inserts->slug    = $slug;
        try {
            $inserts->save();
            return redirect()->back()->with(['success' => 'Thêm danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CateCourse  $cateCourse
     * @return \Illuminate\Http\Response
     */
    public function show(CateCourse $cateCourse)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CateCourse  $cateCourse
     * @return \Illuminate\Http\Response
     */
    public function edit(CateCourse $cateCourse)
    {
        $cateCourseAll = CateCourse::all();
        return view('admin.cateCourse.edit', compact('cateCourse', 'cateCourseAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CateCourse  $cateCourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CateCourse $cateCourse)
    {

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:191',
            'title' => 'required|max:191',
            'slug'  => 'required|max:191',
        ]);
        $file_image = $cateCourse->image;
        if ($request->image) {
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE) {
                if (File::exists(public_path('upload/images/' . $cateCourse->image))) {
                    File::delete(public_path('upload/images/' . $cateCourse->image));
                }
                $file_image = uploadFile($request->file('image'));
            } else {
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $slug = '';
        if (CateCourse::where('slug', $request->slug)->whereNotIn('slug', [$request->slug])->value('slug') == null) {
            $slug = 'khoa-hoc-' . $request->slug . '.' . 'html';
        } else {
            $slug = 'khoa-hoc-' . $request->slug . '-' . str_random(1) . '.' . 'html';
        }
        $cateCourse->name    = $request->name;
        $cateCourse->title   = $request->title;
        $cateCourse->image   = $file_image;
        $cateCourse->parents = 0;
        $cateCourse->slug    = $slug;
        try {
            $cateCourse->save();
            return redirect('admin/cate-course')->with(['success' => 'Sửa danh mục thành công !']);
        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CateCourse  $cateCourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(CateCourse $cateCourse)
    {
        $cateCourse->delete();
        return redirect()->back()->with(['success' => 'Xóa danh mục thành công !']);
    }
}
