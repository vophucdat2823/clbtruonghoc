<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BoxLink;
use App\Bustle;
use App\CateCourse;
use App\CateService;
use App\City;
use App\CityCourses;
use App\CityDistrict;
use App\CityEvent;
use App\Course;
use App\District;
use App\EmailNewsletter;
use App\Event;
use App\ProductStore;
use App\SendQuestion;
use App\SendRequire;
use App\Service;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;

class UserController extends Controller
{
    public function index(BoxLink $boxLink, ProductStore $productStore, CityDistrict $cityDistrict, Banner $banner, Event $event, CityEvent $cityEvent, District $district, City $city)
    {
        $bustle = Bustle::where('status', 0)->limit(4)->get();
        return view('user.home', compact('boxLink', 'productStore', 'cityDistrict', 'banner', 'event', 'cityEvent', 'district', 'city', 'bustle'));
    }
    public function getAddress($id_matp)
    {
        $quanhuyen = District::where('matp', $id_matp)->get();
        echo "<option value=''>Quận / Huyện</option>";
        foreach ($quanhuyen as $key => $qh_ies) {
            echo "<option value='" . $qh_ies->maqh . "'>" . $qh_ies->name . "</option>";
        }
    }
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(),
            [
                'email' => 'required|unique:email_newsletters,email',
            ], [
                'email.required' => 'Vui lòng nhập địa chỉ email của bạn!',
                'email.unique'   => 'Email này đã tồn tại !',
            ]);

        if ($validation->passes()) {
            EmailNewsletter::create(
                [
                    'email'  => $request->email,
                    'status' => 0,
                ]);
            return response()->json([
                'message' => 'Đăng ký nhận bản tin thành công !',
            ]);
        } else {
            return response()->json([
                'message' => $validation->errors(),
                'error'   => true,
                'data'    => $request->all(),
            ]);
        }

    }
    public function filter(Request $request)
    {

        $course = Course::orderBy('id', 'desc');

        // if ($request->type_course_id) {
        //     $course = $course->where('type_course_id',$request->type_course_id);
        // }
        if ($request->sale_price) {
            $sale_price = explode('-', $request->sale_price);
            $course     = $course->where('price', '>=', $sale_price[0])->where('price', '<=', $sale_price[1]);
        }
        $limit = 6;
        if ($request->limit) {
            $limit = $request->limit;
        }
        if ($request->address1) {
            $cityCourses = CityCourses::where('city_id', $request->address1)->pluck('courses_id');
            $course      = $course->whereIn('id', $cityCourses);
        }
        if ($request->address2) {
            $cityDistrict = CityCourses::where('district_id', $request->address2)->pluck('courses_id');
            $course       = $course->whereIn('id', $cityDistrict);
        }
        $course_all = $course->where('status', 0)->get();
        $course     = $course->where('status', 0)->limit($limit)->get();
        $city       = City::all();
        // $typeCourse = TypeCourse::where('status',0)->get();
        return view('user.filter', compact('course', 'course_all', 'city', 'typeCourse'));
    }
    public function cate_detail($detail)
    {

        $cateCourse = CateCourse::orderBy('id', 'desc')->where('slug', $detail)->where('status', 0)->first();

        $cateService = CateService::orderBy('id', 'desc')->where('slug', $detail)->where('status', 0)->first();

        if ($cateCourse) {

            return view('user.cateCourse_detail', compact('cateCourse'));

        } else if ($cateService) {

            return view('user.cateService_detail', compact('cateService'));

        } else {
            abort(404);
        }

        return view('user.cate_detail', compact('course'));
    }
    public function detail($detail)
    {
        $course_detail  = Course::where('slug', $detail)->where('status', 0)->first();
        $service_detail = Service::where('slug', $detail)->where('status', 0)->first();

        if ($course_detail) {
            return view('user.courseDetail', compact('course_detail'));

        } elseif ($service_detail) {
            return view('user.serviceDetail', compact('service_detail'));
        } else {
            abort(404);
        }
    }

    public function sendRequire(Request $request)
    {
        // $validation = Validator::make($request->all(),
        //     [
        //         'email' => 'required',
        //         'name'  => 'required',
        //         'date'  => 'required',
        //         'phone' => 'required',
        //         'time'  => 'required',
        //     ]);
        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withErrors($validator)
        //         ->withInput($request->all());
        // }
        $inserts             = new SendRequire();
        $inserts->email      = $request->email;
        $inserts->primary_id = $request->primary_id;
        $inserts->user_id    = Auth::id();
        $inserts->name       = $request->name;
        $inserts->type       = $request->type;
        $inserts->date       = $request->date;
        $inserts->phone      = $request->phone;
        $inserts->time       = $request->time;
        $inserts->quantity   = $request->quantity;
        try {
            $inserts->save();
            if ($request->question_id[0]) {
                foreach ($request->question_id as $key => $value) {
                    $send               = new SendQuestion;
                    $send->send_id      = $inserts->id;
                    $send->question_id  = $value;
                    $send->rep_question = $request->question[$key];
                    $send->save();
                }
            }
            return redirect()->back()->with(['success' => "Thành công!"]);

        } catch (Exception $message) {
            return redirect()->back()->with(['error' => $message->getMessage()]);
        }
    }
    public function check()
    {
        return view('welcome');
    }
    protected $posts_per_page = 1;
    public function profileCourse(Request $request)
    {
        $send = SendRequire::where('type', 'course')->where('user_id', Auth::id())->orderBy('updated_at', 'desc')->paginate($this->posts_per_page);
        if ($request->ajax()) {
            return [
                'sends'     => view('user.paginationCourse')->with(compact('send'))->render(),
                'next_page' => $send->nextPageUrl(),
            ];
        }
        return view('user.profileCourse', compact('course', 'send'));
    }
    public function profileService(Request $request)
    {
        $send = SendRequire::where('type', 'service')->where('user_id', Auth::id())->orderBy('updated_at', 'desc')->paginate($this->posts_per_page);
        if ($request->ajax()) {
            return [
                'sends'     => view('user.paginationService')->with(compact('send'))->render(),
                'next_page' => $send->nextPageUrl(),
            ];
        }
        return view('user.profileService', compact('send'));
    }
    public function infoAccount()
    {
        return view('user.infoAccount');
    }
    public function changePassWord(Request $request)
    {
        if ($request->ajax()) {
            $validation = Validator::make($request->all(),
                [
                    'password'             => 'required|check_old_pass',
                    'new_password'         => 'required',
                    'confirm_new_password' => 'required|same:new_password',
                ],
                [
                    'password.check_old_pass'       => 'Mật khẩu bạn nhập không không đúng !',
                    'password.required'             => 'Vui lòng nhập mật khẩu cũ để xác nhân !',

                    'confirm_new_password.required' => 'Vui lòng nhập lại mật khẩu để xác nhân !',
                    'confirm_new_password.same'     => 'Nhập lại mật khẩu chưa đúng !',

                    'new_password.required'         => 'Vui lòng nhập mật khẩu mới !',

                ]);
            if ($validation->passes()) {
                User::where('id', Auth::id())->update([
                    'password' => bcrypt($request->new_password),
                ]);
                return response()->json([
                    'error' => false,
                ]);
            } else {
                return response()->json([
                    'message' => $validation->errors(),
                    'error'   => true,
                    'data'    => $request->all(),
                ]);
            }

        }
        return redirect()->back();
    }
    public function changeAccound(Request $request)
    {
        $id = Auth::id();
        $this->validate($request,
            [
                'name'  => 'required|max:191',
                'email' => 'required|max:191|email|check_email',
                'phone' => 'required|max:20',
            ],
            [
                'name.required'     => 'Vui lòng nhập tên của bạn !',
                'email.required'    => 'Vui lòng nhập mail của bạn !',
                'phone.required'    => 'Vui lòng nhập số điện thoại của bạn !',
                'name.max'          => 'Tên bạn không hợp lệ !',
                'email.max'         => 'Mail không hợp lệ !',
                'phone.max'         => 'Số điện thoại không hợp lệ !',
                'email.check_email' => 'Mail đã tồn tại !',
            ]);
        $file_image = Auth::user()->avatar;
        if ($request->hasFile('avatar_file_image')) {
            $check_file = checkFileImage($request->avatar_file_image);
            if ($check_file == CHECK_FILE) {
                $file_image = uploadFile($request->file('avatar_file_image'));
            } else {
                return redirect()->route('service.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }
        User::where('id', Auth::id())->update([
            'avatar' => $file_image,
            'name'   => $request->name,
            'email'  => $request->email,
            'phone'  => $request->phone,
        ]);
        return redirect()->back();
    }

    public function DetailsProduct($slugProducts){
        $details = ProductStore::orderBy('id', 'desc')->where('slug', $slugProducts)->first();
        return view('user/details-pro', ['details' => $details]);
    }

    public function DetailsEvent($slugEvent){
        $detailEvent = Event::orderBy('id', 'desc')->where('slug', $slugEvent)->first();
        return view('user/details-event', ['detailEvent' => $detailEvent]);
    }

    public function Shop($categories = [13]){

        // $proHot = ProductStore::orderBy('id', 'desc')->Where('category_id','like','%13%')->first();
        // $proHot = ProductStore::whereIn('category_id', '13')->get();

        // return Response::json($proHot);
        // return view('user.shop', ['proHots' => json_decode($proHot)]);


    }


}
