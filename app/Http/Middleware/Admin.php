<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            if(Auth::guard('admin')->user()->role_id == 1){
                return $next($request);
            }else{
                Auth::guard('admin')->logout();
                return view('admin.login')->with(['error' => "Tài khoản không có quyền vào trang quản trị !"]);
            }
        }else{
            return redirect('/')->with(['error' => "Bạn chưa đăng nhập !"]);
        }
    }
}
