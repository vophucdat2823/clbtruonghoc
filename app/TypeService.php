<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    protected $table = 'type_service';
    protected $fillable = [
        'service_id','type','name'
    ];
   	public $timestamps = false;
}
