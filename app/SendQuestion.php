<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendQuestion extends Model
{
    protected $table = 'send_questions';
    protected $fillable = [
        'send_id',
        'question_id',
        'rep_question',
    ];
    
    public function getQuestion(){
        return $this->hasOne(Question::class,'id', 'question_id');
    }
    public $timestamps = false;
}
