<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItemCustom extends Model
{
    protected $table = 'admin_menu_items';

    public static function getItemMenu($id) {

		return self::where("menu", '=', $id)->orderBy("sort", "asc")->get();
	}

}
