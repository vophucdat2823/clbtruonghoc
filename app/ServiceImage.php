<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceImage extends Model
{
    protected $table = 'service_images';

    protected $fillable = [
        'image','service_id',
    ];
    public $timestamps = false;

    public function service()
    {
    	return $this->belongsTo(Service::class);
    }

}
