<?php  
namespace App\Helper;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Auth;
/**
 * 
 */
class MailerShipped extends Mailable
{
	use Queueable,SerializesModels;
	public $order;
	public function __construct($order)
	{
		$this->order = $order;
	}

	public function build()
	{
		return $this->view('email.mailer')->with([
			'name' => Auth::user()->name,
			'address' => Auth::user()->address,
			'order' => $this->order,

		]);
	}
}

?>