<?php
namespace App\Helper;

/**
 *
 */
class Cart
{
    public $items      = [];
    public $totalQty   = 0;
    public $totalPrice = 0;

    public function __construct()
    {
        $this->items      = session('cart') ? session('cart') : [];
        $this->totalQty   = $this->get_totalQty();
        $this->totalPrice = $this->get_totalPrice();
    }
    public function add($product, $quantity)
    {
        $key = $product['id'];

        // return $this->items[$product['id']];

        if (isset($this->items[$product['id']])) {
            $this->items[$product['id']]['quantity'] += $quantity > 1 ? $quantity : 1;
        } else {
            $this->items[$product['id']] = [
                "id"          => $product['id'],
                "name"        => $product['name'],
                "slug"        => $product['slug'],
                "description" => $product['description'],
                // "category_id"     =>     $product['category_id'],
                "image"       => $product['image'],
                "price"       => $product['price'],
                "quantity"    => $quantity > 1 ? $quantity : 1,
            ];
        }
        session(['cart' => $this->items]);
    }
    public function remode_cart($id)
    {
        if (isset($this->items[$id])) {
            unset($this->items[$id]);
        }
        session(['cart' => $this->items]);
    }

    public function update($data)
    {
        for ($i = 0; $i < count($data['id']); $i++) {
            if (isset($this->items[$data['id'][$i]])) {
                if (is_int($data['quantity'][$i])) {
                    $qtt = $data['quantity'][$i] > 0 ? $data['quantity'][$i] : 1;
                } else {
                    $qtt = (int) $data['quantity'][$i];
                }
                $this->items[$data['id'][$i]]['quantity'] = $qtt;
            }
            session(['cart' => $this->items]);
        }
    }
    protected function get_totalQty()
    {
        foreach ($this->items as $item) {
            $this->totalQty = $this->totalQty + $item['quantity'];
        }
        return $this->totalQty;
    }
    protected function get_totalPrice()
    {
        foreach ($this->items as $item) {
            $this->totalPrice = $this->totalPrice + ($item['quantity'] * $item['price']);
        }
        return $this->totalPrice;

    }
}
