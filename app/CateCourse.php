<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateCourse extends Model
{
    public function getCourse()
    {
        return $this->hasMany(Course::class,'cate_course_id', 'id');
    }
}
