<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bustle extends Model
{
    protected $table = 'bustles';
    protected $fillable = [
        'status',
    ];
}
