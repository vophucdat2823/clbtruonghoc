<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityDistrict extends Model
{
    protected $table = 'city_district_product_store';
    protected $fillable = [
        'city_id','district_id','product_store_id'
    ];
   public $timestamps = false;

    public function city_district_city()
    {
        return $this->hasOne(City::class,'matp', 'city_id');
    }
}
