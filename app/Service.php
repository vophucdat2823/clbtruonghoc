<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $fillable = [
        'status',
    ];
    public function getCateService(){
        return $this->hasOne(CateService::class,'id', 'cate_service_id');
    }
    public function getTypeService($type){
        return $this->hasMany(TypeService::class,'service_id', 'id')->where('type',$type)->orderBy('id','desc')->get();
    }
    public function getTypeServiceOne($type){
        return $this->hasMany(TypeService::class,'service_id', 'id')->where('type',$type)->first();
    }
    public function getTypeService_id($type,$id){
        return $this->hasMany(TypeService::class,'service_id', 'id')->where('type',$type)->where('service_id',$id)->get();
    }
    public function getServicePartner(){
        return $this->hasMany(ServicePartner::class,'service_id', 'id');
    }
    public function getImages()
    {
        return $this->hasMany(ServiceImage::class);
    }
}
