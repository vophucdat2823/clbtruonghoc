<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailNewsletter extends Model
{
    protected $table = 'email_newsletters';
    protected $fillable = [
        'email','status'
    ];
}
