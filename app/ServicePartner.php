<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePartner extends Model
{
    protected $table = 'service_partners';
    protected $fillable = [
		'service_id',
		'user_id',
		'unit_id',
		'price_id',
		'qty_id',
        'status',
		'quality_id',
		'address_id',
    ];
    public function getService_id($id){
        return $this->hasMany(TypeService::class,'service_id', 'service_id')->where('id',$id)->first();
    }
    public function getService(){
        return $this->hasOne(Service::class, 'id','service_id');
    }
    public function getUser(){
        return $this->hasOne(User::class, 'id','user_id');
    }
}
