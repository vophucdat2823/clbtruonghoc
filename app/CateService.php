<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateService extends Model
{
    public function getService()
    {
        return $this->hasMany(Service::class,'cate_service_id', 'id');
    }
}
