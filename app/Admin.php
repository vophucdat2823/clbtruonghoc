<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{

    protected $table = 'admins';
    protected $fillable = [
        'name', 'email','phone', 'password',
    ];
     protected $hidden = [
        'password', 'remember_token',
    ];



    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
