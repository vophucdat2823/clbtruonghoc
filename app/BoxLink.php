<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxLink extends Model
{
    protected $table = 'box_links';
    protected $fillable = [
        'name','image','stt','link',
    ];
    public $timestamps = false;
}
