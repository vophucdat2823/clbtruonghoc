<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = [
        'primary_id','question','type'
    ];
   	public $timestamps = false;

   	public static function getByType($id)
    {
        return self::where('primary_id', '=', $id)->get();
    }

    // public function city_courses()
    // {
    //     return $this->hasOne(City::class,'matp', 'city_id');
    // }
}
