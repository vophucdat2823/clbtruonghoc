<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImgStore extends Model
{
    protected $table = 'product_img_stores';
    protected $fillable = [
        'images',
        'product_id',
        'id'
    ];
}
