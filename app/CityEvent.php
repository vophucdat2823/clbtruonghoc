<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityEvent extends Model
{
    protected $table = 'city_district_event';
    protected $fillable = [
        'city_id','district_id','event_id','specific_address'
    ];
   public $timestamps = false;

    public function city_event()
    {
        return $this->hasOne(City::class,'matp', 'city_id');
    }
}
