<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCourse extends Model
{
    protected $table = 'type_courses';
    protected $fillable = [
        'course_id','type','name'
    ];
   	public $timestamps = false;
}
