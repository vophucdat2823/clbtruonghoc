<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserParent extends Model
{
	protected $table = 'user_parents';
    protected $fillable = [
        'user_id',
       	'passport',
       	'job',
       	'email',
       	'address',
       	'name_student',
       	'city_id',
       	'level_school',
       	'name_school',
   ];
   public $timestamps = false;
}
