<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseImage extends Model
{
    protected $table = 'course_images';

    protected $fillable = [
        'image','course_id',
    ];
    public $timestamps = false;

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

}
