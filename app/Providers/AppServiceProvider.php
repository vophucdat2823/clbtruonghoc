<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\MenuCustom;
use App\MenuItemCustom;
use App\Helper\Cart;
use Auth;
use Hash;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*',function($view){
            $view->with([
                'carts' => new Cart(),
            ]);
        });
        Validator::extend('check_old_pass',function ($attributes,$value,$parameter,$validator){
        return Hash::check($value, Auth::user()->password);
        }); 

        Validator::extend('check_password',function ($attributes,$value,$parameter,$validator){
            $user = User::where('email',request()->email)->first();
            if ($user) {
                return Hash::check($value, $user->password);
            }else{
               return false;
            }
        });
        Validator::extend('check_email',function ($attributes,$value,$parameter,$validator){
            $user = User::whereNotIn('id',[Auth::id()])->where('email',request()->email)->first();
            if ($user) {
                return false;
            }else{
               return true;
            }
        });
    }
}
