<?php

namespace App\Providers;

use App\Permission;
use App\Role;
use App\User;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */

    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


        // dd($auth->guard('admin')->user());


        Gate::before(function($user){ 
            if ($user->id === 0) {
                return true;
            }
        });

        // Gate::define('super-user', function ($user) {
        //     return true; 
        // });

        if (! $this->app->runningInConsole()) {
            foreach (Permission::all() as $permisson) {
                Gate::define($permisson->name_check, function($user) use ($permisson){
                    return $user->hasPermision($permisson);
                });
            }
        }

    }
}
