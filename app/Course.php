<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = [
        'status',
    ];
    public function cityCourses()
    {
        return $this->hasMany(CityCourses::class,'courses_id','id')->limit(1);
    }
    public function userCourses()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function getCateCourse(){
        return $this->hasOne(CateCourse::class,'id', 'cate_course_id');
    }
    public function getTypeCourse($type){
        return $this->hasMany(TypeCourse::class,'course_id', 'id')->where('type',$type)->orderBy('id','desc')->get();
    } 
    public function getTypeCourseOne($type){
        return $this->hasMany(TypeCourse::class,'course_id', 'id')->where('type',$type)->first();
    }
    public function getTypeCourse_id($type,$id){
        return $this->hasMany(TypeCourse::class,'course_id', 'id')->where('type',$type)->where('course_id',$id)->get();
    }
    public function getImages()
    {
        return $this->hasMany(CourseImage::class);
    }
}
