<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityBustle extends Model
{
    protected $table = 'city_district_bustle';
    protected $fillable = [
        'city_id','district_id','bustle_id','specific_address'
    ];
   public $timestamps = false;

    public function city_bustle()
    {
        return $this->hasOne(City::class,'matp', 'city_id');
    }
}
