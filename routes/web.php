<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
Route::get('/clear-cache', function() {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    /*Artisan::call('cache:table');*/
    /*Artisan::call('migrate');*/
    Cache::flush();
    var_dump('config cache successed');
});
    Route::get('/check', 'UserController@check')->name('check');

    Route::get('/login-admin', 'Admin\LoginController@getLogin')->name('admin.login');
    Route::post('/login-admin', 'Admin\LoginController@postLogin')->name('admin.post.login');
    Route::get('/dang-xuat.html', 'Admin\LoginController@getLogout')->name('admin.logout');

    Auth::routes();

Route::group(['middleware' => 'check'], function () {

    Route::get('/', 'UserController@index')->name('home');
    Route::get('/bo-loc.html', 'UserController@filter')->name('filter');
    Route::get('/thong-tin-khoa-hoc.html', 'UserController@profileCourse')->name('profileCourse');
    Route::get('/thong-tin-dich-vu.html', 'UserController@profileService')->name('profileService');
    Route::get('/thong-tin-tai-khoan.html', 'UserController@infoAccount')->name('infoAccount');
    Route::post('/change_pass', 'UserController@changePass')->name('changePass');
    Route::post('/changePassWord', 'UserController@changePassWord')->name('changePassWord');
    Route::post('/changeAccound', 'UserController@changeAccound')->name('changeAccound');


    Route::get('chi-tiet/{detail}', 'UserController@detail')->name('detail');
    Route::get('chi-tiet-danh-muc/{detail}', 'UserController@cate_detail')->name('cate_detail');

    Route::post('/email-newsletter-post', 'UserController@store')->name('email_newsletter.store');
    Route::get('boot/ajax/quanhuyen/{id_matp}','UserController@getAddress')->name('home.getAddress');
    Route::post('/gui-yeu-cau', 'UserController@sendRequire')->name('user.sendRequire');

    Route::get('/redirect/{social}', 'SocialAuthController@redirect');
    Route::get('/callback/{social}', 'SocialAuthController@callback');



        //Gior Hang
    Route::post('gio-hang', 'CartController@add_cart')->name('add_cart');
    Route::get('remode-cart/{id}', 'CartController@remode_cart')->name('remode_cart');
    Route::get('update_cart', 'CartController@update_cart')->name('update_cart');

    // View giỏ hàng
    Route::get('check_cart', 'CartController@check_cart')->name('check_cart');

    Route::get('buy_cart', 'CartController@buy_cart')->name('buy_cart');
    Route::post('buy_cart', 'CartController@post_buy_cart')->name('buy_cart');

    Route::get('lich-su-dat-hang', 'CartController@history_cart')->name('history_cart');


    Route::get('view-cart-{id}', 'CartController@view_cart')->name('view_cart');

});



Route::group(['prefix' => 'admin','middleware' => 'admin','namespace' => 'Admin'], function () {

        // Route::get('/list', 'MenuController@index')->name('admin.menu.list');

        Route::get('/email-newsletter', 'EmailNewsletterController@index')->name('email_newsletter.index');
        
        Route::post('/excel-email-newsletter', 'EmailNewsletterController@excel_email')->name('email_newsletter.excel_email');

        Route::post('/email-newsletter/update_status', 'EmailNewsletterController@update_status')->name('email_newsletter.update_status');
        Route::get('/email-newsletter/delete_email/{id}', 'EmailNewsletterController@delete_email')->name('email_newsletter.delete_email');

        Route::get('/menu', 'MenuController@index')->name('admin.menu.list');



        Route::get('/dashboard','AdminController@index')->name('admin.dashboard');

        Route::post('uploadImg/{type}', 'AdminController@postImages'); 
        Route::post('deleteImg', 'AdminController@deleteImg'); 

        Route::get('/box-link','BoxLinkController@index')->name('box-link.index');
        Route::post('/box-link','BoxLinkController@store')->name('box-link.store');

        Route::get('/banner','BannerController@index')->name('banner.index');
        Route::post('/banner','BannerController@store')->name('banner.store');
        Route::post('/banner-create','BannerController@create')->name('banner.create');


        Route::get('/question/{type}/{id}','QuestionController@create')->name('question.create');
        Route::put('/question/{type}/{id}','QuestionController@store')->name('question.store');
        Route::get('/question-del/{id}','QuestionController@question')->name('question.question');
        Route::post('/update-question','QuestionController@updateQuestion')->name('question.updateQuestion');



        Route::resource('/cate-event','CateEventController');
        Route::resource('/event','EventController');
        Route::post('/event/update_status', 'EventController@update_status')->name('event.update_status');
        Route::get('/ajax/quanhuyen/{id_matp}','EventController@get_quan_huyen')->name('get_quan_huyen');

        Route::resource('/news','NewsController');

        Route::resource('/account','AccountController');

        Route::resource('/cate-bustle','CateBustleController');
        Route::resource('/bustle','BustleController');
        Route::post('/bustle/update_status', 'BustleController@update_status')->name('bustle.update_status');









        Route::resource('/cate-service','CateServiceController');
        Route::resource('/service','ServiceController');
        Route::post('/service/update_status', 'ServiceController@update_status')->name('service.update_status');
        Route::get('/type-service/{id}','ServiceController@typeService')->name('service.typeService');
        Route::post('/update-type-service','ServiceController@updateTypeService')->name('service.updateTypeService');
        Route::get('/delete-old-img/{id}','ServiceController@deleteImage')->name('service.deleteImage');




        Route::resource('/cate-course','CateCourseController');
        Route::resource('/course','CourseController');
        Route::post('/course/update_status', 'CourseController@update_status')->name('course.update_status');
        Route::get('/type-course/{id}','CourseController@typeCourse')->name('course.typeCourse');
        Route::post('/update-type-course','CourseController@updateTypeCourse')->name('course.updateTypeCourse');
        Route::get('/delete-old-img1/{id}','CourseController@deleteImage')->name('course.deleteImage');



        Route::get('/yeu-cau-khach-hang/khoa-hoc','SendRequireController@indexKh')->name('admin.send.indexKh');
        Route::get('/yeu-cau-khach-hang/dich-vu','SendRequireController@indexDv')->name('admin.send.indexDv');
        Route::get('/yeu-cau-khach-hang/khoa-hoc/chi-tiet/{id}','SendRequireController@index_detailKh')->name('admin.send.index_detailKh');
        Route::get('/yeu-cau-khach-hang/dich-vu/chi-tiet/{id}','SendRequireController@index_detailDv')->name('admin.send.index_detailDv');
        Route::post('/send/update_status', 'SendRequireController@update_status')->name('send.update_status');
        Route::post('/send/destroy/{id}', 'SendRequireController@destroy')->name('send.destroy');



        // START: TRAN MINH TAN
        Route::get('/product/list','ProductStoreController@index')->name('listProduct');
        Route::get('/product/add','ProductStoreController@create')->name('addProduct');
        Route::post('/product/add','ProductStoreController@store')->name('postProduct');
        Route::get('/product/edit','ProductStoreController@edit')->name('editProduct');
        Route::put('/product/edit','ProductStoreController@update')->name('updateProduct');
        Route::get('/product/delete/{id}','ProductStoreController@destroy')->name('deleteProduct');
        Route::post('/update_status', 'ProductStoreController@update_status')->name('update_status_product');
        //END: TRAN MINH TAN
        
        Route::resource('/cate-store','CateStoreController');
        // Route::resource('/product-store','ProductStoreController');
      
        // Route::get('/product/ajax/quanhuyen/{id_matp}','ProductStoreController@get_quan_huyen')->name('get_quan_huyen');
});

Route::group(['middleware' => config('menu.middleware')], function () {
    //Route::get('wmenuindex', array('uses'=>'\Harimayco\Menu\Controllers\MenuController@wmenuindex'));
    $path = rtrim(config('menu.route_path'));
    Route::post($path . '/addcustommenu', array('as' => 'haddcustommenu', 'uses' => '\Harimayco\Menu\Controllers\MenuController@addcustommenu'));
    Route::post($path . '/deleteitemmenu', array('as' => 'hdeleteitemmenu', 'uses' => '\Harimayco\Menu\Controllers\MenuController@deleteitemmenu'));
    Route::post($path . '/deletemenug', array('as' => 'hdeletemenug', 'uses' => '\Harimayco\Menu\Controllers\MenuController@deletemenug'));
    Route::post($path . '/createnewmenu', array('as' => 'hcreatenewmenu', 'uses' => '\Harimayco\Menu\Controllers\MenuController@createnewmenu'));
    Route::post($path . '/generatemenucontrol', array('as' => 'hgeneratemenucontrol', 'uses' => '\Harimayco\Menu\Controllers\MenuController@generatemenucontrol'));
    Route::post($path . '/updateitem', array('as' => 'hupdateitem', 'uses' => '\Harimayco\Menu\Controllers\MenuController@updateitem'));
});
Route::group(['prefix' => 'partner','middleware' => 'auth','namespace' => 'Admin_v2'], function () {
// 'middleware' => ['cannot:super-user']
        Route::get('/dashboard','AdminController@index')->name('partner.dashboard');
        

        // Route::get('/profiles/getSuggestCities','ProductStoreController@getSuggestCities')->name('partner.getSuggestCities');


        
        
        
        
        // Quyền đăng bán sản phẩm 
       Route::group(['middleware' => ['can:product']], function () {
            // Route::resource('/cate-store','CateStoreController');
            Route::resource('/product-store','ProductStoreController');
            Route::post('/update_status', 'ProductStoreController@update_status')->name('product-store.update_status');
        });
            Route::get('/product/ajax/quanhuyen/{id_matp}','ProductStoreController@get_quan_huyen')->name('get_quan_huyen');


            
        // Quyền khóa học
        Route::group(['middleware' => ['can:course']], function () {

            Route::get('/course-partner','CoursePartnerController@index')->name('course-partner.index');
            Route::get('/course-partner-user','CoursePartnerController@index_user')->name('course-partner.index_user');
            Route::post('/course-partner','CoursePartnerController@store')->name('course-partner.store');

            Route::get('/course-partner/{course_id}/edit','CoursePartnerController@edit')->name('course-partner.edit');
            Route::post('/course-partner/{course_id}','CoursePartnerController@update')->name('course-partner.update');
            Route::post('/course-partner-destroy/{course_id}','CoursePartnerController@destroy')->name('course-partner.destroy');
            Route::post('/course-partner-user/update_status', 'CoursePartnerController@update_status')->name('course-partner.update_status');

        });
            // Route::get('/course/ajax/quanhuyen/{id_matp}','CourseController@get_quan_huyen')->name('course.get_quan_huyen');

        // Quyền dịch vụ
        Route::group(['middleware' => ['can:service']], function () {
            
            // Route::resource('/cate-service','CateServiceController');
            Route::get('/service-partner','ServicePartnerController@index')->name('service-partner.index');
            Route::get('/service-partner-user','ServicePartnerController@index_user')->name('service-partner.index_user');
            Route::post('/service-partner','ServicePartnerController@store')->name('service-partner.store');

            Route::get('/service-partner/{service_id}/edit','ServicePartnerController@edit')->name('service-partner.edit');
            Route::post('/service-partner/{service_id}','ServicePartnerController@update')->name('service-partner.update');
            Route::post('/service-partner-destroy/{service_id}','ServicePartnerController@destroy')->name('service-partner.destroy');
            Route::post('/service-partner-user/update_status', 'ServicePartnerController@update_status')->name('service-partner.update_status');
        });
});


Route::get('chi-tiet-san-pham/{slugProducts}', 'UserController@DetailsProduct');
Route::get('su-kien/{slugEvent}', 'UserController@DetailsEvent');

Route::get('/shop', 'UserController@Shop');