
$(document).ready(function() {
	// $('.slide-banner').owlCarousel({
	//     loop:true,
	//     nav:false,
	//     dots:false,
	//     autoplay:true,
	//     smartSpeed:1000,
	//     margin:0,
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:1
	//         },
	//         1000:{
	//             items:3
	//         }
	//     }
	// });
	// $('.slide-dmsp').owlCarousel({
	//     loop:true,
	//     margin:0,
	//     nav:true,
	//     dots:false,
	//     autoplay:false,
	//     smartSpeed:1000,
	//     navText: [$('.am-prev'),$('.am-next')],
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:1
	//         },
	//         1000:{
	//             items:10
	//         }
	//     }
	// });
	// $('.slide-sp').owlCarousel({
	//     loop:true,
	//     margin:10,
	//     nav:true,
	//     dots:false,
	//     autoplay:false,
	//     smartSpeed:1000,
	//     navText: [$('.am-prev1'),$('.am-next1')],
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:4
	//         },
	//         1000:{
	//             items:6
	//         }
	//     }
	// });
	// $('.slide-sp2').owlCarousel({
	//     loop:true,
	//     margin:10,
	//     nav:true,
	//     dots:false,
	//     autoplay:false,
	//     smartSpeed:1000,
	//     navText: [$('.am-prev2'),$('.am-next2')],
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:4
	//         },
	//         1000:{
	//             items:6
	//         }
	//     }
	// });
	// $('.slide-sp3').owlCarousel({
	//     loop:true,
	//     margin:10,
	//     nav:true,
	//     dots:false,
	//     autoplay:false,
	//     smartSpeed:1000,
	//     navText: [$('.am-prev3'),$('.am-next3')],
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:4
	//         },
	//         1000:{
	//             items:6
	//         }
	//     }
	// });
	// $('.slide-splq').owlCarousel({
	//     loop:true,
	//     margin:10,
	//     nav:true,
	//     dots:false,
	//     autoplay:false,
	//     smartSpeed:1000,
	//     navText: [$('.am-prev4'),$('.am-next4')],
	//     responsive:{
	//         0:{
	//             items:1
	//         },
	//         600:{
	//             items:3
	//         },
	//         1000:{
	//             items:4.3
	//         }
	//     }
	// });
	$('.slide button').hover(function() {
		$(this).toggleClass('owl-hint');
	});
	
	$(window).scroll(function() {
		if($(this).scrollTop() != 0){
			$('header').addClass('menu-fix');
			$('.danhmucsp-page .list-dm').addClass('danhmuc-fix');
		}
		else{
			$('header').removeClass('menu-fix');
			$('.danhmucsp-page .list-dm').removeClass('danhmuc-fix');
			
		}
	});
	$('.list-ul li').each(function(){
        $(this).click(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
            }else{
                $('.list-ul li').removeClass('active');
                $(this).addClass('active');
            }
        });
    });
	$('.delete').on('click',function(){
		$(this).parent('li').fadeOut('fast');
	});

    $('.activate').click(function() {
    	let value,trans,temp;

    	$(this).siblings('.activated').css('display', 'block');
    	$(this).css('display', 'none');
    	value=$(this).val();
		trans=$("#transpost_product").val();
		temp=trans+','+value;
		tem=temp.split(',');
		filtered = tem.filter(function (el) {
			return el != "";
		});
		$("#transpost_product").val(filtered.toString());

    });

     $('.activated').click(function() {

		 let value,trans,temp;
    	$(this).siblings('.activate').css('display', 'block');
    	$(this).css('display', 'none');

    	value=parseInt($(this).val());
		 trans=$("#transpost_product").val().split(',');

		 filtered = trans.filter(function (el) {
			 return el != value;
		 });

		 $("#transpost_product").val(filtered.toString());
    	console.log(filtered);

    });

    $('.select').change(function() {
       var current = $(this).val();
       if(current=='1'){
           $(this).css('color','red');
       }
       if(current=='2'){
           $(this).css('color','#63489a');
       }
    }); 
    $('.lb-info i').hover(function(){
    	$(this).parents('label').siblings('.warning').toggle();
    });

    // $('.search-main input').focus(function() {
    // 	$(this).siblings('.search-main-mini').css('display', 'block');
    // });
    // $('.search-main input').focusout(function() {
    // 	$(this).siblings('.search-main-mini').css('display', 'none');
    // });
    //
    $('.header-main .select-option').click(function() {
    	$(this).find('ul').toggle();
    	$('.header-main .select-option li').click(function() {
    		var value_li=$(this).text();
    		$('.select-option span').html(value_li);
    	});
    });
    $('.del-product').click(function() {
    	$('#del-product').modal('show');
    });
    $('.slide_img_home').owlCarousel({
		loop:true,
		margin:30,
		dots:false,
		nav:true,
		autoplay:false,
		autoplaySpeed: 2000,
		autoTimeout: 4000,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		}
	});
 	  $('.messenger-center .form-sent textarea').focusin(function() {
 	  	$(this).addClass('textarea');
 	 	tinymce.init({ selector:'.textarea',forced_root_block : 'false' });
 	 	$('.close-tinymce').css({
 	 		display: 'block'
 	 	});
 		$('.file-insert').css({
 	 		position: 'relative'
 	 	});
 	 	$('.upload-file').addClass('active');
 	 	$('.lbl-upload-file').css({
 	 		display: 'block'
 	 	});
 	 	$('.MultiFile-list').css({
 	 		display: 'block'
 	 	});
 	 	$('button.sent').addClass('active');
 	  });
 	   $('.MultiFile-remove').click(function() {
 	 	$('.upload-file').css({
 	 		display: 'block'
 	 	});
 	 });
 	  $('.messenger-center .form-sent .close-tinymce').click(function() {
 	  	tinymce.remove();
 	  // 	$('.upload-file').css({
 	 	// 	display: 'none',
 	 	// 	top:'60%'
 	 	// });
 	 	$('.upload-file').removeClass('active');
 	 	$('.lbl-upload-file').css({
 	 		display: 'none'
 	 	});
 	 	$('.MultiFile-list').css({
 	 		display: 'none'
 	 	});
 	 	$('.file-insert').css({
 	 		position: 'unset'
 	 	});
 	 	$(this).css({
 	 		display: 'none'
 	 	});
 	 	$('button.sent').removeClass('active');
 	  });
 	$('.quotation-information .define').hover(function() {
 		$('.detail-define').toggleClass('active');;
 	});


    	// anh bìa
	(function() {
		// Display the images to be uploaded.
		var multiPhotoDisplay;

		$('#photos_cover').on('change', function(e) {
			return multiPhotoDisplay(this);
		});

		this.readURL = function(input) {
			var reader;

			// Read the contents of the image file to be uploaded and display it.

			if (input.files && input.files[0]) {
				reader = new FileReader();
				reader.onload = function(e) {
					var $preview;
					$('.image_to_upload').attr('src', e.target.result);
					$preview = $('.preview');
					if ($preview.hasClass('hide')) {
						return $preview.toggleClass('hide');
					}
				};
				return reader.readAsDataURL(input.files[0]);
			}
		};

		multiPhotoDisplay = function(input) {
			var file, i, len, reader, ref;

			// Read the contents of the image file to be uploaded and display it.

			if (input.files && input.files[0]) {
				ref = input.files;
				for (i = 0, len = ref.length; i < len; i++) {
					file = ref[i];
					reader = new FileReader();
					reader.onload = function(e) {
						var image_html;
						image_html = `<li><input type="hidden" name='image' value="${e.target.result}">
	<a class="th" href="${e.target.result}"><img width="75" src="${e.target.result}"></a></li>`;
						$('.preview_img').html(image_html);
						// if ($('.pics-label.hide').length !== 0) {
						// 	$('.pics-label').toggle('hide').removeClass('hide');
						// }
						return $(document).foundation('reflow');
					};
					reader.readAsDataURL(file);
				}
				window.post_files = input.files;
				if (window.post_files !== void 0) {
					return input.files = $.merge(window.post_files, input.files);
				}
			}
		};

	}).call(this);
    // anh sp
    (function() {
	  // Display the images to be uploaded.
	  var multiPhotoDisplay;

	  $('#photos').on('change', function(e) {
	    return multiPhotoDisplay(this);
	  });

	  this.readURL = function(input) {
	    var reader;
	    
	    // Read the contents of the image file to be uploaded and display it.

	    if (input.files && input.files[0]) {
	      reader = new FileReader();
	      reader.onload = function(e) {
	        var $preview;
	        $('.image_to_upload').attr('src', e.target.result);
	        $preview = $('.preview');
	        if ($preview.hasClass('hide')) {
	          return $preview.toggleClass('hide');
	        }
	      };
	      return reader.readAsDataURL(input.files[0]);
	    }
	  };

	  multiPhotoDisplay = function(input) {
	    var file, i, len, reader, ref;
	    
	    // Read the contents of the image file to be uploaded and display it.

	    if (input.files && input.files[0]) {
	      ref = input.files;
	      for (i = 0, len = ref.length; i < 3; i++) {
	        file = ref[i];
	        reader = new FileReader();
	        reader.onload = function(e) {
	          var image_html;
	          image_html = `<li><input type="hidden" name='gallery[]' value="${e.target.result}"><a class="th" href="${e.target.result}"><img width="75" src="${e.target.result}"></a><button type="button" class="delete"><i class="far fa-trash-alt"></i></button></li></li>`;
	          $('#photos_clearing').append(image_html);
	          if ($('.pics-label.hide').length !== 0) {
	            $('.pics-label').toggle('hide').removeClass('hide');
	          }
	          return $(document).foundation('reflow');
	        };
	        reader.readAsDataURL(file);
	      }
	      window.post_files = input.files;
	      if (window.post_files !== void 0) {
	        return input.files = $.merge(window.post_files, input.files);
	      }
	    }
	  };

	}).call(this);

	$(function() {
	  $("#uploadFile").on("change", function() {
	    var files = !!this.files ? this.files : [];
	    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

	    if (/^image/.test(files[0].type)) { // only image file
	      var reader = new FileReader(); // instance of the FileReader
	      reader.readAsDataURL(files[0]); // read the local file

	      reader.onloadend = function() { // set image data as background of div
	        $("#imagePreview").css("background-image", "url(" + this.result + ")");
	        $("#uploadFile").attr('value',this.result);
	        console.log(this.result);
	        $("#uploadFiletext").val(this.result);
	      }
	    }
	  });
	  $('#imagePreview').click(function() {
	    $('#uploadFile').trigger('click');
	  });
	});
	$('.slider-for').slick({
	   slidesToShow: 1,
	   slidesToScroll: 1,
	   arrows: false,
	   fade: true,
	   asNavFor: '.slider-nav'
	 });
	$('.slider-nav').slick({
	   slidesToShow: 3,
	   slidesToScroll: 1,
	   asNavFor: '.slider-for',
	   dots: false,
	   focusOnSelect: true,
	   centerPadding:10,
	   arrows:false
	 });
	
	$('.header-main .timkiem .btn-tk').click(function() {
		$(this).siblings('.form-mb').toggle('slide');
	});

	$(".form-spinner .btn-increment").click(function () {
		let $this       = $(this);
		let input       = $this.siblings("input");

		if (input.data("type") === "number") {
			let step         = input.attr("step");
			let oldValue = input.val();
			let newValue = parseInt(oldValue) + parseInt(step || 1);
			let maxValue = input.attr("max");
			let minValue = input.attr("min");

			if (!maxValue || newValue <= maxValue)
			{
				input.val(newValue);
				$("#qtyproduct").val(newValue);
				$("#amount_quotation").val(newValue);
			}

			else if (input.data("spinner-loop"))
			{
				input.val(minValue);
				$("#qtyproduct").val(minValue);
				$("#amount_quotation").val(minValue);
			}
			else
			{
				input.val(maxValue);
				$("#qtyproduct").val(maxValue);
				$("#amount_quotation").val(maxValue);
			}
		} else if (input.data("type") === "text") {
			let oldIndex = input.data("spinner-index") || 0;
			let items    = input.data("spinner-items");
			let newIndex = oldIndex + 1;

			if (newIndex < items.length) {
				input.val(items[newIndex]);
				input.data("spinner-index", newIndex);
			} else if (input.data("spinner-loop")){
				input.val(items[0]);
				input.data("spinner-index", 0);
			}
		}

	});
	$(".form-spinner .btn-decrement").click(function () {
		let $this       = $(this);
		let input       = $this.siblings("input");

		if (input.data("type") === "number") {
			let step         = input.attr("step");
			let oldValue = input.val();
			let newValue = parseInt(oldValue) - parseInt(step || 1);
			let maxValue = input.attr("max");
			let minValue = input.attr("min");

			if (!minValue || newValue >= minValue)
			{
				$("#qtyproduct").val(newValue);
				$("#amount_quotation").val(newValue);
				input.val(newValue);
			}

			else if (input.data("spinner-loop")){
				input.val(maxValue);
				$("#qtyproduct").val(maxValue);
				$("#amount_quotation").val(maxValue);
			}

			else{
				input.val(minValue);
				$("#qtyproduct").val(minValue);
				$("#amount_quotation").val(minValue);
			}

		} else if (input.data("type") === "text") {
			let oldIndex = input.data("spinner-index") || 0;
			let items    = input.data("spinner-items");
			let newIndex = oldIndex - 1;

			if (newIndex >= 0) {
				input.val(items[newIndex]);
				input.data("spinner-index", newIndex);
			} else if (input.data("spinner-loop")){
				input.val(items[items.length - 1]);
				input.data("spinner-index", items.length - 1);
			}
		}
	});
	
	// $(".form-spinner .btn-increment").click(function () {
    //     let $this       = $(this);
    //     let input       = $this.siblings("input");
    //
    //     if (input.data("type") === "number") {
    //         let step         = input.attr("step");
    //         let oldValue = input.val();
    //         let newValue = parseInt(oldValue) + parseInt(step || 1);
    //         let maxValue = input.attr("max");
    //         let minValue = input.attr("min");
    //
    //         if (!maxValue || newValue <= maxValue)
    //             input.val(newValue);
    //         else if (input.data("spinner-loop"))
    //             input.val(minValue);
    //         else
    //             input.val(maxValue);
    //     } else if (input.data("type") === "text") {
    //         let oldIndex = input.data("spinner-index") || 0;
    //         let items    = input.data("spinner-items");
    //         let newIndex = oldIndex + 1;
	//
    //         if (newIndex < items.length) {
    //             input.val(items[newIndex]);
    //             input.data("spinner-index", newIndex);
    //         } else if (input.data("spinner-loop")){
    //             input.val(items[0]);
    //             input.data("spinner-index", 0);
    //         }
    //    }
    //
    // });
    // $(".form-spinner .btn-decrement").click(function () {
    //     let $this       = $(this);
    //     let input       = $this.siblings("input");
    //
    //     if (input.data("type") === "number") {
    //         let step         = input.attr("step");
    //         let oldValue = input.val();
    //         let newValue = parseInt(oldValue) - parseInt(step || 1);
    //         let maxValue = input.attr("max");
    //         let minValue = input.attr("min");
	//
    //         if (!minValue || newValue >= minValue)
    //             input.val(newValue);
    //         else if (input.data("spinner-loop"))
    //             input.val(maxValue);
    //         else
    //             input.val(minValue);
    //     } else if (input.data("type") === "text") {
    //         let oldIndex = input.data("spinner-index") || 0;
    //         let items    = input.data("spinner-items");
    //         let newIndex = oldIndex - 1;
	//
    //         if (newIndex >= 0) {
    //             input.val(items[newIndex]);
    //             input.data("spinner-index", newIndex);
    //         } else if (input.data("spinner-loop")){
    //             input.val(items[items.length - 1]);
    //             input.data("spinner-index", items.length - 1);
    //         }
    //     }
    // });
	
});