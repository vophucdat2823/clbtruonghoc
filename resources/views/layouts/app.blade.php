<!DOCTYPE html>

<html>



<head>

    <title>@yield('title')</title>

    <!-- <base href="http://tadaha.com/"> -->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">



    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/owl.carousel.min.css') }}">

    <link rel="stylesheet" href="{{ url('public/assets/fontawesome-pro-5.0.13/css/all.css') }}">





    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">







    @yield("stylesheet")

     <link rel='stylesheet' id='responsive-css' href='https://teky.edu.vn/wp-content/themes/teky_v2/css/responsive_new.css?ver=0f0930d09a285ed70ea601886e28e63f15ba4765' type='text/css' media='all' />

    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick-theme.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/app.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/style.css') }}">



</head>

@php

    $menu_name_1      = App\MenuCustom::getById(1)['name'];

    $menu_item_name_1 = App\MenuItemCustom::getItemMenu(1);



    $menu_name_2      = App\MenuCustom::getById(2)['name'];

    $menu_item_name_2 = App\MenuItemCustom::getItemMenu(2);



    $menu_name_3      = App\MenuCustom::getById(3)['name'];

    $menu_item_name_3 = App\MenuItemCustom::getItemMenu(3);



    $menu_name_4      = App\MenuCustom::getById(4)['name'];

    $menu_item_name_4 = App\MenuItemCustom::getItemMenu(4);



    $menu_name_5      = App\MenuCustom::getById(5)['name'];

    $menu_item_name_5 = App\MenuItemCustom::getItemMenu(5);



    $menu_name_6      = App\MenuCustom::getById(6)['name'];

    $menu_item_name_6 = App\MenuItemCustom::getItemMenu(6);



    $menu_name_7      = App\MenuCustom::getById(7)['name'];

    $menu_item_name_7 = App\MenuItemCustom::getItemMenu(7);



    $clb_course         = App\CateCourse::where('status',0)->get();

    $clb_service_class  = App\CateService::where('status',0)->where('id','<>',7)->get();

    $clb_service_parent  = App\Service::where('status',0)->where('cate_service_id',7)->get();

@endphp





<body style="background: #fff">

    <div class="copyright text-black  text-small text-center text-md-left"  style="background: #F5F8FC;text-align: right">

        <div class="py-3"  style="width: 75vw;margin:0 auto">

            <div class="row">

                <p class="m-0 col-md-auto col-12 mr-auto" style="width: 100%;text-align: right;">

                    <font style="vertical-align: inherit;">

                        <font style="vertical-align: inherit;margin-right: 10px"><i class="fal fa-phone txtUpsideDown"></i>  <span>1900 8888 </span> </font>

                        <font style="vertical-align: inherit;"><i class="fal fa-envelope"></i> info@clbtruonghoc.edu.vn</font>

                    </font>

                </p>

            </div>

        </div>

    </div>

    <header class="header-little_information">

        <div class="navigator">

            <div class="width-75-vw clearfix" style="margin: 0 auto;">

                <div class="hn-logo ">

                    <a href="/"><img src="{{ url('public/assets') }}/logo.png" class="img-fluid"></a>

                </div>

                <button type="button" class="expand-menu"><i class="em"></i><i class="em"></i><i class="em"></i></button>

                <div class="menu">

                    <ul>

                        <li class="course current_active_nav">

                            <a href="javascript:0;"><i class="fal fa-graduation-cap"></i> KHOÁ HỌC</a>

                            <ul class="sub-menu">

                                <div class="step-all">

                                    <div class="tab-content content_pane_box">

                                        <div role="tabpanel" class="tab-pane active" id="4786">

                                            <div class="info_pane_box">

                                                <div class="top_pane_box clearfix">

                                                    <div class="left_pane">

                                                        <div class="ls_pane">

                                                            <h3 class="title_pane font-Light text-uppercase text-green">

                                                                <span>Câu lạc bộ khóa học</span></h3>

                                                            @foreach ($clb_course as $key => $clb)

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    @if ($key < 3)

                                                                        <li>

                                                                            <a href="{{ route('cate_detail',['detail' => $clb->slug]) }}">

                                                                                <div class="img_content_pane">

                                                                                    <div class="info_img_pane">

                                                                                        <img src="{{ url('public/upload/images') }}/{{ $clb->image }}">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="text_content_pane">

                                                                                    <p class="font-Condensed" style="margin-bottom:0px">{{$clb->name}}</p>

                                                                                    <span class="text-small font-Light">{{$clb->title}}</span>

                                                                                </div>

                                                                            </a>

                                                                        </li>

                                                                    @endif

                                                                </ul>

                                                            </div>

                                                            @endforeach

                                                        </div>

                                                    </div>

                                                    <div class="left_pane">

                                                        <div class="ls_pane">

                                                            <h3 class="font-Light text-uppercase text-green">

                                                                <span>&nbsp;</span></h3>

                                                            @foreach ($clb_course as $key => $clb)

                                                                @if ($key >= 3)

                                                                    <div class="second_content_pane clearfix">

                                                                        <ul>

                                                                            <li>

                                                                                <a href="{{ route('cate_detail',['detail' => $clb->slug]) }}">

                                                                                    <div class="img_content_pane">

                                                                                        <div class="info_img_pane">

                                                                                            <img src="{{ url('public/upload/images') }}/{{ $clb->image }}">

                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="text_content_pane">

                                                                                        <p class="font-Condensed" style="margin-bottom:0px">{{$clb->name}}</p>

                                                                                        <span class="text-small font-Light">{{$clb->title}}</span>

                                                                                    </div>

                                                                                </a>

                                                                            </li>

                                                                        </ul>

                                                                    </div>

                                                                @endif

                                                            @endforeach

                                                        </div>

                                                    </div>

                                                    {{-- <div class="sub-bg">

                                                        <img src="https://teky.edu.vn/wp-content/uploads/sites/4/2017/09/bg_pane_one-657x350.png">

                                                    </div> --}}

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </ul>

                        </li>

                        <li>

                            <a href="{{URL::to('/shop')}}"><i class="fal fa-shopping-cart"></i> CỬA HÀNG</a>

                        </li>

                        <li  class="course current_active_nav">

                            <a href="javascript:0;"><i class="fal fa-university"></i> DỊCH VỤ</a>

                            <ul class="sub-menu">

                                <div class="step-all">

                                    <div class="tab-content content_pane_box">

                                        <div role="tabpanel" class="tab-pane active" id="4786">

                                            <div class="info_pane_box">

                                                <div class="top_pane_box clearfix">

                                                    <div class="left_pane">



                                                        <div class="ls_pane">

                                                            <h3 class="title_pane font-Light text-uppercase text-green">

                                                                <span>DÀNH CHO TẬP THỂ LỚP</span></h3>

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    @foreach ($clb_service_class as $clb)

                                                                        <li>

                                                                            <a href="{{ route('cate_detail',['detail' => $clb->slug]) }}">

                                                                                <div class="img_content_pane">

                                                                                    <div class="info_img_pane">

                                                                                        <img src="{{ url('public/upload/images') }}/{{ $clb->image }}">

                                                                                    </div>

                                                                                </div>

                                                                                <div class="text_content_pane">

                                                                                    <p class="font-Condensed" style="margin-bottom:0px">{{$clb->name}}</p>

                                                                                    <span class="text-small font-Light">{{$clb->title}}</span>

                                                                                </div>

                                                                            </a>

                                                                        </li>

                                                                    @endforeach

                                                                </ul>

                                                            </div>

                                                        </div>



                                                    </div>

                                                    <div class="left_pane">

                                                        <div class="ls_pane">

                                                            <h3 class="title_pane font-Light text-uppercase text-green">

                                                                <span>DÀNH CHO PHỤ HUYNH</span></h3>

                                                                @foreach ($clb_service_parent as $clb)

                                                                    <div class="second_content_pane clearfix">

                                                                        <ul>

                                                                            <li>

                                                                                <a href="{{ route('detail',['detail' => $clb->slug]) }}">

                                                                                    <div class="img_content_pane">

                                                                                        <div class="info_img_pane">

                                                                                            <img src="{{ url('public/upload/images') }}/{{$clb->image}}">

                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="text_content_pane">

                                                                                        <p class="font-Condensed" style="margin-bottom:0px">{{$clb->name}}</p>

                                                                                        <span class="text-small font-Light">{{str_limit(strip_tags($clb->description),60)}}</span>

                                                                                    </div>

                                                                                </a>

                                                                            </li>

                                                                        </ul>

                                                                    </div>

                                                                @endforeach

                                                        </div>

                                                    </div>

                                                    <div class="left_pane">

                                                        <div class="ls_pane">

                                                            <h3 class="title_pane font-Light text-uppercase text-green">

                                                                <span>DÀNH CHO GIÁO VIÊN</span></h3>

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    <li>

                                                                        <a href="/khoa-hoc/sieu-nhan-lap-trinh/">

                                                                            <div class="img_content_pane">

                                                                                <div class="info_img_pane">

                                                                                    <img src="http://clbtruonghoc.edu.vn/public/upload/images/elearning_dichvugiaovien.jpg">

                                                                                </div>

                                                                            </div><!-- end .img_content_pane-->

                                                                            <div class="text_content_pane">

                                                                                <p class="font-Condensed" style="margin-bottom:0px">Hỗ trợ hệ thống E-learning</p>

                                                                                <span class="text-small font-Light">Trải nghiệm hệ thống E-learning chuyên nghiệp</span>

                                                                            </div><!-- end .text_content_pane-->

                                                                        </a>

                                                                    </li>

                                                                </ul>

                                                            </div>

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    <li>

                                                                        <a href="/khoa-hoc/lam-app-sieu-de/">

                                                                            <div class="img_content_pane">

                                                                                <div class="info_img_pane">

                                                                                    <img src="http://clbtruonghoc.edu.vn/public/upload/images/hotrotieudung_dichvugiaovien.jpg">

                                                                                </div>

                                                                            </div><!-- end .img_content_pane-->

                                                                            <div class="text_content_pane">

                                                                                <p class="font-Condensed" style="margin-bottom:0px">Hỗ trợ tiêu dùng</p>

                                                                                <span class="text-small font-Light">Dịch vụ hỗ trợ tiêu dùng an tâm và uy tín</span>

                                                                            </div><!-- end .text_content_pane-->

                                                                        </a>

                                                                    </li>

                                                                </ul>

                                                            </div>

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    <li>

                                                                        <a href="/khoa-hoc/lam-website-sieu-de/">

                                                                            <div class="img_content_pane">

                                                                                <div class="info_img_pane">

                                                                                    <img src="http://clbtruonghoc.edu.vn/public/upload/images/uudai_dichvugiaovien.jpg">

                                                                                </div>

                                                                            </div><!-- end .img_content_pane-->

                                                                            <div class="text_content_pane">

                                                                                <p class="font-Condensed" style="margin-bottom:0px">Dịch vụ ưu đãi khác</p>

                                                                                <span class="text-small font-Light">Chương trình ưu đãi đặc biệt dành cho Giáo viên</span>

                                                                            </div><!-- end .text_content_pane-->

                                                                        </a>

                                                                    </li>

                                                                </ul>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    {{-- <div class="sub-bg">

                                                        <img src="https://teky.edu.vn/wp-content/uploads/sites/4/2017/09/bg_pane_one-657x350.png">

                                                    </div> --}}

                                                </div>

                                            </div>

                                        </div>

                                        <div role="tabpanel" class="tab-pane " id="4787">

                                            <div class="info_pane_box">

                                                <div class="top_pane_box clearfix">

                                                    <div class="left_pane">

                                                        <div class="ls_pane">

                                                            <h3 class="title_pane font-Light text-uppercase text-green">

                                                                <span>Mầm non</span></h3>

                                                            <div class="second_content_pane clearfix">

                                                                <ul>

                                                                    <li>

                                                                        <a href="javascript:0;">

                                                                            <div class="img_content_pane">

                                                                                <div class="info_img_pane">

                                                                                    <img src="https://teky.edu.vn/wp-content/uploads/sites/4/2018/05/Uaro-70x40.png">

                                                                                </div>

                                                                            </div><!-- end .img_content_pane-->

                                                                            <div class="text_content_pane">

                                                                                <p class="font-Condensed">Mầm non sáng chế (4-6 tuổi)</p>

                                                                                <span class="text-small font-Light">Lắp ráp và lập trình kéo thả với Uaro Kit</span>

                                                                            </div><!-- end .text_content_pane-->

                                                                        </a>

                                                                    </li>

                                                                </ul>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </ul>

                        </li>

                        <li>

                            <a href="{{ route('filter') }}" class="{{Request::is('bo-loc.html') ? 'active_clb' : ''}}"><i class="fal fa-graduation-cap"></i> BỘ LỌC</a>

                        </li>

                        @if (Auth::check())

                        <li class="nav-item">

                            <div class="dropdown cart-target " id="load-cart-target">

                                <a href="#" class="cart dropdown-toggle dropdown-link mx-3 btn btn-outline-secondary btn-round" data-toggle="dropdown" title="Shopping Cart" class="" style="padding: unset;width: 35px;height: 35px;line-height: 35px;position: relative;margin: 0 !important;color: #fff;background: #1B74BB;font-size: 0.8vw;">

                                    <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>

                                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i> 

                                    <i class="fal fa-cart-plus"></i>

                                    <span class="_59tg" data-sigil="count">{{$carts->totalQty}}</span>

                                </a>

                                <div id="cart-info" class="dropdown-menu" style="top:10px !important">

                                    <div id="cart-content">

                                        <div class="empty text-center" >

                                            <div style="padding-left: 0;margin-bottom: 0;max-height: 255px;overflow-y: auto;">

                                                @foreach($carts->items as $key => $item)

                                                <div class="media" style="border-bottom: 2px solid black; padding-bottom: 10px ;align-items: center;">

                                                    <a class="pull-left" href="#">

                                                        <img class="media-object" src="{{url('public/upload/images')}}/{{$item['image']}}" width="50px" alt="Image">

                                                    </a>

                                                    <div class="media-body" style="text-align: initial;position: relative; padding-left: 10px">

                                                        

                                                            <h4 class="media-heading"><a href="#" style="padding: 0">{{$item['name']}}</a></h4>

                                                        

                                                        <p>Giá: {{number_format($item['price'])}}</p>

                                                        <p>Số lượng: {{number_format($item['quantity'])}}</p>

                                                        <span>Số tiền: {{number_format($item['price'] * $item['quantity'])}}</span>

                                                

                                                    </div>

                                                </div>

                                                @endforeach</div>

                                            <div>

                                                <h4 class="pull-left">Tổng Tiền:</h4>

                                                <h3 style="font-size: 17px;color: #755398;">{{number_format($carts->totalPrice)}} VND</h3>

                                            </div>

                                            <div class="clearfix"></div>

                                            @if ($carts->totalPrice > 0)

                                                <em><a href="#" class="btn btn-2">Vào giỏ hàng</a></em>

                                            @else

                                            <em>Giỏ hàng của bạn đang rỗng. <a href="{{route('check_cart')}}" class="btn btn-2">Vào giỏ hàng</a></em>

                                            @endif

                                            

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="dropdown open cart-target" >

                                <button class="mx-3 btn btn-outline-secondary btn-round dropdown-toggle" style="height: 100%;padding: unset;width: 35px;height: 35px;border: none;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    @if (Auth::user()->avatar)

                                        <img src="{{ url('public/upload/images') }}/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" style="width: 100%;height: 100%;object-fit: cover;border-radius: 100%;">

                                    @else

                                        <img src="{{ url('') }}/avata_user_new.jpg" alt="{{ Auth::user()->name }}" style="width: 100%;height: 100%;object-fit: cover;border-radius: 100%;">

                                    @endif

                                </button>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                    <a class="dropdown-item" href="{{ route('infoAccount') }}">Quản lý tài khoản</a>

                                    <a class="dropdown-item" href="#">Thành tích</a>

                                    @if(Auth::user()->role_id != 2)

                                        <a class="dropdown-item" href="{{ route('partner.dashboard') }}">Trang quản trị</a>

                                    @endif

                                    <a class="dropdown-item" href="{{ route('logout') }}"

                                    onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                        Đăng xuất

                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                                        {{ csrf_field() }}

                                    </form>

                                </div>

                            </div>

                        </li>

                        @else



                        <li class="list-unstyled d-flex m-0 login_register">

                            <a class="btn login_register_a1" href="{{ route('login') }}">Đăng nhập</a>

                            <a class="btn login_register_a2" href="{{ route('login') }}" onclick="event.preventDefault();

                                                     document.getElementById('register-form').submit();" >Đăng kí</a>

                            <form id="register-form" action="{{ route('login') }}" method="get" style="display: none;">

                                <input type="hidden" name="register_form" value="register_form">

                            </form>

                        </li>

                        @endif

                    </ul>

                </div>

                    

            </div><!-- container -->

                

        </div><!-- navigator -->

        <div>

            {{-- <div class=""  style="width: 75vw;margin:0 auto">

                <div class="little_information-left left">

                    <ul>

                        <li class="logo-little_information">

                            <a href="/">

                                <img src="{{ url('public/assets') }}/logo.png" class="img-fluid">

                            </a>

                        </li>

                    </ul>

                </div>

                <div class="little_information-right right">

                    <ul class="naw_clb_scol">

                        <li class="naw_clb_scol_li">

                            <a href="javascript:0;"><i class="fal fa-graduation-cap"></i> KHOÁ HỌC</a>

                        </li>

                        <li class="naw_clb_scol_li">

                            <a href="javascript:0;"><i class="fal fa-shopping-cart"></i> CỬA HÀNG</a>

                        </li>

                        <li class="naw_clb_scol_li">

                            <a href="javascript:0;"><i class="fal fa-university"></i> DỊCH VỤ</a>

                        </li>

                        <li class="naw_clb_scol_li">

                            <a href="{{ route('filter') }}" class="{{Request::is('bo-loc.html') ? 'active_clb' : ''}}"><i class="fal fa-graduation-cap"></i> BỘ LỌC</a>

                        </li>

                    </ul>

                </div>

            </div> --}}

            {{-- <div class="login_register">

                <a class="btn login_register_a1" href="javascript:0;">Đăng nhập</a>

                <a class="btn login_register_a2" href="javascript:0;">Đăng kí</a>

            </div> --}}

            

        </div>

    </header>





    

        @yield('content')

    



    <footer class="text-black  text-small background_footer delete_magrin_h5 style_transform">

        <div class="py-5" style="width: 75vw;margin:0 auto">

            <div class="row">

                <div class="col-12 col-md-6 col-lg-3 my-3">

                    <h5 class="mb-4">{{$menu_name_2}}</h5>

                    <hr>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_2 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                </div>

                <div class="col-12 col-md-6 col-lg-3 my-3">

                    <h5 class="mb-4">{{$menu_name_3}}</h5>

                    <hr>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_3 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                </div>

                <div class="col-12 col-md-6 col-lg-3 my-3">

                    <h5 class="mb-4">{{$menu_name_4}}</h5>

                    <hr>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_4 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                    <h5 class="mb-4">{{$menu_name_5}}</h5>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_5 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                    <h5 class="mb-4">{{$menu_name_6}}</h5>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_6 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                </div>

                <div class="col-12 col-md-6 col-lg-3 my-3">

                    <div>

                        <h5>ĐĂNG KÝ NHẬN BẢN TIN</h5>

                        <form id="email_newsletter" action="{{ route('email_newsletter.store') }}" method="POST" role="form" >

                                {{ csrf_field() }}

                            <div class="input-group mb-3">

                                <input type="email" class="form-control" name="email" id="" placeholder="Địa chỉ email">

                                <div class="input-group-append">

                                    <button class="btn btn-primary save_email_newsletter" type="submit"><i class="fal fa-envelope"></i></button>

                                </div>

                            </div>

                        </form>

                    </div>

                    <hr>

                    <h5 class="mb-4">{{$menu_name_7}}</h5>

                    <ul class="list-unstyled text-small">

                        @foreach ($menu_item_name_7 as $menu_it)

                            <li class="my-2"><a class="text-black" href="{{$menu_it['link']}}">{{$menu_it['label']}}</a></li>

                        @endforeach

                    </ul>

                    <h5 class="mb-4">Follow us on Social</h5>

                    <ul class="list-unstyled text-small">

                        <li class="mt-3">

                            <a class="text-black" href="javascript:0;"><img src="{{ url('public/assets') }}/images/icon/facebook.png" alt="Laravel" class="my-0 mr-md-auto"> </a>

                            <a class="text-black" href="javascript:0;"><img src="{{ url('public/assets') }}/images/icon/facebook.png" alt="Laravel" class="my-0 mr-md-auto"> </a>

                            <a class="text-black" href="javascript:0;"><img src="{{ url('public/assets') }}/images/icon/facebook.png" alt="Laravel" class="my-0 mr-md-auto"> </a>

                            <a class="text-black" href="javascript:0;"><img src="{{ url('public/assets') }}/images/icon/facebook.png" alt="Laravel" class="my-0 mr-md-auto"> </a>

                        </li>

                    </ul>

                    <h5 class="mb-4">1900 8888</h5>

                    <h5 class="mb-4">support@clbtruonghoc.edu.vn</h5>

                </div>

            </div>

        </div>

    </footer>

    <div class="left-menu toggle-navbar">

        <span class="close"><i class="fal fa-times"></i></span>

        <div class="menu-list menu-list-notuser">

            <ul class="menu-content">

                <li class="portal">

                    <a href="javascript:0;"><i class="fal fa-graduation-cap"></i> <span>KHOÁ HỌC</span></a>

                    <span class="sub-btn"><i class="fal fa-angle-right"></i></span>

                    <div class="sub-content">

                        <ul>

                            <li class="back"><i class="fal fa-arrow-left"></i> <span>Các Khóa học</span></li>

                            

                            @foreach ($clb_course as $key => $clb)

                                <li><a href="{{ route('cate_detail',['detail' => $clb->slug]) }}">{{$clb->name}}</a></li>

                            @endforeach

                        </ul>

                    </div>

                </li>

                <li class="course current_active_nav">

                    <a href="javascript:0;"><i class="fal fa-shopping-cart"></i> <span>CỬA HÀNG</span></a>

                </li>

                <li class="portal">

                    <a href="javascript:0;"><i class="fal fa-university"></i> <span>DỊCH VỤ</span></a>

                    <span class="sub-btn"><i class="fal fa-angle-right"></i></span>

                    <div class="sub-content">

                        <ul>

                            <li class="back"><i class="fal fa-arrow-left"></i> <span>Dành cho tập thể lớp</span></li>

                            @foreach ($clb_service_class as $clb)

                                <li><a href="{{ route('cate_detail',['detail' => $clb->slug]) }}">{{$clb->name}}</a></li>

                            @endforeach

                            <li class="back"><i class="fal fa-arrow-left"></i> <span>Dành cho phụ huynh</span></li>

                            @foreach ($clb_service_parent as $clb)

                                <li><a href="{{ route('detail',['detail' => $clb->slug]) }}">{{$clb->name}}</a></li>

                            @endforeach

                            <li class="back"><i class="fal fa-arrow-left"></i> <span>Dành cho giáo viên</span></li>

                            @foreach ($clb_service_parent as $clb)

                                <li><a href="{{ route('detail',['detail' => $clb->slug]) }}">{{$clb->name}}</a></li>

                            @endforeach

                        </ul>

                    </div>

                </li>

                <li class="portal">

                    <a href="{{ route('filter') }}" class="{{Request::is('bo-loc.html') ? 'active_clb' : ''}}"><i class="fal fa-graduation-cap"></i> <span>BỘ LỌC</span></a>

                </li>

                {{-- <li class="portal">

                    <a href="/khoa-hoc/">

                        <span>Các Khóa học</span>

                    </a>

                    

                </li> --}}

                {{-- <li class="portal">

                    <a href="/lich-khai-giang/">

                        <span>LỊCH KHAI GIẢNG</span>

                    </a>

                </li> --}}

                {{-- <li class="portal">

                    <a href="https://teky.edu.vn/gioi-thieu/">

                        <span>VỀ TEKY</span>

                    </a>

                    <span class="sub-btn"><i class="fa fa-angle-right"></i></span>

                    <div class="sub-content">

                        <ul>

                            <li class="back"><i class="icon_back"></i> <span>VỀ TEKY</span></li>

                            <li><a href="https://teky.edu.vn/gioi-thieu/">Giới thiệu</a></li>

                            <li><a href="/giang-vien/">Đội ngũ giáo viên</a></li>

                            <li><a href="/lanh-dao/">Đội ngũ lãnh đạo</a></li>

                            <li><a href="/bao-chi/">Báo chí nói về Teky</a></li>

                            <li><a href="/giai-thuong/">Giải thưởng</a></li>

                            <li><a href="/su-kien/">Sự kiện</a></li>

                            <li><a href="https://teky.edu.vn/lien-he/">Liên hệ</a></li>

                        </ul>

                    </div>

                </li> --}}

                {{-- <li class="portal">

                    <a href="/tuyen-dung/">

                        <span>Tuyển dụng</span>

                    </a>

                </li> --}}

            </ul>

        </div>

    </div>

    <div class="copyright text-black  text-small text-center text-md-left background_footer">

        <div class="container py-3">

            <div class="row">

                <p class="m-0 col-md-auto col-12 mr-auto">

                    <font style="vertical-align: inherit;">

                        <font style="vertical-align: inherit;">© 2018 Onlyawhile. </font>

                        <font style="vertical-align: inherit;">Copyright@2019 - Powered by CLB Truong Hoc .</font>

                    </font>

                </p>

            </div>

        </div>

    </div>

    {{-- <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script> --}}

    <script src="{{ url('public/assets/js/all.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/jquery-3.2.1.slim.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/popper.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/jquery-3.4.1.min.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/jquery.matchHeight-min.js') }}" type="text/javascript"></script>

    <script src="{{ url('public/assets/js/index.js') }}" type="text/javascript"></script>



    <script type='text/javascript' src='https://teky.edu.vn/wp-content/plugins/google-language-translator/js/scripts.js?ver=5.0.48'></script>

    @yield('script')

    

    @yield('javascript')

    <script type="text/javascript">

        $('#email_newsletter').on('submit', function(event){

        event.preventDefault();



            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });



            $.ajax({

               url:"{{ route('email_newsletter.store') }}",

               method:"POST",

               data:new FormData(this),

               dataType:'JSON',

               contentType: false,

               cache: false,

               processData: false,

               success:function(data)

               {

                console.log(data);

                    if (data.error == true) {

                        if (data.message.email != undefined) {

                            alert(data.message.email[0]);

                        };

                    } else {

                        alert('Đăng ký nhận bản tin thành công !');





                        $('#email_newsletter').load(location.href + ' #email_newsletter>*');

                    }

                }

            });

        });

    </script>

    <script type="text/javascript">

        $(".expand-menu").on("click",function(event) {

            $(".left-menu").addClass('open');

        });

         $(".close").on("click",function(event) {

            $(".left-menu").removeClass('open');

        });

         $(".sub-btn").on("click",function(event) {

            $(".left-menu").addClass('amz-leftmn');

            $(".menu-content").addClass('open');

            $(".portal").removeClass('active');

            $(this).parent().addClass('active');

        });



         $(".back").on("click",function(event) {

            $(".left-menu").removeClass('amz-leftmn');

            $(".menu-content").removeClass('open');

            $(this).parent().removeClass('active');

        });

    </script>





</body>



</html>

