<!DOCTYPE html>
<html>

<head>
    <title>Đăng nhập</title>
    <!-- <base href="http://tadaha.com/"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick-theme.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/slick.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/assets/css/tadaha.css') }}">
</head>

<body>
    <header class="header-little_information">
        <div class="little_information-left left">
            <ul>
                <li class="logo-little_information" style="height: 100%;">
                    <a href="/">
                        <img src="{{ url('public/assets') }}/logo.png" class="img-fluid" style="width: 20%;height: 100%;">
                    </a>
                </li>
            </ul>
        </div>
        <div class="little_information-right right">
            <ul class="naw_clb_scol">
                <li class="naw_clb_scol_li">
                    <a href="javascript:0;"><i class="fas fa-graduation-cap"></i> KHOÁ HỌC</a>
                </li>
                <li class="naw_clb_scol_li">
                    <a href="javascript:0;"><i class="fas fa-shopping-cart"></i> CỬA HÀNG</a>
                </li>
                <li class="naw_clb_scol_li">
                    <a href="javascript:0;"><i class="fas fa-university"></i> DỊCH VỤ</a>
                </li>
                <li class="naw_clb_scol_li">
                    <a href="{{ route('filter') }}"><i class="fas fa-graduation-cap"></i> BỘ LỌC</a>
                </li>
            </ul>
            <div class="login_register">
                <a class="btn login_register_a1" href="{{ route('login') }}">Đăng nhập</a>
                <a class="btn login_register_a2" href="{{ route('login') }}" onclick="event.preventDefault();document.getElementById('register-form').submit();" >Đăng kí</a>
                    <form id="register-form" action="{{ route('login') }}" method="get" style="display: none;">
                        <input type="hidden" name="register_form" value="register_form">
                    </form>
            </div>
        </div>
    </header>
    @yield('login')
    <div class="modal modal-forget-pass" id="modal-forget-pass">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Nhập email</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <p style="color:red" class="message-pass"></p>
                    <form action="" method="post" id="formmail">
                        <input type="hidden" name="_token" value="mLEGujTqhJvaybunIZSu2vAbETuWsSUGpXHtKxPA">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control email-pass" required>
                        </div>
                        <button type="button" class="btn btn-success btn-check-email">Gửi</button>
                    </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="site/js/index.js"></script>
    @yield('javascript')
    <script>
    $(".btn-check-email").click(function(e) {
        var email = $(".email-pass").val();
        if (email.length != 0) {
            var data = $("#formmail").serialize();
            $.ajax({
                method: 'get',
                url: 'checkmail',
                data: data,
                dataType: 'json'
            }).done(function(data) {
                $(".message-pass").text(data.message);
            }).fail(function(erro) {
                console.log(erro);
            });
        }

    });

    $(".message-login").delay(3000).slideUp();
    $(".message-cart").delay(4000).slideUp('slow');

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image-preview').attr('src', e.target.result);
                $("#imageurl").val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file-upload").change(function() {
        readURL(this);
    });
    $("#btn-random").click(function() {
        let code = creatRandom();
        $(".code-nha-thuoc").text(code);
        $("#codehidden").val(code);

    });

    $(".tab2").click(function() {
        let code = creatRandom();
        $(".code2hidden").text(code);
        $("#code2hidden").val(code);
    });

    function creatRandom() {
        let r = Math.random().toString(36).substring(7);
        return r;
    }
    $("#form-nha-thuoc").validate({
        rules: {
            account: "required",
            password_2: "required",
            code1: {
                "required": true,
                "equalTo": '#codehidden'
            },
            confirm_password: {
                "required": true,
                "equalTo": '#pass2'
            },
            passport: {
                "required": true,
                "number": true,
                'minlength':11
            },
            phone: "required",
            email_parent: "required",
            address: "required",
            name_student: "required",
            name_parent: "required",
            address_school: "required",
            level_school: "required",
            name_school: "required",
            job: "required",
        },
        messages: {
            "account": "Tài khoản không được để trống",
            "password_2": "Mật khẩu không được để trống",
            "confirm_password": {
                "required": "Xác nhận mật khẩu không được để trống",
                "equalTo": 'Mật khẩu không khớp'
            },
            "code1": {
                "required": "Mã xác nhận không được để trống",
                "equalTo": 'Mã xác nhận không khớp'
            },
            "passport": {
                "required": "CMND/Thẻ căn cước không được để trống",
                "number": 'CMND/Thẻ căn cước phải là số',
                "minlength": 'CMND/Thẻ căn cước phải tối thiều 11 ký tự'
            },
            "address": "Địa chỉ không được để trống",
            "phone": "Số điện thoại không được để trống",
            "email_parent": "Email không được để trống",
            "name_student": "Tên học sinh không được để trống",
            "name_parent": "Tên phụ huynh không được để trống",
            "address_school": "Vui lòng chọn Tỉnh/Thành phố",
            "level_school": "Vui lòng chọn cấp trường",
            "name_school": "Tên trường học không được để trống",
            "job": "Nghề nghiệp không được để trống",

        },
        submitHandler: function(form) {
            $(form).submit();
        }
    });


    $(".btn-forget-pass").click(function() {

        $("#modal-forget-pass").modal('show');
    });
    
    

    </script>
</body>

</html>


