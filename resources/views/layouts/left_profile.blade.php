<div class="style_box_filter style_box_filter_profile" style="    border-radius: 15px;">
    <div class="second_content_pane clearfix mb-3 second_content_pane_profile" >
        <ul>
            <li>
                <a href="http://localhost:209/prima/chi-tiet/dich-vu-gia-su-tai-nha.html">
                    <div class="img_content_pane">
                        <div class="">
                            @if (Auth::user()->avatar)
                                <img src="{{ url('public/upload/images') }}/{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}" style="width: 50px;height: 100%;object-fit: cover;border-radius: 100%;">
                            @else
                                <img src="{{ url('public') }}/avata_user_new.jpg" alt="{{ Auth::user()->name }}" style="width: 80px;height: 80px;object-fit: cover;border-radius: 100%;">
                            @endif
                        </div>
                    </div>
                    <div class="text_content_pane" style="padding: 15px 10px;">
                        <p class="font-Condensed" style="margin-bottom:0px;color: #000;font-size: 17px;font-weight: 500;">{{Auth::user()->name }}</p>
                        <span class="text-small">Tham gia: {{Auth::user()->updated_at }}</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <style type="text/css" media="screen">
        .active_profile_1{
            font-weight: 500;
            background: #EEF1F3;
        }
        .menu_profile_a{
            margin-bottom:0px;
            font-size: 15px;
            color: #1B74BB;
            font-weight: 500;
            letter-spacing: 1px;
            line-height: 30px;
            display: block;
            padding:10px 30px
        }
        .menu_profile_a:hover{
            background: #EEF1F3;
        }
        .style_box_filter_profile{
            background: #fff;
            box-shadow: 4px 5px 10px #ccc;
            padding:0 0 30px 0;
        }
        .second_content_pane_profile{
            padding:30px 30px 0 30px
        }
    </style>

    <a style="margin-top: 20px;" class="menu_profile_a" href="#"><i class="fal fa-bell"></i> Thông báo</a>
    <a class="menu_profile_a {{Request::is('thong-tin-tai-khoan.html') ? 'active_profile_1' : ''}}" href="{{ route('infoAccount') }}"><i class="fal fa-user"></i> Quản lý tài khoản</a>
    <a class="menu_profile_a {{Request::is('thong-tin-khoa-hoc.html') ? 'active_profile_1' : ''}}" href="{{ route('profileCourse') }}"><i class="fal fa-graduation-cap"></i> Khóa học</a>
    <a class="menu_profile_a {{Request::is('thong-tin-dich-vu.html') ? 'active_profile_1' : ''}}" href="{{ route('profileService') }}"><i class="fal fa-university"></i> Dịch vụ</a>
    <a class="menu_profile_a" href="#"><i class="fal fa-shopping-cart"></i> Cửa hàng</a>
    <a class="menu_profile_a" href="#"><i class="fal fa-usd-circle"></i> Tài khoản xu</a>
    <div>
        <a class="menu_profile_a" href="javascript:0;"><i class="fal fa-trophy-alt"></i> Thành tích</a>
        <ul class="sub-menuleft-profile">
            <li>
                <a href="#">Thông tin phát triển thể chất</a>
            </li>
            <li>
                <a href="#">Thành tích rèn luyện</a>
            </li>
            <li>
                <a href="#">Kết quả kiểm tra năng lực</a>
            </li>
            <li>
                <a href="#">Lộ trình phát triển</a>
            </li>
            <li>
                <a href="#">Album kỷ yếu</a>
            </li>
        </ul>
    </div>
    
    <style>
        .sub-menuleft-profile{
            padding: 0 50px;
        }
        .sub-menuleft-profile li{
            padding: 5px 0;
        }
        .sub-menuleft-profile li a{
            font-size: 15px;
            color: #1B74BB;
        }
    </style>
    
    <div class="btn-logout text-center">
        <button style="background: #1B74BB; border: none; color: #fff; border-radius: 0; padding: 6px 20px; margin-top: 20px;">Đăng xuất</button>
    </div>
</div>