@extends('layouts.login')

@section('login')

<main>
    <div class="login-page">
        <div class="form-login">
            <h4>
                Xin chào! chúng tôi là <a href="#">CLBTruonghoc</a>
            </h4>
            <p>Chào mừng bạn quay trở lại, xin vui lòng đăng nhập vào tài khoản của bạn</p>
            <div class="login_face_or_google">
                <legend>Đăng nhập bằng:</legend>

                <a class="btn login_facebook" href="{{ url('redirect/facebook') }}"><i class="fab fa-facebook-f" style="margin:0 5px"></i>Facebook</a>
                <a class="btn login_facebook" href="{{ url('redirect/google') }}"><i class="fab fa-google" style="margin:0 5px"></i>Google</a>
            </div>
             <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <legend style="font-size: 1.25rem;margin-bottom: 1rem">Hoặc bằng ID/Email/SĐT:</legend>
                <div class="taikhoan">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Nhập tài khoản của bạn tại đây!">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    {{-- <input type="text" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Nhập tài khoản của bạn tại đây!" required> --}}
                </div>
                <div class="taikhoan">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="current-password"  placeholder="Nhập mật khẩu của bạn tại đây!">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <label class="form-check nho">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Ghi nhớ đăng nhập') }}
                    </label>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Bạn quên mật khẩu?') }}
                        </a>
                    @endif
                </label>
                <button type="submit">Đăng nhập</button>
                <button type="button" data-toggle="modal" data-target="#registerModal" id="btn-random">Đăng ký miễn phí</button>
                <!-- Button trigger modal -->
                <div class="baomat">
                    <p>Bằng cách đăng ký, bạn đồng ý với CLB Trường Học về:</p>
                    <a href="#">Các điều khoản và điều kiện & Chính sách bảo mật</a>
                </div>
            </form>
        </div>
        <div class="anh">
            <a href="#">
                <img src="{{ url('public/assets/image') }}/1553171445_banner.jpg" class="img-fluid">
            </a>
        </div>
    </div>
</main>
@php
    $city = App\City::all();
@endphp
<!-- start register-modal -->
<div class="modal fade register-modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-header justify-content-center rounded-0">
                <h5 class="modal-title text-white" id="registerModalLabel">ĐĂNG KÍ TÀI KHOẢN</h5>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="tab_title_content">
                            <p class="mb-2 register-modal-text-large">Chào mừng bạn đến với trang dành cho Phụ huynh - Học sinh</p>
                            <p class="mb-2 register-modal-text">Để đăng ký tài khoản, xin vui lòng điền đầy đủ thông tin dưới đây. Hệ thống sẽ tự động tạo tài khoản nhanh nhất để hoàn tất đăng ký tài khoản.</p>
                            <p class="register-modal-text-small">Tham khảo hướng dẫn về tài khoản Phụ huynh <a href="#" title="">tại đây.</a></p>
                        </div>
                        
                        <form class="register-modal-form" id="form-nha-thuoc" method="post" action=" {{ route('register') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="tab" value="1" />
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="account">Tài khoản </label>
                                            <i class="fas fa-question-circle dropdown-toggle" id="dropDownGuide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        </div>
                                        <input type="text" class="form-control " id="account" name="account" placeholder="Vui lòng nhập tài khoản">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="pass2">Mật khẩu</label>
                                            <i class="fas fa-question-circle dropdown-toggle"></i>
                                        </div>
                                        <input type="password" class="form-control" id="pass2" name="password_2" placeholder="Vui lòng nhập mật khẩu">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="code">Nhập mã xác nhận</label>
                                        <div class="d-flex">
                                            <input type="text" class="form-control" id="code" name="code1" placeholder="Nhập mã xác nhận">
                                            <input type="hidden" value="" id="codehidden"></input>
                                            <div class="register-modal-code code-nha-thuoc">b58x6</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="rePass">Xác nhận mật khẩu</label>
                                        <input type="password" class="form-control" id="rePass" name="confirm_password" placeholder="Vui lòng nhập lại mật khẩu">
                                    </div>
                                </div>
                            </div>
                            <h3 class="register-modal-heading">Thông tin Phụ huynh</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="name_parent">Tên Phụ huynh</label>
                                            <i class="fas fa-question-circle dropdown-toggle" id="dropDownGuide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        </div>
                                        <input id="name_parent" type="text" class="form-control{{ $errors->has('name_parent') ? ' is-invalid' : '' }}" name="name_parent" value="{{ old('name_parent') }}" required autocomplete="name_parent" autofocus placeholder="Người đại diện">
                                        @if ($errors->has('name_parent'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name_parent') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="passport">CMND/Thẻ căn cước</label>
                                        <input type="text" class="form-control" id="passport" name="passport" placeholder="CMND/Thẻ căn cước">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Số điện thoại</label>
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="Số điện thoại">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="job">Nghề nghiệp</label>
                                        <input type="text" class="form-control" id="job" name="job" placeholder="Nghề nghiệp">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email_parent">Email</label>
                                        <input type="email" class="form-control" id="email_parent" name="email_parent" placeholder="Nhập email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="address_user">Thành Phố</label>
                                        <select name="address_user" id="address_user" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                            <option value="">Thành Phố</option>
                                            @foreach ($city as $ci)
                                            <option value="{{$ci->matp}}">{{$ci->name}}</option>
                                            @endforeach
                                       </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="address_user1">Quận/Huyện</label>
                                        <select name="address_user1" id="address_user1" data-placeholder="Choose a Country..." class="form-control">
                                            <option value="">--Chưa chọn Quận/Huyện--</option>
                                       </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="address">Địa chỉ</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Địa chỉ">
                                    </div>
                                </div>
                            </div>
                            <h3 class="register-modal-heading">Thông tin Học sinh</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="name_student">Tên học sinh</label>
                                            <i class="fas fa-question-circle dropdown-toggle" id="dropDownGuide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        </div>
                                        <input type="text" class="form-control" id="name_student" name="name_student" placeholder="Tên học sinh">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="ThanhPho">Tỉnh/Thành phố</label>
                                            <i class="fas fa-question-circle dropdown-toggle" id="dropDownGuide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        </div>
                                        
                                        <select name="address_school" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                            <option value="">Thành Phố</option>
                                            @foreach ($city as $ci)
                                            <option value="{{$ci->matp}}">{{$ci->name}}</option>
                                            @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="dropdown">
                                            <label for="QuanHuyen">Quận/Huyện</label>
                                            <i class="fas fa-question-circle dropdown-toggle" id="dropDownGuide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        </div>
                                        <select name="address_school1" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control">
                                            <option value="">--Chưa chọn Quận/Huyện--</option>
                                       </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="level_school">Cấp trường</label>
                                        <select name="level_school" id="level_school" class="form-control" required="required">
                                            <option value="">Cấp trường</option>
                                            <option value="1">Cấp 1</option>
                                            <option value="2">Cấp 2</option>
                                            <option value="3">Cấp 3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name_school">Tên trường học</label>
                                        <input type="text" class="form-control" id="name_school" name="name_school" placeholder="Tên trường học">
                                    </div>
                                </div>
                            </div>
                            <div class="form-check text-center mt-3">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck12">
                                <label class="form-check-label" for="defaultCheck1">
                                    Bằng cách đăng ký, bạn đồng ý với CLB Trường Học về: <a href="#" title="">Các điều khoản & Chính sách bảo mật</a> của CLBTruonghoc
                                </label>
                            </div>
                            <div class="modal-footer justify-content-center border-0 pb-4 pb-md-5 pt-0">
                                <button type="submit" class="btn text-uppercase">Đăng ký</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end register-modal -->
@endsection
@section('javascript')
    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("{{ url('') }}/boot/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>
    <script>
        $(document).on("change","#address_user", function(event) {
          var id_matp = $(this).val();
          $.get("{{ url('') }}/boot/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#address_user1").html(data);
          });
        });
    </script>

    <script>
        if ("{{request()->register_form}}" == "register_form") {
            $("#registerModal").modal('show');
        };
    </script>

@endsection
