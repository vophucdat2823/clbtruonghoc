@extends('admin_2.layout.master')

@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách clb trường học</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li>
                    <a href="{{ route('bustle.index') }}">Danh sách clb trường học</a>
                </li>
                <li class="active">
                    <strong>Sửa clb trường học</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('bustle.update',['id'=>$bustle->id]) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin clb trường học</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="{{ route('bustle.index') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên clb trường học(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên clb trường học..." required value="{{$bustle->name}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                @if ($bustle->slug)
                                                    @php
                                                        $slug = explode('/', $bustle->slug);
                                                        $slug_1 = explode('.', $slug[1]);
                                                    @endphp
                                                @endif

                                                <div class="input-group">
                                                    <span class="input-group-addon">{{$slug[0]}}/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn..." required value="{{ $slug_1[0] }}">
                                                    <span class="input-group-addon">.{{$slug_1[1]}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Category:</label>
                                        <div class="col-sm-10">
                                            @if ($bustle->cate_bustle_id != "null")
                                                @php
                                                    $cate_bustle_id = json_decode($bustle->cate_bustle_id);
                                                @endphp

                                                <select name="cate_bustle_id[]" data-placeholder="Choose a Country..." required class="chosen-select" multiple tabindex="4">
                                                    {{showProgram($cateBustle,$cate_bustle_id,0, $char ='')}}
                                                </select>
                                            @else
                                                <select name="cate_bustle_id[]" data-placeholder="Choose a Country..." required class="chosen-select" multiple tabindex="4">
                                                    {{showCateEvent($cateBustle,0,'',$bustle->cate_bustle_id)}}
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>
                                        <div class="col-sm-10">
                                            <select name="address" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                <option value="">Thành Phố</option>
                                                @foreach ($city as $ci)
                                                    <option value="{{$ci->name}}" {{$bustle->address == $ci->name ? 'selected' : ''}}>{{$ci->name}}</option>
                                                @endforeach
                                           </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            @if ($bustle->image)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$bustle->image}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$bustle->image}}" name=""><input type="file" name="image" value="{{$bustle->image}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @elseif(old('image'))
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{old('image')}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{old('image')}}" name=""><input type="file" name="image" value="{{old('image')}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description">{!! $bustle->description !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>


<?php 
    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {

        foreach ($pgramall as $key => $item) {
            if ($item->parents == $parent)
            {
                echo '<option value="'.$item->id.'"';
                if ($program_test) {
                    foreach ($program_test as $value) {
                        if($item->id == $value){
                            echo 'selected="selected"';
                        }
                    }
                }
                if($parent==0){
                    echo 'style="color:red"';
                }
                echo '>';
                if($item->parents == $program_test){
                    if ($item->parents == 0) {
                        echo $char . $item->name;
                    }
                    if ($item->parents != 0) {
                        echo $char . $item->name.' (danh mục con)';
                    }
                }
                if($item->parents != $program_test){
                    echo $char . $item->name;
                }
                echo '</option>';
                if ($item->parents != $program_test){
                    showProgram($pgramall,$program_test, $item->id, $char.'---| ');
                }

            }
            
        }
    }


?>


<?php

function showCateEvent($cateBustle, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateBustle as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateBustle[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateEvent($cateBustle, $item->id, $char . '---| ', $select);
        }
    }
}

?>

@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>
    
    <script type="text/javascript">
        $('.city-country').select2({
            theme: 'bootstrap',
            placeholder: 'cityCountry',
            ajax: {
                url: '{{ url('') }}/admin/profiles/getSuggestCities',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term,
                    };
                },
                processResults: function (data, params) {
                    // console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.place_id,
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
        });
    </script>
@endsection