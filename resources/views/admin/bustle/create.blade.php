@extends('admin.layout.master')

@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách clb trường học</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li>
                    <a href="{{ route('bustle.index') }}">Danh sách clb trường học</a>
                </li>
                <li class="active">
                    <strong>Thêm clb trường học</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('bustle.store') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin clb trường học</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="{{ route('bustle.index') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên clb trường học(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên clb trường học..." required value="{{old('name')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">clb-tuong-hoc/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn..." required value="{{ old('slug') }}">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục(*):</label>
                                        <div class="col-sm-10">
                                            <select name="cate_bustle_id[]" data-placeholder="Choose a Country..." class="chosen-select" required multiple tabindex="4">
                                                {{showCateBustle($cateBustle,0,'',old('cate_bustle_id'))}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="ThanhPho">Tỉnh/Thành Phố:</label>
                                        <div class="col-sm-10">
                                            <select name="address" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                <option value="">Thành Phố</option>
                                                @foreach ($city as $ci)
                                                <option value="{{$ci->name}}">{{$ci->name}}</option>
                                                @endforeach
                                           </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description">{!! old('description') !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php

function showCateBustle($cateBustle, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateBustle as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateBustle[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateBustle($cateBustle, $item->id, $char . '---| ', $select);
        }
    }
}

?>

@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>
    <script type="text/javascript">
        $('.city-country').select2({
            theme: 'bootstrap',
            placeholder: 'cityCountry',
            ajax: {
                url: '{{ url('') }}/admin/profiles/getSuggestCities',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term,
                    };
                },
                processResults: function (data, params) {
                    // console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.place_id,
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
        });
    </script>
@endsection