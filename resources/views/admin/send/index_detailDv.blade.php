@extends('admin.layout.master')

@section('title','Dịch Vụ | Danh sách')
<style>
    .cate_service_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách dịch vụ</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Danh sách dịch vụ</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="product_name">Tên khách hàng</label>
                            <input type="text" value="{{$send->name}}" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="price">Email</label>
                            <input type="text" value="{{$send->email}}" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="quantity">Số điện thoại</label>
                            <input type="text" value="{{$send->phone}}" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group load_status_{{$send->id}}">
                            <label class="control-label" for="status">Trạng thái</label>
                            <div class="clearfix"></div>
                            {{csrf_field()}}
                            @if ($send->status == 0)
                             <button type="button" class="btn btn-danger colum_status"  data-id="{{$send->id}}" data-stt="1">Đang chờ</button>
                            @else
                            <button type="button" class="btn btn-primary colum_status" data-id="{{$send->id}}" data-stt="0">Đã xử lý</button>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Thời gian</label>
                            <input type="text" id="price" name="price" value="{{$send->time}}" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Ngày</label>
                            <input type="text" id="price" name="price" value="{{$send->date}}" disabled class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Số lượng</label>
                            <input type="text" id="price" name="price" value="{{$send->quantity}}" disabled class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                        <h3>
                            {{$send->getService->name}}
                        </h3>
                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">Câu hỏi</th>
                                        <th data-hide="phone">Câu trả lời</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($question as $ques)
                                        <tr>
                                            <td>
                                               {{$ques->getQuestion['name']}}
                                            </td>
                                            <td>
                                               {{$ques['rep_question']}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content m-b-sm border-bottom">
                <legend>Đối tác cung cấp "{{$send->getService->name}}"</legend>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Chất lượng</label>
                            <select name="status" id="status" class="form-control">
                                @foreach ($send->getService->getTypeService('quality') as $quality)
                                    <option value="{{$quality->id}}">{{$quality->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Số lượng</label>
                            <select name="status" id="status" class="form-control">
                                @foreach ($send->getService->getTypeService('qty') as $qty)
                                    <option value="{{$qty->id}}">{{$qty->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Địa điểm</label>
                            <select name="status" id="status" class="form-control">
                                @foreach ($send->getService->getTypeService('address') as $address)
                                    <option value="{{$address->id}}">{{$address->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Giá thanh</label>
                            <select name="status" id="status" class="form-control">
                                @foreach ($send->getService->getTypeService('price') as $price)
                                    <option value="{{$price->id}}">{{$price->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                        <tr>
                                            <th data-toggle="true">Tên nha cung cấp</th>
                                            <th data-hide="phone">Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($send->getService->getServicePartner)
                                            @foreach ($send->getService->getServicePartner as $partner)
                                                <tr>
                                                    <td>
                                                       {{$partner->getUser->name}}
                                                    </td>
                                                    <td>
                                                       {{$partner->getUser->email}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
<!-- FooTable -->
<script src="{{ url('public/admin') }}/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

@endsection
@section('javascript')
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"{{ route('send.update_status') }}",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)

        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
@endsection
