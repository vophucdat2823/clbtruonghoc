@extends('admin.layout.master')

@section('title','Dịch Vụ | Danh sách')
<style>
    .cate_service_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách dịch vụ</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Danh sách dịch vụ</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            {{-- <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="product_name">Product Name</label>
                            <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="price">Price</label>
                            <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="quantity">Quantity</label>
                            <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" selected>Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div> --}}

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">


                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên khách hàng</th>
                                    <th data-toggle="true">Loại dịch vụ</th>
                                    <th data-hide="phone">Email</th>
                                    <th data-hide="phone">Số điện thoại</th>
                                    <th data-hide="phone">Trạng Thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($send as $store)
                                        <tr >
                                            <td>
                                               {{$store['name']}}
                                            </td>
                                            <td>
                                               {{$store->getService->name}}
                                            </td>
                                            <td>
                                               {{$store['email']}}
                                            </td>
                                            <td>
                                               {{$store['phone']}}
                                            </td>
                                            <td class="load_status_{{$store['id']}}">
                                                {{csrf_field()}}
                                                @if ($store['status'] == 0)
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="{{$store['id']}}" data-stt="1">Đang chờ</button>
                                                @else
                                                <button type="button" class="btn btn-primary colum_status" data-id="{{$store['id']}}" data-stt="0">Đã xử lý</button>
                                                @endif
                                            </td>
                                            <td class="text-right">
                                                <form action="{{ route('send.destroy',['id'=>$store['id']]) }}" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        {{ csrf_field() }}
                                                        <a href="{{ route('admin.send.index_detailDv',['id'=>$store['id']]) }}" class="btn-white btn btn-xs" >Chi tiết</a>
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
@section('scripts')
<!-- FooTable -->
<script src="{{ url('public/admin') }}/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

@endsection
@section('javascript')
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"{{ route('send.update_status') }}",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)

        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
@endsection
