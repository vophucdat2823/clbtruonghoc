@extends('admin.layout.master')
@section('style.css')
    {{----}}
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Chỉnh sửa danh mục</h2>
        <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
        </li>
        <li class="active">
            <strong>Chỉnh sửa danh mục</strong>
        </li>
    </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">

    <div class="row">
        <div class="col-lg-12">
             @if (session('success'))
                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('error') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h4>Sửa danh mục</h4>
                            <div class="ibox-tools">                                          
                            </div>
                        </div>
                        <div class="ibox-content" style="padding: 50px">
                            <div class="row">
                                <form class="form-horizontal" action="{{route('cate-bustle.update', $cateBustle->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                    <div class="col-lg-12">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label>Tên danh mục(*):</label> 
                                            <input type="text" id="name" class="form-control" name="name" placeholder="Tên tab danh mục tiếng việt" value="{{ $cateBustle->name }}" style="margin-bottom: 5px">
                                            @if($errors->has('name'))
                                                <span class="text-center text-danger" role="alert">
                                                    {{$errors->first('name')}}
                                                </span>
                                            @endif
                                            <br><span>Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Chuỗi cho đường dẫn tĩnh</label> 
                                            
                                            @if ($cateBustle->slug)
                                                @php
                                                    $slug = explode('/', $cateBustle->slug);
                                                    $slug_1 = explode('.', $slug[1]);
                                                @endphp
                                            @endif

                                            <div class="input-group">
                                                <span class="input-group-addon">{{$slug[0]}}/</span>
                                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" value="{{ $slug_1[0] }}">
                                                <span class="input-group-addon">.{{$slug_1[1]}}</span>
                                            </div>
                                             @if($errors->has('slug'))
                                                <span class="text-center text-danger" role="alert">
                                                    {{$errors->first('slug')}}
                                                </span>
                                            @endif
                                            <span>Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                        </div>
                                        
            
                                        <div class="form-group"><label>Danh mục</label>
                                            
                                            <select name="parents" class="form-control">
                                                @php
                                                    $select = 0;
                                                    if ($cateBustle->parents > 0){
                                                        $select = \App\CateBustle::where('id',$cateBustle->parents)->value('id');
                                                    }
                                                @endphp
                                                <option value="0" {{$cateBustle->parents == 0 ? 'selected' : ''}}>-- ROOT -- </option>
                                                {{showCategoryies($cateBustleAll,$select,0,$char="")}}
                                            </select>
                                            <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn.</span>
                                        </div>
                                    </div>
                                    <div>
                                        <a class="btn btn-sm btn-danger m-t-n-xs" href="{{ route('cate-bustle.index') }}"><strong>Hủy bỏ</strong></a>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Sửa danh mục</strong></button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 

    function showCategoryies($cateBustleAll,$select, $parent = 0, $char ='')
    {


      foreach ($cateBustleAll as $key => $item) {


          if ($item->parents == $parent)
          {
            echo '<option value="'.$item->id.'"';
            if($item->id == $select){
                echo 'selected="selected"';
            }
            if($item->parents == $select){
                echo 'disabled="disabled"';
            }
            if($parent==0){
                echo 'style="color:red"';
            }
            echo '>';
            if($item->parents == $select){
                if ($item->parents == 0) {
                    echo $char . $item->name;
                }
                if ($item->parents != 0) {
                    echo $char . $item->name.' (danh mục con)';
                }
            }
            if($item->parents != $select){
                echo $char . $item->name;
            }
            echo '</option>';
            if ($item->parents != $select){
                showCategoryies($cateBustleAll,$select, $item->id, $char.'---| ');
            }

        }
        
    }
}


?>
@endsection


@section('javascript')
@endsection