@extends('admin.layout.master')

@section('style.css')
@endsection
<style>
    .cate_event_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách tin tức</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách tin tức</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">Tên tin tức</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($news as $ev)
                                        <tr >
                                            <td>
                                               {{$ev->TieuDe}}
                                            </td>                                            
                                            <td class="text-right">
                                                <form action="" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                        <a href="{{ route('event.edit',['id'=>$ev->id]) }}" class="btn-white btn btn-xs" >Edit</a>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
@section('scripts')
<!-- FooTable -->
<script src="{{ url('public/admin') }}/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

@endsection
@section('javascript')
    <script>
        var _token = $('input[name="_token"]').val();
        $(document).on('click', '.colum_status', function(){
            var status = $(this).data("stt");
            var id = $(this).data("id");
             
            if(id != '')
            {
                $.ajax({
                    url:"{{ route('event.update_status') }}",
                    method:"POST",
                    data:{status:status, id:id, _token:_token},
                    success:function(data)
                    {
                        $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
                    }
                })
            }
            else
            {
                $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
            }
        });
        
    </script>
@endsection
