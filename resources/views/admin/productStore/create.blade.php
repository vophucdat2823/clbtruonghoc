@extends('admin.layout.master')
@section('title','Sản Phẩm | Thêm mới')
@section('style')
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thêm mới sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('partner.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Thêm mới sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('postProduct') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin sản phẩm</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Giảm giá</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"> Hình ảnh</a></li>
                        <li class=""><button type="submit" id="addProduct" class="btn btn-success">Thêm sản phẩm </button></li>
                        <a href="{{ route('listProduct') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên sản phẩm(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="{{old('name')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">{{url('chi-tiet')}}/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Slug..." required value="{{ old('slug') }}">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Thương hiệu(*):</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="trademark" id="trademark" class="form-control" placeholder="Thương hiệu..." required value="{{old('trademark')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-1 control-label">SKU:</label>
                                        <div class="col-sm-5">
                                            <div>
                                                <div>
                                                    <input type="text" name="sku" id="sku" class="form-control" placeholder="SKU..."   value="{{old('sku')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục(*):</label>
                                        <div class="col-sm-4">

                                            <select name="category_id[]" data-placeholder="Lựa chọn danh mục..." class="chosen-select"  multiple tabindex="4">
                                                {{showCateStore($cateStore,0,'',old('category_id'))}}
                                            </select>
                                        </div>
                                        <label class="col-sm-1 control-label">Giá(*):</label>
                                        <div class="col-sm-5">
                                            <div>
                                                <div>
                                                    <input type="number" name="price" class="form-control" placeholder="$160.00" required value="{{old('price')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả ngắn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="short_description" id="short_description" class="form-control" placeholder="Mô tả ngắn..." required value="{{old('short_description')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description">{!! old('description') !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Giá sau khi giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price_sale" class="form-control" placeholder="$160.00"  value="{{old('price_sale')}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Phần trăm được giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <input type="number" name="sale" class="form-control" placeholder="50"  value="{{old('sale')}}">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- <label for="document">Documents</label>
                                    <div class="needsclick dropzone" id="document-dropzone">

                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Image 1:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image1"  >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Image 2:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image2" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Image 3:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image3" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Image 4:</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image4" >
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <?php

function showCateStore($cateStore, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateStore as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateStore[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateStore($cateStore, $item->id, $char . '---| ', $select);
        }
    }
}

?>
@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>

    // <script type="text/javascript">
    //     $("#form-create-product").validate({
    //         rules: {
    //             name: "required",
    //             slug: "required",
    //             price: {
    //                 "required": true,
    //                 "number": true
    //             },
    //             price_sale: {
    //                 "number": true
    //             },
    //             sale: {
    //                 "number": true
    //             },
    //             image: "required",
    //             category_id: "required",
    //             trademark: "required",
    //             short_description: "required",

    //         },
    //         messages: {
    //             "name": "Tên sản phẩm không được để trống",
    //             "slug": "Đường đẫn tĩnh không được để trống",
    //             "image": "Vui vòng thêm ảnh",
    //             "trademark": "Thương hiệu không được để trống",
    //             "short_description": "Mô tả ngắn không được để trống",
    //             "price": {
    //                 "required": "Vui lòng nhập giá sản phẩm",
    //                 "number": 'Giá sản phẩm là số'
    //             },
    //             "price_sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "category_id": "Nghề nghiệp không được để trống",

    //         },
    //         errorPlacement: function (error, element) {
    //           error.appendTo(element.parent().parent().parent());
    //         },
    //         submitHandler: function(form) {
    //             $(form).submit();
    //         }
    //     });

    //     // $( document ).ready(function() {
    //     //     Dropzone.autoDiscover = false;
         
    //     //     var myDropzone = new Dropzone(".dropzone" ,{ 
    //     //         url: "{{ route('postProduct') }}",
    //     //         // uploadMultiple: true,
    //     //         // parallelUploads: 100,
    //     //         // maxFiles: 100,
    //     //         // autoProcessQueue: false  
    //     //         // addRemoveLinks: true,
    //     //         autoProcessQueue: false, // this is important as you dont want form to be submitted unless you have clicked the submit button
    //     //         // autoDiscover: false,
    //     //         // paramName: 'pic', // this is optional Like this one will get accessed in php by writing $_FILE['pic'] // if you dont specify it then bydefault it taked 'file' as paramName eg: $_FILE['file'] 
    //     //         // previewsContainer: '#dropzonePreview', // we specify on which div id we must show the files
    //     //         // clickable: false, // this tells that the dropzone will not be clickable . we have to do it because v dont want the whole form to be clickable 
    //     //         accept: function(file, done) {
    //     //             console.log("uploaded");
    //     //             done();
    //     //         },
    //     //         error: function(file, msg){
    //     //             alert(msg);
    //     //         },
    //     //         init: function() {
    //     //             var myDropzone = this;
    //     //             //now we will submit the form when the button is clicked
    //     //             $("#addProduct").on('click',function(e) {
    //     //                 e.preventDefault();
    //     //                 myDropzone.processQueue(); // this will submit your form to the specified action path
    //     //                 // after this, your whole form will get submitted with all the inputs + your files and the php code will remain as usual 
    //     //             //REMEMBER you DON'T have to call ajax or anything by yourself, dropzone will take care of that
    //     //                 });      
    //     //         } // init end
    //     //     });
                
    //     //             // $('#addProduct').click(function(){
    //     //             //     myDropzone.processQueue();
    //     //             // });
    //     // });
    // </script> 

   
@endsection