@extends('admin.layout.master')

@section('title','Sản Phẩm | Chỉnh sửa')

@section('style')

    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Chỉnh sửa sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('partner.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    @php
        $productStore=$productStore::find($_GET['id']);
        $productImgStore=$ProductImgStore->where('product_id','=',$productStore->id)->orderBy('id','desc')->get()->toarray();
    @endphp
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('updateProduct',['id'=>$productStore->id]) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin sản phẩm</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Giảm giá</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"> Hình ảnh</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="{{ route('listProduct') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Name(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="{{$productStore->name}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug:</label>
                                        <div class="col-sm-10">
                                            <input type="hidden" id='id' name="id" value="{{$productStore->id}}" />
                                            <div>
                                                @if ($productStore->slug)
                                                    @php
                                                        $slug_1 = explode('.', $productStore->slug);
                                                        $slug = explode('cua-hang-truc-tuyen-', $productStore->slug);
                                                       
                                                    @endphp
                                                @endif

                                                <div class="input-group">
                                                    <span class="input-group-addon">{{url('chi-tiet')}}/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" required value="{{ array_pop($slug) }}">
                                                    <span class="input-group-addon">.{{$slug_1[1]}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Thương hiệu(*):</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="trademark" id="trademark" class="form-control" placeholder="Thương hiệu..." required value="{{$productStore->trademark}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">SKU:</label>
                                        <div class="col-sm-4">
                                            <div>
                                                <div>
                                                    <input type="text" name="sku" id="sku" class="form-control" placeholder="SKU..."   value="{{$productStore->sku}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Price:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price" class="form-control" placeholder="$160.00" required value="{{$productStore->price}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục:</label>
                                        <div class="col-sm-10">
                                            @if ($productStore->category_id != "null")
                                                @php
                                                    $category_id = json_decode($productStore->category_id);
                                                @endphp

                                                <select name="category_id[]" data-placeholder="Lựa chọn danh mục..."  class="chosen-select" multiple tabindex="4">
                                                    {{showProgram($cateStore,$category_id,0, $char ='')}}
                                                </select>
                                            @else
                                                <select name="category_id[]" data-placeholder="Lựa chọn danh mục..."  class="chosen-select" multiple tabindex="4">
                                                    {{showCateStore($cateStore,0,'',$productStore->category_id)}}
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            @if ($productStore->image)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$productStore->image}}</span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$productStore->image}}" name=""><input type="file" name="image" value="{{$productStore->image}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            <!-- @elseif(old('image'))
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{old('image')}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{old('image')}}" name=""><input type="file" name="image" value="{{old('image')}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div> -->
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            @endif

                                        </div>
                                        <!-- <div class="col-sm-10 col-xs-10">
                                            @if ($productStore->image)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$productStore->image}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$productStore->image}}" name="check_image">
                                                        <input type="file" name="image" id="image" value="{{$productStore->image}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image" id="image">
                                                    </span>
                                                </div>
                                            @endif

                                        </div> -->
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả ngắn(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="short_description" id="short_description" class="form-control" placeholder="Mô tả ngắn..." required value="{{$productStore->short_description}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description">{!! $productStore->description !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Giá sau khi giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="number" name="price_sale" class="form-control" placeholder="$160.00"  value="{{$productStore->price_sale}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Phần trăm được giảm:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <input type="number" name="sale" class="form-control" placeholder="50"  value="{{$productStore->sale}}">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                            <!-- <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($productImgStore as $value)
                                            <tr>
                                                <td>
                                                    <img width='100' height='100' src="{{ url('public/upload/images') }}/{{$value['images']}}">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="{{ url('public/upload/images') }}/{{$value['images']}}">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                            
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> -->


                                @for ($i = 0; $i < 4; $i++)
                                    @if (isset($productImgStore[$i]))
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">IMG</label>
                                            <div class="col-sm-10 col-xs-10">
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$productImgStore[$i]['images']}}</span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image{{$productImgStore[$i]['id']}}" value="{{$productImgStore[$i]['images']}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">IMG</label>
                                            <div class="col-sm-10 col-xs-10">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <!-- <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="imagei{{$i+1}}">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                  
                                @endfor
            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <?php 
    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {

        foreach ($pgramall as $key => $item) {
            if ($item->parents == $parent)
            {
                echo '<option value="'.$item->id.'"';
                if ($program_test) {
                    foreach ($program_test as $value) {
                        if($item->id == $value){
                            echo 'selected="selected"';
                        }
                    }
                }
                if($parent==0){
                    echo 'style="color:red"';
                }
                echo '>';
                if($item->parents == $program_test){
                    if ($item->parents == 0) {
                        echo $char . $item->name;
                    }
                    if ($item->parents != 0) {
                        echo $char . $item->name.' (danh mục con)';
                    }
                }
                if($item->parents != $program_test){
                    echo $char . $item->name;
                }
                echo '</option>';
                if ($item->parents != $program_test){
                    showProgram($pgramall,$program_test, $item->id, $char.'---| ');
                }

            }
            
        }
    }


?>


<?php

function showCateStore($cateStore, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateStore as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateStore[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateStore($cateStore, $item->id, $char . '---| ', $select);
        }
    }
}

?>

@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>
    


    // <script type="text/javascript">
        
        
    //     $("#form-create-product").validate({
    //         rules: {
    //             name: "required",
    //             slug: "required",
    //             price: {
    //                 "required": true,
    //                 "number": true
    //             },
    //             price_sale: {
    //                 "number": true
    //             },
    //             sale: {
    //                 "number": true
    //             },
    //             // image: "required",
    //             category_id: "required",
    //             trademark: "required",
    //             short_description: "required",

    //         },
    //         messages: {
    //             "name": "Tên sản phẩm không được để trống",
    //             "slug": "Đường đẫn tĩnh không được để trống",
    //             //  "image": "Vui vòng thêm ảnh",
    //             "trademark": "Thương hiệu không được để trống",
    //             "short_description": "Mô tả ngắn không được để trống",
    //             "price": {
    //                 "required": "Vui lòng nhập giá sản phẩm",
    //                 "number": 'Giá sản phẩm là số'
    //             },
    //             "price_sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "sale": {
    //                 "number": 'Vui lòng nhập số'
    //             },
    //             "category_id": "Nghề nghiệp không được để trống",

    //         },
    //         errorPlacement: function (error, element) {
    //           error.appendTo(element.parent().parent().parent());
    //         },
    //         submitHandler: function(form) {
    //             $(form).submit();
    //         }
    //     });
    // </script>

@endsection