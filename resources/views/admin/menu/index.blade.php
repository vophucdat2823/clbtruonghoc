@extends('admin.layout.master')

@section('style')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="{{url('public/vendor/harimayco-menu/style.css')}}" rel="stylesheet">
@endsection

@section('content')
    {!! Menu::render() !!}
@endsection
@section('javascript')
<script>
    var menus = {
        "oneThemeLocationNoMenus" : "",
        "moveUp" : "Move up",
        "moveDown" : "Mover down",
        "moveToTop" : "Move top",
        "moveUnder" : "Move under of %s",
        "moveOutFrom" : "Out from under  %s",
        "under" : "Under %s",
        "outFrom" : "Out from %s",
        "menuFocus" : "%1$s. Element menu %2$d of %3$d.",
        "subMenuFocus" : "%1$s. Menu of subelement %2$d of %3$s."
    };
    var arraydata = [];     
    var addcustommenur= '{{ route("haddcustommenu") }}';
    var updateitemr= '{{ route("hupdateitem")}}';
    var generatemenucontrolr= '{{ route("hgeneratemenucontrol") }}';
    var deleteitemmenur= '{{ route("hdeleteitemmenu") }}';
    var deletemenugr= '{{ route("hdeletemenug") }}';
    var createnewmenur= '{{ route("hcreatenewmenu") }}';
    var csrftoken="{{ csrf_token() }}";
    var menuwr = "{{ url()->current() }}";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrftoken
        }
    });
</script>
    {{-- {!! Menu::scripts() !!} --}}
    <script type="text/javascript" src="{{url('public/vendor/harimayco-menu/scripts.js')}}"></script>
    <script type="text/javascript" src="{{url('public/vendor/harimayco-menu/scripts2.js')}}"></script>
    <script type="text/javascript" src="{{url('public/vendor/harimayco-menu/menu.js')}}"></script>
@endsection