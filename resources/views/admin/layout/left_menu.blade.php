<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" src="{{ url('public/admin') }}/img/logo.png" />
                    </span>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear">
                    <span class="block m-t-xs"> <strong class="font-bold">CMS Webify V1.10</strong>
                    </span>
                      <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Về Webify</a></li>
                        <li><a href="#">Liên hệ</a></li>
                        <li><a href="#">Hướng dẫn sử dụng</a></li>
                        
                      </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{Request::is('admin/dashboard') ? 'active' : ''}}">
                <a href="{{route('admin.dashboard')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Trang chủ</span>  </a>
            </li>
            <li class="{{Request::is('admin/list') ? 'active' : ''}}">
                <a href="{{route('admin.menu.list')}}"><i class="fa fa-cubes"></i> <span class="nav-label">Menu</span>  </a>
            </li>
            {{-- <li class="{{Request::is('admin/type-course') ? 'active' : ''}}">
                <a href="{{route('type-course.index')}}"><i class="fa fa-cubes"></i> <span class="nav-label">Loại khóa học</span>  </a>
            </li> --}}
            <li class="{{Request::is('admin/cate-course') ||Request::is('admin/course') || Request::is('admin/yeu-cau-khach-hang/khoa-hoc') || Request::is('admin/course/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Quản lý khóa học</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="{{Request::is('admin/yeu-cau-khach-hang/khoa-hoc') ? 'active' : ''}}"><a href="{{route('admin.send.indexKh')}}">Yêu cầu khách hàng</a></li>
                    <li class="{{Request::is('admin/cate-course') ? 'active' : ''}}"><a href="{{ route('cate-course.index') }}">Danh mục</a></li>

                    <li class="{{Request::is('admin/course') || Request::is('admin/course/create')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Khóa học</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('admin/course') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('admin/course') ? 'active' : ''}}"><a href="{{ route('course.index') }}"> Danh Sách KH</a></li>
                            <li class="{{Request::is('admin/course/create') ? 'active' : ''}}"><a href="{{ route('course.create') }}">Thêm mới KH</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <!-- TRAN MINH TAN -->
            <li class="{{Request::is('partner/cate-store') ||Request::is('partner/product-store') || Request::is('partner/product-store/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">CH Trực tuyến</span><span class="fa arrow"></span></a>

                <ul class="nav nav-second-level collapse" style="">

                    <li class="{{Request::is('partner/cate-store') ? 'active' : ''}}"><a href="{{ route('cate-store.index') }}">Danh mục</a></li>
                    <li class="{{Request::is('partner/product-store') || Request::is('partner/product-store/create')? 'active' : ''}}">

                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Sản phẩm</span><span class="fa arrow"></span></a>

                        <ul class="nav nav-third-level collapse {{Request::is('partner/product-store') ? 'in' : ''}}" style="">

                            <li class="{{Request::is('partner/product-store') ? 'active' : ''}}"><a href="{{ route('listProduct') }}"> Danh sách SP</a></li>

                            <li class="{{Request::is('partner/product-store/create') ? 'active' : ''}}"><a href="{{ route('addProduct') }}">Thêm mới SP</a></li>

                        </ul>
                    </li>

                </ul>
            </li>
            <!-- ==============================   -->
            <li class="{{Request::is('admin/cate-service') ||Request::is('admin/service') || Request::is('admin/yeu-cau-khach-hang/dich-vu') || Request::is('admin/service/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Quản lý dịch vụ</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="{{Request::is('admin/yeu-cau-khach-hang/dich-vu') ? 'active' : ''}}"><a href="{{route('admin.send.indexDv')}}">Yêu cầu khách hàng</a></li>
                    <li class="{{Request::is('admin/cate-service') ? 'active' : ''}}"><a href="{{ route('cate-service.index') }}">Danh mục dịch vụ</a></li>

                    <li class="{{Request::is('admin/service') || Request::is('admin/service/create')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Dịch vụ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('admin/service') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('admin/service') ? 'active' : ''}}"><a href="{{ route('service.index') }}"> Danh Sách Dịch Vụ</a></li>
                            <li class="{{Request::is('admin/service/create') ? 'active' : ''}}"><a href="{{ route('service.create') }}">Thêm mới Dịch Vụ</a></li>
                        </ul>
                    </li>
                </ul>
            </li>


            <li class="{{Request::is('admin/cate-event') ||Request::is('admin/event') || Request::is('admin/event/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Sự Kiện</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="{{Request::is('admin/cate-event') ? 'active' : ''}}"><a href="{{ route('cate-event.index') }}">Danh mục SK</a></li>

                    <li class="{{Request::is('admin/event') || Request::is('admin/event/create')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">QL Sự Kiện</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('admin/event') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('admin/event') ? 'active' : ''}}"><a href="{{ route('event.index') }}"> Danh sách SK</a></li>
                            <li class="{{Request::is('admin/event/create') ? 'active' : ''}}"><a href="{{ route('event.create') }}">Thêm mới SK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Tin tức</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-third-level collapse " style="">
                    <li class="{{Request::is('admin/news') ? 'active' : ''}}"><a href="{{ route('news.index') }}"> Danh sách tin tức</a></li>
                    {{-- <li class="{{Request::is('admin/news/create') ? 'active' : ''}}"><a href="{{ route('event.create') }}">Thêm mới SK</a></li> --}}
                </ul>
            </li>            

            <li class="{{Request::is('admin/cate-bustle') ||Request::is('admin/bustle') || Request::is('admin/bustle/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-university"></i> <span class="nav-label">CLB_Trường Học</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="{{Request::is('admin/cate-bustle') ? 'active' : ''}}"><a href="{{ route('cate-bustle.index') }}">Danh mục CLB</a></li>

                    <li class="{{Request::is('admin/bustle') || Request::is('admin/bustle/create')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">QL Câu Lạc Bộ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('admin/bustle') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('admin/bustle') ? 'active' : ''}}"><a href="{{ route('bustle.index') }}"> Danh sách CLB</a></li>
                            <li class="{{Request::is('admin/bustle/create') ? 'active' : ''}}"><a href="{{ route('bustle.create') }}">Thêm mới SK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            


            <li class="{{Request::is('admin/box-link') || Request::is('admin/banner') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-sliders"></i>  <span class="nav-label">QL Giao diện</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    {{-- <li class="{{Request::is('admin/cate-event') ? 'active' : ''}}"><a href="{{ route('cate-event.index') }}">Danh mục</a></li> --}}

                    <li class="{{Request::is('admin/box-link') || Request::is('admin/banner') ? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Trang chủ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('admin/event') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('admin/box-link') ? 'active' : ''}}"><a href="{{ route('box-link.index') }}"> Box Link</a></li>
                            <li class="{{Request::is('admin/banner') ? 'active' : ''}}"><a href="{{ route('banner.index') }}"> Banner</a></li>
                            {{-- <li class="{{Request::is('admin/event/create') ? 'active' : ''}}"><a href="{{ route('event.create') }}">Thêm mới SP</a></li> --}}
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="{{Request::is('admin/account') || Request::is('admin/account/create') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">QL Tài Khoản</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{Request::is('admin/account') || Request::is('admin/account/create') ? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">TK DV_Khóa Học</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li ><a href="{{ route('account.index') }}">-- Danh sách TK</a></li>
                            <li class=""><a href="{{ route('account.create') }}">-- Thêm mới TK</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="{{Request::is('admin/email-newsletter') ? 'active' : ''}}">
                <a href="{{route('email_newsletter.index')}}"><i class="fa fa-envelope"></i> <span class="nav-label">Email nhận bản tin</span>  </a>
            </li>
            
            {{-- 
            <li class="{{Request::is('admin/program/list') || Request::is('admin/pgram_post/list') || Request::is('admin/pgram_post/create')? 'active' : ''}}">
                <a href="#"><i class="fa fa-sliders"></i> <span class="nav-label">QL Programs</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{Request::is('admin/pgram_post/list')? 'active' : ''}}"><a href="{{route('admin.pgram_post.list')}}">Bài viết program</a></li>


                    <li class="{{Request::is('admin/pgram_post/create') ? 'active' : ''}}"><a href="{{route('admin.pgram_post.create')}}">Thêm bài viết</a></li>


                    <li class="{{Request::is('admin/program/list') ? 'active' : ''}}"><a href="{{route('admin.program.list')}}">Danh mục program</a></li>
                           
                </ul>
            </li>
            <li class="{{Request::is('admin/chooses/list') ? 'active' : ''}}">
                <a href="{{route('admin.chooses.list')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Slider Teachers</span>  </a>
            </li>
            
            <li class="{{Request::is('admin/menu/list') || Request::is('admin/custom-display/list/home') || Request::is('admin/network/list') || Request::is('admin/custom-display/list/student-services') || Request::is('admin/custom-display/list/about-us')? 'active' : ''}}">
                <a href="#"><i class="fa fa-sliders"></i> <span class="nav-label">Giao diện</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{Request::is('admin/menu/list') ? 'active' : ''}}"><a href="{{route('admin.menu.list')}}">Menu</a></li>
                    <li class="{{Request::is('admin/custom-display/list/home') ? 'active' : ''}}"><a href="{{route('admin.custom_display.list',['name'=>'home'])}}">Home</a></li>

                    <li class="{{Request::is('admin/custom-display/list/student-services') ? 'active' : ''}}"><a href="{{route('admin.custom_display.list',['name'=>'student-services'])}}">Student Service</a></li>

                    <li class="{{Request::is('admin/custom-display/list/about-us') ? 'active' : ''}}"><a href="{{route('admin.custom_display.list',['name'=>'about-us'])}}">About us</a></li>

                    <li class="{{Request::is('admin/network/list') ? 'active' : ''}}"><a href="{{route('admin.network.list')}}">VNU-ISOCIAL</a></li>
                           
                </ul>
            </li>
            <li class="{{Request::is('admin/caidat') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Cấu hình</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{Request::is('admin/caidat') ? 'active' : ''}}"><a href="{{ route('caidat') }}">Cài đặt hiển thị</a></li>
                </ul>
            </li> --}}
            
        </ul>

    </div>
    </nav>