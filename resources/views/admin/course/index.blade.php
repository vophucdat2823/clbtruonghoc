@extends('admin.layout.master')

@section('title','Khóa học | Danh sách')
<style>
    .cate_course_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách khóa học</h2>
                <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Danh sách khóa học</strong>
                </li>
            </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">


            {{-- <div class="ibox-content m-b-sm border-bottom">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="product_name">Product Name</label>
                            <input type="text" id="product_name" name="product_name" value="" placeholder="Product Name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="price">Price</label>
                            <input type="text" id="price" name="price" value="" placeholder="Price" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="quantity">Quantity</label>
                            <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" selected>Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div> --}}

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">


                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên khóa học</th>
                                    <th data-hide="phone">Danh mục</th>
                                    <th data-hide="all">Đường dẫn</th>
                                    <th data-hide="all">Đơn vị cung cấp</th>
                                    <th data-hide="all" >Chất lượng</th>
                                    <th data-hide="all" >Thời lượng</th>
                                    <th data-hide="all">Địa điểm</th>
                                    <th data-hide="all">Giá thành</th>
                                    <th data-hide="all">Số lượng học viên</th>
                                    <th data-hide="all">Học phí</th>
                                    <th data-hide="phone">Trạng Thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($course as $store)
                                    
                                        <tr >
                                            <td>
                                               {{$store['name']}}
                                            </td>
                                            <td>
                                               {{$store->getCateCourse['name']}}
                                            </td>
                                            <td>
                                               {{$store['slug']}}
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('unit') as $unit)
                                                    {{$unit->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('quality') as $quality)
                                                    {{$quality->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('time') as $time)
                                                    {{$time->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('address') as $address)
                                                    {{$address->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('price') as $price)
                                                    {{$price->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('qty') as $qty)
                                                    {{$qty->name}},
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($store->getTypeCourse('tuition') as $tuition)
                                                    {{$tuition->name}},
                                                @endforeach
                                            </td>
                                            <td class="load_status_{{$store['id']}}">
                                                {{csrf_field()}}
                                                @if ($store['status'] == 0)
                                                <button type="button" class="btn btn-primary colum_status" data-id="{{$store['id']}}" data-stt="1">Hoạt đông</button>
                                                @else
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="{{$store['id']}}" data-stt="0">Kết thúc</button>
                                                @endif
                                            </td>
                                            <td class="text-right">
                                                <form action="{{ route('course.destroy',['id'=>$store['id']]) }}" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        {{ csrf_field() }}
                                                        <a href="{{ route('question.create',['type'=>"course" ,'id'=>$store->id ]) }}" class="btn-white btn btn-xs" >Câu hỏi</a>
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                        <a href="{{ route('course.edit',['id'=>$store['id']]) }}" class="btn-white btn btn-xs" >Edit</a>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
@section('scripts')
<!-- FooTable -->
<script src="{{ url('public/admin') }}/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

@endsection
@section('javascript')
<script type="text/javascript">
   
</script>
<script>
    var _token = $('input[name="_token"]').val();
    $(document).on('click', '.colum_status', function(){
      var status = $(this).data("stt");
      var id = $(this).data("id");
     
      if(id != '')
      {
       $.ajax({
        url:"{{ route('course.update_status') }}",
        method:"POST",
        data:{status:status, id:id, _token:_token},
        success:function(data)

        {
            console.log(data);
             $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
        }
       })
      }
      else
      {
       $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
      }
     });
    
</script>
@endsection
