@extends('admin.layout.master')

@section('title','Danh mục | Danh sách')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách danh mục</h2>
            <ol class="breadcrumb">
            <li>
                <a href="{{ route('partner.dashboard') }}">Trang chủ</a>
            </li>
            <li class="active">
                <strong>Danh sách danh mục</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm danh mục</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="{{route('cate-store.store')}}" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <div class="form-group">
                                                <label>Tên danh mục(*):</label>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Tên tab danh mục tiếng việt" value="{{ old('name') }}" >
                                                @if($errors->has('name'))
                                                    <span class="text-center text-danger" role="alert">
                                                        {{$errors->first('name_vi')}}
                                                    </span>
                                                @endif
                                                <br><span style="margin-top: 5px">Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh</label>
                                                <div class="input-group">
                                                    <!-- <span class="input-group-addon">/cua-hang-truc-tuyen-</span> -->
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" value="">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                                 @if($errors->has('slug'))
                                                    <span class="text-center text-danger" role="alert">
                                                        {{$errors->first('slug')}}
                                                    </span><br>
                                                @endif
                                                <br><span style="margin-top: 5px">Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Danh mục cha</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th >STT</th>
                                                        <th >Tên danh mục</th>
                                                        <th>Đường dẫn tĩnh</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($cateStore0))
                                                        @foreach($cateStore0 as $key => $cate)
                                                            <tr>
                                                                <td>{{$key+1}}</td>
                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post">{{ $cate->name }}</span></a></td>

                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post-tacgia">{{ str_limit($cate->slug,30) }}</span></a></td>

                                                                <td>
                                                                    <a href="{{route('cate-store.edit', $cate->id)}}" type="button" class="btn btn-info">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                            data-target="#modal-delete-{{$cate->id}}">
                                                                        <i class="fa fa-fw fa-trash-o"></i>
                                                                    </button>
                                                                    <div class="modal modal-danger fade" id="modal-delete-{{$cate->id}}">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span></button>
                                                                                        @php
                                                                                            $delete = \App\ProductStore::where('category_id',$cate->id)->get();
                                                                                            $delete_cart = \App\CateStore::whereIn('parents',[$cate->id])->get();
                                                                                        @endphp
                                                                                        @if ($delete->count() == 0 && $delete_cart->count() == 0)
                                                                                            <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                        @else
                                                                                            <h4 class="modal-title">
	                                                                                           Danh mục hiện tại đang chứa dữ liệu không thể xóa !
                                                                                            </h4>
                                                                                        @endif
                                                                                </div>
                                                                                <div class="modal-body" style="background: white!important; text-align: center">
                                                                                    <form method="POST" action="{{route('cate-store.destroy', $cate->id)}}">
                                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                                        {{ csrf_field() }}

                                                                                        @if ($delete->count() == 0 && $delete_cart->count() == 0)
                                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                            <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                                <i class="fa fa-save"></i>
                                                                                            </button>
                                                                                        @else
                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                        @endif
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')

    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <!-- <script>

      CKEDITOR.replace( 'conten_cate_vi', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>
    <script>

      CKEDITOR.replace( 'conten_cate_en', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script> -->
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
@endsection
