@extends('admin.layout.master')
@section('title','Admin - Tạo tài khoản')
@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    {{-- <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet"> --}}
    <link href="{{ url('public/admin') }}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <style>

        .wizard > .content > .body  position: relative; }

    </style>


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tạo tài khoản đối tác</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li>
                    <a href="{{ route('event.index') }}">Danh sách tài khoản đối tác</a>
                </li>
                <li class="active">
                    <strong>Tạo tài khoản đối tác</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Tạo tài khoản đối tác</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <h2>
                            Tạo tài khoản đối tác
                        </h2>
                        <p>
                            Admin nhập thông tin đối tác tại đây
                        </p>
                        <form id="form" action="{{ route('account.store') }}" method="post" class="wizard-big">
                            {!! csrf_field() !!}
                            <h1>Tài khoản</h1>
                            <fieldset>
                                <h2>Thông tin tài khoản</h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label for="email">Tên đăng nhập *</label>
                                            <input id="email" name="email" value="taikhoan@gmail.com" type="enail" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Mật khẩu *</label>
                                            <input id="password" name="password" type="text" value="vifonic_secret" class="form-control required">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="text-center">
                                            <div style="margin-top: 20px">
                                                <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1>Đối tác</h1>
                            <fieldset>
                                <h2>Thông tin đối tác</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="name" >Tên công ty *</label>
                                            <input id="name" name="name" type="text" value="{{old('name')}}"  class="form-control required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="phone" >Số điện thoại*</label>
                                            <input id="phone" name="phone" type="tel"  value="{{old('phone')}}" class="form-control required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tỉnh/Thành Phố:</label>
                                            <select name="address1" id="ThanhPho" data-placeholder="Choose a Country..." class="form-control required">
                                                <option value="">Thành Phố</option>
                                                @foreach ($city as $ci)
                                                <option value="{{$ci->matp}}">{{$ci->name}}</option>
                                                @endforeach
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label >Quận/Huyện:</label>
                                            <select name="address" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control required">
                                                <option value="">--Chưa chọn Quận/Huyện--</option>
                                           </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="specific_address" >Địa chỉ cụ thể *</label>
                                            <input id="specific_address" name="specific_address" type="text"  value="{{old('specific_address')}}" class="form-control required">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1>Quyền tài khoản</h1>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Quyền:</label>
                                    <div class="col-sm-10">
                                        <select name="role_id" class=" form-control required">
                                            <option value="">Chọn quyền cho tài khoản</option>
                                            @foreach ($role as $rl)
                                                <option value="{{$rl->id}}" {{old('role_id') == $rl->id ? 'selected' : ''}}>{{$rl->name}}</option>
                                            @endforeach
                                       </select>
                                    </div>
                                </div>
                            <div class="text-center" style="margin-top: 120px">
                            </div>
                            </fieldset>
                            <h1>Hoàn thành</h1>
                            <fieldset>
                                <h2>Xác nhận tạo tài khoản</h2>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Chosen -->
    <script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

    <!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Steps -->
    <script src="{{ url('public/admin') }}/js/plugins/staps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="{{ url('public/admin') }}/js/plugins/validate/jquery.validate.min.js"></script>


@endsection

@section('javascript')
    <script type="text/javascript">
        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("{{ url('') }}/admin/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>
    <script>
        $('.price_range').hide();
        $(document).on("change","#EventType", function(event) {
            var value = $(this).val();
            if (value == 1) {
                $('.price_range').hide('150');
            }else {
                $('.price_range').show('150');
            };
            
        });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("Tiếp theo");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("Quay lại");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
@endsection