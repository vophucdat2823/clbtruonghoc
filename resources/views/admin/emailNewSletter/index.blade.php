@extends('admin.layout.master')

@section('title','Email nhận bản tin')
@section('style.css')
@endsection

@section('style')
<!-- Toastr style -->
    <link href="{{ url('public/admin') }}/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">


@endsection

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách email nhận bản tin</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách email nhận bản tin</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce" >

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <form action="{{ route('email_newsletter.excel_email') }}" method="post" role="form">
                                    {{ csrf_field() }}
                                    <div class="col-sm-5 m-b-xs">
                                        <select class="input-sm form-control input-s-sm inline" name="status_export">
                                            <option value="">-- Xuất file excel --</option>
                                            <option value="1">Xuất tất cả</option>
                                            <option value="2">Xuất email đã được xử lý</option>
                                            <option value="3">Xuất email chưa được xử lý</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" name="name_excel" placeholder="Tên file excel" class="input-sm form-control"> 
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn btn-sm btn-primary"> Xuất!</button> 
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <table class="footable table table-stripped toggle-arrow-tiny" id="load_ajax_boxlink"  data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Địa chỉ email</th>
                                    <th data-hide="phone">Trang thái</th>
                                    <th data-hide="phone">Ngày đăng ký</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($emailNewSletter as $email)
                                        <tr class="load_email_del_{{$email->id}}">
                                            <td>
                                               {{$email->email}}
                                            </td>
                                            <td class="load_status_{{$email->id}}">
                                                {{csrf_field()}}
                                                @if ($email->status == 0)
                                                <button type="button" class="btn btn-primary colum_status" data-id="{{$email->id}}" data-stt="1">Đã xử lý</button>
                                                @else
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="{{$email->id}}" data-stt="0">Đang chờ</button>
                                                @endif
                                            </td>
                                            <td>
                                                {{$email->created_at}}
                                            </td>
                                            
                                            <td class="text-right">
                                                <div class="btn-group">
                                                    <a class=" btn btn-xs btn-danger delete_email" data-id="{{$email->id}}" data-toggle="modal">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        
@endsection
@section('scripts')

<!-- Toastr -->
    <script src="{{ url('public/admin') }}/js/plugins/toastr/toastr.min.js"></script>

@endsection

@section('javascript')
    <script>
        var _token = $('input[name="_token"]').val();
        $(document).on('click', '.colum_status', function(){
            var status = $(this).data("stt");
            var id = $(this).data("id");
             
            if(id != '')
            {
                $.ajax({
                    url:"{{ route('email_newsletter.update_status') }}",
                    method:"POST",
                    data:{status:status, id:id, _token:_token},
                    success:function(data)
                    {
                        toastr["success"](data.message);
                        $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
                        
                    }
                })
            }
            else
            {
                toastr["success"]("Thất bại: Không có giá trị !");
            }
        });
        $(document).on('click', '.delete_email', function(event) {
            event.preventDefault();
            var id_del = $(this).data('id');
            $.ajax({
                url: '{{ url('admin/email-newsletter/delete_email') }}'+'/'+id_del,
                type: 'GET',
                dataType: 'json',
                success:function(data){
                    if (data.error) {
                        toastr["error"](data.message);
                    }else {
                        toastr["success"](data.message);
                        $('.load_email_del_'+id_del).load(location.href + ' .load_email_del_'+id_del+'>*')
                    };
                }
            })
        });
        
    </script>
@endsection
