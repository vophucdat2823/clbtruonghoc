@extends('admin.layout.master')
@section('title','Dịch Vụ | Chỉnh sửa')

@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    
<!-- Toastr style -->
    <link href="{{ url('public/admin') }}/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">



@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Chỉnh sửa dịch vụ</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li>
                    <a href="{{ route('service.index') }}">Danh sách dịch vụ</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa dịch vụ</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('service.update',['id'=>$service->id]) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin dịch vụ</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Địa chỉ</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"> Image</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="{{ route('service.index') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên dịch vụ(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name_dichvu" id="name" class="form-control" placeholder="Tên dịch vụ..." required value="{{$service->name}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Đường dẫn:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                @if ($service->slug)
                                                    @php
                                                        $slug_1 = explode('.', $service->slug);
                                                        $slug = explode('dich-vu-', $slug_1[0]);
                                                    @endphp
                                                @endif

                                                <div class="input-group">
                                                    <span class="input-group-addon">{{url('chi-tiet')}}/dich-vu-</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" required value="{{ array_pop($slug) }}">
                                                    <span class="input-group-addon">.{{$slug_1[1]}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Link combo(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-addon">URL:</span>
                                                    <input type="text" class="form-control" name="url_link_combo" id="url_link_combo" placeholder="Đường dẫn:Mặc định #" value="{{ $service->url_link_combo }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Điều kiện:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="study_condition" class="form-control" placeholder="Mặc định: Không" value="{{$service->study_condition}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Danh mục dịch vụ(*):</label>
                                        <div class="col-sm-10">

                                            <select name="cate_service_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    <option value="">Chọn danh mục</option>
                                                    @foreach ($cateService as $cate)
                                                        <option value="{{$cate->id}}" {{$service->cate_service_id == $cate->id ? "selected" : ''}}>{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            @if ($service->image)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$service->image}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$service->image}}" name=""><input type="file" name="image" value="{{$service->image}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @elseif(old('image'))
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{old('image')}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{old('image')}}" name=""><input type="file" name="image" value="{{old('image')}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Banner</label>
                                        <div class="col-sm-10 col-xs-10">
                                            @if ($service->banner)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$service->banner}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$service->banner}}" name=""><input type="file" name="banner" value="{{$service->banner}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @elseif(old('banner'))
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{old('banner')}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{old('banner')}}" name=""><input type="file" name="banner" value="{{old('banner')}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="banner">
                                                    </span>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" id="image_file">Ảnh mô tả thêm(*):</label>
                                        <div class="col-sm-10 col-xs-10">
                                            <input type="file" name="file_image[]" id="image_file" multiple>
                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Mô tả dịch vụ:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Mô tả dịch vụ">{!! $service->description !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Đơn vị cung cấp(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_unit">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_unit">
                                    @if (!empty(old('type') == "unit"))
                                        @for ($i = 0; $i < count(old('type') == "unit"); $i++)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="{{old('name')[$i]}}">
                                                                <input type="hidden" v-model="apartment.mang_vi" class="form-control" name="type[]" value="unit">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_unit">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    @elseif($service->getTypeService('unit'))
                                    <div class="loadTypeService_unit">
                                        @foreach ($service->getTypeService('unit') as $unit)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="{{$unit->id}}" data-type="unit" class="form-control" name="name_update" value="{{$unit->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_unit" data-id="{{$unit->id}}">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    </div>
                                    
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Giá thành(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_price">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_price">
                                    @if (!empty(old('type') == "price"))
                                        @for ($i = 0; $i < count(old('type') == "price"); $i++)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="{{old('name')[$i]}}">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="price">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_price">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    @elseif($service->getTypeService('price'))
                                    <div class="loadTypeService_price">
                                        @foreach ($service->getTypeService('price') as $price)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="{{$price->id}}" data-type="price" class="form-control" name="name_update" value="{{$price->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_price" data-id="{{$price->id}}">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Chất lượng(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_quality">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_quality">
                                    @if (!empty(old('type') == "quality"))
                                        @for ($i = 0; $i < count(old('type') == "quality"); $i++)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-8" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="{{old('name')[$i]}}">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="quality">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_quality">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    @elseif($service->getTypeService('quality'))
                                    <div class="loadTypeService_quality">
                                        @foreach ($service->getTypeService('quality') as $quality)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="{{$quality->id}}" data-type="quality" class="form-control" name="name_update" value="{{$quality->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_quality" data-id="{{$quality->id}}">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Số lượng(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_qty">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_qty">
                                    @if (!empty(old('type') == "qty"))
                                        @for ($i = 0; $i < count(old('type') == "qty"); $i++)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="{{old('name')[$i]}}">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="qty">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_qty">
                                                                <i class="fa fa-fw fa-trash-o"></i>'
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    @elseif($service->getTypeService('qty'))
                                    <div class="loadTypeService_qty">
                                        @foreach ($service->getTypeService('qty') as $qty)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="{{$qty->id}}" data-type="qty" class="form-control" name="name_update" value="{{$qty->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_qty" data-id="{{$qty->id}}">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Địa chỉ(*):</label>
                                      <div class="col-sm-4">
                                        <button type="button"  class="btn btn-block btn-success addNewApartment_address">
                                          ADD +
                                        </button>
                                      </div>
                                    </div>
                                    <div class="list-apartments_address">
                                    @if (!empty(old('type') == "address"))
                                        @for ($i = 0; $i < count(old('type') == "address"); $i++)
                                            <div class="form-group load">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" value="{{old('name')[$i]}}">
                                                                <input type="text" v-model="apartment.mang_vi" class="form-control" name="type[]" value="address">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeApartment_address">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    @elseif($service->getTypeService('address'))
                                    <div class="loadTypeService_address">
                                        @foreach ($service->getTypeService('address') as $address)
                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-1">
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="form-group col-xs-10" style="margin-left: 0">
                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>
                                                                <input type="text" v-model="apartment.mang_vi" data-id="{{$address->id}}" data-type="address" class="form-control" name="name_update" value="{{$address->name}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 pull-right" style="text-align: right">
                                                            <button type="button" class="btn btn-block btn-danger removeTypeService" data-type="_address" data-id="{{$address->id}}">
                                                                <i class="fa fa-fw fa-trash-o"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive" id="load_ajax_del">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($service->getImages as $element)
                                                <tr>
                                                    <td>
                                                        <img style="width: 75px; object-fit: center" src="{{ url('public/upload/images') }}/{{$element->image}}" id="show_img_ajax">
                                                    </td>
                                                    <td>
                                                        {{-- @if ($element->image)
                                                            <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                                <div class="form-control" data-trigger="fileinput">
                                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                    <span class="fileinput-filename">{{$element->image}}</span>
                                                                </div>
                                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                <span class="input-group-addon btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select file</span>
                                                                    <span class="fileinput-exists">Change</span>
                                                                    <input type="hidden" value="{{$element->image}}" name=""><input type="file" name="old_img[{{$element->id}}]" value="{{$element->image}}" aria-required="true" class="error" aria-invalid="true">
                                                                </span>
                                                            </div>
                                                        @endif --}}
                                                        <div class="col-sm-10 col-xs-10">
                                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                <div class="form-control" data-trigger="fileinput">
                                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                                    <span class="fileinput-filename">{{$element->image}}</span>
                                                                </div>
                                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                <span class="input-group-addon btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select file</span>
                                                                    <span class="fileinput-exists">Change</span>
                                                                    <input type="file" name="old_img[{{$element->id}}]" onchange="hien_thi_anh(this)" value="{{$element->image}}">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-danger" id="img_del_ajax" data-id="{{$element->id}}" onclick="return confirm('Delete {{$element->id}} ')">
                                                            <i class="fa fa-trash"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>

@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Toastr -->
    <script src="{{ url('public/admin') }}/js/plugins/toastr/toastr.min.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>

    <script type="text/javascript">

        $("body").on('click','.removeApartment_unit',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_unit").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Giá thành!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="unit">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_unit">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_unit").prepend(str);

        });
        $("body").on('click','.removeApartment_price',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_price").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Giá thành!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="price">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_price">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_price").prepend(str);

        });

        $("body").on('click','.removeApartment_quality',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_quality").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Chất lượng!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="quality">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_quality">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_quality").prepend(str);

        });
        $("body").on('click','.removeApartment_qty',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_qty").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Số lượng!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="qty">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_qty">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_qty").prepend(str);

        });
        $("body").on('click','.removeApartment_address',function(){
            $(this).parent().parent().parent().parent().remove();
        });

        $(".addNewApartment_address").click(function(){
            var str='<div class="form-group">';
                str+='<div class="col-xs-offset-1 col-xs-1">';
                str+='</div>';
                  str+='<div class="col-sm-8">';
                    str+='<div class="row">';
                     str+='<div class="form-group col-xs-10"style="margin-left: 0">';
                      str+='<div class="input-group">';
                        str+='<span class="input-group-addon" style="color: #f8ac59;font-weight: bold">>></span>';
                        str+='<input type="text" v-model="apartment.mang_vi" class="form-control" name="name[]" placeholder="Địa chỉ!">';
                        str+='<input type="hidden" class="form-control" name="type[]" value="address">';
                      str+='</div>';
                    str+='</div>';
                    str+='<div class="col-xs-2 pull-right" style="text-align: right">';
                      str+='<button type="button"  class="btn btn-block btn-danger removeApartment_address">';
                        str+='<i class="fa fa-fw fa-trash-o"></i>';
                      str+='</button>';
                    str+='</div>';
                  str+='</div>';
                str+='<div>';
            str+='</div>';
            str+='</div>';
            $(".list-apartments_address").prepend(str);

        });



    </script>


    <script>
        $(document).on("click",".removeTypeService", function(event) {
          var id_matp = $(this).data('id');
          var _type = $(this).data('type');
          $.get("{{ url('') }}/admin/type-service/"+id_matp,function(data) {
            if (data) {
                toastr["success"]("Xóa thành công.");
                $(".loadTypeService"+_type).load(location.href + ' .loadTypeService'+_type+'>*');
            }else {
                toastr["error"]("Xóa thất bại.");
                
            };
          });
        });
    </script>

    <script>


        $(document).ready(function(){
           $(document).on("blur" ,"input[name='name_update']" ,function(){
            var id = $(this).data("id");
            var type = $(this).data("type");
            var name = $(this).val();
            if(id != '')
              {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               $.ajax({
                url:"{{ route('service.updateTypeService') }}",
                method:"POST",
                data:{name:name, type:type, id:id},
                success:function(data)

                {
                    if (data) {
                        toastr["success"]("Thay đổi thành công.");
                    }else {
                        toastr["error"]("Thay đổi thất bại.");
                    };
                }
               })
              }
            else
              {
               toastr["error"]("Dữ liệu lỗi.");
              }
          });
        });
    </script>
    <script type="text/javascript">
        function hien_thi_anh(file)
          {
            if(file.files[0]){
              var reader = new FileReader();
              reader.onload = function(e) {
                // console.log($(this).parent().parent().parent().parent());
                // $(this).parent().parent().parent().parent().find('').attr('src', e.target.result);
              }
              reader.readAsDataURL(file.files[0]);
            }
          }

    $(document).on('click', '#img_del_ajax', function(event) {
        event.preventDefault(); // khoir load trang
        var del_img = $(this).data('id');
        $.ajax({
            url: '{{url('')}}/admin/delete-old-img/'+del_img,
            type: 'GET',
            dataType: 'json',
            success:function(res){
                console.log(res);
                toastr["success"]("Xóa ảnh thành công.");
                $('#load_ajax_del').load(location.href + " #load_ajax_del>*");
            }
        })
    });
    </script>

@endsection