@extends('admin.layout.master')
@section('style.css')
    {{----}}
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Danh sách danh mục</h2>
            <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
            </li>
            <li class="active">
                <strong>Danh mục sự kiện</strong>
            </li>
        </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thêm danh mục</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" action="{{route('cate-event.store')}}" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <div class="form-group">
                                                <label>Danh mục sự kiện(*):</label>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Tên tab danh mục tiếng việt" value="{{ old('name') }}" >
                                                @if($errors->has('name'))
                                                    <span class="text-center text-danger" role="alert">
                                                        {{$errors->first('name_vi')}}
                                                    </span>
                                                @endif
                                                <br><span style="margin-top: 5px">Tên riêng sẽ hiển thị trên trang mạng của bạn</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Chuỗi cho đường dẫn tĩnh(*):</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">su-kien/</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" value="">
                                                    <span class="input-group-addon">.html</span>
                                                </div>
                                                 @if($errors->has('slug'))
                                                    <span class="text-center text-danger" role="alert">
                                                        {{$errors->first('slug')}}
                                                    </span><br>
                                                @endif
                                                <br><span style="margin-top: 5px">Chuỗi cho đường dẫn tĩnh là phiên bản của tên hợp chuẩn với Đường dẫn (URL). Chuỗi này bao gồm chữ cái thường, số và dấu gạch ngang (-).</span>
                                            </div>

                                            <div class="form-group"><label>Chọn danh mục(*):</label>
                                                    <select name="parents" class="form-control" style="text-transform:uppercase">
                                                        {{-- <option value="">-- Chọn danh mục -- </option> --}}
                                                        <option value="0">-- ROOT -- </option>
                                                        {{showCatepost($cateEvent,0,'',old('choose_id'))}}
                                                    </select>
                                                    @if($errors->has('parents_cate'))
                                                        <span class="text-center text-danger" role="alert">
                                                            {{$errors->first('parents_cate')}}
                                                        </span><br>
                                                    @endif
                                                <span>Chuyên mục khác với thẻ, bạn có thể sử dụng nhiều cấp chuyên mục. Ví dụ: Trong chuyên mục nhạc, bạn có chuyên mục con là nhạc Pop, nhạc Jazz. Việc này hoàn toàn là tùy theo ý bạn.</span>
                                            </div>
                                          </div>
                                          <div>
                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Thêm danh mục</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Danh mục cha</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th >STT</th>
                                                        <th >Danh mục sự kiện</th>
                                                        <th>Đường dẫn tĩnh</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($cateEvent0))
                                                        @foreach($cateEvent0 as $key => $cate)
                                                            <tr>
                                                                <td>{{$key+1}}</td>
                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post">{{ $cate->name }}</span></a></td>

                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post-tacgia">{{ str_limit($cate->slug,30) }}</span></a></td>

                                                                <td>
                                                                    <a href="{{route('cate-event.edit', $cate->id)}}" type="button" class="btn btn-info">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                            data-target="#modal-delete-{{$cate->id}}">
                                                                        <i class="fa fa-fw fa-trash-o"></i>
                                                                    </button>
                                                                    <div class="modal modal-danger fade" id="modal-delete-{{$cate->id}}">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span></button>
                                                                                        @php
                                                                                            $delete = \App\Event::where('cate_event_id',$cate->id)->get();
                                                                                            $delete_cart = \App\CateEvent::whereIn('parents',[$cate->id])->get();
                                                                                        @endphp
                                                                                        @if ($delete->count() == 0 && $delete_cart->count() == 0)
                                                                                            <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                        @else
                                                                                            <h4 class="modal-title">
	                                                                                           Danh mục hiện tại đang chứa dữ liệu không thể xóa !
                                                                                            </h4>
                                                                                        @endif
                                                                                </div>
                                                                                <div class="modal-body" style="background: white!important; text-align: center">
                                                                                    <form method="POST" action="{{route('cate-event.destroy', $cate->id)}}">
                                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                                        {{ csrf_field() }}

                                                                                        @if ($delete->count() == 0 && $delete_cart->count() == 0)
                                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                            <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                                <i class="fa fa-save"></i>
                                                                                            </button>
                                                                                        @else
                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                        @endif
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Danh mục con</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">

                                    <div class="ibox-content">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>

                                                        <th >STT</th>
                                                        <th >Danh mục sự kiện</th>
                                                        <th>Đường dẫn tĩnh</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($cateEvent1))
                                                        @foreach($cateEvent1 as $key => $cate)
                                                            <tr>
                                                                <td>{{$key+1}}</td>
                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post">{{ $cate->name }}</span></a></td>

                                                                <td><a href="{{ url('') }}/{{$cate->slug}}" target="_blank"> <span class="tag-post-tacgia">{{ str_limit($cate->slug,30) }}</span></a></td>

                                                                <td>
                                                                    <a href="{{route('cate-event.edit', $cate->id)}}" type="button" class="btn btn-info">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                                            data-target="#modal-delete-{{$cate->id}}">
                                                                        <i class="fa fa-fw fa-trash-o"></i>
                                                                    </button>
                                                                    <div class="modal modal-danger fade" id="modal-delete-{{$cate->id}}">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                     @php
                                                                                        $delete = \App\Event::where('cate_event_id',$cate->id)->get();
                                                                                        $delete_cart = \App\CateEvent::whereIn('parents',[$cate->id])->get();
                                                                                    @endphp
                                                                                    @if ($delete->count() == 0  && $delete_cart->count() == 0)
                                                                                        <h4 class="modal-title">Bạn có chắc chắn muốn xóa danh mục này ?</h4>
                                                                                    @else
                                                                                        <h4 class="modal-title">
                                                                                            Danh mục hiện tại đang chứa dữ liệu không thể xóa !
                                                                                        </h4>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="modal-body" style="background: white!important; text-align: center">
                                                                                    <form method="POST" action="{{route('cate-event.destroy', $cate->id)}}">
                                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                                        {{ csrf_field() }}
                                                                                        @if ($delete->count() == 0  && $delete_cart->count() == 0)
                                                                                            <button type="button" class="btn btn-danger pull-left" style="margin-left: 10%" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                            <button style="margin-left: 15px" type="submit" class="btn btn-success">Xác nhận
                                                                                                <i class="fa fa-save"></i>
                                                                                            </button>
                                                                                        @else
                                                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ
                                                                                                <i class="fa fa-fw fa-close"></i>
                                                                                            </button>
                                                                                        @endif
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin: 0 auto;text-align: center;">
                            {{-- {{$categorys->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@php

function showCatepost($cateEvent, $parent = 0, $char ='',$select = 0)
{
    foreach ($cateEvent as $key => $item) {
        if ($item->parents == $parent) {
            echo '<option value="'.$item->id.'"';
            if($parent == 0){
                echo 'style="color:red"';
            }
            if($select != 0 && $item->id == $select){
                echo 'selected="selected"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCatepost($cateEvent ,$item->id , $char.'---| ',$select);
        }
    }
}
@endphp

@endsection
@section('javascript')
@endsection
