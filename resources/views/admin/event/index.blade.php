@extends('admin.layout.master')

@section('style.css')
@endsection
<style>
    .cate_event_id_product{
        background-color: #cfd1d2;
        color: #102b4e;
        border-radius: 5px;
        padding: 0% 1%;
        text-decoration: none;
        margin: 0% 1%;
    }
</style>

@section('content')

    
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Danh sách sự kiện</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                    </li>
                    <li class="active">
                        <strong>Danh sách sự kiện</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Tên sự kiện</th>
                                    <th data-hide="all">Danh mục sự kiện</th>
                                    <th data-hide="phone">Loại sự kiện</th>
                                    <th data-hide="phone">Trạng thái</th>
                                    <th class="text-right" data-sort-ignore="true">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($event as $ev)
                                        <tr >
                                            <td>
                                               {{$ev->name}}
                                            </td>
                                            {{-- <td>Model 1</td> --}}
                                            @php
                                               $cate_event_id = $ev->cate_event_id != null ? json_decode($ev->cate_event_id) : [];
                                            @endphp

                                            <td>
                                                @if (count($cate_event_id)>0)
                                                    @php
                                                        // dd();
                                                        $cateEvent = App\CateEvent::whereIn('id',$cate_event_id)->get();
                                                    @endphp
                                                    @foreach ($cateEvent as $cate)
                                                        <b class="cate_event_id_product">{{$cate->name}}</b>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                {{$ev->even_type == 'Miễn Phí' ? $ev->even_type : $ev->even_type.'|'.$ev->price_range}}
                                            </td>
                                            {{-- <td>1000</td> --}}
                                            <td class="load_status_{{$ev->id}}">
                                                {{csrf_field()}}
                                                @if ($ev->status == 0)
                                                <button type="button" class="btn btn-primary colum_status" data-id="{{$ev->id}}" data-stt="1">Active</button>
                                                @else
                                                 <button type="button" class="btn btn-danger colum_status"  data-id="{{$ev->id}}" data-stt="0">Pending</button>
                                                @endif
                                                {{-- <span class="label label-primary">Enable</span> --}}
                                            </td>
                                            <td class="text-right">
                                                <form action="{{ route('event.destroy',['id'=>$ev->id]) }}" method="POST" style="margin-bottom: 0">
                                                    <div class="btn-group">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button type="submit" class="btn-white btn btn-xs">Delete</button>
                                                        <a href="{{ route('event.edit',['id'=>$ev->id]) }}" class="btn-white btn btn-xs" >Edit</a>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
@section('scripts')
<!-- FooTable -->
<script src="{{ url('public/admin') }}/js/plugins/footable/footable.all.min.js"></script>





<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {

        $('.footable').footable();

    });

        
    

</script>

@endsection
@section('javascript')
    <script>
        var _token = $('input[name="_token"]').val();
        $(document).on('click', '.colum_status', function(){
            var status = $(this).data("stt");
            var id = $(this).data("id");
             
            if(id != '')
            {
                $.ajax({
                    url:"{{ route('event.update_status') }}",
                    method:"POST",
                    data:{status:status, id:id, _token:_token},
                    success:function(data)
                    {
                        $('.load_status_'+id).load(location.href + ' .load_status_'+id+'>*')
                    }
                })
            }
            else
            {
                $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
            }
        });
        
    </script>
@endsection
