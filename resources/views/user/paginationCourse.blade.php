@foreach ($send as $store)
        <div class="col-12 col-sm-6 col-md-4 mb-4 style_padding_box ">
            <article class="boxfilter boxfilter--1 ">
                <div class="boxfilter__info-hover" >
                    <div style="height: 100%">
                        <div class="Boxinfo Boxinfo-100">
                            <div class="Boxinfo-content">
                                <div class="Boxinfo-body">
                                    <p>
                                        <i class="fal fa-university"></i> Đơn vị tổ chức: {{$store->getService->getTypeServiceOne('unit')['name']}}
                                    </p>
                                    <p>
                                        <i class="fal fa-users"></i> Lớp tiêu chuẩn: {{$store->getService->getTypeServiceOne('qty')['name']}} học sinh
                                    </p>
                                    <p>
                                       <i class="fal fa-usd-circle"></i> Học phí: {{$store->getService->getTypeServiceOne('tuition')['name']}}
                                    </p>
                                    <p>
                                        <i class="fal fa-paper-plane"></i> Điều kiện học: {{$store->getService->study_condition}}
                                    </p>
                                </div>
                                <div class="Boxinfo-action" style="z-index: 100">
                                    <form action="{{ url('/chi-tiet') }}/{{$store->getService->slug}}" method="get" role="form">
                                        <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="boxfilter__img"></div>
                <a href="#" class="boxfilter_link">
                    <div class="boxfilter__img--hover" style="background-image: url('{{ url('public/upload/images') }}/{{$store->getService->image}}') ">
                        <img src="{{ url('public/assets/transparent') }}/cuahang_home.png" alt="">
                    </div>
                </a>
                <div class="clearfix">
                </div>
                <div class="boxfilter__info boxfilter__info1">
                    <h3>{{$store->getService->name}}</h3>
                    <p>
                        <i class="fal fa-clock"></i> Thời lượng : {{$store->getService->getTypeServiceOne('time')['name']}}
                        
                    </p>
                    <p>
                        <i class="fal fa-map-marker-alt"></i> Địa điểm:  {{$store->getService->getTypeServiceOne('address')['name']}}
                        
                    </p>
                </div>
            </article>
        </div>
@endforeach
