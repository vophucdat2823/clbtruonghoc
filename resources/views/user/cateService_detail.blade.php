@extends('layouts.app')
@section('title','Clb Trường Học')
@section('content')
    <main style="width: 75vw;margin:0 auto">
        <div class="my-5">
            <div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-5">
                                    <legend>{{$cateService->name}}</legend>
                                </div>                           
                                <div class="row">
                                    <section class="boxfilters">
                                        @foreach ($cateService->getService as $service)
                                            <div class="col-6 col-sm-6 col-md-3 mb-4 style_padding_box ">
                                                <article class="boxfilter boxfilter--1 ">
                                                    <div class="boxfilter__info-hover" >
                                                        <div class="" style="height: 100%">
                                                            <div class="Boxinfo Boxinfo-100">
                                                                <div class="Boxinfo-content">
                                                                    <div class="Boxinfo-body">
                                                                        <p>
                                                                            <i class="fal fa-university"></i> Đơn vị tổ chức: {{$service->getTypeServiceOne('unit')['name']}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-users"></i> Lớp tiêu chuẩn: {{$service->getTypeServiceOne('qty')['name']}}
                                                                        </p>
                                                                        <p>
                                                                           <i class="fal fa-usd-circle"></i> Học phí: {{$service->getTypeServiceOne('price')['name']}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-paper-plane"></i> Điều kiện học: {{$service['study_condition']}}
                                                                        </p>
                                                                    </div>
                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                        <form action="{{ url('/chi-tiet') }}/{{$service->slug}}" method="get" role="form">
                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="boxfilter__img"></div>
                                                    <a href="#" class="boxfilter_link">
                                                        <div class="boxfilter__img--hover" style="background-image: url('{{ url('public/upload/images') }}/{{$service->image}}') ">
                                                            <img src="{{ url('public/assets/transparent') }}/cuahang_home.png" alt="">
                                                        </div>
                                                    </a>
                                                    <div class="clearfix">
                                                    </div>
                                                    <div class="boxfilter__info boxfilter__info1">
                                                        <h5>{{$service->name}}</h5>
                                                    </div>
                                                </article>
                                            </div>
                                        @endforeach
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script>
    $(document).on("change","#ThanhPho", function(event) {
      var id_matp = $(this).val();
      $.get("{{ url('') }}/boot/ajax/quanhuyen/"+id_matp,function(data) {
        console.log(data);
        $("#QuanHuyen").html(data);
      });
    });
</script>
@endsection
