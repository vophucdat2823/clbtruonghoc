@extends('layouts.app')
@section('title','details-pro')
@section('content')

<main>
    <div class="w-100 background_div">
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-5 pb-5">
            <div class="col-lg-12 p-2 bg-white box-details-pro-header">
                <div class="row">
                    <div class="col-lg-4 pr-0 box-details-images">
                        <div class="images-product-big">
                            <img src="{{url('public/upload/images/'.$details['image'])}}" alt="">
                        </div>
                        <div class="list-images-product">
                            {{-- <ul>
                                <li>
                                    <a href="#">
                                        <img src="http://clbtruonghoc.edu.vn//public/upload/images/3.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="http://clbtruonghoc.edu.vn//public/upload/images/3.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="http://clbtruonghoc.edu.vn//public/upload/images/3.jpg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="http://clbtruonghoc.edu.vn//public/upload/images/3.jpg" alt="">
                                    </a>
                                </li>
                            </ul> --}}
                        </div>
                    </div>
                    <div class="col-lg-8 pl-0 box-derestion-pro">
                        <div style="border-bottom: 1px solid #ececec;padding: 0 10px">
                            <h1>{{$details->name}}</h1>
                            <div class="info-thuonghieu">
                                <p>
                                    <span style="font-weight: 600;">Thương hiệu: </span><span style="color: #29ABE2;">Lock&Lock</span>
                                    <span> - </span>
                                    <span style="color: #999999;">SKU: 3312485690773</span>
                                </p>
                            </div>
                        </div>
                        <div style="padding: 0 10px">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="info-price">
                                        <p>Giá: <span style="font-size: 28px">{{number_format($details->price)}}₫</span></p>
                                        @if ($details->price_sale)
                                            <p>Tiết kiệm: <span style="font-size: 28px">{{(($details->price-$details->price_sale)/$details->price)*100}}%</span> ({{number_format($details->price - $details->price_sale)}}₫)</p>
                                            <p>Giá thị trường: ({{number_format($details->price_sale)}}₫)</p>
                                        @endif
                                    </div>
                                    <div class="tomtat">
                                        <p>
                                            <?php echo $details->description ?>
                                        </p>
                                    </div>
                                    <div class="add-to-cart">
                                        
                                        <div class="quantity">
                                            <button class="plus-btn" id="btnMinus" type="button" name="button">
                                                <img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-minus-512.png" alt="">
                                            </button>
                                            <input type="text" id="txtQuantity" name="name" value="1">
                                            <button class="minus-btn" type="button" name="button">
                                                <img src="http://simpleicon.com/wp-content/uploads/plus.svg" alt="">
                                            </button>
                                        </div>
                                        <button class="addcart">Thêm vào giỏ hàng</button>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="items-prima">
                                        <h6>
                                            <i class="fal fa-check-circle"></i> Prima Trading
                                            <br/>
                                            <span>
                                                Cam kết chính hiệu 100%
                                            </span>
                                        </h6>
                                        <h6>
                                            <i class="fal fa-check-circle"></i>
                                            <span>
                                                Prima hoàn tiền 111% 
                                                Nếu phát hiện hàng giả
                                            </span>
                                        </h6>
                                    </div>
                                    <div class="items-prima">
                                        <h6>
                                            <i class="fas fa-phone-alt"></i> Liên hệ
                                            <br/>
                                            <span>
                                                Hotline đặt hàng: 19009237<br/>
                                                (Cả ngày thứ 7, CN)
                                            </span>
                                        </h6>
                                        <h6>
                                            <i class="fas fa-phone-alt"></i> Mail
                                            <span>
                                                shop@clbtruonghoc.edu.vn
                                            </span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .items-prima {
                border-color: #ececec;
            }
            .items-prima h6{
                color: #29ABE2;
                font-size: 18px;
            }
            .items-prima span{
                color: #333;
                font-size: 14px;
            }
        </style>
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-1 pb-1">
            <h5>MÔ TẢ SẢN PHẨM</h5>
            <div class="col-lg-12 p-2 bg-white box-mota">
                <div class="row">
                    <div class="col-lg-9 border-right">
                        <div class="motasanpham">
                            {{-- {{! $details->description !}} --}}
                            <?php echo $details->description ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-1 pb-1">
            <div class="col-lg-12 p-2 bg-white box-mota">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="items-sp-doc">
                            <img src="http://clbtruonghoc.edu.vn/public/upload/images/2.jpg" alt="">
                            <a href="#">Bình Giữ Nhiệt Lock&Lock Name Tumbler LHC4125B (500ml)</a>
                            <p>270.000</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
@endsection

@section('javascript')
<script>
$(document).ready(function(){
    var count = 1;
    $("#txtQuantity").val(count);
    $("#btnMinus").click(function(){
        $("#txtQuantity").val(count--);
    });
    $(".minus-btn").click(function(){
        $("#txtQuantity").val(count++);
    });
});
</script>
@endsection
