@extends('layouts.app')
@section('title','Clb Trường Học')
@section('content')
<main>
    <div class="">
        <div class="w-100 background_div">
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                <div class="col-12 col-md-6 my-3 pr-0 ">
                    <div style="margin-bottom: 8px">
                        <div class="carousel-wrap">
                            <div class="owl-carousel slide-banner">
                                @foreach ($banner->where('type','slider')->get() as $ban_slider)
                                <div class="item"><img class="transparent slider_image"
                                        src="{{ url('public/upload/images') }}/{{$ban_slider->images}}"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row style_img_baner1">
                        <div class="col-6 col-md-6 pr-ct-5">
                            <img class="transparent slider_image_banner"
                                src="{{ url('public/assets/transparent') }}/image1_home.png"
                                style="background-image: url('{{ url('public/upload/images') }}/{{$banner->where('id',1)->first()->images}}')"
                                width="100%">
                        </div>
                        <div class="col-6 col-md-6" style="padding-left: 5px;padding-right: 15px;">
                            <img class="transparent slider_image_banner"
                                src="{{ url('public/assets/transparent') }}/image1_home.png"
                                style="background-image: url('{{ url('public/upload/images') }}/{{$banner->where('id',2)->first()->images}}')"
                                width="100%">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 my-3 pr-0">
                    <div class="row style_img_baner2">
                        @foreach ($banner->where('id','>',2)->where('id','<=',10)->get() as $key => $ban_all)
                            <div class="col-6 col-md-6 {{$key % 2 == 0 ? 'le' : 'chan'}}">
                                <img class="transparent slider_image_banner"
                                    src="{{ url('public/assets/transparent') }}/image_home.png "
                                    style="background-image: url('{{ url('public/upload/images') }}/{{$ban_all->images}}')"
                                    width="100%">
                            </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="width-75-vw mr-0 mr-auto ml-auto">
            <div class="row ">
                <div class="col-12 ">
                    <ul class="image-carousel__item-list" style="width: 100%; transform: translate(0px, 0px);">
                        <li class="image-carousel__item" style="padding: 0px; width: 20%; margin-right: 2%">
                            <div class="home-category-list__group"><a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',1)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <!-- <img src="{{ url('public/upload/images') }}/{{$boxLink->where('stt',1)->value('image')}}style="fill: #115A80" alt=""> -->
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask:  url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',1)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',1)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',1)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',2)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask:  url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',2)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',2)->value('image')}}&quot;) no-repeat 50% 50%; ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',2)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="image-carousel__item" style="padding: 0px; width: 20%; margin-right: 2%">
                            <div class="home-category-list__group">
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',3)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask:  url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',3)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',3)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',3)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',4)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',4)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',4)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',4)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="image-carousel__item" style="padding: 0px; width: 20%; margin-right: 2%">
                            <div class="home-category-list__group">
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',5)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',5)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',5)->value('image')}}&quot;) no-repeat 50% 50%; ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',5)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',6)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',6)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',6)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',6)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="image-carousel__item" style="padding: 0px; width: 20%; margin-right: 2%">
                            <div class="home-category-list__group">
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',7)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',7)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',7)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',7)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',8)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',8)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',8)->value('image')}}&quot;) no-repeat 50% 50%; ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',8)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="image-carousel__item" style="padding: 0px; width: 20%; margin-right: 2%">
                            <div class="home-category-list__group">
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',9)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',9)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',9)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',9)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',10)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',10)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',10)->value('image')}}&quot;) no-repeat 50% 50%; ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',10)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="image-carousel__item" style="padding: 0px; width: 20%;">
                            <div class="home-category-list__group">
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',11)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',11)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',11)->value('image')}}&quot;) no-repeat 50% 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',11)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                                <a class="home-category-list__category-grid"
                                    href="{{$boxLink->where('stt',12)->value('link')}}">
                                    <div class="_1z7IKz">
                                        <div class="_2BlbuP">
                                            <div class="_3ZDC1p SpPcVL">
                                                <div class="SpPcVL _3XaILN"
                                                    style="background-color: #115A80;-webkit-mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',12)->value('image')}}&quot;) no-repeat 50% 50%;mask: url(&quot;{{ url('public/upload/images') }}/{{$boxLink->where('stt',12)->value('image')}}&quot;) no-repeat 50% 50%;  ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1SKT3L">
                                            <div class="vvKCN3">{{$boxLink->where('stt',12)->value('name')}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css" media="screen">
        time.icon {
            font-size: 0.5em;
            /* change icon size */
            display: block;
            position: relative;
            width: 3em;
            height: 3em;
            background-color: #fff;
            margin: 0 auto;
            border-radius: 0em;
            /* box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd; */
            overflow: hidden;
            -webkit-backface-visibility: hidden;
            -webkit-transform: rotate(0deg) skewY(0deg);
            -webkit-transform-origin: 50% 10%;
            transform-origin: 50% 10%;
        }

        time.icon * {
            display: block;
            width: 100%;
            font-size: 1em;
            font-weight: bold;
            font-style: normal;
            text-align: center;
        }

        time.icon strong {
            position: absolute;
            top: 0;
            /* padding: 0.4em 0; */
            color: #fff;
            background-color: #fd9f1b;
            /* border-bottom: 1px dashed #f37302; */
            /* box-shadow: 0 2px 0 #fd9f1b; */
        }

        time.icon em {
            position: absolute;
            bottom: 0.3em;
            color: #fd9f1b;
        }

        time.icon span {
            width: 100%;
            font-size: 0.8em;
            letter-spacing: -0.05em;
            padding-top: 0.8em;
            color: #2f2f2f;
        }

        time.icon:hover,
        time.icon:focus {
            -webkit-animation: swing 0.6s ease-out;
            animation: swing 0.6s ease-out;
        }

        @-webkit-keyframes swing {
            0% {
                -webkit-transform: rotate(0deg) skewY(0deg);
            }

            20% {
                -webkit-transform: rotate(12deg) skewY(4deg);
            }

            60% {
                -webkit-transform: rotate(-9deg) skewY(-3deg);
            }

            80% {
                -webkit-transform: rotate(6deg) skewY(-2deg);
            }

            100% {
                -webkit-transform: rotate(0deg) skewY(0deg);
            }
        }

        @keyframes swing {
            0% {
                transform: rotate(0deg) skewY(0deg);
            }

            20% {
                transform: rotate(12deg) skewY(4deg);
            }

            60% {
                transform: rotate(-9deg) skewY(-3deg);
            }

            80% {
                transform: rotate(6deg) skewY(-2deg);
            }

            100% {
                transform: rotate(0deg) skewY(0deg);
            }
        }
    </style>

    <div class="">
        <div align="center" style="padding: 10px 0">
            <p style="font-size: 30px; font-weight: 600; text-transform: uppercase;" class="my-2">SỰ KIỆN
            </p>
            <!-- Phần hiển thị 3 -->
        </div>
        <div class="w-100 background_div">
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                @if (Auth::check())
                    @php
                        $cityEvent_auth =$cityEvent->where('district_id',Auth::user()->address)->orderBy('event_id','desc')->limit(8)->get();
                    @endphp
                    @foreach ($cityEvent_auth as $event_address)
                        @if ($event->where('id',$event_address->event_id)->first()->status == 0)
                            @php
                                $date_play = explode("-", $event->where('id',$event_address->event_id)->first()->date_play);
                            @endphp
                            <div class="col-6 col-sm-4 col-md-3 my-3 style_padding_box">
                                <div class="card_custom"
                                    style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">

                                    <img src="{{ url('public/assets/transparent') }}/cuahang_home.png"
                                        alt="{{$event->where('id',$event_address->event_id)->first()->name}}"
                                        class="card-img-top transparent"
                                        style="background-image: url('{{ url('public/upload/images') }}/{{$event->where('id',$event_address->event_id)->first()->image}}')"
                                        width="100%">


                                    <div class="card-body" style="padding: 0.7rem;">
                                        <h5 class="card-title box_sukien">
                                            <a style="font-size: 22px;" href="/su-kien/{{$event->where('id',$event_address->event_id)->first()->slug}}" title="{{$event->where('id',$event_address->event_id)->first()->name}}">
                                                {{str_limit($event->where('id',$event_address->event_id)->first()->name,60)}}
                                            </a>
                                        </h5>
                                        <div class="d-flex justify-content-between" style="align-items: flex-end;">
                                            <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><i
                                                    class="fas fa-map-marker-alt"></i>
                                                <b>{{str_replace('Quận','',str_replace('Huyện','',$district->where('maqh',$event_address->district_id)->first()['name']))}}</b>
                                            </p>
                                            <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><i
                                                    class="fas fa-map-marker-alt"></i>
                                                <b>{{str_replace('Tỉnh','',str_replace('Thành phố','',$city->where('matp',$district->where('maqh',$event_address->district_id)->first()['matp'])->first()['name']))}}</b>
                                            </p>
                                            <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><b>Loại sự kiện:
                                                    {{$event->where('id',$event_address->event_id)->first()->even_type}}</b></p>

                                            <div class="pull-right" align="center">
                                                <time datetime="2014-09-20" class="icon">
                                                    <em>Tháng {{$date_play[1]}}</em>
                                                    <strong>{{$date_play[2]}}</strong>
                                                    <span>Thứ 7</span>
                                                </time>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    @foreach ($event->where('status',0)->orderBy('id','desc')->limit(8)->get() as $ev)
                        @php
                            $district_id_get = $cityEvent->where('event_id',$ev->id)->first();
                            $district_id_first = $district_id_get['district_id'];
                            $date_play = explode("-", $ev->date_play);
                        @endphp
                        <div class="col-6 col-sm-4 col-md-3 my-3 style_padding_box">
                            <div class="card_custom"
                                style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                                <img src="{{ url('public/assets/transparent') }}/cuahang_home.png" alt="{{$ev->name}}"
                                    class="card-img-top transparent"
                                    style="background-image: url('{{ url('public/upload/images') }}/{{$ev->image}}')"
                                    width="100%">

                                <div class="card-body" style="padding: 0.7rem;">
                                    <h5 class="card-title box_sukien"><strong><a href="#"
                                                title="{{$ev->name}}">{{str_limit($ev->name,60)}}</a></strong></h5>
                                    <div class="d-flex justify-content-between" style="align-items: flex-end;">
                                        <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><i
                                                class="fas fa-map-marker-alt"></i>
                                            <b>{{str_replace('Quận','',str_replace('Huyện','',$district->where('maqh',$district_id_first)->first()['name']))}}</b>
                                        </p>
                                        <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><i
                                                class="fas fa-map-marker-alt"></i>
                                            <b>{{str_replace('Tỉnh','',str_replace('Thành phố','',$city->where('matp',$district->where('maqh',$district_id_first)->first()['matp'])->first()['name']))}}</b>
                                        </p>
                                        <p style="margin-bottom: 0;font-size: 0.6vw" class="responsive_a"><b>Loại sự kiện:
                                                {{$ev->even_type}}</b></p>

                                        <div class="pull-right" align="center">
                                            <time datetime="2014-09-20" class="icon">
                                                <em>Tháng {{$date_play[1]}}</em>
                                                <strong>{{$date_play[2]}}</strong>
                                                <span>Thứ 7</span>
                                            </time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="">

        <div align="center" style="padding: 10px 0">
            <p style="font-size: 30px; font-weight: 600; text-transform: uppercase;" class="my-2">CHƯƠNG TRÌNH GAMESHOW
            </p>
            <!-- Phần hiển thị 3 -->
        </div>
        <div class="w-100 background_div">
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                <div class="col-6 col-sm-6 col-md-4 my-5 mr_1904">
                    <div class="card_custom"
                        style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                        <img src="{{ url('public/assets') }}/image/thetarget.jpg" alt="456" class="card-img-top">
                        <div class="card-body" style="padding: 0.7rem;">
                            <div style="min-height: 100px;">
                                <a href="#" style="text-align: justify;font-size: 1.5vw" class="rungchuongvang"> The target - Cùng nhau về đích</a>
                            </div>
                            <p style="margin-bottom: 0;font-weight: 400;color:#115A80 ">Phí tham dự: <span
                                    style="font-weight: 400;color:#FBB03B ">miễn phí</span></p>
                            <div class="d-flex justify-content-between" style="align-items: center;">
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"><i
                                        class="fas fa-map-marker-alt"></i> <b>Online</b></p>
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"> <b>Còn 200 lượt</b></p>

                                <form method="POST" action="http://127.0.0.1:8000/product/9" accept-charset="UTF-8"
                                    class="pull-right">
                                    <input name="_token" type="hidden" value="pm5VTuumHrU4VLMQvTR5A0OqRMUKzDidR0y0kWrc">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" value="Đăng ký" class="btn"
                                        style="background:#115A80;color: #fff;border-radius: unset;text-transform: uppercase;/* font-weight: 500; */padding: 0.1rem 0.4rem;font-size: 1vw;">
                                </form>
                                <div class="pull-right" align="center">
                                    <div class="panel panel-warning"
                                        style="border: 0.3px solid #f6f6f6;padding: 0.1rem;">
                                        <div class="panel-body" style="background: #F09D25;color: #fff">
                                            <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b>
                                        </div>
                                        <div class="panel-body">
                                            <!-- <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b> -->
                                            <p style="margin-bottom: 0;font-size: 0.4vw"><b>20</b></p>
                                            <p style="margin-bottom: 0;font-size: 0.2vw">Thứ 7</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4 my-5 mr_1904">
                    <div class="card_custom"
                        style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                        <img src="{{ url('public/assets') }}/image/rungchuongvang.jpg" alt="456" class="card-img-top">
                        <div class="card-body" style="padding: 0.7rem;">
                            <div style="min-height: 100px;">
                                <a href="#" style="text-align: justify;font-size: 1.5vw" class="rungchuongvang"> Chương trình Rung Chuông Vàng</a>
                            </div>
                            <p style="margin-bottom: 0;font-weight: 400;color:#115A80 ">Phí tham dự: <span
                                    style="font-weight: 400;color:#FBB03B ">miễn phí</span></p>
                            <div class="d-flex justify-content-between" style="align-items: center;">
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"><i
                                        class="fas fa-map-marker-alt"></i> <b>Online</b></p>
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"> <b>Còn 200 lượt</b></p>

                                <form method="POST" action="http://127.0.0.1:8000/product/9" accept-charset="UTF-8"
                                    class="pull-right">
                                    <input name="_token" type="hidden" value="pm5VTuumHrU4VLMQvTR5A0OqRMUKzDidR0y0kWrc">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" value="Đăng ký" class="btn"
                                        style="background:#115A80;color: #fff;border-radius: unset;text-transform: uppercase;/* font-weight: 500; */padding: 0.1rem 0.4rem;font-size: 1vw;">
                                </form>
                                <div class="pull-right" align="center">
                                    <div class="panel panel-warning"
                                        style="border: 0.3px solid #f6f6f6;padding: 0.1rem;">
                                        <div class="panel-body" style="background: #F09D25;color: #fff">
                                            <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b>
                                        </div>
                                        <div class="panel-body">
                                            <!-- <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b> -->
                                            <p style="margin-bottom: 0;font-size: 0.4vw"><b>20</b></p>
                                            <p style="margin-bottom: 0;font-size: 0.2vw">Thứ 7</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4 my-5 mr_1904">
                    <div class="card_custom"
                        style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                        <img src="{{ url('public/assets') }}/image/Olympia.jpg" alt="456" class="card-img-top">
                        <div class="card-body" style="padding: 0.7rem;">
                            <div style="min-height: 100px;">
                                <a href="#" style="text-align: justify;font-size: 1.5vw" class="rungchuongvang"> Đường Lên Đỉnh Olympia</a>
                            </div>
                            <p style="margin-bottom: 0;font-weight: 400;color:#115A80 ">Phí tham dự: <span
                                    style="font-weight: 400;color:#FBB03B ">miễn phí</span></p>
                            <div class="d-flex justify-content-between" style="align-items: center;">
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"><i
                                        class="fas fa-map-marker-alt"></i> <b>Online</b></p>
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"> <b>Còn 200 lượt</b></p>

                                <form method="POST" action="http://127.0.0.1:8000/product/9" accept-charset="UTF-8"
                                    class="pull-right">
                                    <input name="_token" type="hidden" value="pm5VTuumHrU4VLMQvTR5A0OqRMUKzDidR0y0kWrc">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" value="Đăng ký" class="btn"
                                        style="background:#115A80;color: #fff;border-radius: unset;text-transform: uppercase;/* font-weight: 500; */padding: 0.1rem 0.4rem;font-size: 1vw;">
                                </form>
                                <div class="pull-right" align="center">
                                    <div class="panel panel-warning"
                                        style="border: 0.3px solid #f6f6f6;padding: 0.1rem;">
                                        <div class="panel-body" style="background: #F09D25;color: #fff">
                                            <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b>
                                        </div>
                                        <div class="panel-body">
                                            <!-- <b class="panel-title" style="font-size: 0.35vw">Tháng 5</b> -->
                                            <p style="margin-bottom: 0;font-size: 0.4vw"><b>20</b></p>
                                            <p style="margin-bottom: 0;font-size: 0.2vw">Thứ 7</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="">

        <div align="center" style="padding: 10px 0">
            <p style="font-size: 30px; font-weight: 600; text-transform: uppercase;" class="my-2">CÂU LẠC BỘ TV
            </p>
            <!-- Phần hiển thị 3 -->
        </div>
        <div class="w-100 background_div">
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                @foreach ($bustle as $bust)
                <div class="col-6 col-sm-4 col-md-3 my-3 style_padding_box">
                    <div class="card_custom"
                        style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                        <img src="{{ url('public/assets/transparent') }}/cuahang_home.png" alt="{{$bust->name}}"
                            class="card-img-top transparent"
                            style="background-image: url('{{ url('public/upload/images') }}/{{$bust->image}}')"
                            width="100%">
                        <div class="card-body" style="padding: 0.7rem;">
                            <div class="d-flex justify-content-between" style="align-items: center;margin-bottom: 1rem">
                                <div style="min-height: 70px">
                                    <a href="#" class="store_9b" style="font-size: 22px;">{{$bust->name}}</a>
                                </div>
                                <p style="margin-bottom: 0;font-size: 0.37vw"><i class="fas fa-map-marker-alt"></i>
                                    <b>{{str_replace('Tỉnh','',str_replace('Thành phố','',$bust->address))}}</b></p>
                            </div>
                            <div class="d-flex justify-content-between box_clbtruonghoc"
                                style="align-items: flex-start;">
                                <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a" class="txt_clb_school">
                                    {{str_limit(strip_tags($bust->description),100)}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="mb-5">
        <div align="center" style="padding: 10px 0">
            <p style="font-size: 30px; font-weight: 600; text-transform: uppercase;" class="my-2">CỬA HÀNG TRỰC TUYẾN
            </p>
            <!-- Phần hiển thị 4 -->
        </div>
        <div class="w-100 background_div">
            <div class="row width-75-vw mr-0 mr-auto ml-auto">
                <input type="hidden" name="_token" value="3MIzKnb2j1RGXlXHhgnwuCdDGPQI3Tn5FZP6vnON">
                @php
                    $listProductStore=$productStore->where('status','=',0)->orderBy('id', 'desc')->limit(8)->get()->toarray();
                @endphp
                @foreach ($listProductStore as $item)
                    <div class="col-6 col-sm-4 col-md-3 my-3 style_padding_box ">
                        <div class="card_custom"
                            style="box-shadow: 5px 5px 5px #666;-moz-box-shadow: 5px 5px 5px #666;-webkit-box-shadow: 3px 3px 3px #ccc;">
                            <img src="{{url('public/upload/images/'.$item['image'])}}"
                                alt="{{$item['name']}}" class="card-img-top transparent"
                                style="background-image: url('{{url('public/upload/images/'.$item['image'])}}');max-height: 215px; object-fit: cover;"
                                width="100%">
                            <div class="card-body" style="padding: 0.7rem;">
                                <h5 class="card-title box_cuahangtructuyen">
                                    <a href="/chi-tiet-san-pham/{{$item['slug']}}" class="store_9b" title="{{$item['name']}}" style="font-size: 22px;font-weight: 500;"> {{$item['name']}}</a>
                                </h5>
                                <div class="d-flex justify-content-between" style="align-items: center;">
                                    <p style="margin-bottom: 0;font-size: 1vw" class="responsive_a"> <b>Giá: {{number_format($item['price'])}}đ</b>
                                    </p>

                                    <button type="button" data-id="28" id="add_cart_ajax" data-md="2802092000"
                                        class="btn btn-warning"
                                        style="background:#115A80;color: #fff;border-radius: unset;text-transform: uppercase;/* font-weight: 500; */padding: 0.1rem 0.4rem;font-size: 1vw;">Mua
                                        ngay</button>
                                    <input type="hidden" id="qs-quantity28" size="5" class="item-quantity" name="quantity28"
                                        value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</main>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
@endsection

@section('javascript')
<script src="{{ url('public/assets/js/javascript.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('.box_cuahangtructuyen').matchHeight();
        $('.box_sukien').matchHeight();
        $('.box_clbtruonghoc').matchHeight();
        $('.slider_image_banner').matchHeight();
    });
</script>
@endsection