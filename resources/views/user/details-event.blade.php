@extends('layouts.app')
@section('title','details-pro')
@section('content')
<main>

    <div class="box_title_categories box_title_categories_detail text-left" style="background-image: url('https://edu2review.com/upload/article-images/2019/06/11304/1920x1080_trung-tam-anh-ngu-huy-trinh.jpg') !important;background-repeat: no-repeat">
        <div class="bg_overlay" style="opacity: 0.6;"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 style="text-align: center;">{{$detailEvent->name}}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="background_div">
        <div class="row width-75-vw mr-0 mr-auto ml-auto pt-5 pb-5">
            <div class="col-lg-8 pt-2 pb-2 bg-white border-right content-event">
                <?php echo $detailEvent->description ?>
            </div>
            <div class="col-lg-4 pt-2 pb-2 bg-white border-left">
                <div class="title">
                    <h4 style="font-weight: 600;">Liên quan</h4>
                    <div style="background: #1b74bb; width: 10%; height: 3px;"></div>
                </div>
                <div class="box-moinhat" style="display: inline-flex;margin-top: 20px;">
                    <img style="width: 130px; height: 80px; border-radius: 10px;margin-right: 10px;" src="https://edu2review.com/upload/article-images/2019/06/11228/320x180_trung-tam-mc-ielts-cover.jpg" alt="">
                    <a href="#">
                        <h3 style="font-size: 18px;">Tổng quan các khóa học tại trung tâm Mc IELTS</h3>
                    </a>
                </div>
                <div class="box-moinhat" style="display: inline-flex;margin-top: 20px;">
                    <img style="width: 130px; height: 80px; border-radius: 10px;margin-right: 10px;" src="https://edu2review.com/upload/article-images/2019/06/11228/320x180_trung-tam-mc-ielts-cover.jpg" alt="">
                    <a href="#">
                        <h3 style="font-size: 18px;">Tổng quan các khóa học tại trung tâm Mc IELTS</h3>
                    </a>
                </div>
                <div class="box-moinhat" style="display: inline-flex;margin-top: 20px;">
                    <img style="width: 130px; height: 80px; border-radius: 10px;margin-right: 10px;" src="https://edu2review.com/upload/article-images/2019/06/11228/320x180_trung-tam-mc-ielts-cover.jpg" alt="">
                    <a href="#">
                        <h3 style="font-size: 18px;">Tổng quan các khóa học tại trung tâm Mc IELTS</h3>
                    </a>
                </div>
                <div class="box-moinhat" style="display: inline-flex;margin-top: 20px;">
                    <img style="width: 130px; height: 80px; border-radius: 10px;margin-right: 10px;" src="https://edu2review.com/upload/article-images/2019/06/11228/320x180_trung-tam-mc-ielts-cover.jpg" alt="">
                    <a href="#">
                        <h3 style="font-size: 18px;">Tổng quan các khóa học tại trung tâm Mc IELTS</h3>
                    </a>
                </div>
            </div>
        </div>
    </div>
</main>
<style>
    .content-event{
        font-size: 16px;
    }
</style>
@endsection