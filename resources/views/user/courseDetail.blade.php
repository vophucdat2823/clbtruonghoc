@extends('layouts.app')
@section('title','Clb Trường Học')
@section('stylesheet')
<link href="{{ url('public/admin/css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
<link href="{{ url('public/admin/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ url('public/admin/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="box_title_categories box_title_categories_detail text-left" style="background-image: url('{{ url("public/upload/images") }}/{{$course_detail->banner}}') !important;background-repeat: no-repeat">
            <div class="bg_overlay"></div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <div class="content_categories content_categories_detail">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 pull-right box_detail_right">
                        <div class="info_box_detail_right">
                            <div class="box_top_right hidden-xs">
                                <img src="{{url("public/assets/transparent/image_detail.png")}}" class="transparent" style="background-image: url('{{ url('public/upload/images') }}/{{$course_detail->image}}')" width="100%">
                            </div>
                            <div class="box_price_right">
                                <h5 class="text-center">
                                    <span class="text-uppercase font-Light">{{$course_detail->name}}</span><br />
                                </h5>
                            </div><!-- end .box_price_right-->
                            <div class="bprod-btn_new">
                                <div class="bc-btn text-center">
                                    <a class="note border-top-5cc2a8 border-right-5cc2a8 bg-5cc2a8 bg-gradient-5cc2a8 color-000000"data-toggle="tab" href="#menu3">Đăng ký</a>
                                </div>
                            </div>
                            <div class="ls_gr_text">
                                <ul>
                                    @if ($course_detail->getTypeCourseOne('unit')['name'])
                                        <li><i class="fal fa-university"></i> Đơn vị tổ chức: {{$course_detail->getTypeCourseOne('unit')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('quality')['name'])
                                        <li><i class="fal fa-clock"></i> Chất lượng: {{$course_detail->getTypeCourseOne('quality')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('address')['name'])
                                        <li><i class="fal fa-map-marker-alt"></i> Địa điểm: {{$course_detail->getTypeCourseOne('address')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('time')['name'])
                                        <li><i class="fal fa-clock"></i> Thời lượng: {{$course_detail->getTypeCourseOne('time')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('price')['name'])
                                        <li><i class="fal fa-users"></i> Giá thành: {{$course_detail->getTypeCourseOne('price')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('qty')['name'])
                                        <li><i class="fal fa-users"></i> Lớp tiêu chuẩn: {{$course_detail->getTypeCourseOne('qty')['name']}}</li>
                                    @endif
                                    @if ($course_detail->getTypeCourseOne('tuition')['name'])
                                        <li><i class="fal fa-usd-circle"></i> Học phí: {{$course_detail->getTypeCourseOne('tuition')['name']}}</li>
                                    @endif
                                    <li><i class="fal fa-paper-plane"></i> Điều kiện để học: {{$course_detail['study_condition']}}</li>
                                </ul>
                            </div><!-- end .ls_gr_text-->
                        </div>
                    </div>
                    <div class="col-sm-9 col-xs-12 pull-left box_detail_left">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist box_detail_left_ul">
                            <li class="nav-item box_detail_left_li_1 " >
                              <a class="box_detail_left_a bg-5CC2A8" style="border-right: 0"  onclick="event.preventDefault();">&ensp;</a>
                            </li>
                            <li class="nav-item box_detail_left_li">
                              <a class="nav-link box_detail_left_a active" data-toggle="tab" href="#home">MÔ TẢ DỊCH VỤ</a>
                            </li>
                            <li class="nav-item box_detail_left_li">
                              <a class="nav-link box_detail_left_a" data-toggle="tab" href="#menu2">HÌNH ẢNH</a>
                            </li>
                            <li class="nav-item box_detail_left_li ">
                              <a class="nav-link box_detail_left_a " data-toggle="tab" href="#menu3">Đăng ký</a>
                            </li>


                        </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div id="home" class="container tab-pane active "><br>
                                <div class="box_people_view m-b-40 bg-f6f6f6">
                                    {!! $course_detail->description !!}
                                    <div class="box_viewmore">
                                        <ul class="list-inline">
                                            <li class="icon_click">Xem toàn bộ <i class="fa fa-chevron-circle-right"></i></li>
                                            
                                        </ul>
                                        <div class="bg_view"></div>
                                    </div><!-- end .box_viewmore-->
                                </div>
                            </div>
                            <style type="text/css" media="screen">
                                .owl-carousel img {
                                    vertical-align: top;
                                                    
                                }
                                .wrapCarouselFull {
                                    display: block;
                                    position: relative;
                                    margin: 0 auto;
                                    max-width: 640px;
                                    padding: 20px;
                                    background: #fff;
                                }

                                .wrapCarouselThumbs {
                                    display: block;
                                    position: relative;
                                    margin: 0 auto;
                                    max-width: 640px;
                                    padding: 20px;
                                    margin-top: 0;
                                    background: #fff;
                                    padding-top: 0;
                                }

                                .wrapCarouselThumbs .owl-item {
                                    border: 3px solid #ccc;
                                }

                                .wrapCarouselThumbs .owl-item.current {
                                    border: 3px solid #333333;
                                }

                            </style>
                            <div id="menu2" class="container tab-pane fade"><br>
                                <div class="wrapCarouselFull">
                                    <div class="owl-carousel carousel-full">
                                        @foreach ($course_detail->getImages as $img)
                                            <div class="item-carousel-full">
                                                <img src="{{ url('public/assets/transparent/image_detail_guiyeucau.png') }}" style="background-image: url('{{ url('public/upload/images') }}/{{$img->image}}'); " class="transparent"  alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="wrapCarouselThumbs">
                                    <div class="owl-carousel carousel-thumbs">
                                        @foreach ($course_detail->getImages as $img)
                                            <div class="item-carousel-thumbs">
                                                <img src="{{ url('public/assets/transparent/image_detail_guiyeucau2.png') }}" style="background-image: url('{{ url('public/upload/images') }}/{{$img->image}}'); " class="transparent"  alt="">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div id="menu3" class="container tab-pane fade " style="padding: 0 !important"><br>
                              <div class="box_people_view m-b-40 bg-f6f6f6" style="max-height: 100%;">
                                    <form class="form-inline" action="{{ route('user.sendRequire') }}" method="POST">
                                        {{ csrf_field() }}
                                    <input type="hidden" name="primary_id" value="{{$course_detail->id}}">
                                    <input type="hidden" name="type" value="course">
                                        <div class="box_ls_people">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Bạn muốn đặt tiệc vào thời gian nào?</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people form-inline" >
                                                        <div class="form-group col-sm-6" style="padding-left: 0" id="data_1">
                                                            <div class="input-group date" style="position: relative;align-items: center;">
                                                                <input type="text" class="form-control" name="date" value="03/04/2014" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                                <span class="input-group-addon" style="position: absolute;right: 5px;">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group clockpicker col-sm-6" style="padding-right: 0" data-autoclose="true">
                                                            <div class="input-group" style="position: relative;align-items: center;">
                                                                <input type="text" class="form-control" value="09:30" name="time" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">

                                                                <span class="input-group-addon" style="position: absolute;right: 5px;">
                                                                    <i class="far fa-clock"></i>
                                                                </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Ngân sách tổ chức tiệc cho 1 HS là bao nhiêu?</p>
                                                    </div><!-- end .info_ls_people-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="price" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Số lượng suất ăn là bao nhiêu suất?</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="quantity" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Bạn có thể tham khảo combo tiệc sinh nhật có sẵn của CLBtruonghoc <a href="#">tại đây</a></p>
                                                    </div>
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Để CLB truonghoc có thể tư vấn chi tiết hơn, ban vui lòng để lại email và số điện thoại cá nhân nhé! </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Họ tên</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="name" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>

                                                
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Số điện thoại</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="phone" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-3">
                                                    <div class="info_ls_people">
                                                        <p class="font-Bold title_people_ls">Email</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 col-md-9">
                                                    <div class="info_ls_people">
                                                        <input type="text" class="form-control" name="email" id="" style="background: #F2F2F2;border-radius: unset;border: 0">
                                                    </div>
                                                </div>
                                                @php
                                                    $question = App\Question::where('type','course')->where('primary_id',$course_detail->id)->get();
                                                @endphp

                                                @foreach ($question as $ques)
                                                    <div class="col-sm-6">
                                                        <div class="info_ls_people">
                                                            <label for="" style="text-align: left;float: inline-start;">{{$ques->question}}</label>
                                                            <input type="text" class="form-control" name="question[]" id="" style="width: 100%;background: #F2F2F2;border-radius: unset;border: 0">
                                                            <input type="hidden" name="question_id[]" value="{{$ques->id}}">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div><!-- end .box_ls_people-->
                                        <div class="row box_ls_people" style="width: 100%;text-align: center;margin-top: 15px">
                                            <div class="col-12">
                                                <button type="submit" class="btn pull-right" style="background: #458CC7; border-radius: 15px;color: #fff;font-size: 13pt;padding: 3px 40px;">Đăng ký</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <!-- Data picker -->
   <script src="{{ url('public/admin/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <!-- Clock picker -->
    <script src="{{ url('public/admin/js/plugins/clockpicker/clockpicker.js') }}"></script>
    <!-- Date range picker -->
    <script src="{{ url('public/admin/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
@endsection

@section('javascript')
<script type="text/javascript">
    $(".box_detail_left .box_people_view .box_viewmore li.icon_click").click(function () {
        $('.box_people_view').css("max-height", "100%");
        $('.box_viewmore').css('background', 'none');
        $('.bg_view').css('background', 'none');
        $('.box_viewmore ul li.icon_click').css("display", "none");
    });

    $('.expand-menu').click(function () {
        $('.toggle-navbar').addClass('open');
        $('.submenu-bg').show();
        $('html').css('overflow', 'hidden');
    });
    $('.toggle-navbar .close').click(function () {
        $('.toggle-navbar').removeClass('open');
        $('.submenu-bg').hide();
        $('html').css('overflow', 'inherit');
    });

    $('.left-menu .menu-list ul li .sub-btn').click(function () {
        $('.left-menu .menu-list .menu-content').addClass('open');
        $(this).parent().addClass('active');
        $('.left-menu').addClass('amz-leftmn');
    });

    $('.left-menu .menu-list ul li .sub-content ul li.back').click(function () {
        $('.left-menu .menu-list .menu-content').removeClass('open');
        $('.left-menu .menu-list ul li').removeClass('active');
        $('.left-menu').removeClass('amz-leftmn').removeClass('ebay-leftmn');
    });

    $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    $('.clockpicker').clockpicker();



var slider = $(".carousel-full");
var thumbnailSlider = $(".carousel-thumbs");
var duration = 500;
var syncedSecondary = true;

setTimeout(function() {
    $(".cloned .item-slider-model a").attr("data-fancybox", "group-2");
}, 500);

// carousel function for main slider
slider
    .owlCarousel({
        loop: true,
        nav: true,
        navText: ["", ""],
        items: 1,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 600
    })
    .on("changed.owl.carousel", syncPosition);

// carousel function for thumbnail slider
thumbnailSlider

    .on("initialized.owl.carousel", function() {
        thumbnailSlider
            .find(".owl-item")
            .eq(0)
            .addClass("current");
    })
    .owlCarousel({
        loop: false,
        nav: false,
        margin: 10,
        smartSpeed: 600,
        center:true,
        nav: true,
        navText: [
            "<i class='fa fa-caret-left'></i>",
            "<i class='fa fa-caret-right'></i>"
        ],
        responsive: {
            0: {
                items: 4
            },
            600: {
                items: 4
            },
            1200: {
                items: 4,
                margin: 20
            }
        }
    })
    .on("changed.owl.carousel", syncPosition2);

// on click thumbnaisl
thumbnailSlider.on("click", ".owl-item", function(e) {
    e.preventDefault();
    var number = $(this).index();
    slider.data("owl.carousel").to(number, 300, true);
});

function syncPosition(el) {
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

    if (current < 0) {
        current = count;
    }
    if (current > count) {
        current = 0;
    }

    thumbnailSlider
        .find(".owl-item")
        .removeClass("current")
        .eq(current)
        .addClass("current");
    var onscreen = thumbnailSlider.find(".owl-item.active").length - 1;
    var start = thumbnailSlider
        .find(".owl-item.active")
        .first()
        .index();
    var end = thumbnailSlider
        .find(".owl-item.active")
        .last()
        .index();

    if (current > end) {
        thumbnailSlider.data("owl.carousel").to(current, 100, true);
    }
    if (current < start) {
        thumbnailSlider.data("owl.carousel").to(current - onscreen, 100, true);
    }
}

function syncPosition2(el) {
    if (syncedSecondary) {
        var number = el.item.index;
        slider.data("owl.carousel").to(number, 100, true);
    }
}
</script>
@endsection
