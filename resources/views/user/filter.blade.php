@extends('layouts.app')
@section('title','Clb Trường Học')
@section('content')
    <main style="width: 75vw;margin:0 auto">
        <div class="my-5">
            <div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-3" >
                                <div class="style_box_filter">
                                    <form action="" method="get" accept-charset="utf-8">
                                        <div class="form-group">
                                            <label for="">Loại khoá học</label>
                                            <select name="type_course_id" id="input" class="form-control">
                                                <option value="">-- Loại khoá học --</option>
                                                {{-- @foreach ($typeCourse as $type)
                                                <option value="{{$type->id}}" {{request()->type_course_id == $type->id ? 'selected' : ''}}>{{$type->name}}</option>
                                                @endforeach --}}
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Khoảng học phí</label >
                                            <select  name="sale_price" id="sortButton" class="form-control">
                                                <option @if(request()->sale_price == '') selected @endif class="sort" value="">Default</option>
                                                <option @if(request()->sale_price == '0-100000') selected @endif class="sort" value="0-100000">0 -> 100.000đ</option>
                                                <option @if(request()->sale_price == '100000-200000') selected @endif class="sort" value="100000-200000">100.000đ -> 200.000đ</option>
                                                <option @if(request()->sale_price == '200000-400000') selected @endif class="sort" value="200000-400000">200.000đ -> 400.000đ</option>
                                                <option @if(request()->sale_price == '400000-800000') selected @endif class="sort" value="400000-800000">400.000đ -> 800.000đ</option>
                                                <option @if(request()->sale_price == '800000-1000000') selected @endif class="sort" value="800000-1000000">800.000 -> 1000.000</option>
                                                <option @if(request()->sale_price == '1000000-100000000000000000') selected @endif class="sort" value="1000000-100000000000000000">Từ 1 triệu trở lên</option>
                                            </select>
                                        </div>
                                        
                                        {{-- <div class="form-group">
                                            <label class="control-label">Tỉnh/Thành Phố:</label>
                                                <select name="address1" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                    <option value="">Thành Phố</option>
                                                    @foreach ($city as $ci)
                                                    <option value="{{$ci['matp']}}" {{request()->address1 == $ci['matp'] ? "selected" : ""}}>{{$ci['name']}}</option>
                                                    @endforeach
                                               </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Quận/Huyện:</label>
                                                <select name="address2" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control">
                                                    @php
                                                        $district      = App\District::where('matp',request()->address1)->get();
                                                    @endphp
                                                        <option value=''>Quận / Huyện</option>
                                                    @forelse ($district as $dis)
                                                        
                                                        <option value="{{$dis->maqh}}" {{request()->address2 == $dis->maqh ? 'selected' : ''}}>{{$dis->name}}</option>
                                                    @empty
                                                        
                                                    @endforelse
                                               </select>
                                        </div> --}}
                                        <input type="hidden" name="limit" value="{{request()->limit}}">
                                        <button type="submit" class="btn w-100" style="background: #5CC2A8; color: #fff; font-weight: 400;text-transform: uppercase">Lọc kết quả</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                {{-- <div class="mb-5">
                                    <legend>Kết quả tìm kiếm khoá học</legend>
                                    <span style="color: #1B74BB;font-weight: 400">{{$course_all->count()}}</span><span> kết quả</span>
                                </div> --}}                           
                                <div class="row">

                                    <section class="boxfilters">
                                        @foreach ($course as $prod)
                                            <div class="col-12 col-sm-6 col-md-4 mb-4 style_padding_box ">
                                                <article class="boxfilter boxfilter--1 ">
                                                    <div class="boxfilter__info-hover" >
                                                        <div class="" style="height: 100%">
                                                            <div class="Boxinfo Boxinfo-100">
                                                                <div class="Boxinfo-content">
                                                                    <div class="Boxinfo-body">
                                                                        <p>
                                                                            <i class="fal fa-university"></i> Đơn vị tổ chức: {{$prod->userCourses['name']}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-users"></i> Lớp tiêu chuẩn: {{$prod['number_student']}} học sinh
                                                                        </p>
                                                                        <p>
                                                                           <i class="fal fa-usd-circle"></i> Học phí: {{number_format($prod['price'])}}đ/khoá
                                                                        </p>
                                                                        <p>
                                                                            <i class="fal fa-paper-plane"></i> Điều kiện học: {{$prod['study_condition']}}
                                                                        </p>
                                                                    </div>
                                                                    <div class="Boxinfo-action" style="z-index: 100">
                                                                        <form action="{{ url('/chi-tiet') }}/{{$prod->slug}}" method="get" role="form">
                                                                            <button type="submit" class="btn-info w-100" style="background: #5CC2A8;position: relative;">ĐĂNG KÝ HỌC</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="boxfilter__img"></div>
                                                    <a href="#" class="boxfilter_link">
                                                        <div class="boxfilter__img--hover" style="background-image: url('{{ url('public/upload/images') }}/{{$prod->image}}') ">
                                                            <img src="{{ url('public/assets/transparent') }}/cuahang_home.png" alt="">
                                                        </div>
                                                    </a>
                                                    <div class="clearfix">
                                                    </div>
                                                    <div class="boxfilter__info boxfilter__info1">
                                                        <p>
                                                            <i class="fal fa-clock"></i> Thời lượng : {{$prod->number_lesson}} buổi
                                                            
                                                        </p>
                                                        <p>
                                                            {{-- <i class="fal fa-map-marker-alt"></i> Địa điểm:  {{$prod->cityCourses[0]['specific_address']}} --}}
                                                            
                                                        </p>
                                                    </div>
                                                </article>
                                            </div>
                                        @endforeach
                                    </section>
                                </div>
                                @if ($course_all->count() >= 6 && $course_all->count() >= request()->limit)
                                    <div class="mb-5">
                                        <a class="btn btn-success" href=""
                                        onclick="event.preventDefault();
                                                         document.getElementById('limit_id').submit();">
                                            Xem thêm
                                        </a>
                                        <form id="limit_id" action="" method="get" style="display: none;">
                                            <input type="hidden" name="sale_price" value="{{request()->sale_price}}">
                                            <input type="hidden" name="address1" value="{{request()->address1}}">
                                            <input type="hidden" name="address2" value="{{request()->address2}}">
                                            <input type="hidden" name="limit" value="{{request()->limit != null ? request()->limit + 6 : 12}}">
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
@endsection

@section('javascript')
<script type="text/javascript">
    $(function() {
        $('.boxfilters_col').matchHeight();
    });

</script>
<script>
    $(document).on("change","#ThanhPho", function(event) {
      var id_matp = $(this).val();
      $.get("{{ url('') }}/boot/ajax/quanhuyen/"+id_matp,function(data) {
        console.log(data);
        $("#QuanHuyen").html(data);
      });
    });
</script>
@endsection
