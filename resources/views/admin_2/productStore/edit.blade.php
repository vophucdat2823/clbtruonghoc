@extends('admin_2.layout.master')

@section('title','Sản Phẩm | Chỉnh sửa')

@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    <link href="{{ url('public/admin') }}/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public/admin') }}/css/plugins/codemirror/codemirror.css" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Chỉnh sửa sản phẩm</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('partner.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa sản Phẩm</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('product-store.update',['id'=>$productStore->id]) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Product info</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"> Address</a></li>
                        {{-- <li class=""><a data-toggle="tab" href="#tab-3"> Discount</a></li> --}}
                        <li class=""><a data-toggle="tab" href="#tab-4"> Images</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        <a href="{{ route('product-store.index') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Name(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="Product name..." required value="{{$productStore->name}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Slug:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                @if ($productStore->slug)
                                                    @php
                                                        $slug_1 = explode('.', $productStore->slug);
                                                        $slug = explode('cua-hang-truc-tuyen-', $productStore->slug);
                                                    @endphp
                                                @endif

                                                <div class="input-group">
                                                    <span class="input-group-addon">{{url('chi-tiet')}}/cua-hang-truc-tuyen-</span>
                                                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Đường dẫn tĩnh" required value="{{ array_pop($slug) }}">
                                                    <span class="input-group-addon">.{{$slug_1[1]}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-2 control-label">Price:</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" name="price" class="form-control" placeholder="$160.00" required value="{{$productStore->price}}">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Category:</label>
                                        <div class="col-sm-10">
                                            @if ($productStore->category_id != "null")
                                                @php
                                                    $category_id = json_decode($productStore->category_id);
                                                @endphp

                                                <select name="category_id[]" data-placeholder="Choose a Country..." required class="chosen-select" multiple tabindex="4">
                                                    {{showProgram($cateStore,$category_id,0, $char ='')}}
                                                </select>
                                            @else
                                                <select name="category_id[]" data-placeholder="Choose a Country..." required class="chosen-select" multiple tabindex="4">
                                                    {{showCateStore($cateStore,0,'',$productStore->category_id)}}
                                                </select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">IMG</label>
                                        <div class="col-sm-10 col-xs-10">
                                            @if ($productStore->image)
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{$productStore->image}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{$productStore->image}}" name=""><input type="file" name="image" value="{{$productStore->image}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @elseif(old('image'))
                                                <div class="fileinput input-group fileinput-exists" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename">{{old('image')}}</span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{old('image')}}" name=""><input type="file" name="image" value="{{old('image')}}" aria-required="true" class="error" aria-invalid="true">
                                                    </span>
                                                </div>
                                            @else
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                        <span class="fileinput-new">Select file</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="form-group"><label class="col-sm-2 control-label">Description:</label>
                                        <div class="col-sm-10"><textarea name="description" id="description" placeholder="Description">{!! $productStore->description !!}</textarea></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <label class="" for="email">
                                    <button type="button" class="btn btn-success addNewCkeditor">
                                          Add address +
                                      </button>
                                </label>
                                <div class="listTabCkeditor">
                                @if ($cityDistrict)
                                    @foreach ($cityDistrict as $key => $cai)
                                    <fieldset class="form-horizontal">
                                        <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                            <div class="input-group">
                                                <div class="input-group-addon" style="padding:0;border:0">
                                                  <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address1[]" data-placeholder="Choose a Country..." class="chosen-select form-control ThanhPhoedit_{{$key}}">
                                                            <option value="">Thành Phố</option>
                                                            {{-- @php
                                                                dd($ci->city_district_city->name);
                                                            @endphp --}}

                                                            @foreach ($city as $ci)
                                                                <option value="{{$ci->matp}}" {{$cai->city_id == $ci->matp ? 'selected' : ''}}>{{$ci->name}}</option>
                                                            @endforeach

                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Quận/Huyện:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address2[]" data-placeholder="Choose a Country..." class="form-control QuanHuyenedit_{{$key}}">
                                                            @php
                                                                $district      = App\District::where('matp',$cai->city_id)->get();
                                                            @endphp
                                                            @forelse ($district as $dis)
                                                                <option value="{{$dis->maqh}}" {{$cai->district_id == $dis->maqh ? 'selected' : ''}}>{{$dis->name}}</option>
                                                            @empty
                                                                <option value="">--Chưa chọn Quận/Huyện--</option>
                                                            @endforelse
                                                       </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><hr>
                                    </fieldset>
                                    @endforeach
                                @else
                                    <fieldset class="form-horizontal">
                                        <div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">
                                            <div class="input-group">
                                                <div class="input-group-addon" style="padding:0;border:0">
                                                  <button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address1[]" id="ThanhPho" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                            <option value="">Thành Phố</option>
                                                            @foreach ($city as $ci)
                                                            <option value="{{$ci->matp}}">{{$ci->name}}</option>
                                                            @endforeach
                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Quận/Huyện:</label>
                                                    <div class="col-sm-10">
                                                        <select name="address2[]" id="QuanHuyen" data-placeholder="Choose a Country..." class="form-control">
                                                            <option value="">--Chưa chọn Quận/Huyện--</option>
                                                       </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><hr>
                                    </fieldset>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-stripped">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Image preview
                                                </th>
                                                <th>
                                                    Image url
                                                </th>
                                                <th>
                                                    Sort order
                                                </th>
                                                <th>
                                                    Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <img src="img/gallery/2s.jpg">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" disabled value="http://mydomain.com/images/image1.png">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" value="1">
                                                </td>
                                                <td>
                                                    <button class="btn btn-white"><i class="fa fa-trash"></i> </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>


<?php 
    function showProgram($pgramall,$program_test, $parent = 0, $char ='')
    {

        foreach ($pgramall as $key => $item) {
            if ($item->parents == $parent)
            {
                echo '<option value="'.$item->id.'"';
                if ($program_test) {
                    foreach ($program_test as $value) {
                        if($item->id == $value){
                            echo 'selected="selected"';
                        }
                    }
                }
                if($parent==0){
                    echo 'style="color:red"';
                }
                echo '>';
                if($item->parents == $program_test){
                    if ($item->parents == 0) {
                        echo $char . $item->name;
                    }
                    if ($item->parents != 0) {
                        echo $char . $item->name.' (danh mục con)';
                    }
                }
                if($item->parents != $program_test){
                    echo $char . $item->name;
                }
                echo '</option>';
                if ($item->parents != $program_test){
                    showProgram($pgramall,$program_test, $item->id, $char.'---| ');
                }

            }
            
        }
    }


?>


<?php

function showCateStore($cateStore, $parent = 0, $char = '', $select = 0)
{
    $cate_child = array();
    foreach ($cateStore as $key => $item) {
        if ($item->parents == $parent) {
            $cate_child[] = $item;
            unset($cateStore[$key]);
        }
    }
    if ($cate_child) {
        foreach ($cate_child as $key => $item) {
            echo '<option value="' . $item->id . '"';
            if ($parent == 0) {
                echo 'style="color:red"';
            }
            if ($select != 0 && $item->id == $parent) {
                echo 'style="color:red"';
            }
            echo '>';
            echo $char . $item->name;
            echo '</option>';
            showCateStore($cateStore, $item->id, $char . '---| ', $select);
        }
    }
}

?>

@endsection


@section('scripts')
<!-- Chosen -->
<script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

<!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>


    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


@endsection

@section('javascript')

    <script type="text/javascript">

        $('.chosen-select').chosen({width: "100%"});
    </script>
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    <script>
      CKEDITOR.replace( 'description', {
          filebrowserBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('public/pulgin/ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('public/pulgin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
    </script>
    <script type="text/javascript">
        $('.city-country').select2({
            theme: 'bootstrap',
            placeholder: 'cityCountry',
            ajax: {
                url: '{{ url('') }}/admin/profiles/getSuggestCities',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        keyword: params.term,
                    };
                },
                processResults: function (data, params) {
                    // console.log(data);
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.place_id,
                            }
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2,
        });
    </script>


    {{-- <script type="text/javascript">
        $("#form-create-product").validate({
            rules: {
                name: "required",
                slug: "required",
                price: {
                    "required": true,
                    "number": true
                },
                // image: "required",
                category_id: "required",
            },
            messages: {
                "name": "Tên sản phẩm không được để trống",
                "slug": "Đường đẫn tĩnh không được để trống",
                // "image": "Vui vòng thêm ảnh",
                "price": {
                    "required": "Vui lòng nhập giá sản phẩm",
                    "number": 'Giá sản phẩm là số'
                },
                "category_id": "Nghề nghiệp không được để trống",

            },
            errorPlacement: function (error, element) {
              error.appendTo(element.parent().parent().parent());
            },
            submitHandler: function(form) {
                $(form).submit();
            }
        });
    </script> --}}

    <script>
        $(document).on("change","#ThanhPho", function(event) {
          var id_matp = $(this).val();
          $.get("{{ url('') }}/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
            console.log(data);
            $("#QuanHuyen").html(data);
          });
        });
    </script>

    <script type="text/javascript">
     $(document).ready(function () {
            var max_fields      = 12; //maximum input boxes allowed
            var wrapper         = $(".listTabCkeditor"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

             var x = 0;


         $(document).on("click",".addNewCkeditor",function(){
             x++;
            var thanhpho = 'thanhpho_' + x;
            var quanhuyen = 'quanhuyen_' + x;

            var str='<fieldset class="form-horizontal"><div id="image-diplay" style="margin-top: 20px;margin-bottom: 20px">';
                str+='<div class="input-group">';
                    str+='<div class="input-group-addon" style="padding:0;border:0">';
                      str+='<button type="button" class="btn btn-danger removeCkeditor" style="padding-bottom: 100%;padding-top: 100%;height: 100% !important;"><i class="fa fa-fw fa-trash-o "></i></button>';
                    str+='</div>';
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Tỉnh/Thành Phố:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address1[]" id="'+thanhpho+'" data-placeholder="Choose a Country..." class="chosen-select form-control">';
                                str+='<option value="">Thành Phố</option>';
                                str+='@foreach ($city as $ci)';
                                str+='<option value="{{$ci->matp}}">';
                                str+='{{$ci->name}}';
                                str+='</option>';
                                str+='@endforeach';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';                 
                    str+='<div class="form-group">';
                        str+='<label class="col-sm-2 control-label">Quận/Huyện:</label>';
                        str+='<div class="col-sm-10">';
                            str+='<select name="address2[]" id="'+quanhuyen+'" data-placeholder="Choose a Country..." class="form-control">';
                                str+='<option value="">--Chưa chọn Quận/Huyện--</option>';
                           str+='</select>';
                        str+='</div>';
                    str+='</div>';
                str+='</div></fieldset>';

                $(document).on("change","#"+thanhpho, function(event) {
                  var id_matp = $(this).val();
                  $.get("{{ url('') }}/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
                    $("#"+quanhuyen).html(data);
                  });
                });
            console.log(str);
              
            $(wrapper).append(str);
         });
          $("body").on('click','.removeCkeditor',function(){
            $(this).parent().parent().parent().remove();x--;
         });
     });
 </script>
    @if ($cityDistrict)
        @foreach ($cityDistrict as $key => $cai)
       
            <script>
                $("body").on("change",".ThanhPhoedit_{{$key}}", function(event) {
                  var id_matp = $(this).val();
                    console.log(id_matp);
                  $.get("{{ url('') }}/partner/product/ajax/quanhuyen/"+id_matp,function(data) {
                    $(".QuanHuyenedit_{{$key}}").html(data);
                  });
                });
            </script>
        @endforeach
    @endif
@endsection