@extends('admin_2.layout.master')

@section('style')

@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    	@foreach ($cateCourse as $cate)
	        <div class="col-lg-12">
	            <div class="ibox float-e-margins">
	                <div class="ibox-title">
	                    <h5>{{$cate->name}}</h5>
	                    <div class="ibox-tools">
	                        <a class="collapse-link">
	                            <i class="fa fa-chevron-up"></i>
	                        </a>
	                        <a class="close-link">
	                            <i class="fa fa-times"></i>
	                        </a>
	                    </div>
	                </div>
	                <div class="ibox-content">
	                    {{-- <div class="row">
	                        <div class="col-sm-5 m-b-xs"><select class="input-sm form-control input-s-sm inline">
	                            <option value="0">Option 1</option>
	                            <option value="1">Option 2</option>
	                            <option value="2">Option 3</option>
	                            <option value="3">Option 4</option>
	                        </select>
	                        </div>
	                        <div class="col-sm-4 m-b-xs">
	                            <div data-toggle="buttons" class="btn-group">
	                                <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
	                                <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
	                                <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
	                            </div>
	                        </div>
	                        <div class="col-sm-3">
	                            <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
	                                <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
	                        </div>
	                    </div> --}}
	                    <div class="table-responsive">
	                        <table class="table table-striped">
	                            <thead>
	                            <tr>
	                                <th>Tên khóa học</th>
                                    <th>Trạng Thái</th>
                                    <th>Action</th>
	                            </tr>
	                            </thead>
	                            <tbody>
                            	@foreach ($cate->getCourse as $course)
		                            <tr>
		                                <td>{{$course->name}}</td>
		                                <td>Chưa cung cấp</td>
		                                <td class="">
                                            <a href="{{ route('course-partner.edit',['course_id'=>$course->id ]) }}" class="btn-white btn btn-xs" >Cung cấp khóa học này</a>
                                        </td>
		                            </tr>
                            	@endforeach
	                            </tbody>
	                        </table>
	                    </div>

	                </div>
	            </div>
	        </div>
        @endforeach 

    </div>
</div>

@endsection