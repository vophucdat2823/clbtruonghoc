@extends('admin_2.layout.master')
@section('title',$service->name)
@section('style')

{{-- <link href="{{ url('public/admin') }}/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> --}}

{{-- <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" /> --}}

    <link href="{{ url('public/admin/css/plugins/dropzone/basic.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/plugins/codemirror/codemirror.css') }}" rel="stylesheet">


@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Cung cấp dịch vụ</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboard') }}">Trang chủ</a>
                </li>
                <li class="active">
                    <strong>Cung cấp dịch vụ</strong>
                </li>
            </ol>
        </div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <form action="{{ route('service-partner.update',['id'=>$service->id]) }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form-create-product">
            {{ csrf_field() }}
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Thông tin dịch vụ</a></li>
                        <li class=""><button type="submit" class="btn btn-success">Submit</button></li>
                        {{-- <a href="{{ route('service.index') }}" class="btn btn-primary" style="margin-left: 15px">Quay lại</a> --}}
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">Tên dịch vụ(*):</label>
                                        <div class="col-sm-10">
                                            <div>
                                                <div>
                                                    <input type="text" id="name" class="form-control" disabled placeholder="Tên dịch vụ..." value="{{$service->name}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Đơn vị cung cấp(*):</label>
                                      <div class="col-sm-10">
                                            <select name="unit_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    @foreach ($service->getTypeService_id("unit",$service->id) as $cate)
                                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Giá thành(*):</label>
                                      <div class="col-sm-10">
                                            <select name="price_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    @foreach ($service->getTypeService_id("price",$service->id) as $cate)
                                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Chất lượng(*):</label>
                                      <div class="col-sm-10">
                                            <select name="quality_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    @foreach ($service->getTypeService_id("quality",$service->id) as $cate)
                                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Số lượng(*):</label>
                                      <div class="col-sm-10">
                                            <select name="qty_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    @foreach ($service->getTypeService_id("qty",$service->id) as $cate)
                                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-2" for="email">Địa chỉ(*):</label>
                                      <div class="col-sm-10">
                                            <select name="address_id" data-placeholder="Choose a Country..." class="chosen-select" required tabindex="4">
                                                    @foreach ($service->getTypeService_id("address",$service->id) as $cate)
                                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>


@endsection


    @section('scripts')
    <!-- Chosen -->
    <script src="{{ url('public/admin') }}/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Select2 -->
    <script src="{{ url('public/admin') }}/js/plugins/select2/select2.full.min.js"></script>

    <!-- Jasny -->
    <script src="{{ url('public/admin') }}/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="{{ url('public/admin') }}/js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/codemirror.js"></script>
    <script src="{{ url('public/admin') }}/js/plugins/codemirror/mode/xml/xml.js"></script>
@endsection

@section('javascript')

    <script type="text/javascript">
        $('.chosen-select').chosen({width: "100%"});
    </script>
@endsection