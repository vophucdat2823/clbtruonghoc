<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" src="{{ url('public/admin/img/logo.png') }}" />
                    </span>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear">
                    <span class="block m-t-xs"> <strong class="font-bold">Quản trị đối tác</strong>
                    </span>
                      <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Về Webify</a></li>
                        <li><a href="#">Liên hệ</a></li>
                        <li><a href="#">Hướng dẫn sử dụng</a></li>
                        
                      </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{Request::is('partner/dashboard') ? 'active' : ''}}">
                <a href="{{route('partner.dashboard')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Trang chủ</span>  </a>
            </li>


            @can('product')

                <li class="{{Request::is('partner/cate-store') ||Request::is('partner/product-store') || Request::is('partner/product-store/create') ? 'active' : ''}}">

                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">CH Trực tuyến</span><span class="fa arrow"></span></a>

                     <ul class="nav nav-second-level collapse" style="">

                        <li class="{{Request::is('partner/cate-store') ? 'active' : ''}}"><a href="{{ route('cate-store.index') }}">Danh mục</a></li>



                        <li class="{{Request::is('partner/product-store') || Request::is('partner/product-store/create')? 'active' : ''}}">

                            <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Sản phẩm</span><span class="fa arrow"></span></a>

                             <ul class="nav nav-third-level collapse {{Request::is('partner/product-store') ? 'in' : ''}}" style="">

                                <li class="{{Request::is('partner/product-store') ? 'active' : ''}}"><a href="{{ route('product-store.index') }}"> Danh sách SP</a></li>

                                <li class="{{Request::is('partner/product-store/create') ? 'active' : ''}}"><a href="{{ route('product-store.create') }}">Thêm mới SP</a></li>

                            </ul>

                        </li>

                    </ul>

                </li>

            @endcan
            @can('course')
            <li class="{{Request::is('partner/course-partner') || Request::is('partner/course-partner-user') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">CLB Khóa học</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    <li class="{{Request::is('partner/course-partner') || Request::is('partner/course-partner-user')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Khóa học</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('partner/course-partner') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('partner/course-partner') ? 'active' : ''}}"><a href="{{ route('course-partner.index') }}"> Danh Sách khóa học</a></li>
                            <li class="{{Request::is('partner/course-partner-user') ? 'active' : ''}}"><a href="{{ route('course-partner.index_user') }}">Khóa học của tôi</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
                
            @endcan
            @can('service')
            <li class="{{Request::is('partner/service-partner') || Request::is('partner/service-partner-user') ? 'active' : ''}}">
                <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Quản lý dịch vụ</span><span class="fa arrow"></span></a>
                 <ul class="nav nav-second-level collapse" style="">
                    {{-- <li class="{{Request::is('partner/service-partner') ? 'active' : ''}}"><a href="{{ route('service-partner.index') }}">Danh mục dịch vụ</a></li> --}}

                    <li class="{{Request::is('partner/service-partner') || Request::is('partner/service-partner-user')? 'active' : ''}}">
                        <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Dịch vụ</span><span class="fa arrow"></span></a>
                         <ul class="nav nav-third-level collapse {{Request::is('partner/service-partner') ? 'in' : ''}}" style="">
                            <li class="{{Request::is('partner/service-partner') ? 'active' : ''}}"><a href="{{ route('service-partner.index') }}"> Danh Sách Dịch Vụ</a></li>
                            <li class="{{Request::is('partner/service-partner-user') ? 'active' : ''}}"><a href="{{ route('service-partner.index_user') }}">Dịch Vụ của tôi</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
                
            @endcan
        </ul>
    </div>
</nav>